// -> Beyond Codebase
import React, { useContext } from 'react';
import { css } from 'aphrodite';
// -> Within Codebase
import ClickableListItem from '../../../../ClickableListItem/ClickableListItem';
import Button from '../../../../Button/Button';
import { ICouragePanelProps } from '../../CouragePanel';
import { MoreDetailsIcon } from '../../../../VisualUtilities/IconPresets';
import { UIContext, ThemeInfo } from '../../../../UI_InfoProvider/UI_InfoProvider';
import { styleGen } from '../../CouragePanelStyles';

interface IChaptersProps {
  chapters: any;
  navigateToChaptersGrid(): void;
  navigateToChapter(chapterID: string): void;
  parentProps: ICouragePanelProps
}

const Chapters: React.FC<IChaptersProps> = (props) => {
  const {
    chapters, navigateToChaptersGrid,
    navigateToChapter, parentProps } = props;
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const {
    chaptersCradle, chaptersTitleBarCradle, chaptersTitleBarRightCradle,
    sectionTitle, quantityTotal, chaptersListCradle
  } = styleGen(parentProps, themeInfo);

  return (
    <div className={css(chaptersCradle)}>
      <div className={css(chaptersTitleBarCradle)}>
        {/* - TODO: -> Internationalize text */}
        <p className={css(sectionTitle)}>Chapters</p>
        <div className={css(chaptersTitleBarRightCradle)}>
          <div className={css(quantityTotal)}>{chapters.length}</div>
          <div style={{ marginRight: themeInfo.distance.two }}></div>
          <Button
            icon={<MoreDetailsIcon color={themeInfo.palette.black} size={themeInfo.styles.standardIconSize} />}
            onClick={() => navigateToChaptersGrid()}
            buttonCradlePadding="none"
            transparent
            height={themeInfo.styles.standardIconSize}
            width={themeInfo.styles.standardIconSize}
          />
        </div>
      </div>
      <div className={css(chaptersListCradle)}>
        {
          chapters.map((chapter: any) => {
            const { avatarURL, name, id } = chapter;
            const [ name1, name2 ] = name.split(' ');

            return (
              <ClickableListItem
                key={id}
                avatarURL={avatarURL}
                avatarSize={25}
                listItemText={name}
                name1={name1}
                name2={name2}
                onClick={() => navigateToChapter(id)}
              />
            );
          })
        }
      </div>
    </div>    
  );
}

export default Chapters;