// -> Beyond Codebase
import React, { useContext } from 'react';
import { css } from 'aphrodite';
// -> Within Codebase
import LinkWrapper from '../../../../VisualUtilities/LinkWrapper/LinkWrapper';
import IconButton from '../../../../Button/Variants/IconButton/IconButton';
import { UIContext, ThemeInfo } from '../../../../UI_InfoProvider/UI_InfoProvider';
import { ICouragePanelProps } from '../../CouragePanel';
import { styleGen } from '../../CouragePanelStyles';
import { determineLinkIcon, mergeSocialsAndComms } from '../../helpers';
import { styleGuide } from '../../../../../styleGuide/styleGuide';

interface ISocialsAndCommsProps {
  socials: any;
  communications: any;
  parentProps: ICouragePanelProps
}

const SocialsAndComms: React.FC<ISocialsAndCommsProps> = (props) => {
  const { socials, communications, parentProps } = props;
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const {
    sectionTitle, socialsAndCommsCradle, socialsAndCommsItemsCradle, infoItem,
    noContentFallbackText
  } = styleGen(parentProps, themeInfo);

  const socialsAndComms = mergeSocialsAndComms(socials, communications);

  return (
    <div className={css(socialsAndCommsCradle)}>
      {/* - TODO: -> Internationalize text */}
      <p className={css(sectionTitle)}>Socials and Comms</p>
      <div style={{ marginTop: themeInfo.distance.two }}></div>
      <div className={css(socialsAndCommsItemsCradle)}>
        {
          (
            (socials.children && (socials.children.length > 0)) ||
            (communications.children && (communications.children.length > 0))
          ) ? (
            <>
              {
                socialsAndComms.children && socialsAndComms.children.map((item: any, index: number) => {
                  const linkIcon = determineLinkIcon(item.name, themeInfo);

                  return (
                    <LinkWrapper
                      link={item.link}
                      key={index}
                      customStyles={{ ...socialsAndCommsItemCradleStyle }}
                    >
                      <IconButton icon={linkIcon} onClick={() => {}} />
                      <div style={{ marginRight: themeInfo.distance.one }}></div>
                      {/* - TODO: -> Internationalize this text */}
                      <p className={css(infoItem)}>{item.name}</p>
                    </LinkWrapper>
                  );
                })
              }
            </>
          ) : (
            // - TODO: -> Internationalize Text
            <p className={css(noContentFallbackText)} style={{ marginTop: themeInfo.distance.three, marginBottom: themeInfo.distance.three }}>
              Oops, looks like the organization hasn't added any of their links to social networks or communications channels yet.
            </p>
          )
        }
      </div>
    </div>    
  );
}

export default SocialsAndComms;

const socialsAndCommsItemCradleStyle = {
  height: 'auto',
  width: 'auto',
  marginRight: styleGuide.distance.one,
  marginBottom: styleGuide.distance.one,
  padding: styleGuide.distance.one,
  display: 'flex',
  alignItems: 'center',
  borderRadius: styleGuide.borderRadii.one,
  transition: styleGuide.transitions.bgColorTransition,
  ':hover': {
    cursor: 'pointer',
    backgroundColor: styleGuide.palette.grey1,
  },
  ':visited': { color: styleGuide.palette.black }
}
