// -> Beyond Codebase
import React, { useContext } from 'react';
import { css } from 'aphrodite';
// -> Within Codebase
import LineSegment from '../../../../VisualUtilities/LineSegment/LineSegment';
import LinkWrapper from '../../../../VisualUtilities/LinkWrapper/LinkWrapper';
import { UIContext, ThemeInfo } from '../../../../UI_InfoProvider/UI_InfoProvider';
import { ICouragePanelProps } from '../../CouragePanel';
import {
  VERTICAL, COURAGE_WEBSITE_ABOUT_LINK, COURAGE_WEBSITE_BASIS_OF_UNITY_LINK,
  COURAGE_WEBSITE_MEMBERSHIP_LINK, COURAGE_WEBSITE_FAQ_LINK, COURAGE_WEBSITE_LINK,
} from '../../../../../constants';
import ShadowHoverButton from '../ShadowHoverButton/ShadowHoverButton';
import { styleGen } from '../../CouragePanelStyles';

interface ITitleAndLinkOptionsProps {
  name: string;
  parentProps: ICouragePanelProps;
  navigateToCourageWorkingGroups: any;
}

const TitleAndLinkOptions: React.FC<ITitleAndLinkOptionsProps> = (props) => {
  const { name, parentProps, navigateToCourageWorkingGroups } = props;
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const {
    titleAndOptionsRow, titleCradle, title, websiteLinks
  } = styleGen(parentProps, themeInfo);

  return (
    <div className={css(titleAndOptionsRow)}>
      <div className={css(titleCradle)}>
        <p className={css(title)}>{name}</p>
      </div>
        <LineSegment
          direction={VERTICAL}
          height='100%'
          size={1}
          color={themeInfo.palette.primary}
          customStyles={{
            marginRight: themeInfo.distance.two,
            marginLeft: themeInfo.distance.two,
          }}
        />
        <div style={{ marginTop: themeInfo.distance.two }}></div>
        <div className={css(websiteLinks)}>
          <LinkWrapper
          link={COURAGE_WEBSITE_LINK}
          customStyles={{
            marginRight: themeInfo.distance.one,
            marginBottom: themeInfo.distance.one,
          }}
        >
          {/* - TODO: -> Internationalize this text */}
          <ShadowHoverButton buttonText="Website"/>
        </LinkWrapper>
        <LinkWrapper
          link={COURAGE_WEBSITE_ABOUT_LINK}
          customStyles={{
            marginRight: themeInfo.distance.one,
            marginBottom: themeInfo.distance.one,
          }}
        >
          {/* - TODO: -> Internationalize this text */}
          <ShadowHoverButton buttonText="About"/>
        </LinkWrapper>
        <LinkWrapper
          link={COURAGE_WEBSITE_BASIS_OF_UNITY_LINK}
          customStyles={{
            marginRight: themeInfo.distance.one,
            marginBottom: themeInfo.distance.one,
          }}
        >
          {/* - TODO: -> Internationalize this text */}
          <ShadowHoverButton buttonText="Basis of Unity"/>
        </LinkWrapper>
        <LinkWrapper
          link={COURAGE_WEBSITE_FAQ_LINK}
          customStyles={{
            marginRight: themeInfo.distance.one,
            marginBottom: themeInfo.distance.one,
          }}
        >
          {/* - TODO: -> Internationalize this text */}
          <ShadowHoverButton buttonText="FAQ"/>
        </LinkWrapper>
        <LinkWrapper
          link={COURAGE_WEBSITE_MEMBERSHIP_LINK}
          customStyles={{
            marginRight: themeInfo.distance.one,
            marginBottom: themeInfo.distance.one,
          }}
        >
          {/* - TODO: -> Internationalize this text */}
          <ShadowHoverButton buttonText="Become a Member"/>
        </LinkWrapper>
        {/* - TODO: -> Internationalize text */}
        <ShadowHoverButton onClick={navigateToCourageWorkingGroups} buttonText="Working Groups" />
      </div>
    </div>    
  );
}

export default TitleAndLinkOptions;
