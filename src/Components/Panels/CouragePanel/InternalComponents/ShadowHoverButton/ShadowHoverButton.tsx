// -> Beyond Codebase
import React, { useContext } from 'react';
// -> Within Codebase 
import { UIContext, ThemeInfo } from '../../../../UI_InfoProvider/UI_InfoProvider';
import Button from '../../../../Button/Button';
import { ROUNDED } from '../../../../../constants';

interface IShadowHoverButtonProps {
  buttonText: string;
  onClick?: any;
}

const ShadowHoverButton: React.FC<IShadowHoverButtonProps> = (props) => {
  const { buttonText, onClick } = props;
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);

  const computedOnClick = onClick ? onClick : () => {}

  return (
    <Button
      // - TODO: -> Internationalize Text
      buttonText={buttonText}
      buttonGeometry={ROUNDED}
      iconTextSpacing={themeInfo.distance.one}
      color={themeInfo.palette.white}
      textColor={themeInfo.palette.black}
      customCradleStyles={{
        transition: `
          ${themeInfo.transitions.boxShadowTransition},
          ${themeInfo.transitions.bgColorTransition}
        `,
        backgroundColor: 'transparent',
        paddingLeft: themeInfo.distance.DISTANCE_COEFFICIENT * 0.5,
        paddingRight: themeInfo.distance.DISTANCE_COEFFICIENT * 0.5,
        paddingTop: themeInfo.distance.DISTANCE_COEFFICIENT * 0.5,
        paddingBottom: themeInfo.distance.DISTANCE_COEFFICIENT * 0.5
      }}
      customCradleHoverStyles={{
        boxShadow: themeInfo.shadows.one,
        backgroundColor: themeInfo.palette.white,
      }}
      customTextStyles={{
        ...themeInfo.fonts.tertiary.regular,
        color: themeInfo.palette.primary,
        fontSize: '1.4rem',
      }}
      onClick={computedOnClick}
    />
  );
}

export default ShadowHoverButton;
