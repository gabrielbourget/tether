// -> Beyond Codebase
import React from 'react';
import cloneDeep from 'lodash.clonedeep'
// -> Within Codebase
import {
  TwitterIcon, FacebookIcon, SlackIcon,
  WebsiteIcon, EmailIcon,
} from '../../VisualUtilities/IconPresets'
import { ThemeInfo } from '../../UI_InfoProvider/UI_InfoProvider';
import { FACEBOOK, TWITTER, WEBSITE, SLACK, EMAIL } from '../../../constants';
import { Resource } from '../../../Types';

export const determineLinkIcon = (name: string, themeInfo: ThemeInfo) => {
  let linkIcon;

  switch(name) {
    case FACEBOOK:
      linkIcon = <FacebookIcon color={themeInfo.palette.black} size={themeInfo.styles.standardIconSize} />;
      break;
    case TWITTER:
      linkIcon = <TwitterIcon color={themeInfo.palette.black} size={themeInfo.styles.standardIconSize} />;
      break;
    case SLACK:
      linkIcon = <SlackIcon color={themeInfo.palette.black} size={themeInfo.styles.standardIconSize} />;
      break;
    case EMAIL:
      linkIcon = <EmailIcon color={themeInfo.palette.black} size={themeInfo.styles.standardIconSize} />;
      break;
    case WEBSITE:
      linkIcon = <WebsiteIcon color={themeInfo.palette.black} size={themeInfo.styles.standardIconSize} />;
      break;
    default: break;
  }

  return linkIcon;
}

export const mergeSocialsAndComms = (socials: Resource, communications: Resource): Resource => {
  const socialsCopy = cloneDeep(socials);
  const communicationsCopy = cloneDeep(communications);
  const socialsAndComms: Resource = {
    id: '12345', // -> Dummy ID is ok, don't really need a real one in this context.
    name: 'Socials and Comms',
    children: []
  }

  if (socialsCopy.children && socialsAndComms.children) {
    socialsAndComms.children.push(...socialsCopy.children);
  }
  if (communicationsCopy.children && socialsAndComms.children) {
    socialsAndComms.children.push(...communicationsCopy.children);
  }

  return socialsAndComms;
}
