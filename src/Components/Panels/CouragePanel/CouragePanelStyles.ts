import { StyleSheet } from 'aphrodite';
import { ThemeInfo } from '../../UI_InfoProvider/UI_InfoProvider';
import { ICouragePanelProps } from './CouragePanel';

export const styleGen = (props: ICouragePanelProps, themeInfo: ThemeInfo) => {
  // const {} = props;
  const { palette } = themeInfo;

  return StyleSheet.create({
    panelCradle: {
      minHeight: '100%',
      height: 'auto',
      maxHeight: 900, // -> This bounds the panel to a square shape at maximum dimensions, change if desired.
      maxWidth: 900,
      width: '100%',
      padding: themeInfo.distance.namedVariants.screenPadding,
      display: 'flex',
      backgroundColor: palette.white,
      borderRadius: themeInfo.borderRadii.one,
      boxShadow: themeInfo.shadows.five,

      '@media only screen and (max-width: 800)': {
        flexDirection: 'column',
      },

      '@media only screen and (max-width: 500px)': {
        width: 350
      },

      '@media only screen and (max-width: 375px)': {
        width: 300
      }
    },
    sectionTitle: { ...themeInfo.typography.sectionTitle },
    noContentFallbackText: {
      ...themeInfo.typography.noContentFallbackText,
      textAlign: 'center',
    },    
    infoItem: {
      fontFamily: themeInfo.fonts.primary,
      fontSize: '1.2rem',
      color: palette.black,
    },
    quantityTotal: {
      height: 'auto',
      width: 'auto',
      minHeight: 25,
      minWidth: 25,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: '100%',
      backgroundColor: palette.primary,
      ...themeInfo.fonts.tertiary.heavy,
      color: palette.white,
      fontSize: '1.2rem',
    },
    // -> COLUMN 1
    col1: {
      height: 'auto',
      width: '50%',
      display: 'flex',
      flexDirection: 'column',
      // justifyContent: 'center',

      '@media only screen and (max-width: 800)': {
        justifyContent: 'center',
        width: '100%',
      }
    },
    titleAndOptionsRow: {
      height: 'auto',
      width: '100%',
      display: 'flex',
      alignItems: 'center',

      '@media only screen and (max-width: 500)': {
        width: '100%',
      }
    },
    titleCradle: {
      height: 'min-content',
      width: 'auto',
      padding: themeInfo.distance.two,
      paddingTop: themeInfo.distance.one,
      backgroundColor: palette.primary,
      borderRadius: themeInfo.borderRadii.one,
    },
    title: {
      ...themeInfo.fonts.tertiary.bold,
      color: palette.white,
      fontSize: '4rem',
      lineHeight: 1
    },
    websiteLinks: {
      minHeight: '100%',
      height: 'auto',
      width: 'auto',
      marginBottom: -2.5,
      flex: 1,
      display: 'flex',
      flexWrap: 'wrap',
      alignItems: 'center',
    },
    orgDescriptionCradle: {
      height: 'min-content',
      width: '100%',
    },
    orgDescription: {
      ...themeInfo.fonts.tertiary.italic,
      fontSize: '1.5rem',
      color: themeInfo.palette.black,
    },
    socialsAndCommsCradle: {
      height: 'min-content',
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    socialsAndCommsItemsCradle: {
      height: 'auto',
      width: '100%',
      display: 'flex',
      flexWrap: 'wrap',
      marginLeft: -themeInfo.distance.one,
    },
    socialsAndCommsItemCradle: {
      height: 'auto',
      width: 'auto',
      marginRight: themeInfo.distance.one,
      marginBottom: themeInfo.distance.one,
      padding: themeInfo.distance.one,
      display: 'flex',
      alignItems: 'center',
      borderRadius: themeInfo.borderRadii.one,
      transition: themeInfo.transitions.bgColorTransition,
      ':hover': {
        cursor: 'pointer',
        backgroundColor: palette.grey1,
      },
      ':visited': { color: palette.black }
    },
    chaptersCradle: {
      height: '100%',
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      flex: 1,
    },
    chaptersTitleBarCradle: {
      height: 'min-content',
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    chaptersTitleBarRightCradle: {
      height: '100%',
      width: 'min-content',
      display: 'flex',
      alignItems: 'center',
    },
    chaptersListCradle: {
      height: '100%',
      width: '100%',
      paddingTop: themeInfo.distance.two,
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      overflowY: 'scroll',
    },
    // - COLUMN 2
    col2: {
      height: '100%',
      width: '50%',
      display: 'flex',
      flexDirection: 'column',

      '@media only screen and (max-width: 500)': {
        justifyContent: 'center',
        width: '100%',
      }
    },
  });
}