// -> Beyond Codebase
import React, { useContext, useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { css } from 'aphrodite';
// -> Within Codebase
import EmbeddableResourceBrowser from '../../EmbeddableResourceBrowser/EmbeddableResourceBrowser';
import LineSegment from '../../VisualUtilities/LineSegment/LineSegment';
import { UIContext, ThemeInfo } from '../../UI_InfoProvider/UI_InfoProvider';
import { CHAPTERS_ROUTE, HORIZONTAL, CHAPTER_ROUTE, COURAGE_WORKING_GROUPS_ROUTE } from '../../../constants';
import { courageData as staticData } from '../../../staticData/Courage/courageData';
import { underConstructionAlert } from '../../../helpers';
// -> Within Component
import TitleAndLinkOptions from './InternalComponents/TitleAndLinkOptions/TitleAndLinkOptions';
import SocialsAndComms from './InternalComponents/SocialsAndComms/SocialsAndComms';
import Chapters from './InternalComponents/Chapters/Chapters';
import { styleGen } from './CouragePanelStyles';

export interface ICouragePanelProps {
  courageData: any;
}

const CouragePanel: React.FC<ICouragePanelProps> = (props) => {
  const [ windowWidth, setWindowWidth ] = useState(window.innerWidth);
  const history = useHistory();
  
  // - TODO: -> Resolve prop drilling issue, courage data not getting to this component.
  // const { courageData } = props;
  const {
    name, description, communications,
    chapters, resources, socials, workingGroups
  } = staticData;
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const {
    panelCradle, col1, col2, orgDescriptionCradle, orgDescription,
  } = styleGen(props, themeInfo);

  useEffect(() => {
    setWindowWidth(window.innerWidth);
    window.addEventListener('resize', handleResize);
  }, []);

  const handleResize = () => {
    // console.log('CouragePanel: handleResize() invoked');
    setWindowWidth(window.innerWidth);
  }
  
  const navigateToChaptersGrid = () => history.push(CHAPTERS_ROUTE);
  const navigateToChapter = (chapterID: string) => {
    const chapterRouteURL = CHAPTER_ROUTE.replace(':id', chapterID);
    history.push(chapterRouteURL);
  }

  const navigateToCourageWorkingGroups = () => {
    history.push({
      pathname: COURAGE_WORKING_GROUPS_ROUTE,
      state: { workingGroups }
    });
  }

  return (
    <div className={css(panelCradle)}>
      <div className={css(col1)}>

        {/* TITLE AND LINK OPTIONS */}
        <TitleAndLinkOptions
          name={name}
          parentProps={props}
          navigateToCourageWorkingGroups={navigateToCourageWorkingGroups}
        />

        <LineSegment
          direction={HORIZONTAL}
          width='100%'
          size={1}
          color={themeInfo.palette.primary}
          customStyles={{
            marginTop: themeInfo.distance.two,
            marginBottom: themeInfo.distance.two,
          }}
        />

        {/* ORGANIZATION DESCRIPTION */}
        <div className={css(orgDescriptionCradle)}>
          {/* - TODO: -> Internationalize text */}
          <p className={css(orgDescription)}>{ description }</p>
        </div>

        <div style={{ marginTop: themeInfo.distance.two }}></div>

        {/* SOCIALS AND COMMS */}
        <SocialsAndComms socials={socials} communications={communications} parentProps={props} />

        {/* CHAPTERS */}
        <Chapters
          chapters={chapters}
          navigateToChaptersGrid={navigateToChaptersGrid}
          navigateToChapter={navigateToChapter}
          parentProps={props}
        />
      </div>

      {/* Either creating horizontal space or vertical space depending on layout config of the card. */}
      {
        (windowWidth >= 800) ? (
          <div style={{ marginRight: themeInfo.distance.three }}></div>
        ) : (
          <div style={{ marginTop: themeInfo.distance.three }}></div>
        )
      }

      {/* RESOURCES */}
      <div className={css(col2)}>
        <EmbeddableResourceBrowser
          resources={resources}
          navigateToFullSizedResourceBrowser={() => underConstructionAlert()}
        />
      </div>
    </div>
  );
}

export default CouragePanel;
