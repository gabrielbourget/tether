import { StyleSheet } from 'aphrodite';
import { ThemeInfo } from '../../UI_InfoProvider/UI_InfoProvider';
// import { SCREEN_WIDTH_800 } from '../../../constants';
import { IWorkingGroupPanelProps } from './WorkingGroupPanel';

export const styleGen = (
  props: IWorkingGroupPanelProps,
  themeInfo: ThemeInfo,
  screenWidth: number,
) => {
  const { palette } = themeInfo;

  return StyleSheet.create({
    sectionTitle: { ...themeInfo.typography.sectionTitle },
    infoItem: { ...themeInfo.typography.default },
    noContentFallbackText: {
      ...themeInfo.typography.noContentFallbackText,
      textAlign: 'center',
    },
    panelCradle: {
      minHeight: '100%',
      // maxHeight: 900, // -> This bounds the panel to a square shape at maximum dimensions, change if desired.
      // height: (screenWidth > SCREEN_WIDTH_800) ? '100%' : 'auto',
      height: 'auto',
      maxWidth: 975,
      width: '100%',
      padding: themeInfo.distance.namedVariants.screenPadding,
      display: 'flex',
      backgroundColor: palette.white,
      borderRadius: themeInfo.borderRadii.one,
      boxShadow: themeInfo.shadows.one,

      '@media only screen and (max-width: 800)': {
        flexDirection: 'column',
      },

      '@media only screen and (max-width: 500px)': {
        width: 350
      },

      '@media only screen and (max-width: 375px)': {
        width: 300
      }
    },
    // -> COLUMN 1
    col1: {
      height: 'auto',
      width: '50%',
      display: 'flex',
      flexDirection: 'column',
      // justifyContent: 'center',

      '@media only screen and (max-width: 800)': {
        justifyContent: 'center',
        width: '100%',
      }
    },
    titleCradle: {
      height: 'min-content',
      width: 'min-content',
      padding: themeInfo.distance.two,
      paddingTop: themeInfo.distance.one,
      display: 'flex',
      flexWrap: 'wrap',
      whiteSpace: 'nowrap', // - TODO: -> Resolve dilemma of wanting this and not wanting this at the same time.
      backgroundColor: palette.primary,
      borderRadius: themeInfo.borderRadii.one,
    },
    title: {
      ...themeInfo.fonts.tertiary.bold,
      color: palette.white,
      fontSize: '3.5rem',
      lineHeight: 1
    },    
    // - COLUMN 2
    col2: {
      height: '100%',
      width: '50%',
      display: 'flex',
      flexDirection: 'column',

      '@media only screen and (max-width: 500)': {
        justifyContent: 'center',
        width: '100%',
      }
    },
  });
}
