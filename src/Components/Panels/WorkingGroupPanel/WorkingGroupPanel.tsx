// -> Beyond Codebase
import React, { useContext, useEffect, useState } from 'react';
import { css } from 'aphrodite'
// -> Within Codebase
import ClickableListItem from '../../ClickableListItem/ClickableListItem';
import EmbeddableResourceBrowser from '../../EmbeddableResourceBrowser/EmbeddableResourceBrowser';
import { UIContext, ThemeInfo } from '../../UI_InfoProvider/UI_InfoProvider';
import { WorkingGroup, Person } from '../../../Types';
import { determineScreenDimensions, underConstructionAlert } from '../../../helpers';
// -> Within Component
import SocialsAndComms from './InternalComponents/SocialsAndComms/SocialsAndComms';
import Members from './InternalComponents/Members/Members';
import { styleGen } from './WorkingGroupPanelStyles';
import Tag from '../../Tag/Tag';
import { ROUNDED } from '../../../constants';

const { width: screenWidth } = determineScreenDimensions();

export interface IWorkingGroupPanelProps {
  chapterName: string;
  workingGroupData: WorkingGroup
}

const WorkingGroupPanel: React.FC<IWorkingGroupPanelProps> = (props) => {
  const [ windowWidth, setWindowWidth ] = useState(screenWidth);
  const {
    chapterName, workingGroupData: {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      id: workingGroupID, name, description, socials,
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      communications, coChairs, members, resources, 
    }
  } = props;
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const {
    panelCradle, col1, col2, titleCradle, title,
    sectionTitle, infoItem, noContentFallbackText
  } = styleGen(props, themeInfo, windowWidth);

  useEffect(() => {
    setWindowWidth(window.innerWidth);
    window.addEventListener('resize', handleResize);
  }, []);

  const handleResize = () => {
    // console.log('WorkingGroupPanel: handleResize() invoked: screen width -> ', window.innerWidth);
    setWindowWidth(window.innerWidth);
  }

  const navigateToPerson = (personID: string) => {
    underConstructionAlert();
  }

  return (
    <div className={css(panelCradle)}>
      <div className={css(col1)}>
        <div className={css(titleCradle)}>
          <p className={css(title)}>{name}</p>
        </div>

        <div style={{ marginTop: themeInfo.distance.one }}></div>

        <Tag
          labelText={chapterName.toUpperCase()}
          tagGeometry={ROUNDED}
          activeColor={themeInfo.palette.primary_p2}
          activeTextColor={themeInfo.palette.primary_m2}
          customTextStyles={{ fontSize: '0.5rem', fontWeight: 'bold' }}
          customCradleStyles={{
            paddingLeft: themeInfo.distance.one,
            paddingRight: themeInfo.distance.one
          }}
        />

        <div style={{ marginTop: themeInfo.distance.two }}></div>

        <SocialsAndComms socials={socials} communications={communications} />

        <div style={{ marginTop: themeInfo.distance.two }}></div>

        <div className={css(sectionTitle)}>Description</div>

        <div style={{ marginTop: themeInfo.distance.two }}></div>

        {
          description ? (
            <p className={css(infoItem)}>{description}</p>
          ) : (
            // - TODO: -> Internationalize text
            <p className={css(noContentFallbackText)}>The working group has not yet provided a description.</p>
          )
        }

        <div style={{ marginTop: themeInfo.distance.two }}></div>

        {/* - TODO: -> Internationalze Text */}
        <p className={css(sectionTitle)}>
          { (coChairs.length > 1) ? "Coordinators" : "Coordinator" }
        </p>

        <div style={{ marginTop: themeInfo.distance.two }}></div>

        {
          (coChairs.length > 0) ? (
            <>
              {
                coChairs.map((coChair: Person) => {
                  const { name, id, avatarURL } = coChair
                  const [ name1, name2 ] = name.split(' ');

                  return (
                    <ClickableListItem
                      key={id}
                      listItemText={name}
                      name1={name1}
                      name2={name2}
                      avatarURL={avatarURL}
                      onClick={underConstructionAlert}
                    />
                  );
                })
              }
            </>
          ) : (
            // - TODO: -> Internationalize text
            <p className={css(noContentFallbackText)}>
              The working group has not yet chosen a coordinator.
            </p>
          )
        }

        <div style={{ marginTop: themeInfo.distance.two }}></div>

        <Members members={members} navigateToPerson={navigateToPerson} />
      </div>

      {
        (windowWidth >= 800) ? (
          <div style={{ marginRight: themeInfo.distance.three }}></div>
        ) : (
          <div style={{ marginTop: themeInfo.distance.three }}></div>
        )
      }

      <div className={css(col2)}>
        {/* RESOURCES */}
        <EmbeddableResourceBrowser
          resources={resources}
          navigateToFullSizedResourceBrowser={underConstructionAlert}
        />
      </div>
    </div>
  );
}

export default WorkingGroupPanel;
