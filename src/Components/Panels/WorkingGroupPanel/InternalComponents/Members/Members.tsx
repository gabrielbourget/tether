// -> Beyond Codebase
import React, { useContext } from 'react';
import { css } from 'aphrodite';
// -> Within Codebase
import ClickableListItem from '../../../../ClickableListItem/ClickableListItem';
// import Button from '../../../../Button/Button';
// import { MoreDetailsIcon } from '../../../../VisualUtilities/IconPresets';
import { UIContext, ThemeInfo } from '../../../../UI_InfoProvider/UI_InfoProvider';
import { Person } from '../../../../../Types';
// -> Within Component
import { styleGen } from './MembersStyles';

export interface IMembersProps {
  members: Person[];
  navigateToPerson(personID: string): void;
}

const WorkingGroups: React.FC<IMembersProps> = (props) => {
  const { members, navigateToPerson } = props;
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const {
    membersCradle, membersTitleBarCradle, membersTitleBarRightCradle,
    sectionTitle, quantityTotal, membersListCradle, noContentFallbackText
  } = styleGen(props, themeInfo);

  return (
    <div className={css(membersCradle)}>
      <div className={css(membersTitleBarCradle)}>
        {/* - TODO: -> Internationalize text */}
        <p className={css(sectionTitle)}>Members</p>
        <div className={css(membersTitleBarRightCradle)}>
          <div className={css(quantityTotal)}>{members.length}</div>
        </div>
      </div>
      <div className={css(membersListCradle)}>
        {
          (members.length > 0) ? (
            <>
              {
                members?.map((workingGroup: any) => {
                  const { name, id: personID } = workingGroup;
                  const [ name1, name2 ] = name.split(' ');

                  return (
                    <ClickableListItem
                      key={personID}
                      listItemText={name}
                      name1={name1}
                      name2={name2}
                      onClick={() => navigateToPerson(personID)}
                    />
                  );
                })
              }
            </>
          ) : (
            // - TODO: -> Internationalize Text
            <p className={css(noContentFallbackText)}>
              Looks like the working group has not yet defined any members.
            </p>
          )
        }
      </div>
    </div>  
  );
}

export default WorkingGroups;