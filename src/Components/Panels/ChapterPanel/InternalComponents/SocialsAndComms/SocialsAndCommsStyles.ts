import { StyleSheet } from 'aphrodite';
import { ThemeInfo } from '../../../../UI_InfoProvider/UI_InfoProvider';
import { styleGuide } from '../../../../../styleGuide/styleGuide';
import { ISocialsAndCommsProps } from './SocialsAndComms';

export const styleGen = (props: ISocialsAndCommsProps, themeInfo: ThemeInfo) => {
  // const { } = props;
  // const { palette } = themeInfo;

  return StyleSheet.create({
    sectionTitle: { ...themeInfo.typography.sectionTitle },
    infoItem: { ...themeInfo.typography.default },
    noContentFallbackText: {
      ...themeInfo.typography.noContentFallbackText,
      textAlign: 'center',
    },
    socialsAndCommsCradle: {
      height: 'min-content',
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    socialsAndCommsItemsCradle: {
      height: 'auto',
      width: '100%',
      display: 'flex',
      flexWrap: 'wrap',
      marginLeft: -themeInfo.distance.one,
    },    
  });
}

export const socialsAndCommsItemCradleStyle = {
  height: 'auto',
  width: 'auto',
  marginRight: styleGuide.distance.one,
  marginBottom: styleGuide.distance.one,
  padding: styleGuide.distance.one,
  display: 'flex',
  alignItems: 'center',
  borderRadius: styleGuide.borderRadii.one,
  transition: styleGuide.transitions.bgColorTransition,
  ':hover': {
    cursor: 'pointer',
    backgroundColor: styleGuide.palette.grey1,
  },
  ':visited': { color: styleGuide.palette.black }
}