// -> Beyond Codebase
import React, { useContext } from 'react';
import { css } from 'aphrodite';
// -> Within Codebase
import LinkWrapper from '../../../../VisualUtilities/LinkWrapper/LinkWrapper';
import IconButton from '../../../../Button/Variants/IconButton/IconButton';
import { UIContext, ThemeInfo } from '../../../../UI_InfoProvider/UI_InfoProvider';
import { determineLinkIcon, mergeSocialsAndComms } from '../../../../../helpers';
// -> Within Component
import { styleGen, socialsAndCommsItemCradleStyle } from './SocialsAndCommsStyles';

export interface ISocialsAndCommsProps {
  socials: any;
  communications: any;
}

const SocialsAndComms: React.FC<ISocialsAndCommsProps> = (props) => {
  const { socials, communications } = props;
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const {
    sectionTitle, socialsAndCommsCradle, socialsAndCommsItemsCradle, infoItem,
    noContentFallbackText
  } = styleGen(props, themeInfo);

  const socialsAndComms = mergeSocialsAndComms(socials, communications);

  return (
    <div className={css(socialsAndCommsCradle)}>
      {/* - TODO: -> Internationalize text */}
      <p className={css(sectionTitle)}>Socials and Comms</p>
      <div style={{ marginTop: themeInfo.distance.two }}></div>
      <div className={css(socialsAndCommsItemsCradle)}>
        {
          (
            (socials.children && (socials.children.length > 0)) ||
            (communications.children && (communications.children.length > 0))
          ) ? (
            <>
              {
                socialsAndComms.children && socialsAndComms.children.map((item: any, index: number) => {
                  const linkIcon = determineLinkIcon(item.name, themeInfo);

                  return (
                    <LinkWrapper
                      link={item.link}
                      key={index}
                      customStyles={{ ...socialsAndCommsItemCradleStyle }}
                    >
                      <IconButton icon={linkIcon} onClick={() => {}} />
                      <div style={{ marginRight: themeInfo.distance.one }}></div>
                      {/* - TODO: -> Internationalize this text */}
                      <p className={css(infoItem)}>{item.name}</p>
                    </LinkWrapper>
                  );
                })
              }
            </>
          ) : (
            // - TODO: -> Internationalize Text
            <p className={css(noContentFallbackText)} style={{ marginTop: themeInfo.distance.three, marginBottom: themeInfo.distance.three }}>
              Looks like the chapter hasn't added any of their links to social networks or communications channels yet.
            </p>
          )
        }
      </div>
    </div>    
  );
}

export default SocialsAndComms;
