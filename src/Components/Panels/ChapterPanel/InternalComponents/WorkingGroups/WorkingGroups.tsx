// -> Beyond Codebase
import React, { useContext } from 'react';
import { css } from 'aphrodite';
// -> Within Codebase
import ClickableListItem from '../../../../ClickableListItem/ClickableListItem';
import Button from '../../../../Button/Button';
import { MoreDetailsIcon } from '../../../../VisualUtilities/IconPresets';
import { UIContext, ThemeInfo } from '../../../../UI_InfoProvider/UI_InfoProvider';
// -> Within Component
import { styleGen } from './WorkingGroupsStyles';

export interface IWorkingGroupsProps {
  chapterID: string,
  workingGroups: any;
  navigateToWorkingGroupsGrid(chapterID: string): void;
  navigateToWorkingGroup(chapterID: string, workingGroupID: string): void;
}

const WorkingGroups: React.FC<IWorkingGroupsProps> = (props) => {
  const {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    workingGroups, navigateToWorkingGroupsGrid,
    navigateToWorkingGroup, chapterID
  } = props;
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const {
    workingGroupsCradle, workingGroupsTitleBarCradle, workingGroupsTitleBarRightCradle,
    sectionTitle, quantityTotal, workingGroupsListCradle, noContentFallbackText
  } = styleGen(props, themeInfo);

  return (
    <div className={css(workingGroupsCradle)}>
      <div className={css(workingGroupsTitleBarCradle)}>
        {/* - TODO: -> Internationalize text */}
        <p className={css(sectionTitle)}>Working Groups</p>
        <div className={css(workingGroupsTitleBarRightCradle)}>
          <div className={css(quantityTotal)}>{workingGroups.length}</div>
          <div style={{ marginRight: themeInfo.distance.two }}></div>
          <Button
            icon={<MoreDetailsIcon color={themeInfo.palette.black} size={themeInfo.styles.standardIconSize} />}
            onClick={() => navigateToWorkingGroupsGrid(chapterID)}
            buttonCradlePadding="none"
            transparent
            height={themeInfo.styles.standardIconSize}
            width={themeInfo.styles.standardIconSize}
          />
        </div>
      </div>
      <div className={css(workingGroupsListCradle)}>
        {
          (workingGroups.length > 0) ? (
            <>
              {
                workingGroups?.map((workingGroup: any) => {
                  const { name, id: workingGroupID } = workingGroup;
                  // const [ name1, name2 ] = name.split(' ');

                  return (
                    <ClickableListItem
                      key={workingGroupID}
                      listItemText={name}
                      // name1={name1}
                      // name2={name2}
                      onClick={() => navigateToWorkingGroup(chapterID, workingGroupID)}
                    />
                  );
                })
              }
            </>
          ) : (
            // - TODO: -> Internationalize Text
            <p className={css(noContentFallbackText)}>
              Looks like the chapter hasn't defined any working groups yet.
            </p>
          )
        }
      </div>
    </div>  
  );
}

export default WorkingGroups;