// -> Beyond Codebase
import React, { useContext } from 'react';
import { css } from 'aphrodite';
// -> Within Codebase
import ClickableListItem from '../../../../ClickableListItem/ClickableListItem';
import { UIContext, ThemeInfo } from '../../../../UI_InfoProvider/UI_InfoProvider';
import { Resource } from '../../../../../Types';
// -> Within Component
import { styleGen } from './SlackChannelsStyles';

export interface ISlackChannelsProps {
  slackChannels?: Resource | any;
  navigateToSlackChannel(): void;
}

const SlackChannels: React.FC<ISlackChannelsProps> = (props) => {
  const { slackChannels, navigateToSlackChannel } = props;
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const {
    sectionTitle, noContentFallbackText, quantityTotal,
    slackChannelsCradle, slackChannelsTitleBarCradle,
    slackChannelsListCradle
  } = styleGen(props, themeInfo);

  return (
    <div className={css(slackChannelsCradle)}>
      <div className={css(slackChannelsTitleBarCradle)}>
        {/* - TODO: -> Internationalize text */}
        <p className={css(sectionTitle)}>Slack Channels</p>
        <div className={css(quantityTotal)}>
          {(slackChannels && slackChannels.children) ? slackChannels.children.length : 0}
        </div>
      </div>
      <div className={css(slackChannelsListCradle)}>
        {
          ((slackChannels && slackChannels.children) && slackChannels.children.length > 0) ? (
            <>
              {
                slackChannels.children.map((slackChannel: any) => {
                  const { name, id } = slackChannel;

                  return (
                    <ClickableListItem
                      key={id}
                      listItemText={name}
                      onClick={navigateToSlackChannel}
                    />
                  );
                })
              }
            </>
          ) : (
            // - TODO: -> Internationalize Text
            <p className={css(noContentFallbackText)}>
              Looks like the chapter hasn't defined any Slack channels yet.
            </p>
          )
        }
      </div>
    </div>
  );
}

export default SlackChannels;