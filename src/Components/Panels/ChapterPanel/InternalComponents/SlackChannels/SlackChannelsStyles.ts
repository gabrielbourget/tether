import { StyleSheet } from 'aphrodite';
import { ThemeInfo } from '../../../../UI_InfoProvider/UI_InfoProvider';
import { ISlackChannelsProps } from './SlackChannels';

export const styleGen = (props: ISlackChannelsProps, themeInfo: ThemeInfo) => {
  const { slackChannels } = props;
  const { palette } = themeInfo;

  // console.log('SlackChannelsStyles: slack channels -> ', slackChannels);

  return StyleSheet.create({
    sectionTitle: { ...themeInfo.typography.sectionTitle },
    noContentFallbackText: {
      ...themeInfo.typography.noContentFallbackText,
      textAlign: 'center',
    },
    quantityTotal: {
      height: 'auto',
      width: 'auto',
      minHeight: 25,
      minWidth: 25,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: '100%',
      backgroundColor: palette.primary,
      ...themeInfo.fonts.tertiary.heavy,
      color: palette.white,
      fontSize: '1.2rem',
    },
    slackChannelsCradle: {
      height: '100%',
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      flex: 1,
    },
    slackChannelsTitleBarCradle: {
      height: 'min-content',
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    slackChannelsListCradle: {
      height: '100%',
      width: '100%',
      paddingTop: themeInfo.distance.two,
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      justifyContent: (slackChannels.children.length > 0) ? 'flex-start' : 'center',
      overflowY: 'scroll',
    }
  });
}
