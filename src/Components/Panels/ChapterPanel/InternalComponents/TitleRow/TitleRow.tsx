// -> Beyond Codebase
import React, { useContext } from 'react';
import { css } from 'aphrodite';
// -> Within Codebase
import AvatarImage from '../../../../AvatarImage/AvatarImage';
import { UIContext, ThemeInfo } from '../../../../UI_InfoProvider/UI_InfoProvider';
import { ROUND } from '../../../../../constants';
// -> Within Component
import { styleGen } from './TitleRowStyles';

export interface ITitleRowProps {
  avatarURL?: string;
  name: string;
}

const TitleRow: React.FC<ITitleRowProps> = (props) => {
  const { avatarURL, name } = props;
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const { titleRow, titleCradle, title } = styleGen(props, themeInfo);

  const [ name1, name2 ] = name.split(' ');

  return (
    <div className={css(titleRow)}>
      <AvatarImage
        size={55}
        avatarURL={avatarURL}
        name1={name1}
        name2={name2}
        avatarGeometry={ROUND}
      />
      <div style={{ marginRight: themeInfo.distance.four }}></div>
      <div className={css(titleCradle)}>
        <p className={css(title)}>{name}</p>
      </div>
    </div>
  );
}

export default TitleRow;
