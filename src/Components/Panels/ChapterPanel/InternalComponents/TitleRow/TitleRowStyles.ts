import { StyleSheet } from 'aphrodite';
import { ThemeInfo } from '../../../../UI_InfoProvider/UI_InfoProvider';
import { ITitleRowProps } from './TitleRow';

export const styleGen = (props: ITitleRowProps, themeInfo: ThemeInfo) => {
  const { palette } = themeInfo;

  return StyleSheet.create({
    titleRow: {
      height: 'min-content',
      width: '100%',
      display: 'flex',
      alignItems: 'center',
    },
    titleCradle: {
      height: 'min-content',
      width: 'min-content',
      padding: themeInfo.distance.two,
      paddingTop: themeInfo.distance.one,
      display: 'flex',
      flexWrap: 'wrap',
      whiteSpace: 'nowrap', // - TODO: -> Resolve dilemma of wanting this and not wanting this at the same time.
      backgroundColor: palette.primary,
      borderRadius: themeInfo.borderRadii.one,
    },
    title: {
      ...themeInfo.fonts.tertiary.bold,
      color: palette.white,
      fontSize: '3.5rem',
      lineHeight: 1
    },    
  });
}
