import { StyleSheet } from 'aphrodite';
import { ThemeInfo } from '../../UI_InfoProvider/UI_InfoProvider';
import { SCREEN_WIDTH_800 } from '../../../constants';
import { IChapterPanelProps } from './ChapterPanel';

export const styleGen = (
  props: IChapterPanelProps,
  themeInfo: ThemeInfo,
  screenWidth: number
) => {
  // const { chapterData: { workingGroups } } = props;
  const { palette } = themeInfo;

  return StyleSheet.create({
    panelCradle: {
      minHeight: '100%',
      maxHeight: 900, // -> This bounds the panel to a square shape at maximum dimensions, change if desired.
      height: (screenWidth > SCREEN_WIDTH_800) ? '100%' : 'auto',
      // height: 'auto',
      maxWidth: 900,
      width: '100%',
      padding: themeInfo.distance.namedVariants.screenPadding,
      display: 'flex',
      backgroundColor: palette.white,
      borderRadius: themeInfo.borderRadii.one,
      boxShadow: themeInfo.shadows.one,

      '@media only screen and (max-width: 800)': {
        flexDirection: 'column',
      },

      '@media only screen and (max-width: 500px)': {
        width: 350
      },

      '@media only screen and (max-width: 375px)': {
        width: 300
      }
    },
    // -> COLUMN 1
    col1: {
      height: 'auto',
      width: '50%',
      display: 'flex',
      flexDirection: 'column',
      // justifyContent: 'center',

      '@media only screen and (max-width: 800)': {
        justifyContent: 'center',
        width: '100%',
      }
    },
    // - COLUMN 2
    col2: {
      height: '100%',
      width: '50%',
      display: 'flex',
      flexDirection: 'column',

      '@media only screen and (max-width: 500)': {
        justifyContent: 'center',
        width: '100%',
      }
    },
  });
}
