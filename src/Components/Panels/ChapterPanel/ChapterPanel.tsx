// -> Beyond Codebase
import React, { useContext, useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { css } from 'aphrodite';
// -> Within Codebase
import ClickableItemList from '../../Lists/ClickableItemList/ClickableItemList';
import EmbeddableResourceBrowser from '../../EmbeddableResourceBrowser/EmbeddableResourceBrowser';
import { UIContext, ThemeInfo } from '../../UI_InfoProvider/UI_InfoProvider';
import { underConstructionAlert } from '../../../helpers';
import { SLACK, CHAPTER_WORKING_GROUPS_ROUTE, CHAPTER_WORKING_GROUP_ROUTE } from '../../../constants';
import { staticFillerChapterData } from '../../../staticData/staticFillerChapterData';
import { CourageChapter } from '../../../Types';
// -> Within Component
import TitleRow from './InternalComponents/TitleRow/TitleRow';
import SocialsAndComms from './InternalComponents/SocialsAndComms/SocialsAndComms';
import WorkingGroups from './InternalComponents/WorkingGroups/WorkingGroups';
import { styleGen } from './ChapterPanelStyles';

const OTTAWA_CHAPTER_ID = '39f49ec3-0bc7-499d-841f-905975813568';

export interface IChapterPanelProps {
  chapterData: CourageChapter;
}

const ChapterPanel: React.FC<IChapterPanelProps> = (props) => {
  const [ windowWidth, setWindowWidth ] = useState(window.innerWidth);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const history = useHistory();
  const { chapterData } = props;
  // const history = useHistory();
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const { panelCradle, col1, col2 } = styleGen(props, themeInfo, windowWidth);

  const {
    avatarURL, communications, name, socials, resources,
    workingGroups, id: chapterID
  } = chapterData

  let slackChannels;
  // -> Digging through the data structure for convenience
  if (chapterData.id === OTTAWA_CHAPTER_ID) {
    const slack = (communications && communications.children) && communications.children.find((item: any) => item.name === SLACK);
    slackChannels = (slack && slack.children) && slack.children.find((item: any) => item.name === "Slack Channels");
  } else {
    slackChannels = staticFillerChapterData.find((chapter: any) => chapter.id === chapterData.id)?.slackChannels;
  }

  useEffect(() => {
    setWindowWidth(window.innerWidth);
    window.addEventListener('resize', handleResize);

    return () => window.removeEventListener('resize', handleResize);
  }, []);

  const handleResize = () => {
    console.log('ChapterPanel: handleResize() invoked: windowWidth -> ', window.innerWidth);
    setWindowWidth(window.innerWidth);
  }

  const navigateToWorkingGroupsGrid = (chapterID: string) => {
    console.log('ChapterPanel: navigateToWorkingGroupsGrid: chapterID -> ', chapterID);
    const isOttawaID = (chapterID === '39f49ec3-0bc7-499d-841f-905975813568');
    console.log(`This is the ottawa ID: ${isOttawaID}`)
    const chapterWorkingGroupsURL = CHAPTER_WORKING_GROUPS_ROUTE.replace(':chapterID', chapterID);
    history.push({
      pathname: chapterWorkingGroupsURL,
      state: { workingGroups }
    });
  }

  const navigateToWorkingGroup = (chapterID: string, workingGroupID: string) => {
    let workingGroupURL = CHAPTER_WORKING_GROUP_ROUTE.replace(':chapterID', chapterID);
    workingGroupURL = workingGroupURL.replace(':workingGroupID', workingGroupID);
    history.push(workingGroupURL);
  }

  return (
    <div className={css(panelCradle)}>
      <div className={css(col1)}>
        {/* TITLE ROW */}
        <TitleRow avatarURL={avatarURL} name={name} />

        <div style={{ marginTop: themeInfo.distance.two }}></div>

        {/* SOCIALS AND COMMS */}
        <SocialsAndComms socials={socials} communications={communications} />

        {/* - TODO: -> Fix the vertical overflow issues with this component. */}
        {/* SLACK CHANNELS */}
        <ClickableItemList
          items={slackChannels}
          navigateToItem={() => underConstructionAlert()}
          listTitle="Slack Channels"
          showItemQuantity
        />

        <div style={{ marginTop: themeInfo.distance.two }}></div>

        {/* WORKING GROUPS */}
        <WorkingGroups
          chapterID={chapterID}
          navigateToWorkingGroupsGrid={navigateToWorkingGroupsGrid}
          navigateToWorkingGroup={navigateToWorkingGroup}
          workingGroups={workingGroups}
        />
      </div>

      {/* Either creating horizontal space or vertical space depending on layout config of the card. */}
      {
        (windowWidth >= 800) ? (
          <div style={{ marginRight: themeInfo.distance.three }}></div>
        ) : (
          <div style={{ marginTop: themeInfo.distance.three }}></div>
        )
      }

      <div className={css(col2)}>
        {/* RESOURCES */}
        <EmbeddableResourceBrowser
          resources={resources}
          navigateToFullSizedResourceBrowser={() => underConstructionAlert()}
        />
      </div>
    </div>
  );
}

export default ChapterPanel;
