// -> Beyond Codebase
import React, { useContext } from 'react';
// -> Within Codebase
import { UIContext } from '../UI_InfoProvider/UI_InfoProvider';
import NavBar from '../NavBar/NavBar';
import Backdrop from '../Backdrop/Backdrop';
import RoutingModule from '../RoutingModule/RoutingModule';

const App: React.FC = () => {
  const uiContext = useContext(UIContext);
  const { backdropVisible } = uiContext;

  const two = "three";
  
  return (
    <>
      <NavBar />
      { backdropVisible && <Backdrop /> }
      <RoutingModule />    
    </>
  );
}

export default App;