// -> Beyond Codebase
import React from 'react';
import { Switch, Route } from 'react-router-dom';
// -> Within Codebase
import TestGround from '../../Pages/TestGround/TestGround';
import AboutTether from '../../Pages/AboutTether/AboutTether';
import Courage from '../../Pages/Courage/Courage';
import CourageWorkingGroups from '../../Pages/CourageWorkingGroups/CourageWorkingGroups';
import CourageWorkingGroup from '../../Pages/CourageWorkingGroup/CourageWorkingGroup';
import Chapters from '../../Pages/Chapters/Chapters';
import Chapter from '../../Pages/Chapter/Chapter';
import ChapterWorkingGroups from '../../Pages/ChapterWorkingGroups/ChapterWorkingGroups'
import WorkingGroup from '../../Pages/ChapterWorkingGroup/ChapterWorkingGroup';
import People from '../../Pages/People/People';
import FullPageResourceBrowser from "../../Pages/FullPageResourceBrowser/FullPageResourceBrowser";
import RouteNotFoundFallback from '../Fallbacks/RouteNotFoundFallback/RouteNotFoundFallback';
import { courageData } from '../../staticData/Courage/courageData';
import { gabrielData } from "../../staticData/gabrielData";
import { ottawaScaleDefund } from '../../staticData/OttawaScaleDefundCoalition/ottawaScaleDefund';
import {
  ABOUT_TETHER_ROUTE, TEST_GROUND_ROUTE, COURAGE_ROUTE,
  CHAPTERS_ROUTE, CHAPTER_ROUTE, CHAPTER_WORKING_GROUPS_ROUTE,
  CHAPTER_WORKING_GROUP_ROUTE, COURAGE_WORKING_GROUPS_ROUTE,
  COURAGE_WORKING_GROUP_ROUTE, PEOPLE_ROUTE, GABRIEL_DATA_ROUTE,
  OTTAWA_SCALE_DEFUND
} from '../../constants';

const RoutingModule: React.FC = () => {

  return (
    <Switch>
      <Route exact path={COURAGE_ROUTE} component={Courage} courageData={courageData} />
      <Route exact path={ABOUT_TETHER_ROUTE} component={AboutTether} />
      <Route exact path={COURAGE_WORKING_GROUPS_ROUTE} component={CourageWorkingGroups} />
      <Route exact path={COURAGE_WORKING_GROUP_ROUTE} component={CourageWorkingGroup} />
      <Route exact path={CHAPTERS_ROUTE} component={Chapters} />
      <Route exact path={CHAPTER_ROUTE} component={Chapter} />
      <Route exact path={CHAPTER_WORKING_GROUPS_ROUTE} component={ChapterWorkingGroups} />
      <Route exact path={CHAPTER_WORKING_GROUP_ROUTE} component={WorkingGroup} />
      <Route exact path={PEOPLE_ROUTE} component={People} />
      <Route exact path={TEST_GROUND_ROUTE} component={TestGround} />
      {/* STEALTH ROUTES */}
      <Route exact path={GABRIEL_DATA_ROUTE} component={FullPageResourceBrowser} resources={gabrielData} />
      <Route exact path={OTTAWA_SCALE_DEFUND} component={FullPageResourceBrowser} resources={ottawaScaleDefund} />
      {/* FALLBACK ROUTE */}
      <Route path="*" component={RouteNotFoundFallback} />
    </Switch>
  );
}

export default RoutingModule;
