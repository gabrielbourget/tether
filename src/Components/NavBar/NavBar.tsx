import React, {useContext} from 'react';
import { useHistory } from 'react-router-dom';
import { css } from 'aphrodite';

import Button from '../Button/Button';
import ImageWrapper from '../VisualUtilities/ImageWrapper/ImageWrapper';
import { UIContext } from '../UI_InfoProvider/UI_InfoProvider';
// - TODO: -> Figure out scaling issue with using SVG version of logo.
// import { ReactComponent as CourageLogo } from '../../assets/images/courage_logo.svg';
import courageLogo from '../../assets/images/CourageLogoPNG.png';
import { ORTHOGONAL, ABOUT_TETHER_ROUTE, COURAGE_ROUTE } from '../../constants';

import { styleGen } from './NavBarStyles';


export interface NavBarProps {

}

const NavBar: React.FC<NavBarProps> = (props) => {
  const history = useHistory();
  const uiInfo = useContext(UIContext);
  const { themeInfo } = uiInfo;
  
  // -> Styles
  const {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    navBarCradle, homeLogoCradle, dropdownCradle
  } = styleGen({themeInfo});

  const navigateHome = () => history.push(COURAGE_ROUTE);
  const navigateToAboutTether = () => history.push(ABOUT_TETHER_ROUTE);

  return (
    <div className={css(navBarCradle)}>
      <div className={css(homeLogoCradle)}>
        <Button
          height={40}
          width={40}
          transparent
          buttonCradlePadding={0}
          buttonGeometry={ORTHOGONAL}
          // - TODO: -> Figure out scaling issue with using SVG version of logo.
          // icon={<CourageLogo height="40px" width="40px" />}
          icon={<ImageWrapper src={courageLogo} alt="Courage Logo" height="75%" width="75%"/>}
          customCradleStyles={{ padding: 0 }}
          onClick={navigateHome}
        />
      </div>
      {/* TODO: -> Internationalize text */}
      <Button
        buttonText="ABOUT"
        transparent
        buttonGeometry={ORTHOGONAL}
        customTextStyles={{
          ...themeInfo.fonts.tertiary.extraBoldItalic,
          color: themeInfo.palette.primary,
          fontSize: '1.3rem',
          lineHeight: 1
        }}
        customCradleStyles={{
          borderBottom: `2px solid ${themeInfo.palette.primary}`,
          paddingTop: 0,
          paddingRight: 0,
          paddingBottom: 2,
          paddingLeft: 0,
        }}
        onClick={navigateToAboutTether}
      />
      {/* <div className={css(dropdownCradle)}>

      </div> */}
    </div>
  );
}

export default NavBar;
