import React from 'react';
import { FiClipboard } from 'react-icons/fi';

interface IIconPresetProps {
  size?: number;
  color?: string;
}

export const FormIcon: React.FC<IIconPresetProps> = (props) => {
  const { size, color } = props;

  return (
    <FiClipboard size={size} color={color} />
  );
}

export default FormIcon;
