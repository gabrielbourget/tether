import React from "react";
import { AiFillTool } from "react-icons/ai";

interface IIconPresetProps {
  size?: number;
  color?: string;
}

export const ToolIcon: React.FC<IIconPresetProps> = (props) => {
  const { size, color } = props;

  return (
    <AiFillTool size={size} color={color} />
  );
}

export default ToolIcon;
