import { StyleSheet } from 'aphrodite';
import { ThemeInfo } from '../../UI_InfoProvider/UI_InfoProvider';
import { ISmallWorkingGroupCardProps } from './SmallChapterWorkingGroupCard';

const TOP_BAR_HEIGHT = 50;

export const styleGen = (
  props: ISmallWorkingGroupCardProps,
  themeInfo: ThemeInfo,
  additionalInfo: any,
) => {
  const { topBarColor, topBarTextColor, workingGroupData: { description } } = props;
  const { palette } = themeInfo;
  const { scrollInitiated, cardHeight, cardWidth, contentOverflowStatus } = additionalInfo

  const computedContentCradleHeight = cardHeight - TOP_BAR_HEIGHT;

  console.log('computedContentCradleHeight -> ', computedContentCradleHeight);

  return StyleSheet.create({
    sectionTitle: { ...themeInfo.typography.sectionTitle },
    infoItem: { ...themeInfo.typography.default },
    noContentFallbackText: {
      ...themeInfo.typography.noContentFallbackText,
      textAlign: 'center',
    },
    infoItemRow: {
      height: 'auto',
      width: '100%',
      display: 'flex',
      alignItems: 'center',
    },
    infoItemRowClickable: {
      height: 'auto',
      width: '100%',
      marginLeft: -5,
      padding: themeInfo.distance.one,
      display: 'flex',
      alignItems: 'center',
      borderRadius: themeInfo.borderRadii.one,
      transition: themeInfo.transitions.bgColorTransition,
      ':hover': {
        cursor: 'pointer',
        backgroundColor: palette.grey1,
      },
      ':visited': { color: palette.black }
    },
    cardCradle: {
      height: cardHeight,
      width: cardWidth,
      display: 'flex',
      flexDirection: 'column',
      backgroundColor: palette.white,
      borderRadius: themeInfo.borderRadii.one,
      boxShadow: themeInfo.shadows.one,
    },
    topBar: {
      height: TOP_BAR_HEIGHT,
      width: '100%',
      padding: themeInfo.distance.one,
      paddingLeft: themeInfo.distance.two,
      display: 'flex',
      backgroundColor: topBarColor ? topBarColor : palette.primary,
      borderTopLeftRadius: themeInfo.borderRadii.one,
      borderTopRightRadius: themeInfo.borderRadii.one,
      boxShadow: scrollInitiated ? `0px 10px 50px rbga(0,0,0,0.7)` : 'none',
    },
    topBarRow: {
      height: '100%',
      width: '100%',
      display: 'flex',
    },
    topBarText: {
      height: '100%',
      ...themeInfo.fonts.tertiary.bold,
      fontSize: '1.8rem',
      lineHeight: 1.15,
      color: topBarTextColor ? topBarTextColor : palette.white,
      display: 'flex',
      alignItems: 'center',
      flex: 1,
    },
    workingGroupDetailsCradle: {
      height: computedContentCradleHeight,
      width: '100%',
      maxHeight: (cardHeight - TOP_BAR_HEIGHT),
      padding: themeInfo.distance.one,
      paddingLeft: themeInfo.distance.two,
      paddingTop: themeInfo.distance.two,
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'flex-start',
      alignItems: 'flex-start',
      overflowY: 'scroll',
      boxShadow: contentOverflowStatus ? themeInfo.shadows.namedVariants.contentOverflowWorkingGroupCard : 'none',
    },
    descriptionCradle: {
      height: 'min-content',
      width: '100%',
      display: 'flex',
      justifyContent: description ? 'flex-start' : 'center',
    },
    atAGlanceSectionCradle: {
      height: '100%',
      width: '100%',
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
    },
    socialsAndCommsSectionCradle: {
      height: '100%',
      width: '100%',
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
    },
  });
}
