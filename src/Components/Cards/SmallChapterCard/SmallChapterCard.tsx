import React, { useContext } from 'react';
// -> Beyond Codebase
import { css } from 'aphrodite';
import { useHistory } from 'react-router-dom';
// -> Within Codebase
import AvatarImage from '../../AvatarImage/AvatarImage';
import IconButton from '../../Button/Variants/IconButton/IconButton';
import {
  PersonIcon, MoreDetailsIcon,
  ChatBubbleIcon, SlackIcon, TwitterIcon, ToolboxIcon,
} from '../../VisualUtilities/IconPresets'
import LineSegment from '../../VisualUtilities/LineSegment/LineSegment';
import { UIContext, ThemeInfo } from '../../UI_InfoProvider/UI_InfoProvider';
import { CHAPTER_ROUTE, HORIZONTAL, SLACK, TWITTER } from '../../../constants';
import { CourageChapter, Resource } from '../../../Types';
import { courageData } from '../../../staticData/Courage/courageData';
// -> Within Component
import { styleGen } from './SmallChapterCardStyles';

export const SMALL_CHAPTER_CARD_HEIGHT = 305;
export const SMALL_CHAPTER_CARD_WIDTH = 250;
const AVATAR_IMAGE_SIZE = 70;
const SLACK_SERVER_LINK = courageData.communications.children?.find((commsItem: Resource) => commsItem.name === SLACK)?.link;
const OTTAWA_CHAPTER_ID = '39f49ec3-0bc7-499d-841f-905975813568';

export interface ISmallChapterCardProps {
  chapterData: CourageChapter | any; // -> 'any' type here is temporary since I splice in static 
  topBarColor?: string;              //     filler data from another source for the other chapters for now.
  topBarTextColor?: string;
}

const additionalInfo = {
  cardHeight: SMALL_CHAPTER_CARD_HEIGHT,
  cardWidth: SMALL_CHAPTER_CARD_WIDTH,
  avatarImageSize: AVATAR_IMAGE_SIZE,
}

// - TODO: -> Refactor component to receive external data instead of internal static data.
export const SmallChapterCard: React.FC<ISmallChapterCardProps> = (props) => {
  const history = useHistory();
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const { chapterData } = props;
  const {
    cardCradle, topBar, topBarText, avatarImageCradle,
    chapterDetailsCradle, sectionTitle, atAGlanceSectionCradle,
    socialsAndCommsSectionCradle, infoItem, infoItemRow,
    infoItemRowClickable, topBarRow
  } = styleGen(props, themeInfo, additionalInfo);
  
  // -> This is contingent on Courage chapters staying to a 'Courage <CityName>' format.
  // const [ name1, name2 ] = chapterData.name.split(' ');

  let slackChannels;
  // -> Digging through the data structure for convenience
  if (chapterData.id === OTTAWA_CHAPTER_ID) {
    slackChannels = chapterData
                    .communications.children.find((item: any) => item.name === SLACK)
                    .children.find((child: any) => child.name === "Slack Channels");
  } else {
    slackChannels = chapterData?.slackChannels;
  }

  const TWITTER_LINK = chapterData.socials.children.find((item: any) => item.name === TWITTER)?.link;
  
  // -> Routes the user to a page with information regarding a specific chapter.
  const navigateToChapter = (chapterID: string) => {
    const chapterRouteURL = CHAPTER_ROUTE.replace(':id', chapterID);
    history.push(chapterRouteURL);
  }

  return (
    <div className={css(cardCradle)}>
      {/* -- TOP BAR -- */}
      <div className={css(topBar)}>
        <div className={css(topBarRow)}>
          <p className={css(topBarText)}>{chapterData.name}</p>
          <IconButton
            icon={<MoreDetailsIcon color={themeInfo.palette.white} size={themeInfo.styles.standardIconSize} />}
            onClick={() => navigateToChapter(chapterData.id)}
          />
        </div>
      </div>
      {/* -- CHAPTER AVATAR IMAGE -- */}
      <div className={css(avatarImageCradle)}>
        <AvatarImage
          // name1={name1}
          // name2={name2}
          avatarURL={chapterData?.avatarURL}
          size={AVATAR_IMAGE_SIZE}
          customCradleStyles={{ transition: themeInfo.transitions.boxShadowTransition }}
          customCradleHoverStyles={themeInfo.shadows.two}
          onClick={() => navigateToChapter(chapterData.id)}
        />
      </div>
      <div className={css(chapterDetailsCradle)}>
        {/* -- AT A GLANCE DETAILS -- */}
        <div className={css(atAGlanceSectionCradle)}>
          {/* - TODO: -> Internationalize this text  */}
          <p className={css(sectionTitle)}>At a Glance</p>

          <div style={{ marginTop: themeInfo.distance.two }}/>

          <div className={css(infoItemRow)}>
            <div style={{ marginLeft: -2.5 }}>
              <PersonIcon color={themeInfo.palette.black} size={themeInfo.styles.standardIconSize}/>
            </div>
            <div style={{ marginRight: themeInfo.distance.two }}></div>
          {/* - TODO: -> Internationalize this text */}
            <p className={css(infoItem)}>Members:</p>
            <div style={{ marginRight: themeInfo.distance.one }} />
            <p className={css(infoItem)}>{chapterData.memberCount}</p>
          </div>
          
          <div style={{marginTop: themeInfo.distance.two }} />

          <div className={css(infoItemRow)}>
            <ToolboxIcon color={themeInfo.palette.black} size={themeInfo.styles.standardIconSize - 2.5} />
            <div style={{ marginRight: themeInfo.distance.two }}></div>
            {/* - TODO: -> Internationalize this text */}
            <p className={css(infoItem)}>Working Groups:</p>
            <div style={{ marginRight: themeInfo.distance.one }} />
            <p className={css(infoItem)}>{chapterData.workingGroups.length}</p>
          </div>

          <div style={{marginTop: themeInfo.distance.two }} />

          <div className={css(infoItemRow)}>
            <ChatBubbleIcon color={themeInfo.palette.black} size={themeInfo.styles.standardIconSize - 5} />
            <div style={{ marginRight: themeInfo.distance.three }}></div>
            {/* - TODO: -> Internationalize this text */}
            <p className={css(infoItem)}>Slack Channels:</p>
            <div style={{ marginRight: themeInfo.distance.one }} />
            <p className={css(infoItem)}>{slackChannels.children.length}</p>
          </div>
        </div>

        <LineSegment
          direction={HORIZONTAL}
          size={1}
          width='100%'
          color={themeInfo.palette.grey1}
          customStyles={{ marginTop: themeInfo.distance.two, marginBottom: themeInfo.distance.two }}
        />

        {/* -- SOCIALS AND COMMS -- */}
        <div className={css(socialsAndCommsSectionCradle)}>
          {/* - TODO: -> Internationalize this text  */}
          <p className={css(sectionTitle)}>Socials and Comms</p>
          
          <div style={{ marginTop: themeInfo.distance.two }}/>

          <a
            className={css(infoItemRowClickable)}
            href={TWITTER_LINK}
            rel="noopener noreferrer nofollow"
            target="_blank"
          >
            <IconButton
              icon={<TwitterIcon color={themeInfo.palette.black} size={themeInfo.styles.standardIconSize} />}
              onClick={() => {}}
            />
            <div style={{ marginRight: themeInfo.distance.two }}></div>
            {/* - TODO: -> Internationalize this text */}
            <p className={css(infoItem)}>{TWITTER}</p>
          </a>
          
          <a
            className={css(infoItemRowClickable)}
            href={SLACK_SERVER_LINK}
            rel="noopener noreferrer nofollow"
            target="_blank"
          >
            <IconButton
              icon={<SlackIcon color={themeInfo.palette.black} size={themeInfo.styles.standardIconSize} />}
              onClick={() => {}}
            />
            <div style={{ marginRight: themeInfo.distance.two }}></div>
            {/* - TODO: -> Internationalize this text */}
            <p className={css(infoItem)}>{SLACK}</p>
          </a>
        </div>
      </div>
    </div>
  );
}

export default SmallChapterCard;
