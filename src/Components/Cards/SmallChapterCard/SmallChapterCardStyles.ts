import { StyleSheet } from 'aphrodite';
import { ISmallChapterCardProps } from './SmallChapterCard';
import { ThemeInfo } from '../../UI_InfoProvider/UI_InfoProvider';

const TOP_BAR_HEIGHT = 70;

export const styleGen = (
  props: ISmallChapterCardProps,
  themeInfo: ThemeInfo,
  additionalInfo: any,
) => {
  const { topBarColor, topBarTextColor } = props;
  const { palette } = themeInfo;
  const { avatarImageSize, cardHeight, cardWidth } = additionalInfo;

  // -> Place avatar image below top bar and then back up half its height.
  const computedAvatarVerticalShift = (TOP_BAR_HEIGHT - (avatarImageSize / 2));
  const computedChapterCradlePaddingTop = ((avatarImageSize / 2) - themeInfo.distance.two);

  return StyleSheet.create({
    cardCradle: {
      height: cardHeight,
      width: cardWidth,
      position: 'relative',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'flex-start',
      alignItems: 'center',
      backgroundColor: palette.white,
      borderRadius: themeInfo.borderRadii.one,
      boxShadow: themeInfo.shadows.one,
    },
    topBar: {
      height: TOP_BAR_HEIGHT,
      width: '100%',
      padding: themeInfo.distance.one,
      paddingLeft: themeInfo.distance.two,
      display: 'flex',
      // alignItems: 'center',
      backgroundColor: topBarColor ? topBarColor : palette.primary,
      borderTopLeftRadius: themeInfo.borderRadii.one,
      borderTopRightRadius: themeInfo.borderRadii.one,
    },
    topBarRow: {
      height: 'min-content',
      width: '100%',
      display: 'flex',
      alignItems: 'center',
    },
    topBarText: {
      flex: 1,
      ...themeInfo.fonts.tertiary.bold,
      fontSize: '1.8rem',
      color: topBarTextColor ? topBarTextColor : palette.white,
    },
    avatarImageCradle: {
      height: 'auto',
      width: 'auto',
      position: 'absolute',
      display: 'grid',
      placeItems: 'center',
      top: computedAvatarVerticalShift,
    },
    sectionTitle: {...themeInfo.typography.sectionTitle},
    infoItem: { ...themeInfo.typography.default },
    infoItemRow: {
      height: 'auto',
      width: '100%',
      display: 'flex',
      alignItems: 'center',
    },
    infoItemRowClickable: {
      height: 'auto',
      width: '100%',
      marginLeft: -5,
      padding: themeInfo.distance.one,
      display: 'flex',
      alignItems: 'center',
      borderRadius: themeInfo.borderRadii.one,
      transition: themeInfo.transitions.bgColorTransition,
      ':hover': {
        cursor: 'pointer',
        backgroundColor: palette.grey1,
      },
      ':visited': { color: palette.black }
    },
    chapterDetailsCradle: {
      height: '100%',
      width: '100%',
      paddingTop: computedChapterCradlePaddingTop,
      paddingRight: themeInfo.distance.one,
      paddingBottom: themeInfo.distance.one,
      paddingLeft: themeInfo.distance.two,
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'flex-start',
      alignItems: 'flex-start',
    },
    atAGlanceSectionCradle: {
      height: '100%',
      width: '100%',
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
    },
    socialsAndCommsSectionCradle: {
      height: '100%',
      width: '100%',
      display: 'flex',
      flex: 1,
      flexDirection: 'column',
    },
  });
}