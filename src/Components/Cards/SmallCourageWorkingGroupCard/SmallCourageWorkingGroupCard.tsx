// -> Beyond Codebase
import React, { useContext, useState, useLayoutEffect, useRef } from 'react';
import { css } from 'aphrodite';
// import { useHistory } from 'react-router-dom';
// -> Within Codebase
import IconButton from '../../Button/Variants/IconButton/IconButton';
import LineSegment from '../../VisualUtilities/LineSegment/LineSegment';
import ClickableListItem from '../../ClickableListItem/ClickableListItem';
import { MoreDetailsIcon, SlackIcon, GroupOfPeopleIcon } from '../../VisualUtilities/IconPresets';
import { UIContext, ThemeInfo } from '../../UI_InfoProvider/UI_InfoProvider';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { WorkingGroup, Person, Resource } from '../../../Types';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HORIZONTAL, SLACK } from '../../../constants';
import { underConstructionAlert } from '../../../helpers';
// -> Within Component
import { styleGen } from './SmallCourageWorkingGroupCardStyles';

export interface ISmallWorkingGroupCardProps {
  chapterID?: string;
  workingGroupData: WorkingGroup | any; // -> 'any' type here is temporary since I splice in static 
  topBarColor?: string;                 //     filler data from another source for the other chapters for now.
  topBarTextColor?: string;
  navigateToWorkingGroup: any;
}

export const SMALL_WORKING_GROUP_CARD_HEIGHT = 305;
export const SMALL_WORKING_GROUP_CARD_WIDTH = 250;

const SmallChapterWorkingGroupCard: React.FC<ISmallWorkingGroupCardProps> = (props) => {
  const cardScrollAreaRef = useRef<HTMLDivElement>(null);
  const [ scrollInitiated, setScrollInitiated ] = useState(false);
  const [ contentOverflowStatus, setContentOverflowStatus ] = useState(false);
  // const history = useHistory();

  useLayoutEffect(() => {
    const clientHeight = cardScrollAreaRef.current?.clientHeight;
    const scrollHeight = cardScrollAreaRef.current?.scrollHeight;

    // console.log('SmallWorkingGroupCard: scroll area clientHeight ->', clientHeight);
    // console.log('SmallWorkingGroupCard: scroll area scrollHeight ->', scrollHeight);
    
    if (clientHeight && scrollHeight) {
      const contentOverflowStatus = (clientHeight < scrollHeight) ? true : false;
      setContentOverflowStatus(contentOverflowStatus);
    }
  }, [])

  const {
    navigateToWorkingGroup, workingGroupData: {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      name, id: workingGroupID, description, communications,
      coChairs, members
    }
  } = props;

  console.log('SmallCourageWorkingGroupCard: description -> ', description);

  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const additionalInfo = {
    scrollInitiated,
    cardHeight: SMALL_WORKING_GROUP_CARD_HEIGHT,
    cardWidth: SMALL_WORKING_GROUP_CARD_WIDTH,
    contentOverflowStatus,
  };
  const {
    cardCradle, topBar, topBarRow, topBarText, sectionTitle,
    infoItem, infoItemRow, workingGroupDetailsCradle, noContentFallbackText
  } = styleGen(props, themeInfo, additionalInfo);

  // const computedSlackChannelName = 'placeholder_channel_name';

  //const computedSlackChannelName = communications.children.find((item: Resource) => item.name === SLACK).children
  //                                  .find((item: Resource) => item.name === "Slack Channels").children[0].name
  const computedSlackChannelName = "N/A";

  const handleScroll = (e: any) => {
    let element = e.target
    // -> Do something at end of scroll
    // if (element.scrollHeight - element.scrollTop === element.clientHeight) {
    // }

    // -> Do something if scrolled at all
    if ((element.scrollHeight - element.scrollTop) !== 0) {
      setScrollInitiated(true);
      // console.log("ayyy i'm scrolllinggg");
    } else setScrollInitiated(false);
  }                                    

  return (
    <div className={css(cardCradle)} ref={cardScrollAreaRef}>
      {/* -- TOP BAR -- */}
      <div className={css(topBar)}>
        <div className={css(topBarRow)}>
          <p className={css(topBarText)}>{name}</p>
          <IconButton
            icon={<MoreDetailsIcon color={themeInfo.palette.white} size={themeInfo.styles.standardIconSize} />}
            onClick={() => navigateToWorkingGroup(workingGroupID)}
            height={themeInfo.styles.standardIconSize}
            width={themeInfo.styles.standardIconSize}
          />
        </div>
      </div>
      <div className={css(workingGroupDetailsCradle)} onScroll={handleScroll}>
        {/* - TODO: -> Internationalize Text */}
        {/* <div className={css(sectionTitle)}>Description</div>

        <div style={{ marginTop: themeInfo.distance.two }}></div> */}

        {
          description ? (
            <p className={css(infoItem)}>{description}</p>
          ) : (
            // - TODO: -> Internationalize text
            <p className={css(noContentFallbackText)}>The working group has not yet provided a description.</p>
          )
        }

        <div style={{ marginTop: themeInfo.distance.two }}></div>

        {/* - TODO: -> Internationalze Text */}
        <p className={css(sectionTitle)}>
          { (coChairs.length > 1) ? "Co-Chairs" : "Chair" }
        </p>

        <div style={{ marginTop: themeInfo.distance.two }}></div>

        {
          (coChairs.length > 0) ? (
            <>
              {
                coChairs.map((coChair: Person) => {
                  const { name, id, avatarURL } = coChair
                  const [ name1, name2 ] = name.split(' ');

                  return (
                    <ClickableListItem
                      key={id}
                      listItemText={name}
                      name1={name1}
                      name2={name2}
                      avatarURL={avatarURL}
                      onClick={underConstructionAlert}
                    />
                  );
                })
              }
            </>
          ) : (
            // - TODO: -> Internationalize text
            <p className={css(noContentFallbackText)}>
              The working group has not yet chosen a chair/co-chairs.
            </p>
          )
        }

        <LineSegment
          direction={HORIZONTAL}
          size={1}
          width="100%"
          color={themeInfo.palette.grey1}
          customStyles={{ marginTop: themeInfo.distance.two, marginBottom: themeInfo.distance.two }}
        />

        <div className={css(infoItemRow)} style={{ paddingLeft: themeInfo.distance.one }}>
          <GroupOfPeopleIcon color={themeInfo.palette.black} size={themeInfo.styles.standardIconSize} />
          <div style={{ marginRight: themeInfo.distance.two }}></div>
          {/* - TODO: -> Internationalize this text */}
          <p className={css(infoItem)}>Members:</p>
          <div style={{ marginRight: themeInfo.distance.one }} />
          <p className={css(infoItem)}>{members.length}</p>
        </div>

        <div style={{ marginTop: themeInfo.distance.two }}></div>

        <ClickableListItem
          listItemText={computedSlackChannelName}
          icon={<SlackIcon color={themeInfo.palette.black} size={themeInfo.styles.standardIconSize} />}
          onClick={underConstructionAlert}
        />
      </div>
    </div>
  );
}

export default SmallChapterWorkingGroupCard;