// -> Beyond Codebase
import React, { useContext } from 'react';
import { css } from 'aphrodite';
// -> Within Codebase
import LineSegment from '../../../VisualUtilities/LineSegment/LineSegment';
import { UIContext, ThemeInfo } from '../../../UI_InfoProvider/UI_InfoProvider';
import { mapResourceTypeToResourceIcon } from '../../../../helpers';
import { ROUNDED, HORIZONTAL, DIRECTORY } from '../../../../constants';
import { Resource } from '../../../../Types';
// import { ResourceBrowserHistory } from '../../../../DataStructures';
// -> Within Component
import ListHeader from './InternalComponents/ListHeader/ListHeader';
import ResourceListItem from './InternalComponents/ResourceListItem/ResourceListItem';
import { styleGen } from './ListResourceBrowserStyles';

// export interface IListResourceBrowserProps {
//   resources: Resource;
//   onResourceClick(link?: string): Window | void | null;
//   navigateForward: any;
//   navigateBackward: any;
//   navigateToDirectory: any;
//   history: ResourceBrowserHistory
//   dispatch: any;
// }
export interface IListResourceBrowserProps {
  resources: Resource;
  onResourceClick(link?: string): Window | void | null;
  navigateForward: any;
  navigateBackward: any;
  navigateToDirectory: any;
  history: string[],
  currentPosition: number,
  dispatch: any;
}

const ListResourcesBrowser: React.FC<IListResourceBrowserProps> = (props) => {
  const {
    resources, onResourceClick, dispatch, navigateForward,
    navigateBackward, navigateToDirectory, history, currentPosition
  } = props;
  const { themeInfo }: { themeInfo: ThemeInfo} = useContext(UIContext);
  const { 
    browserCradle, browserContentCradle, noContentFallbackText
  } = styleGen(props, themeInfo);

  const resourceCount = resources.children ? resources.children.length : 0;

  return (
    <div className={css(browserCradle)}>
      <ListHeader
        dispatch={dispatch}
        navigateBackward={navigateBackward}
        navigateForward={navigateForward}
        history={history}
        currentPosition={currentPosition}
      />
      <div className={css(browserContentCradle)}>
        {
          (resources.children && (resources.children.length > 0)) ? (
            <>
              {
                resources.children && resources.children.map((resource: Resource, index: number) => {
                  const { id, name, types, link } = resource;
                  const icon = mapResourceTypeToResourceIcon(types);
                  const computedOnClick = (types && types.includes(DIRECTORY))
                    ? () => dispatch(navigateToDirectory(id)) : () => onResourceClick(link);

                  return (
                    <div key={id}>
                      <ResourceListItem
                        icon={icon}
                        name={name}
                        types={types}
                        innerCradleGeometry={ROUNDED}
                        customInnerCradleStyles={{
                          transition: themeInfo.transitions.bgColorTransition,
                        }}
                        customInnerCradleHoverStyles={{
                          backgroundColor: themeInfo.palette.grey1
                        }}
                        onClick={computedOnClick}
                      />
                      {
                        (index <= (resourceCount - 1)) ? (
                          <LineSegment
                            direction={HORIZONTAL}
                            width="100%"
                            size={1}
                            color={themeInfo.palette.grey1}
                          />
                        ) : null
                      }
                    </div>
                  );
                })
              }
            </>
          ) : (
            // - TODO: -> Internationalize text
            <p className={css(noContentFallbackText)}>This directory contains no resources.</p>
          )
        }
      </div>
    </div>
  );
}

export default ListResourcesBrowser;
