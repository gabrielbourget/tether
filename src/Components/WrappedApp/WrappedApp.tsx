// -> Beyond Codebase
import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
// -> Within Codebase
import UI_InfoProvider from '../UI_InfoProvider/UI_InfoProvider';
import App from '../App/App';

const apolloClient = new ApolloClient({
  uri: "/api",
});

const WrappedApp: React.FC = () => {
  return (
    <ApolloProvider client={apolloClient}>
      {/* eslint-disable-next-line react/jsx-pascal-case */}
      <UI_InfoProvider>
        <Router>
          <App />
        </Router>
      </UI_InfoProvider>
    </ApolloProvider>
  );
}

export default WrappedApp;
