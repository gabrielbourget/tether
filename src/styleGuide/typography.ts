export const generateTypographyTable = (fonts: any, palette: any) => {
  const { primary, tertiary } = fonts;

  return {
    sectionTitle: {
      fontFamily: primary,
      fontWeight: 'bold',
      fontSize: '1.6rem',
      lineHeight: 1.0,
      textDecoration: 'underline',
    },
    noContentFallbackText: {
      ...tertiary.italic,
      fontSize: '1.2rem',
      color: palette.grey2,
      textAlign: 'center',
    },
    h1: {
      fontFamily: primary,
      fontWeight: 'bold',
      fontSize: '3.6rem',
      color: palette.black,
    },
    h2: {
      fontFamily: primary,
      fontWeight: 'bold',
      fontSize: '2.8rem',
      color: palette.black,
    },
    h3: {
      fontFamily: primary,
      fontWeight: 'bold',
      fontSize: '2.0rem',
      color: palette.black,
    },
    h4: {
      fontFamily: primary,
      fontWeight: 'bold',
      fontSize: '1.8rem',
      color: palette.black,
    },
    h5: {
      fontFamily: primary,
      fontWeight: 'bold',
      fontSize: '1.6rem',
      color: palette.black,
    },
    h6: {
      fontFamily: primary,
      fontWeight: 'bold',
      fontSize: '1.4rem',
      color: palette.black,
    },
    h7: {
      fontFamily: primary,
      fontWeight: 'bold',
      fontSize: '1.2rem',
      color: palette.black,
    },
    default: {
      fontFamily: primary,
      fontSize: '1.2rem',
      color: palette.black,
    }
  };
};
