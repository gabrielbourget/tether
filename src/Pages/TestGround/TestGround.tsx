// -> Beyond Codebase
import React from 'react';
// import React, { useContext } from 'react';
import { StyleSheet, css } from 'aphrodite';
// -> Within Codebase
// import { courageData } from '../../staticData/courageData';
// import { UIContext } from '../../Components/UI_InfoProvider/UI_InfoProvider';
// import CourageCircleLoader from '../../Components/Loaders/CircleLoader/Variants/CourageCircleLoader/CourageCircleLoader';
import FillUnderNavBarCradle from '../../Components/LayoutUtilities/Cradles/FillUnderNavBarCradle/FillUnderNavBarCradle';
// import SmallWorkingGroupCard from '../../Components/Cards/SmallChapterWorkingGroupCard/SmallChapterWorkingGroupCard';
// import { CourageChapter } from '../../Types';

const styleGen = () => {
  return StyleSheet.create({
    cradle: {
      height: '100%',
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    }
  });
}

// const COURAGE_OTTAWA_ID = '39f49ec3-0bc7-499d-841f-905975813568';
// const workingGroups = courageData.chapters.find((chapter: CourageChapter) => chapter.id === COURAGE_OTTAWA_ID)?.workingGroups;

const TestGround: React.FC = () => {
  // const uiInfo = useContext(UIContext);
  const { cradle } = styleGen();

  return (
    <FillUnderNavBarCradle>
      <div className={css(cradle)}>
        {/* {
          workingGroups && (
            <SmallWorkingGroupCard
              chapterID={COURAGE_OTTAWA_ID}
              workingGroupData={workingGroups[0]}
            />
          )
        } */}
      </div>
    </FillUnderNavBarCradle>
  );
}

export default TestGround;
