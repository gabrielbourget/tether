// -> Beyond Codebase
import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { css } from 'aphrodite';
// -> Within Codebase
import SmallChapterCard, { SMALL_CHAPTER_CARD_HEIGHT, SMALL_CHAPTER_CARD_WIDTH } from '../../Components/Cards/SmallChapterCard/SmallChapterCard';
// import LineSegment from '../../Components/VisualUtilities/LineSegment/LineSegment';
import { UIContext, ThemeInfo } from '../../Components/UI_InfoProvider/UI_InfoProvider';
import { courageData } from '../../staticData/Courage/courageData';
import { COURAGE_ROUTE } from '../../constants';
// -> Within Component
import CourageBackButton from './InternalComponents/CourageBackButton';
import { styleGen } from './ChaptersStyles';
import { staticFillerChapterData } from '../../staticData/staticFillerChapterData';

export interface IChaptersProps {

}

const OTTAWA_CHAPTER_ID = '39f49ec3-0bc7-499d-841f-905975813568';

const additionalParams = {
  cardHeight: SMALL_CHAPTER_CARD_HEIGHT,
  cardWidth: SMALL_CHAPTER_CARD_WIDTH
};

// - TODO: -> Once full stack is set up, pull chapter data from GraphQL endpoint.
export const Chapters: React.FC<IChaptersProps> = (props) => {
  const history = useHistory();
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  // const { } = props;
  const {
    pageCradle, topBarCradle, chapterCardGridCradle,
    pageTitleCradle, pageTitle
  } = styleGen(props, themeInfo, additionalParams);

  const navigateToHub = () => history.push(COURAGE_ROUTE);

  return (
    <div className={css(pageCradle)}>
      <div className={css(topBarCradle)}>
        <CourageBackButton onClick={navigateToHub} />
        <div className={css(pageTitleCradle)}>
          {/* - TODO: -> Internationalize text content */}
          <p className={css(pageTitle)}>CHAPTERS</p>
        </div>
        {/* <div className={css(topBarBottomLineSegmentCradle)}>
          <LineSegment
            color={themeInfo.palette.primary}
            direction={HORIZONTAL}
            size={0.05}
            width={800}
          />
        </div> */}
      </div>

      <div style={{ marginTop: themeInfo.distance.four }}></div>
      
      <div className={css(chapterCardGridCradle)}>
        {
          courageData.chapters.map((chapter) => {
            let chapterDataForProps;
            // -> Special case for Ottawa chapter data for now since I have real data for it.
            if (chapter.id === OTTAWA_CHAPTER_ID) {
              chapterDataForProps = chapter;
            // -> Otherwise grab statically prepared data as placeholder for now.
            } else {
              chapterDataForProps = staticFillerChapterData.find((currChapter: any) => currChapter.id === chapter.id);
            }

            return (
              <SmallChapterCard chapterData={chapterDataForProps} key={chapter.id} />
            );
          })
        }
      </div>
    </div>
  );
}

export default Chapters;
