// -> Beyond Codebase
import React, { useContext } from 'react';
import { useParams } from 'react-router-dom';
import { css } from 'aphrodite';
// -> Within Codebase
import FillUnderNavBarCradle from '../../Components/LayoutUtilities/Cradles/FillUnderNavBarCradle/FillUnderNavBarCradle';
import WorkingGroupPanel from '../../Components/Panels/WorkingGroupPanel/WorkingGroupPanel';
import { UIContext, ThemeInfo } from '../../Components/UI_InfoProvider/UI_InfoProvider';
import { courageData } from '../../staticData/Courage/courageData';
// -> Within Component
import { styleGen } from './CourageWorkingGroupStyles';
import { WorkingGroup as WorkingGroupType } from '../../Types';

// - TODO: -> Pull data from GraphQL Endpoint
const CourageWorkingGroup: React.FC = () => {
  const { workingGroupID } = useParams();
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const {
    pageCradle, noContentFallbackText, noContentFallbackTextCradle
  } = styleGen(themeInfo);

  const chapterName = "NATIONAL WORKING GROUP"
  const workingGroupData = courageData.workingGroups.find((workingGroup: WorkingGroupType) => workingGroup.id === workingGroupID);

  return (
    <FillUnderNavBarCradle>
      <div className={css(pageCradle)}>
        {
          (chapterName && workingGroupData) ? (
            <WorkingGroupPanel chapterName={chapterName} workingGroupData={workingGroupData} />
          ) : (
            <div className={css(noContentFallbackTextCradle)}>
              {/* - TODO: -> Internationalize Text */}
              <p className={css(noContentFallbackText)}>
                Something went wrong while retriving working group information.
              </p>
            </div>
          )
        }
      </div>
    </FillUnderNavBarCradle>
  );
}

export default CourageWorkingGroup;
