import { StyleSheet } from 'aphrodite';
import { ThemeInfo } from '../../Components/UI_InfoProvider/UI_InfoProvider';
import { ICourageWorkingGroupsProps } from './CourageWorkingGroups';
import { NAV_BAR_HEIGHT } from '../../constants';

export const styleGen = (
  props: ICourageWorkingGroupsProps,
  themeInfo: ThemeInfo,
  additionalParams: any
) => {
  const { palette } = themeInfo;
  const { cardHeight, cardWidth } = additionalParams;

  const TOP_ROW_HEIGHT = 50;
  const WORKING_GROUPS_ROW_LAYOUT = `${TOP_ROW_HEIGHT}px 1fr`;
  const computedPageCradleHeight = `calc(100vh - ${NAV_BAR_HEIGHT}px)`;

  return StyleSheet.create({
    noContentFallbackText: {
      ...themeInfo.typography.noContentFallbackText,
      textAlign: 'center',
    },
    pageCradle: {
      height: 'auto',
      minHeight: computedPageCradleHeight,
      width: '100%',
      paddingLeft: themeInfo.distance.namedVariants.screenPadding,
      paddingRight: themeInfo.distance.namedVariants.screenPadding,
      paddingBottom: themeInfo.distance.six,
      display: 'grid',
      gridTemplateColumns: '1fr',
      gridTemplateRows: `${WORKING_GROUPS_ROW_LAYOUT}`,
      backgroundImage: themeInfo.gradients.bgPrimaryToTransparentVertical,
    },
    topBarCradle: {
      height: 50,
      width: '100%',
      display: 'flex',
      position: 'relative',
      alignItems: 'center',
    },
    topBarBottomLineSegmentCradle: {
      height: 'auto',
      width: 800,
      margin: '0 auto',
      position: 'absolute',
      bottom: 0, left: 0, right: 0,
    },
    pageTitleCradle: {
      height: 'auto',
      width: 'min-content',
      whiteSpace: 'nowrap',
      position: 'absolute',
      left: 0,
      right: 0,
      margin: '0 auto',
      padding: themeInfo.distance.one,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      // backgroundColor: palette.primary,
      borderRadius: themeInfo.borderRadii.one,
    },
    pageTitle: {
      ...themeInfo.fonts.tertiary.bold,
      fontSize: '2.2rem',
      color: palette.primary,
      lineHeight: 1.0,
      textDecoration: 'underline',
    },
    workingGroupCardGridCradle: {
      // height: 'auto',
      // minHeight: computedContentCradleHeight,
      height: '100%',
      width: '100%',
      display: 'grid',
      gridTemplateColumns: `repeat(auto-fill, ${cardWidth}px)`,
      gridTemplateRows: `repeat(auto-fit, ${cardHeight}px)`,
      justifyContent: 'center',
      justifyItems: 'center',
      gridGap: '2.5rem',
      gridAutoFlow: 'dense',
    },
    noWorkingGroupsFallbackCradle: {
      // height: 'auto',
      // minHeight: computedContentCradleHeight,
      height: '100%',
      width: '100%',
      display: 'grid',
      placeItems: 'center',
    },
    noWorkingGroupsFallbackTextCradle: {
      height: 'min-content',
      width: 'min-content',
      padding: themeInfo.distance.three,
      display: 'grid',
      placeItems: 'center',
      background: palette.white,
      boxShadow: themeInfo.shadows.one,
      borderRadius: themeInfo.borderRadii.one,
      whiteSpace: 'nowrap',
    }
  });
}