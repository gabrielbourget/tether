import React, { useContext } from 'react';
import { css } from 'aphrodite';
import { useHistory, useLocation } from 'react-router-dom';
// -> Within Codebase
import SmallCourageWorkingGroupCard, { SMALL_WORKING_GROUP_CARD_HEIGHT, SMALL_WORKING_GROUP_CARD_WIDTH } from '../../Components/Cards/SmallCourageWorkingGroupCard/SmallCourageWorkingGroupCard';
// import LineSegment from '../../Components/VisualUtilities/LineSegment/LineSegment';
import BackNavigationButton from '../../Components/Button/Variants/BackNavigationButton/BackNavigationButton'
import { UIContext, ThemeInfo } from '../../Components/UI_InfoProvider/UI_InfoProvider';
import { WorkingGroup } from '../../Types';
import { COURAGE_ROUTE, COURAGE_WORKING_GROUP_ROUTE } from '../../constants';
// -> Within Component
import { styleGen } from './CourageWorkingGroupsStyles';

export interface ICourageWorkingGroupsProps {

}

interface ILocationState {
  workingGroups: WorkingGroup[];
}

const additionalParams = {
  cardHeight: SMALL_WORKING_GROUP_CARD_HEIGHT,
  cardWidth: SMALL_WORKING_GROUP_CARD_HEIGHT,
};

const CourageWorkingGroups: React.FC<ICourageWorkingGroupsProps> = (props) => {
  const history = useHistory();
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const {
    pageCradle, topBarCradle, workingGroupCardGridCradle,
    pageTitleCradle, pageTitle, noContentFallbackText,
    noWorkingGroupsFallbackCradle, noWorkingGroupsFallbackTextCradle,
  } = styleGen(props, themeInfo, additionalParams);

  const { state: { workingGroups }} = useLocation<ILocationState>();
  // - TODO: -> Data pull from GraphQL API workingGroups undefined/null here. This will
  //            not have a value if this route is navigated to without clicking through
  //            from some other component that's set up to pass along the working groups
  //            data. As of June 02020, <ChapterPanel /> has such a click through.
  if (!workingGroups) {

  }

  const navigateToHomePage = () => history.push(COURAGE_ROUTE);

  const navigateToWorkingGroup = (workingGroupID: string) => {
    let workingGroupURL = COURAGE_WORKING_GROUP_ROUTE.replace(':workingGroupID', workingGroupID);
    history.push(workingGroupURL);
  }

  return (
    <div className={css(pageCradle)}>
      <div className={css(topBarCradle)}>
        {/* - TODO: -> Internationalize Text */}
        <BackNavigationButton buttonText="COURAGE" onClick={navigateToHomePage} />
        <div className={css(pageTitleCradle)}>
          {/* - TODO: -> Internationalize Text */}
          <p className={css(pageTitle)}>NATIONAL WORKING GROUPS</p>
        </div>
        <div style={{ marginTop: themeInfo.distance.four }}></div>
      </div>
      {
        (workingGroups && (workingGroups.length > 0)) ? (
          <div className={css(workingGroupCardGridCradle)}>
            {
              workingGroups.map((workingGroup: WorkingGroup) => {
                const { id } = workingGroup;

                return (
                  <SmallCourageWorkingGroupCard
                    key={id}
                    workingGroupData={workingGroup}
                    navigateToWorkingGroup={navigateToWorkingGroup}
                  />
                );
              })
            }
          </div>
        ) : (
          <div className={css(noWorkingGroupsFallbackCradle)}>
            <div className={css(noWorkingGroupsFallbackTextCradle)}>
              {/* - TODO: -> Internationalize Text */}
              <p className={css(noContentFallbackText)}>
                The Organization has not yet defined any national working groups.
              </p>
            </div>
          </div>
        )
      }
    </div>
  );
}

export default CourageWorkingGroups;