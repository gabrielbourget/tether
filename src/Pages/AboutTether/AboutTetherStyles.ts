import { StyleSheet } from 'aphrodite';
import { ThemeInfo } from '../../Components/UI_InfoProvider/UI_InfoProvider';

// const BOTTOM_GLOW_CIRCLE_DIAMETER = 450;

export const styleGen = (themeInfo: ThemeInfo) => {

  // - TODO: -> Convert hex of primary color directly to rgb in the code
  //            and then interpolate those values into this string.
  // const computedGlow = `0px -90px 500px rgba(237,109,89,1.0)`;

  return StyleSheet.create({
      pageCradle: {
      width: '100%',
      height: '100%',
      position: 'relative',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
    },
    textCradle: {
      height: 'auto',
      width: 500,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      textAlign: 'center',
      lineHeight: 1.5,
      fontWeight: 300,

      '@media only screen and (max-width: 500px)': {
        width: 350
      },

      '@media only screen and (max-width: 375px)': {
        width: 250
      }
    },
    headerText: {
      fontSize: '5rem',
      ...themeInfo.fonts.secondary.boldItalic,
      color: themeInfo.palette.primary,
      borderBottom: `2px solid ${themeInfo.palette.primary}`,
      lineHeight: 1.0,
    },
    bodyText: {
      fontFamily: themeInfo.fonts.primary,
      fontSize: '1.8rem',
      color: themeInfo.palette.black,
    },
    // bottomGlowCircle: {
    //   height: BOTTOM_GLOW_CIRCLE_DIAMETER,
    //   width: BOTTOM_GLOW_CIRCLE_DIAMETER,
    //   margin: '0 auto',
    //   position: 'absolute',
    //   bottom: (0 - BOTTOM_GLOW_CIRCLE_DIAMETER),
    //   left: 0, right: 0,
    //   borderRadius: '100%',
    //   boxShadow: computedGlow,
    // }
  });
}
