// -> Beyond Codebase
import React, { useContext } from 'react';
import { css } from 'aphrodite';
import { useHistory } from 'react-router-dom';
// -> Within Codebase
import {UIContext, ThemeInfo } from '../../Components/UI_InfoProvider/UI_InfoProvider';
import FillUnderNavBarCradle from '../../Components/LayoutUtilities/Cradles/FillUnderNavBarCradle/FillUnderNavBarCradle'
import Spacer from '../../Components/LayoutUtilities/Spacer/Spacer';
import Button from '../../Components/Button/Button';
import { VERTICAL, COURAGE_ROUTE } from '../../constants';
// -> Within Component
import { styleGen } from './AboutTetherStyles';

export const AboutTether: React.FC = () => {
  const history = useHistory();
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const {
    pageCradle, textCradle, headerText, bodyText,
  } = styleGen(themeInfo);

  const navigateToTheHub = () => history.push(COURAGE_ROUTE);

  // - TODO: -> Link to Courage site: https://couragecoalition.ca

  return (
    <FillUnderNavBarCradle>
      <div className={css(pageCradle)}>
        <div className={css(textCradle)}>
          {/* TODO: -> Internationalize Text */}
          <p className={css(headerText)}>About Tether</p>
          <Spacer amount={themeInfo.distance.six} direction={VERTICAL}/>
          {/* TODO: -> Internationalize Text */}
          <p className={css(bodyText)}>
            Tether links into a hub which brings together key information about the
            different Courage chapters across the country and their working groups.
            It is meant to provide an easy-to-access origin point into the research
            and resources that we generate through our work together, and to grow
            iteratively over time as a democratically managed knowledge commons.
          </p>
        </div>
        <Spacer amount={themeInfo.distance.six} direction={VERTICAL} />
        {/* TODO: -> Internationalize text */}
        <Button
          buttonText="Access The Hub"
          onClick={navigateToTheHub}
          customCradleHoverStyles={{ boxShadow: themeInfo.shadows.two }}
          customCradleStyles={{ transition: themeInfo.transitions.boxShadowTransition }}
        />
        {/* <div className={css(bottomGlowCircle)}></div> */}
      </div>
    </FillUnderNavBarCradle>
  );
}

export default AboutTether;
