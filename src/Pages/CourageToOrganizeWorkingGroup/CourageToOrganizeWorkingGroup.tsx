// -> Beyond Codebase
import React, { useContext } from 'react';
// import { useParams } from 'react-router-dom';
import { css } from 'aphrodite';
// -> Within Codebase
import FillUnderNavBarCradle from '../../Components/LayoutUtilities/Cradles/FillUnderNavBarCradle/FillUnderNavBarCradle';
import WorkingGroupPanel from '../../Components/Panels/WorkingGroupPanel/WorkingGroupPanel';
import { UIContext, ThemeInfo } from '../../Components/UI_InfoProvider/UI_InfoProvider';
import { courageData } from '../../staticData/Courage/courageData';
// -> Within Component
import { styleGen } from './CourageToOrganizeWorkingGroupStyles';
import { WorkingGroup as WorkingGroupType } from '../../Types';

// - TODO: -> Pull data from GraphQL Endpoint
const CourageToOrganizeWorkingGroup: React.FC = () => {
  // const { workingGroupID } = useParams();
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const {
    pageCradle, noContentFallbackText, noContentFallbackTextCradle
  } = styleGen(themeInfo);

  const chapterName = "NATIONAL WORKING GROUP"
  const workingGroupData = courageData.workingGroups.find((workingGroup: WorkingGroupType) => workingGroup.id === '722d29c0-95c0-49c2-b129-4fb450735fa6');

  return (
    <FillUnderNavBarCradle>
      <div className={css(pageCradle)}>
        {
          (chapterName && workingGroupData) ? (
            <WorkingGroupPanel chapterName={chapterName} workingGroupData={workingGroupData} />
          ) : (
            <div className={css(noContentFallbackTextCradle)}>
              {/* - TODO: -> Internationalize Text */}
              <p className={css(noContentFallbackText)}>
                Something went wrong while retriving working group information.
              </p>
            </div>
          )
        }
      </div>
    </FillUnderNavBarCradle>
  );
}

export default CourageToOrganizeWorkingGroup;
