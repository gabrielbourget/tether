// -> Beyond Codebase
import React, { useContext } from 'react';
import { css } from 'aphrodite';
import { useHistory, useLocation, useParams } from 'react-router-dom';
// -> Within Codebase
import SmallChapterWorkingGroupCard, { SMALL_WORKING_GROUP_CARD_HEIGHT, SMALL_WORKING_GROUP_CARD_WIDTH } from '../../Components/Cards/SmallChapterWorkingGroupCard/SmallChapterWorkingGroupCard';
// import LineSegment from '../../Components/VisualUtilities/LineSegment/LineSegment';
import BackNavigationButton from '../../Components/Button/Variants/BackNavigationButton/BackNavigationButton'
import { UIContext, ThemeInfo } from '../../Components/UI_InfoProvider/UI_InfoProvider';
import { WorkingGroup } from '../../Types';
import { CHAPTER_ROUTE, CHAPTER_WORKING_GROUP_ROUTE } from '../../constants';
// -> Within Component
import { styleGen } from './ChapterWorkingGroupsStyles';

export interface IChapterWorkingGroupsProps {
  
}

interface ILocationState {
  workingGroups: WorkingGroup[];
}

const additionalParams = {
  cardHeight: SMALL_WORKING_GROUP_CARD_HEIGHT,
  cardWidth: SMALL_WORKING_GROUP_CARD_WIDTH,
};

// - TODO: -> Once full stack is set up, pull working groups data from GraphQL endpoint.
//           -> Caveat: May already have it from the data pull done one level up in the UI
//                      hierarchy for a chapter.
const ChapterWorkingGroups: React.FC<IChapterWorkingGroupsProps> = (props) => {
  const history = useHistory();
  const { chapterID } = useParams();
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const {
    pageCradle, topBarCradle, workingGroupCardGridCradle,
    pageTitleCradle, pageTitle, noContentFallbackText,
    noWorkingGroupsFallbackCradle, noWorkingGroupsFallbackTextCradle
  } = styleGen(props, themeInfo, additionalParams);

  const { state: { workingGroups }} = useLocation<ILocationState>();
  // - TODO: -> Data pull from GraphQL API workingGroups undefined/null here. This will
  //            not have a value if this route is navigated to without clicking through
  //            from some other component that's set up to pass along the working groups
  //            data. As of June 02020, <ChapterPanel /> has such a click through.
  if (!workingGroups) {

  }

  const navigateToChapter = (chapterID: string) => {
    const chapterURL = CHAPTER_ROUTE.replace(':id', chapterID);
    history.push(chapterURL);
  }

  const navigateToWorkingGroup = (chapterID: string = "abcd1234", workingGroupID: string) => {
    let workingGroupURL = CHAPTER_WORKING_GROUP_ROUTE.replace(':chapterID', chapterID);
    workingGroupURL = workingGroupURL.replace(':workingGroupID', workingGroupID);
    history.push(workingGroupURL);
  }

  return (
    <div className={css(pageCradle)}>
      <div className={css(topBarCradle)}>
        {/* - TODO: -> Internationalize Text */}
        <BackNavigationButton buttonText="CHAPTER" onClick={() => navigateToChapter(chapterID)} />
        <div className={css(pageTitleCradle)}>
          {/* - TODO: -> Internationalize text content */}
          <p className={css(pageTitle)}>WORKING GROUPS</p>
        </div>
        <div style={{ marginTop: themeInfo.distance.four }}></div>
      </div>
      {
        (workingGroups && (workingGroups.length > 0)) ? (
          <div className={css(workingGroupCardGridCradle)}>
            {
              workingGroups.map((workingGroup: WorkingGroup) => {
                const { id } = workingGroup;

                return (
                  <SmallChapterWorkingGroupCard
                    key={id}
                    chapterID={chapterID}
                    workingGroupData={workingGroup}
                    navigateToWorkingGroup={navigateToWorkingGroup}
                  />
                );
              })
            }
          </div>
        ) : (
          <div className={css(noWorkingGroupsFallbackCradle)}>
            <div className={css(noWorkingGroupsFallbackTextCradle)}>
              {/* - TODO: -> Internationalze text */}
              <p className={css(noContentFallbackText)}>
                The chapter has not yet defined any working groups.
              </p>
            </div>
          </div>
        )
      }
    </div>
  );
}

export default ChapterWorkingGroups;
