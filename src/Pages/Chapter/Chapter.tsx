// -> Beyond Codebase
import React, { useContext } from 'react';
import { useParams } from 'react-router-dom';
import { css } from 'aphrodite';
// -> Within Codebase
import FillUnderNavBarCradle from '../../Components/LayoutUtilities/Cradles/FillUnderNavBarCradle/FillUnderNavBarCradle';
import ChapterPanel from '../../Components/Panels/ChapterPanel/ChapterPanel';
import { UIContext, ThemeInfo } from '../../Components/UI_InfoProvider/UI_InfoProvider';
import { courageData } from '../../staticData/Courage/courageData';
// -> Within Component
import { styleGen } from './ChapterStyles';

// - TODO: -> Pull data from GraphQL endpoint
const Chapter: React.FC = () => {
  const { id: chapterID }: { id: string } = useParams();
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const {
    pageCradle, noContentFallbackText, noContentFallbackTextCradle
  } = styleGen(themeInfo); 

  // console.log('Chapter: chapterID from route param is -> ', chapterID);
  const chapterData = courageData.chapters.find((chapter) => chapter.id === chapterID);
  // console.log('Chapter: chapter data for this ID is -> ', chapterData);

  return (
    <FillUnderNavBarCradle>
      <div className={css(pageCradle)}>
        {
          chapterData ? (
            <ChapterPanel chapterData={chapterData} />
          ) : (
            <div className={css(noContentFallbackTextCradle)}>
              {/* - TODO: -> Internationalze text */}
              <p className={css(noContentFallbackText)}>
                Something went wrong while retrieving chapter and working group information.
              </p>
            </div>            
          )
        }
      </div>
    </FillUnderNavBarCradle>
  );
}

export default Chapter;
