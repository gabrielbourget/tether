import { StyleSheet } from 'aphrodite';
import { ThemeInfo } from '../../Components/UI_InfoProvider/UI_InfoProvider';

export const styleGen = (themeInfo: ThemeInfo) => {
  const { palette } = themeInfo;

  return StyleSheet.create({
    noContentFallbackText: {
      ...themeInfo.typography.noContentFallbackText,
      textAlign: 'center',
    },
    pageCradle: {
      height: '100%',
      width: '100%',
      overflowY: 'auto',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      padding: themeInfo.distance.six,
      background: themeInfo.gradients.bgPrimaryToTransparentVertical,
    },
    noContentFallbackTextCradle: {
      height: 'min-content',
      width: 'min-content',
      padding: themeInfo.distance.three,
      display: 'grid',
      placeItems: 'center',
      background: palette.white,
      boxShadow: themeInfo.shadows.one,
      borderRadius: themeInfo.borderRadii.one,
      whiteSpace: 'nowrap',
    }    
  });
}
