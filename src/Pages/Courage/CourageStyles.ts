import { StyleSheet } from 'aphrodite';
import { ThemeInfo } from '../../Components/UI_InfoProvider/UI_InfoProvider';

export const styleGen = (themeInfo: ThemeInfo) => {
  const { palette } = themeInfo;

  return StyleSheet.create({
    pageCradle: {
      height: '100%',
      width: '100%',
      overflowY: 'auto',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      padding: themeInfo.distance.six,
      // background: `url(../../assets/images/photo-treatment_01.jpg)`,
      // background: "url('https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80')",
      background: `
        linear-gradient(0deg, ${palette.secondary}, transparent),
        url('https://pbs.twimg.com/profile_banners/846059531960553472/1494435661/1500x500')
      `,
      // backgroundImage: `linear-gradient(0deg, ${palette.primary_p2}, ${palette.white})`,
      backgroundSize: 'cover',
      // backgroundColor: 'transparent',
    }
  });
}