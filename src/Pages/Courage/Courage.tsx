// -> Beyond Codebase
import React, { useContext } from 'react';
import { css } from 'aphrodite';
// -> Within Codebase
import CouragePanel from '../../Components/Panels/CouragePanel/CouragePanel';
import FillUnderNavBarCradle from '../../Components/LayoutUtilities/Cradles/FillUnderNavBarCradle/FillUnderNavBarCradle';
import { UIContext, ThemeInfo } from '../../Components/UI_InfoProvider/UI_InfoProvider';
import { PoliticalOrganization } from '../../Types';
// -> Within Component
import { styleGen } from './CourageStyles';

interface ICourageProps {
  courageData: PoliticalOrganization;
}

// - TODO: -> Pull data from GraphQL endpoint.
const Courage: React.FC<ICourageProps> = (props) => {
  const { courageData } = props;
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const { pageCradle } = styleGen(themeInfo);

  return (
    <FillUnderNavBarCradle>
      <div className={css(pageCradle)}>
        <CouragePanel courageData={courageData} />
      </div>
    </FillUnderNavBarCradle>
  );
}

export default Courage;
