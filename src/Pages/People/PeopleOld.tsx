// -> Beyond Codebase
import React, { useContext, useState, useEffect } from 'react';
import { css } from 'aphrodite';
// -> Within Codebase
import FillUnderNavBarCradle from '../../Components/LayoutUtilities/Cradles/FillUnderNavBarCradle/FillUnderNavBarCradle';
import Button from '../../Components/Button/Button';
import { UIContext, ThemeInfo } from '../../Components/UI_InfoProvider/UI_InfoProvider';
import { server } from '../../lib/api';
import { Person } from '../../Types/person';

// -> Within Component
import { styleGen } from './PeopleStyles';

const PEOPLE_QUERY = `
  query People {
    people {
      id
      name
      firstName
      lastName
    }
  }
`;

const DELETE_PERSON = `
  mutation DeletePerson($id: ID!) {
    deletePerson(id: $id) {
      id
      name
      firstName
      lastName
    }
  }
`;

interface PersonData {
  people: Person[];
}

interface DeletePersonData {
  data: Person;
}

interface DeletePersonVariables {
  id: string;
}

const PeopleOld: React.FC = () => {
  const [people, setPeople] = useState<Person[] | null>(null);
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const { pageCradle } = styleGen(themeInfo);

  useEffect(() => {
    fetchPeopleProfiles();
  }, [people]);

  const fetchPeopleProfiles = async () => {
    const { data } = await server.fetch<PersonData>({query: PEOPLE_QUERY});
    setPeople(data.people);
    console.log('People: fetchPeopleProfiles(): people -> ', data);
  }

  const deletePerson = async (id: string) => {
    const { data } = await server.fetch<DeletePersonData, DeletePersonVariables>({ 
      query: DELETE_PERSON,
      variables: { id }
    });

    console.log('People: deletePerson(): deleted person -> ', data);

    fetchPeopleProfiles();
  }



  return (
    <FillUnderNavBarCradle>
      <div className={css(pageCradle)}>
        <p>People</p>
        <div style={{ marginTop: themeInfo.distance.two}}></div>
        <ul>
          {
            people && people.map((person: Person) => (
              <div key={person.id} style={{ display: 'flex' }}>
                <li>{`${person.name}`}</li>
                <div style={{ marginRight: themeInfo.distance.three }}></div>
                <Button
                  buttonText="Delete Person"
                  onClick={() => deletePerson(person.id)}
                />
              </div>
            ))
          }
        </ul>
      </div>
    </FillUnderNavBarCradle>
  );
}

export default PeopleOld;
