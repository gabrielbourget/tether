/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: People
// ====================================================

export interface People_people {
  __typename: "Person";
  id: string;
  name: string;
  firstName: string | null;
  lastName: string | null;
}

export interface People {
  people: (People_people | null)[] | null;
}
