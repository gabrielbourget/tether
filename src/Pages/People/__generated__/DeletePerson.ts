/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeletePerson
// ====================================================

export interface DeletePerson_deletePerson {
  __typename: "Person";
  id: string;
  name: string;
  firstName: string | null;
  lastName: string | null;
}

export interface DeletePerson {
  deletePerson: DeletePerson_deletePerson;
}

export interface DeletePersonVariables {
  id: string;
}
