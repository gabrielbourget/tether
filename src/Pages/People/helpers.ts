import { gql } from 'apollo-boost';
import { Person } from '../../Types';

export const PEOPLE_QUERY = gql`
  query People {
    people {
      id
      name
      firstName
      lastName
    }
  }
`;

export const DELETE_PERSON = gql`
  mutation DeletePerson($id: ID!) {
    deletePerson(id: $id) {
      id
      name
      firstName
      lastName
    }
  }
`;

export interface PersonData {
  people: Person[];
}

export interface DeletePersonData {
  data: Person;
}

export interface DeletePersonVariables {
  id: string;
}