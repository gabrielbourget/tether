// -> Beyond Codebase
import React, { useContext } from 'react';
import { css } from 'aphrodite';
import { useQuery, useMutation } from 'react-apollo';
// -> Within Codebase
import FillUnderNavBarCradle from '../../Components/LayoutUtilities/Cradles/FillUnderNavBarCradle/FillUnderNavBarCradle';
import Button from '../../Components/Button/Button';
import { UIContext, ThemeInfo } from '../../Components/UI_InfoProvider/UI_InfoProvider';
// import { useQuery, useMutation } from '../../lib/api';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Person } from '../../Types/person';
// -> Within Component
import { styleGen } from './PeopleStyles';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { People as PeopleData, People_people } from './__generated__/People';
import { DeletePerson as DeletePersonData, DeletePersonVariables } from './__generated__/DeletePerson'
import { PEOPLE_QUERY, DELETE_PERSON } from './helpers';

const People: React.FC = () => {
  const { data, loading, error, refetch } = useQuery<PeopleData>(PEOPLE_QUERY)
  const [
    deletePerson,
    { loading: deletePersonLoading, error: deletePersonError }
  ] = useMutation<DeletePersonData, DeletePersonVariables>(DELETE_PERSON);
  const { themeInfo }: { themeInfo: ThemeInfo } = useContext(UIContext);
  const { pageCradle } = styleGen(themeInfo);


  const handleDeletePerson = async (id: string) => {
    await deletePerson({ variables: { id }});
    refetch();
  }

  if (error) {
    return (
      <h2>There was an error loading people</h2>
    )
  }

  const deletePersonLoadingMessage = deletePersonLoading ? (
    <div style={{
      position: 'absolute',
      top: 100,
      left: 100,
      padding: themeInfo.distance.four,
      borderRadius: themeInfo.borderRadii.two,
      backgroundColor: themeInfo.palette.white,
      boxShadow: themeInfo.shadows.three,
    }}>
      <h4>Deleting a person...</h4>
    </div>
  ) : null;
  
  const deletePersonErrorMessage = deletePersonError ? (
    <div style={{
      position: 'absolute',
      top: 200,
      left: 100,
      padding: themeInfo.distance.four,
      borderRadius: themeInfo.borderRadii.two,
      backgroundColor: themeInfo.palette.white,
      boxShadow: themeInfo.shadows.three,
    }}>
      <h4>Something went wrong while deleting a person...</h4>
    </div>
  ) : null;

  return (
    <FillUnderNavBarCradle>
      <div className={css(pageCradle)}>
        <p>People</p>
        <div style={{ marginTop: themeInfo.distance.two }}></div>
        {
          loading ? (
            <p>Loading...</p>
          ) : (
            <ul>
              {
                data && data.people && data.people.map((person: any) => (
                  <div key={person.id} style={{ display: 'flex' }}>
                    <li>{`${person.name}`}</li>
                    <div style={{ marginRight: themeInfo.distance.three }}></div>
                    <Button
                      buttonText="Delete Person"
                      onClick={() => handleDeletePerson(person.id)}
                    />
                  </div>
                ))
              }
            </ul>
          )
        }
        { deletePersonLoadingMessage }
        { deletePersonErrorMessage }
      </div>
    </FillUnderNavBarCradle>
  );
}

export default People;
