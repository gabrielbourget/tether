import { Resource, Person } from '../Types';

export type PoliticalOrganization = {
  id: string;
  name: string;
  description?: string;
  avatarURL?: string;
  socials: Resource;
  communications: Resource;
  resources: Resource;
  chapters: CourageChapter[];
  workingGroups: WorkingGroup[];
}

export type CourageChapter = {
  id: string;
  name: string;
  location: string;
  province: string;
  avatarURL?: string;
  socials: Resource;
  communications: Resource;
  resources: Resource;
  workingGroups: WorkingGroup[];
  members?: Person[];
  memberCount?: number; // - TODO: -> Remove once dummy chapter data is done with.
}

export type WorkingGroup = {
  id: string;
  name: string;
  description?: string;
  socials: Resource;
  communications: Resource;
  coChairs: Person[];
  members: Person[];
  resources: Resource;
}
