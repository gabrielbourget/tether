export type EventType = "Physical Injury" | "Killing" | "Sexual Assault" | "Sexual Harassment" | "Death" | "Forcible Confinement" | "Uttering Threats" | "Intimidation" | "Harassment" | "Pointing a firearm at an individual";

export interface Source {
  id: string;
  description: string;
  link: string;
}

export interface Event {
  id: string;
  timeStamp: number;
  monthResolution?: boolean;
  description: string | string[];
  institutionsInvolved?: string[];
  victims: string[];
  perpetrators?: string[]
  eventTypes?: EventType[];
  sources: Source[];
}