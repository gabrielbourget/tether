export type Resource = {
  id: string;
  name: string;
  shorthand?: string;
  types?: string[]; // - TODO: -> Type this as a fixed selection of strings
  link?: string;
  value?: string;
  description?: string;
  children?: Resource[];
}
