// - PROVINCES AND TERRITORIES
export const BRITISH_COLUMBIA = 'British Columbia';
export const ALBERTA = 'Alberta';
export const SASKATCHEWAN = 'Saskatchewan';
export const MANITOBA = 'Manitoba';
export const ONTARIO = 'Ontario';
export const QUEBEC = 'Quebec';
export const NEW_BRUNSWICK = 'New Brunswick';
export const NOVA_SCOTIA = 'Nova Scotia';
export const NEWFOUNDLAND_AND_LABRADOR = 'Newfoundland and Labrador';
export const YUKON = 'Yukon';
export const NORTH_WEST_TERRITORIES = 'North West Territories';
export const IQALUIT = 'Iqaluit';

// - CITIES
export const VICTORIA = 'Victoria';
export const VANCOUVER = 'Vancouver';
export const CALGARY = 'Calgary';
export const SASKATOON = 'Saskatoon';
export const REGINA = 'Regina';
export const WINNEPEG = 'Winnipeg';
export const TORONTO = 'Toronto';
export const OTTAWA = 'Ottawa';
export const MONTREAL = 'Montreal';
export const HALIFAX = 'Halifax';