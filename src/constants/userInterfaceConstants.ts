export const NAV_BAR_HEIGHT = 45;

// -> SCREEN SIZE BREAKPOINTS
export const SCREEN_WIDTH_375 = 375;
export const SCREEN_WIDTH_500 = 500;
export const SCREEN_WIDTH_800 = 800;

// -> DISPLAY MODES
export const GRID = 'Grid';
export const LIST = 'List';