import {
  TWITTER, COURAGE_VICTORIA, COURAGE_VANCOUVER, COURAGE_CALGARY,
  COURAGE_SASKATOON, COURAGE_REGINA, COURAGE_WINNEPEG,
  COURAGE_TORONTO, COURAGE_MONTREAL, COURAGE_HALIFAX, DIRECTORY
} from "../constants";

export const staticFillerChapterData = [
  {
    id: 'c9d7ba7a-81c5-4bdf-be31-c1cd39e07e20',
    name: COURAGE_VICTORIA,
    memberCount: 25,
    avatarURL: 'https://pbs.twimg.com/profile_images/861661665829339136/vcDldF65_400x400.jpg',
    workingGroups: [{}, {}, {}, {}, {}],
    slackChannels: {
      children: [{}, {}, {}, {}, {}, {}]
    },
    socials: {
      name: 'Socials',
      types: [DIRECTORY],
      children: [
        {
          name: TWITTER,
          networkName: TWITTER,
          link: 'https://twitter.com/Courage_VIC',
        }
      ]
    },
  },
  {
    id: '4b8c85db-3baa-447e-b60e-ee480310626f',
    name: COURAGE_VANCOUVER,
    memberCount: 40,
    avatarURL: 'https://pbs.twimg.com/profile_images/861657807333937152/Ui5advoo_400x400.jpg',
    workingGroups: [{}, {}, {}, {}, {}, {}, {}],
    slackChannels: {
      children: [{}, {}, {}, {}, {}, {}, {}, {}]
    },
    socials: {
      name: 'Socials',
      types: [DIRECTORY],
      children: [
        {
          name: TWITTER,
          networkName: TWITTER,
          link: 'https://twitter.com/Courage_VAN',
        }
      ]
    },
  },
  {
    id: '0c04dbe2-1e89-444f-8cc0-ffde534d6116',
    name: COURAGE_CALGARY,
    memberCount: 28,
    avatarURL: 'https://pbs.twimg.com/profile_images/862002736325316608/Xq1jTN6D_400x400.jpg',
    workingGroups: [{}, {}, {}, {}],
    slackChannels: {
      children: [{}, {}, {}, {}, {}, {}]
    },
    socials: {
      name: 'Socials',
      types: [DIRECTORY],
      children: [
        {
          name: TWITTER,
          networkName: TWITTER,
          link: 'https://twitter.com/Courage_CGY',
        }
      ]
    },
  },
  {
    id: '7d95ae38-eb97-40d7-bb5a-c2a9e3348fb9',
    name: COURAGE_SASKATOON,
    memberCount: 24,
    avatarURL: 'https://pbs.twimg.com/profile_images/861666943404724224/wHtPHmRK_400x400.jpg',
    workingGroups: [{}, {}, {}, {}, {}],
    slackChannels: {
      children: [{}, {}, {}, {}, {}, {}]
    },
    socials: {
      name: 'Socials',
      types: [DIRECTORY],
      children: [
        {
          name: TWITTER,
          networkName: TWITTER,
          link: 'https://twitter.com/Courage_SSK',
        }
      ]
    },
  },
  {
    id: '981d5f06-39ba-4aa6-805a-434a24ee380c',
    name: COURAGE_REGINA,
    memberCount: 26,
    avatarURL: 'https://pbs.twimg.com/profile_images/861664087389818882/uwHLqC54_400x400.jpg',
    workingGroups: [{}, {}, {}],
    slackChannels: {
      children: [{}, {}, {}, {}, {}, {}]
    },
    socials: {
      name: 'Socials',
      types: [DIRECTORY],
      children: [
        {
          name: TWITTER,
          networkName: TWITTER,
          link: 'https://twitter.com/Courage_REG',
        }
      ]
    },
  },
  {
    id: 'ea65277e-154d-45d9-9ec6-d3c04f274be2',
    name: COURAGE_WINNEPEG,
    memberCount: 31,
    avatarURL: 'https://pbs.twimg.com/profile_images/860589695931097088/MA7ZhXzn_400x400.jpg',
    workingGroups: [{}, {}, {}, {}, {}],
    slackChannels: {
      children: [{}, {}, {}, {}, {}, {}]
    },
    socials: {
      name: 'Socials',
      types: [DIRECTORY],
      children: [
        {
          name: TWITTER,
          networkName: TWITTER,
          link: 'https://twitter.com/Courage_WPG',
        }
      ]
    },
  },
  {
    id: '60a86a4f-392b-4efe-b48d-98584e193847',
    name: COURAGE_TORONTO,
    memberCount: 50,
    avatarURL: 'https://pbs.twimg.com/profile_images/860580763456028672/wFhYDaqD_400x400.jpg',
    workingGroups: [{}, {}, {}, {}, {}, {}],
    slackChannels: {
      children: [{}, {}, {}, {}, {}, {}, {}, {}, {}]
    },
    socials: {
      name: 'Socials',
      types: [DIRECTORY],
      children: [
        {
          name: TWITTER,
          networkName: TWITTER,
          link: 'https://twitter.com/Courage_TOR',
        }
      ]
    },
  },
  {
    id: 'e7129e80-3e71-4a25-b900-5a2997acd1d5',
    name: COURAGE_MONTREAL,
    memberCount: 45,
    avatarURL: 'https://pbs.twimg.com/profile_images/862010072674598912/wMJnToWE_400x400.jpg',
    workingGroups: [{}, {}, {}, {}, {}, {},],
    slackChannels: {
      children: [{}, {}, {}, {}, {}, {}, {}]
    },
    socials: {
      name: 'Socials',
      types: [DIRECTORY],
      children: [
        {
          name: TWITTER,
          networkName: TWITTER,
          link: '',
        }
      ]
    },
  },
  {
    id: '5d6d6ff8-b8ea-4d2a-9bfe-a9e381b99035',
    name: COURAGE_HALIFAX,
    memberCount: 27,
    avatarURL: 'https://pbs.twimg.com/profile_images/862015443908804608/6WdKo2of_400x400.jpg',
    workingGroups: [{}, {}, {}, {}],
    slackChannels: {
      children: [{}, {}, {}, {}, {}]
    },
    socials: {
      name: 'Socials',
      types: [DIRECTORY],
      children: [
        {
          name: TWITTER,
          networkName: TWITTER,
          link: '',
        }
      ]
    },
  },
];
