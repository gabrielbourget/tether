import { 
  COURAGE_VICTORIA, COURAGE_VANCOUVER, COURAGE_CALGARY,
  COURAGE_SASKATOON, COURAGE_REGINA, COURAGE_WINNEPEG,
  COURAGE_TORONTO, COURAGE_OTTAWA, COURAGE_MONTREAL,
  COURAGE_HALIFAX
} from '../constants';

export const people = [
  {
    id: '5e88faf4-263d-4d3f-b98c-0534c3b1c317',
    name: 'Silas Xuereb',
    firstName: 'Silas',
    lastName: 'Xuereb',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: '74df8f54-9892-492a-b4c5-7bce88605e45',
    name: 'Fred Langlois',
    firstName: 'Fred',
    lastName: 'Langlois',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: 'b1df823f-6da4-4660-9c30-a8fc7a34adfb',
    name: 'Gabriel Bourget',
    firstName: 'Gabriel',
    lastName: 'Bourget',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: '31df33c0-c62c-4813-a1c2-e2993aae6536',
    name: 'James Hutt',
    firstName: 'James',
    lastName: 'Hutt',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: 'b20de0d8-12f7-4b0b-b05e-0b3e1f625bac',
    name: 'Julia Szwarc',
    firstName: 'Julia',
    lastName: 'Szwarc',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: '0e8c4dd5-a36f-4ea8-92d6-31fadaf314de',
    name: 'Matt Lipton',
    firstName: 'Matt',
    lastName: 'Lipton',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: '154f29aa-1870-4119-a018-0de3edf2b87b',
    name: 'Clem',
    firstName: 'Clem',
    lastName: '',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: '26d1406a-e157-4bc1-a011-f27c4c2a2dc3',
    name: 'Meagan Wiper',
    firstName: 'Meagan',
    lastName: 'Wiper',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: '94c0fab7-be85-40ca-9391-a0afe5cd0865',
    name: 'Ethan Mitchell',
    firstName: 'Ethan',
    lastName: 'Mitchell',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: 'f674ec89-d44c-4432-833e-0d58e41997ab',
    name: 'Sam Hersh',
    firstName: 'Sam',
    lastName: 'Hersh',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: '44ffdba4-9885-4c71-8078-174797126512',
    name: 'Shivangi Misra',
    firstName: 'Shivangi',
    lastName: 'Misra',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: 'c6a1f621-882e-4304-8bb7-273b986184b9',
    name: 'David K',
    firstName: 'David',
    lastName: 'K',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: 'ec2cf87e-0a03-4efd-b921-9161cde9fe67',
    name: 'Tyler Paziuk',
    firstName: 'Tyler',
    lastName: 'Paziuk',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: 'acb74f0e-cf61-40d9-9d7f-4ae59b5f9917',
    name: 'Debbie Owusu-Akyeeah',
    firstName: 'Debbie',
    lastName: 'Owusu-Akyeeah',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: 'a9d4b6ef-6a7d-4b17-aa87-d4de5d8379d1',
    name: 'Adrian Murray',
    firstName: 'Adrian',
    lastName: 'Murray',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: 'af43048e-05a6-45c8-b729-13943bf00411',
    name: 'Cameron Roberts',
    firstName: 'Cameron',
    lastName: 'Roberts',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: '6c711d48-219a-4ed6-a78b-cad5e2ba54c3',
    name: 'Laura Shantz',
    firstName: 'Laura',
    lastName: 'Shantz',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: '',
    name: '',
    firstName: '',
    lastName: '',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: '',
    name: '',
    firstName: '',
    lastName: '',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: 'c40ee152-a057-49e9-a506-faabadf21798',
    name: 'Sam B',
    firstName: 'Sam',
    lastName: 'B',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: 'ee9492c9-1dcc-4f59-bb0a-d41099e42705',
    name: 'Karim A',
    firstName: 'Karim',
    lastName: 'A',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: '2dbbc389-d5cc-4ac1-b261-e3d0748fa7fa',
    name: 'Melanie Mathias',
    firstName: 'Melanie',
    lastName: 'Mathias',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: '',
    name: '',
    firstName: '',
    lastName: '',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: '',
    name: '',
    firstName: '',
    lastName: '',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
  {
    id: '',
    name: '',
    firstName: '',
    lastName: '',
    avatarURL: '',
    chapter: { id: '39f49ec3-0bc7-499d-841f-905975813568', name: COURAGE_OTTAWA },
    socials: [
      {
        networkName: '',
        link: ''
      },
    ]
  },
];