import {
  DOCUMENT_FILE, DIRECTORY, GOOGLE_DRIVE, PDF_FILE, SPREADSHEET_FILE,
} from "../../constants";

import { Event } from "../../Types";

export const timelineSources: any = [
  {
    name: "killercopscanada",
    link: "https://killercopscanada.wordpress.com/",
  },
  {
    name: "A Timeline of Ottawa POlice Violence",
    link: "https://leveller.ca/2019/01/ottawa-police-violence/",
  },
  {
    name: "Not our Friends: The Ottawa POlice's Long History of Violence and Racism",
    link: "https://leveller.ca/2019/02/not-our-friends-the-ottawa-polices-long-history-of-violence-and-racism/",
  },
];

export const OPSTimeline: Event[] = [
  {
    id: "7180e091-a5da-41cc-aabe-31a0aacdb08d",
    timeStamp: 595832400000,
    description: "Earl Edwards, Jamaican immigrant and choir master, was shot from behind and wounded in the hand during a traffic stop on Hunt Club Road. Edwards and his wife Ena were stopped by OPP Constable Arno Giek. Earl Edwards was shot while seated in his car, and Ena Edwards was detained in the back of the police cruiser.",
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Earl Edwards",
    ],
    perpetrators: [
      "Arno Giek"
    ],
    eventTypes: [
      "Physical Injury"
    ],
    sources: [
      {
        id: "ede2f885-4230-4f77-a082-8f5aab3dfe48",
        description: "Upton, Sean. 'Manotick OPP officer faces charge in shooting.' Ottawa Citizen, 21 November 1989.",
        link: "www.newspapers.com/image/463698167/?terms=earl%2Bedwards",
      }
    ],
  },
  {
    id: "bbc8a410-b730-4732-b650-4a471efb599e",
    timeStamp: 685857600000,
    description: [
      "Vince Gardner, a Jamaican immigrant, was shot and killed by Constable John Monette. Gardner was admitted to Ottawa Civic Hospital where he underwent three operations. Gardner died in hospital, without leaving, on November 16.",
      "The shooting occurred during a botched drug raid on a private residence on Gould Street, and after a series of escalating racist attacks by neighbours on the home’s occupants, including having trash dumped in the yard and car headlights smashed. Constable Monette claimed he mistook a guitar for a firearm and was cleared of all charges, including manslaughter.",
    ],
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Vince Gardner",
    ],
    perpetrators: [
      "John Monette",
    ],
    eventTypes: [
      "Killing"
    ],
    sources: [
      {
        id: "fedde600-3f5b-4b33-9b86-c44614a0beca",
        description: "Abraham, Carolyn. “Anatomy of a botched drug bust: The shooting of Vincent Gardner.” Ottawa Citizen, 13 February 1993.",
        link: "www.newspapers.com/image/464633270/?terms=vincent%2Bgardner",
      }
    ],
  },
  {
    id: "67aa5943-6c61-4c50-bf8a-c6ac86408cdd",
    timeStamp: 764571600000,
    description: "Marc Valin died in custody following a beating by police. Valin had complained to friends in letters and to his cellmate at OCDC that he had not felt well since his arrest in on February 24, 1994, during which police beat him. OCDC staff reportedly didn’t believe Valin, and gave him Tylenol. He was admitted to hospital on March 15 and died 10 days later.",
    institutionsInvolved: [
      "Ottawa Police Service",
      "Ottawa-Carleton Detention Centre"
    ],
    victims: [
      "Mark Valin"
    ],
    eventTypes: [
      "Killing"
    ],
    sources: [
      {
        id: "aada4656-e0af-49ad-8346-a16704dab3c5",
        description: "Kainz, Alana. “Policy probe brutality claim in man’s death.” Ottawa Citizen, 28 March 1994.",
        link: "www.newspapers.com/image/464876753/?terms=marc%2Bvalin",
      }
    ],
  },
  {
    id: "66496e66-ad31-4975-9673-4556271ae983",
    timeStamp: 795675600000,
    description: "Wayne Johnson, a Black man, died after being pepper sprayed and chased by police to the Rideau River. He fell in and drowned. According to reports, police failed to assist him. Police were unable to find Johnson’s body. It was found five days later by friends of Johnson, just three metres from where he was last seen",
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Wayne Johnson"
    ],
    eventTypes: [
      "Killing"
    ],
    sources: [
      {
        id: "7d3e7c3e-467c-4500-8e5d-eea4b94ea992",
        description: "Stern, Leonard. “Trouble plagues dispirited police force; Leadership, morale problems dog chief who is grappling to unite officers.” Ottawa Citizen, 12 May 1995.",
        link: "www.newspapers.com/image/464826518/?terms=wayne%2Bjohnson"
      }
    ],
  },
  {
    id: "b9234b1b-62cf-47eb-ae5e-06860a52bd44",
    timeStamp: 804571200000,
    monthResolution: true,
    description: "Terry Norris was pepper sprayed by police during an altercation at a gas station. He died shortly thereafter.",
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Terry Norris"
    ],
    eventTypes: [
      "Killing"
    ],
    sources: [
      {
        id: "704ee821-520d-4f30-b653-b60435c4bf7a",
        description: "McGregor, Glen. “Police ill-trained in use of pepper spray: Company: Ottawa-Carleton Police Services Board sues U.S. manufacturer for providing faulty spray device.” Ottawa Citizen. 19 February 2000.",
        link: "groups.google.com/forum/#!topic/misc.activism.progressive/PwbHYHmxd8c"
      }
    ],
  },
  {
    id: "a6d04f51-2047-4567-b294-748d001b9c0e",
    timeStamp: 804571200000,
    monthResolution: true,
    description: [
      "Ottawa police forced their way into the apartment of Jean-Paul Gravelle and pepper sprayed him, believing he was an individual that had dined and dashed from a restaurant on Somerset. Police later admitted that Gravelle, a white man, was not the individual they were looking for.",
      "Gravelle sued police over long-lasting health complications resulting from the assault. Police sued the pepper spray manufacturer, blaming the company for the impacts on Gravelle’s health."
    ],
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Jean-Paul Gravelle"
    ],
    eventTypes: [
      "Physical Injury"
    ],
    sources: [
      {
        id: "e944bd7c-38de-4f45-a3d5-054289423c31",
        description: "McGregor, Glen. “Ottawa police sue maker of pepper spray: If ‘harmless’ product hurt innocent man, blame supplier, not us, force argues.” Ottawa Citizen, 2 June 1999.",
        link: "www.newspapers.com/image/465844526"
      },
    ],
  },
  {
    id: "ac4eeeca-e764-455e-8f6f-06eab35ec484",
    timeStamp: 805867200000,
    description: "Troy Emmerson was shot and killed by Constable Dan Delaney outside his apartment following a three hour stand-off, during which Emmerson refused to drop a gun he was aiming at his own head.",
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Troy Emmerson",
    ],
    perpetrators: [
      "Dan Delaney",
    ],
    eventTypes: [
      "Killing"
    ],
    sources: [
      {
        id: "8bf669d7-0084-4938-a961-3cad45a377c3",
        description: "Aubry, Jack and Sandra Gionas. “Police did not need to kill son, man says; Single bullet ends 3-hour standoff with young father.” Ottawa Citizen, 17 July 1995.",
        link: "www.newspapers.com/image/464836484/?terms=troy%2Bemmerson"
      },
    ],
  },
  {
    id: "2efb8a67-03fa-4a73-a5d9-4b253a88b16a",
    timeStamp: 856760400000,
    description: "Charles Cooper, a white man, died after being shot in the chest with a “less lethal” metal bean bag while inside his apartment. At the time of the shooting, Cooper was self-harming with a knife.",
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Charles Cooper"
    ],
    eventTypes: [
      "Killing"
    ],
    sources: [
      {
        id: "c7666783-ce9d-4d0e-aaaf-559f54feaf42",
        description: "Stern, Leonard. “Beanbag gun has spotty history : `Non-lethal’ weapon called unacceptable by police conference.” Ottawa Citizen, 27 February 1997.",
        link: "www.newspapers.com/image/465718430/?terms=charles%2Bcooper",
      },
    ],
  },
  {
    id: "2186ef18-3e2c-46fa-872e-b6b876974420",
    timeStamp: 975128400000,
    description: "While handcuffed, Julie Cayer had her head repeatedly slammed against the hood of a police squad car by Constable Martin Cardinal. The incident was caught on film and, after lengthy court proceedings, Cardinal eventually pled guilty. He was sentenced to 75 days community service and also lost eight days of pay.",
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Julie Cayer"
    ],
    perpetrators: [
      "Martin Cardinal"
    ],
    eventTypes: [
      "Physical Injury"
    ],
    sources: [
      {
        id: "3f32825d-4f07-436a-8f2f-80cdcc30368d",
        description: "“Ottawa police officer pleads guilty to assault.” CBC News, 26 July 2005.",
        link: "www.cbc.ca/news/canada/ottawa-police-officer-pleads-guilty-to-assault-1.567148"
      },
    ],
  },
  {
    id: "14b7b38a-7dd2-45ab-8c60-b73249925b41",
    timeStamp: 993182400000,
    description: "Benoit Aube was run over by a police van while allegedly fleeing a robbery on a bicycle. Aube was trapped under one of the police van’s wheels for 30 minutes. He died in hospital on June 24.",
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Benoit Aube"
    ],
    eventTypes: [
      "Killing"
    ],
    sources: [
      {
        id: "e82f353a-1863-49ae-8c6d-dd4e57a7375b",
        description: "“Man run over by police dies.” Ottawa Citizen. 24 June 2001.",
        link: "www.newspapers.com/image/466536855/?terms=benoit%2Baube",
      },
    ],
  },
  {
    id: "93ebc1fe-c14e-4b46-bdb0-26c46e89a7fc",
    timeStamp: 994219200000,
    description: "Stephane Drouin, a 19-year-old white man, died in hospital after his van crashed into a hydro pole on Canada Day, during a high speed chase by police.",
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Stephane Drouin"
    ],
    eventTypes: [
      "Death"
    ],
    sources: [
      {
        id: "1aa68ca0-5a11-4f2f-8dd0-5207738d8dfc",
        description: "Sands, Aaron. “‘They killed my brother’: Family of man killed in police chase demands ‘justice.’ Ottawa Citizen. 4 July 2001.",
        link: "www.newspapers.com/image/466536449/?terms=stephane%2Bdrouin",
      },
    ],
  },
  {
    id: "40f7d997-d425-438f-ad54-b29d96f7bce1",
    timeStamp: 1219464000000,
    description: [
      "Roxanne Carr had two bones broken in her wrist, was restrained and led by a belt, and was left naked in her cell for more than two hours following her arrest. Footage of the assault in the cellblock was made public.(12) Carr was arrested and thrown to the ground eight minutes after police arrived at her rental unit, where she was chatting with friends on the porch. Police were called by Carr’s boyfriend, who wanted her to leave the building.",
      "Police charged Carr with resisting arrest and assaulting an officer, charges that were eventually withdrawn. A judge eventually ruled that the Ottawa Police had no grounds to arrest Carr and must pay her $254,000 in damages. No officers were charged or held responsible for their involvement.",
    ],
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Roxanne Carr"
    ],
    eventTypes: [
      "Physical Injury"
    ],
    sources: [
      {
        id: "2de8e032-a2ad-4d49-9b9f-c2e636b49f1a",
        description: "“Carr cellblock video no. 1.” Ottawa Citizen.  25 February 2013. ",
        link: "www.youtube.com/watch?v=cAreMIKzm7o",
      },
    ],
  },
  {
    id: "125ad19d-d8d0-4ea8-b18b-2ab070fba62d",
    timeStamp: 1220328000000,
    description: "After complying with an order to kneel on the floor of her cell, a half-naked woman, unnamed in media accounts, was kicked and tasered twice by Sergeant Steve Desjourdy. Desjourdy pled guilty during a Police Services Act hearing, and was demoted to Constable for 90 days.",
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Unnamed"
    ],
    perpetrators: [
      "Steve Desjourdy"
    ],
    eventTypes: [
      "Physical Injury"
    ],
    sources: [
      {
        id: "801aa436-add3-466d-8737-d24e06bf4326",
        description: "Dimmock, Gary. “Desjourdy had previous run-in for how he treated female prisoner.” Ottawa Citizen. 20 May 2014.",
        link: "ottawacitizen.com/news/local-news/desjourdy-had-previous-run-in-for-how-he-treated-female-prisoner",
      },
    ],
  },
  {
    id: "cce12a5f-5e1c-4778-9656-e8e18fc77a92",
    timeStamp: 1222574400000,
    description: [
      "After being stopped and questioned by police while walking home from a party, Stacey Bonds, a Black woman, was taken to the police station. There she was kicked twice by Special Constable Melanie Morris, pinned to the floor with a riot shield, and held down by three male officers while Sergeant Steve Desjourdy cut her shirt and bra off with a pair of scissors. Bonds was then left half-naked in her cell for three hours and later charged with assaulting an officer.",
      "The charges were thrown out, with the judge stating there was no reason for Bonds’ arrest in the first place. Sergeant Desjourdy was eventually docked 20 days pay. No other officers were held responsible for the assault.",
    ],
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Stacey Bonds"
    ],
    perpetrators: [
      "Melanie Morris",
      "Steve Desjourdy",
    ],
    eventTypes: [
      "Physical Injury",
      "Sexual Assault",
    ],
    sources: [
      {
        id: "a3e73fc7-2ba5-49fe-ae3d-c85139a8d37f",
        description: "'Ottawa cop charged with sex assault in Stacy Bonds case.' Ottawa Citizen. 15 March 2011",
        link: "https://ottawa.ctvnews.ca/ottawa-cop-charged-with-sex-assault-in-stacy-bonds-case-1.618831",
      },
    ],
  },
  {
    id: "2828ce86-04f5-4485-9f0c-120377c66184",
    timeStamp: 1246420800000,
    monthResolution: true,
    description: "While being dragged by officers to his cell, Terry Delay, a homeless Indigenous man, was kicked twice by Special Constable Melanie Morris. Police charged Delay with assaulting an officer. Video was released of the incident. A judge threw out the charge against Delay.",
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Terry Delay"
    ],
    perpetrators: [
      "Melanie Morris"
    ],
    eventTypes: [
      "Physical Injury",
    ],
    sources: [
      {
        id: "5cc3d481-6d59-404c-b6fe-de0a505db985",
        description: "“Video of homeless man kicked released.” CBC News. 1 December 2010.",
        link: "www.cbc.ca/news/canada/ottawa/video-of-homeless-man-being-kicked-released-1.877111",
      },
    ],
  },
  {
    id: "f02306e9-2df9-4f4d-a05a-b0ec3b5bf8de",
    timeStamp: 1313208000000,
    description: "Witnesses say they saw police trip and push Hugh Styres, a homeless man, into the pavement in Sandy Hill, causing a pool of blood to form. Constables Colin Bowie and Thanh Tran were eventually acquitted of all charges.",
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Hugh Styres"
    ],
    perpetrators: [
      "Colin Bowie",
      "Thanh Tran",
    ],
    eventTypes: [
      "Physical Injury"
    ],
    sources: [
      {
        id: "6f848341-1e5c-415a-900d-9023012f34ae",
        description: "“Police officers found not guilty of assaulting homeless man.” CBC News. 11 October 2013.",
        link: "www.cbc.ca/news/canada/ottawa/police-officers-found-not-guilty-of-assaulting-homeless-man-1.1991160"
      },
    ],
  },
  {
    id: "e1c52e10-b983-44fa-8668-b50892e7ccda",
    timeStamp: 1469332800000,
    description: [
      "Following an incident at a local coffee shop, Abdirahman Abdi, a Somali-Canadian man, was pursued by two police officers and intercepted outside his apartment building. According to eye-witness accounts recounted in the Ottawa Citizen, Abdi was “pepper-sprayed, beaten with a baton and punched as officers arrested him. Some pleaded with officers to stop and tried to alert them that Abdi was mentally ill.”",
      "At the time of Abdi’s beating, Montsion was wearing so-called “assault gloves” designed with a thick carbon layer to mimic the effect of brass knuckles. Abdi was taken to hospital, where he was pronounced dead the following afternoon",
    ],
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Abdirahman Abdi"
    ],
    perpetrators: [
      "Daniel Montsion"
    ],
    eventTypes: [
      "Killing"
    ],
    sources: [
      {
        id: "57bf0525-c18c-48a8-9436-1de8e2b4a55c",
        description: "Yogaretnam, Shaamini. “Updated: Ottawa police officer facing manslaughter charge in death of Abdirahman Abdi.” Ottawa Citizen. 6 March 2017.",
        link: "ottawacitizen.com/news/local-news/ottawa-police-officer-to-be-charged-with-manslaughter-in-death-of-abdirahman-abdi"
      },
      {
        id: "a13a1ebb-6a46-4a83-9f3e-2697e400b618",
        description: "Nixon, Sarah. “Charges in Abdi case still a long way from justice.” The Leveller 9:6, Spring 2017.",
        link: "www.leveller.ca/wp-content/uploads/2017/03/LEVELLER-9.6-FINAL-WEB.pdf",
      },
    ],
  },
  {
    id: "e7822ee4-a494-4821-ab71-f9d62f35154e",
    timeStamp: 1512104400000,
    monthResolution: true,
    description: "Constable Carl Keenan was suspended with pay after being charged with assaulting a woman while off duty. During an internal investigation by Ottawa Police, two female officers came forward with sexual harassment complaints against Keenan. They stated the harassment took place during their training and while Keenan was assigned as their coach officer.",
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Unnamed"
    ],
    perpetrators: [
      "Carl Keenan"
    ],
    eventTypes: [
      "Sexual Harassment",
      "Sexual Assault",
    ],
    sources: [
      {
        id: "82d00019-7ca4-4a1d-ad67-0e6e6509adb8",
        description: "Trinh, Judy. “Police officer facing assault charge also accused of workplace sexual harassment.” CBC News. 4 January 2019.",
        link: "www.cbc.ca/news/canada/ottawa/ottawa-police-criminal-trial-assault-bodily-harm-woman-1.4964964"
      },
    ],
  },
  {
    id: "b5d0c17b-d1d1-4469-85f5-76e2c7c8ed99",
    timeStamp: 1537329600000,
    description: "Constable Eric Post was arrested by the Ottawa Police sexual assault unit and charged with 32 criminal acts, including sexual assault, forcible confinement, harassment, uttering threats, intimidation, and pointing his firearm at an individual during an on-duty sexual assault.",
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Unnamed"
    ],
    perpetrators: [
      "Eric Post"
    ],
    eventTypes: [
      "Sexual Assault",
      "Harassment",
      "Forcible Confinement",
      "Uttering Threats",
      "Intimidation",
      "Pointing a firearm at an individual"
    ],
    sources: [
      {
        id: "c7cce7fd-19ea-4cbc-a1d9-901432db4a21",
        description: "Britneff, Beatrice. “11 new charges against Ottawa police officer accused of sex assault, uttering threats.” Global News. 28 September 2018.",
        link: "globalnews.ca/news/4495728/11-new-charges-ottawa-police-officer",
      },
    ],
  },
  {
    id: "59e5d40b-306f-4d17-b8cd-b6da7aff964e",
    timeStamp: 1538366400000,
    description: "Police charged Sergeant Aasim Ansari with sexual assault. The charges allege Ansari was on duty in the area of a residence for people from Nunavut who are in Ottawa for medical treatment when the assault occurred.",
    institutionsInvolved: [
      "Ottawa Police Service"
    ],
    victims: [
      "Unnamed"
    ],
    perpetrators: [
      "Aasim Ansari"
    ],
    eventTypes: [
      "Sexual Assault"
    ],
    sources: [
      {
        id: "67dfd6af-9127-48d1-8836-32dcec7ab499",
        description: "Yogaretnam, Shaamini. SIU charges Ottawa police sergeant with sexual assault. Ottawa Citizen. October 1, 2018.",
        link: "ottawacitizen.com/news/local-news/siu-charges-ops-sergeant-with-sexual-assault",
      },
    ],
  },
  {
    id: "39340b9e-ae44-40d8-8a8c-15b5cf15bd2f",
    timeStamp: 1548910800000,
    description: [
      "Greg Ritchie, a 30-year-old Ojibwe man from Saugeen First Nation near Owen Sound, Ontario, has been identified as the man shot and killed by Ottawa police on January 31. Family members have spoken publicly to say he was experiencing mental health crises and was heading to a pharmacy to pick up medication when he was shot and killed by police. The responding officers have been identified as Ottawa constables Thanh Tran and Daniel Vincelette. Witnesses have reported hearing more than two shots. Tran is a repeat offender. He and another officer had been charged, in September 2011, with assault causing bodily harm following the arrest of an intoxicated 50-year-old homeless man.",
      "Family members say Ritchie, who had been taken from his mother and placed in foster care, had struggled with mental health issues from a young age. He had moved to Ottawa to live with his brother and his partner. Ritchie’s sister-in-law reports that he was in good spirits the morning he was killed, having received his Ontario Disability Support Program payment and going out for a coffee. He then set out to get his medication, suffering a headache and recovering from a concussion. He had been a customer at the pharmacy at Elmvale Acres Mall since arriving in Ottawa.",
      "Police allegedly received a call about a “suspicious incident.” This is a painfully poignant description given that Ritchie’s family says he had an ongoing fear that people viewed him suspiciously because of the way he looked and because of his Indigenous identity.",
      "In the words of his sister-in-law, Chantel Ritchie:",
      "“And the thing is, that’s not the kind of guy he is. He gets scared…and that’s the saddest part. We know that he was in complete and utter terror in a moment like that. He’s scared of just going into a grocery store…of just being in a crowd, because he’s afraid that people want to do something to him or don’t like him because of the way he looks.",
      "“And honestly, we’ve seen it. People just take one look and that’s it. He’s First Nations, he’s been homeless before, and he is afraid. People just take all of that in one look and then make assumptions and then act on it. And it just really hurts that we weren’t there to be able to calm him down because there’s no way that any of this would have happened if we were there. There’s no way.” (quoted in CBC News 2019)",
      "Chantal Ritchie says Greg Ritchie felt better around family and was very involved in learning about his culture. She worries that cultural materials he carried with him might have been misinterpreted as weapons by the police who killed him. In her words:",
      "“I could tell right away he suffered from mental illness, but when he was around family he was very happy. He was very into his culture and learning about his ancestors. He did sometimes exhibit that he feels the pain of what happened to his people…but he was just happy to be around family and to be at powwows and helping at those events.",
      "“He was sometimes hired to keep sacred fires going at ceremonies, and spent time in woods and rivers looking for arrowheads. It also comforted him to recreate arrowheads and other artifacts.",
      "“Those things he keeps on his person because it makes him feel safe. We always tell him, don’t bring it with you anywhere, because we were afraid of this very thing happening.” (quoted in CBC News 2019)",
      "One witness, Shireen Moodley, reports hearing multiple rapid-fire gunshots",
    ],
    institutionsInvolved: [
      "Ottawa Police Service",
    ],
    victims: [
      "Greg Ritchie"
    ],
    perpetrators: [
      "Thanh Tran",
      "Daniel Vincelette",
    ],
    eventTypes: [
      "Killing"
    ],
    sources: [
      {
        id: "5e8b2745-3a4e-4186-b60c-ad98d664df5f",
        description: "CBC News. 2019. “Greg Ritchie ID’d as Man Shot and KIlled by Police at Mall.” CBC News. February 1.",
        link: "https://www.cbc.ca/news/canada/ottawa/ottawa-fatal-police-shooting-family-1.5000285",
      },
    ],
  },
  {
    id: "a0592b1a-5ff5-485d-b6ec-5b179c7cab43",
    timeStamp: 1562472000000,
    description: [
      "Two people, a 36-year-old man and a 62-year-old woman have been killed in a six-car crash following a police pursuit on July 7, 2019 on Highway 417 near Ottawa. Officers of the Ottawa Police Service and the Ontario Provincial Police (OPP) had initiated pursuit of a vehicle driven by the 36-year-old victim.",
      "The Special Investigations Unit (SIU, the agency that examines cases of police harm to civilians in Ontario) reports that the crash incident was initiated with a call from a man reporting that his daughter had been abused by a family friend at a Kanata hotel. This means the identity of the alleged suspect was known and under Ontario rules a police pursuit should not have been undertaken (where a suspect’s identity is known).",
      "The SIU has designated one Ottawa police officer as the subject of its investigation, and also identified three other “witness officers,” two of whom are OPP officers. The agency assigned three investigators, two forensic specialists, and a collision reconstruction expert to its investigation team.",
      "According to Ottawa defence lawyer Lawrence Greenspon, the main job for the SIU is to determine why the pursuit was launched given the identity of the suspect was known to police. While rules govern when a suspect should be pursued, Greenspon notes that too often those regulations are not followed. He outlines the too regular script followed by police:",
      "“Every time there’s a crash like this it’s the same story: ‘We started the pursuit and it was stopped before the fatal collision took place.’ The big question here that needs to be answered is: Why were they chasing him in the first place? If they knew his identity, why start chasing him?” (quoted in Duffy, Yogaretnam, Gillis, and Miller 2019).",
      "In Ottawa, the police service’s policy “requires that officers only engage in a pursuit if they believe a crime has been committed and if there’s no alternative for apprehending a suspect who poses a threat to public safety”"
    ],
    institutionsInvolved: [
      "Ottawa Police Service",
      "Ontario Provincial Police",
    ],
    victims: [
      "Unnamed",
      "Unnamed",
    ],
    eventTypes: [
      "Death"
    ],
    sources: [
      {
        id: "bb52eaf7-238b-4b8b-a0c4-6b4d965551c1",
        description: "Duffy, Andrew, Shaamini Yogaretnam, Megan Gillis, and Jacquie Miller. 2019. “Updated — Highway 417 Crash: Vehicle Pursuit Stemmed from Sexual Assault Investigation.” Ottawa Citizen July 9.",
        link: "https://ottawacitizen.com/news/local-news/multi-vehicle-crash-closes-westbound-highway-417-at-panmure-road",
      }
    ]
  },
  {
    id: "cca257b9-fb7e-45bb-9789-9cabf649ec63",
    timeStamp: 1602043200000,
    description: [
      "Anthony Aust, a 23-year-old Black man, fell to his death from a twelfth-floor apartment after Ottawa police used a so-called dynamic entry, a tactic identified by courts as showing disregard for Charter rights. Police used the dynamic entry while executing a drug raid, according to the Special Investigations Unit (SIU), the agency that examines cases of police harm to civilians, in the morning of October 7, 2020.",
      "It has not been reported publicly what happened after police entered the apartment, what police did, or how long they were there before the victim fell. It has been reported that other people were present inside the apartment at the time of the raid, including younger siblings, mother, grandmother, and stepfather, but not whether they sustained any injuries.",
      "Dynamic entries are akin to the infamous “no knock” raids familiar  in the United States. Masked officers break into a building, often deploying “distraction devices,” such as flash grenades, with guns drawn. While legally required to knock and announce their presence as police, this is not always done (Yogaretnam 2020)",
      "On February 11, 2020, Superior Court Justice Sally Gomery ruled, with reference to another drug raid case, that police use of dynamic entry is “serious misconduct” (Yogaretnam 2020). In her decision she concluded:",
      "“The knock-and-announce principle has been part of Canadian law for decades. The officers who testified did not say when the Ottawa police decided that dynamic entry should be the rule rather than the exception. However long the practice has been in place, it reflects a casual disregard for charter rights.” (quoted in Yogaretnam 2020)",
      "She denied blanket justifications for the use of dynamic entry based on police claims of “officer safety.” In her words:",
      "“This does not justify systemic use of dynamic entry to execute all, or almost all, search warrants of private dwellings in Ottawa. The police cannot operate from an assumption that they should break in the door of any residence that they have a warrant to search. The court must be concerned about disassociating itself from this practice.” (quoted in Yogaretnam 2020)",
      "Police have since said that regardless of this finding they will continue to use the practices and clearly have done so.",
      "While few details related to the death have been released publicly, this seems to be another fatality in the statist war on drugs. Decriminalization is essential, not still more police violence.",
      "Three investigators and one forensic investigator have been assigned to the case by the SIU."
    ],
    victims: [
      "Anthony Aust"
    ],
    institutionsInvolved: [
      "Ottawa Police Service",
    ],
    eventTypes: [
      "Death"
    ],
    sources: [
      {
        id: "011e8783-1a57-4461-abe5-f16ca885d89c",
        description: "Yogaretnam, Shaamini. 2020. “Ottawa Police Used Controversial Entry Tactic Before Man Fell to His Death, SIU Investigating.” October 7.",
        link: "https://ottawacitizen.com/news/local-news/siu-investigating-after-man-falls-to-death-during-drug-warrant-execution",
      }
    ]
  },
];
