import { DIRECTORY, DOCUMENT_FILE, PDF_FILE, SPREADSHEET_FILE, VIDEO_FILE, WEBSITE } from "../../../constants";
import { WorkingGroup } from "../../../Types";

export const NDP_WG: WorkingGroup = {
  id: "6ac52d27-23e2-4981-9a14-13eb4d18653c",
  name: "NDP Working Group",
  description: "Working group for Courage that's looking into collaboration with and participation within the NDP.",
  coChairs: [

  ],
  members: [

  ],
  socials: {
    id: "669a5348-504b-475a-a3db-6bce09fb2675",
    name: "Socials",
    types: [DIRECTORY],
    children: [

    ]
  },
  communications: {
    id: "ff86b0a0-53bf-4240-8fc6-ebdda050c44d",
    name: "Communications",
    children: [
      {
        id: "43718bab-0af8-44be-8d8a-679c09d26f05",
        name: "Slack",
        link: "https://www.independentleft.slack.com",
        children: [
          {
            id: "d57d5534-118f-4695-8dfc-85d877ee1e62",
            name: "Slack Channels",
            children: [
              {
                id: "4c547bc0-49f0-471f-9174-f84d73d2979b",
                name: "governance",
              }
            ]
          }
        ]
      }
    ]
  },
  resources: {
    id: "c1650b22-d059-4c20-891a-dac6c1c0a100",
    name: "NDP Working Group",
    types: [DIRECTORY],
    children: [
      {
        id: "5fff151f-dbc1-4314-9e77-dd7f80156ccf",
        name: "Meeting Minutes",
        types: [DIRECTORY],
        children: [
          {
            id: "180db196-e310-4d02-a307-6785798bd32b",
            name: "2021",
            types: [DIRECTORY],
            children: [
              {
                id: "0fb273c8-8d81-4f2b-bbae-2e1afdfe2e7a",
                name: "January",
                types: [DIRECTORY],
                children: [
                  {
                    id: "f68af7bc-de50-4d40-8310-fcdd771f74a6",
                    name: "January 09",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "1a0b494a-3038-4434-926b-5bed81fc56de",
                        name: "Courage National - NDP Working Group Meeting Minutes - January 09, 2021",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/19Z1HQIaRZgfkUeS2irnvJ-O-zzpttYhoWSpeqUJETU4/edit",
                      },
                    ]
                  },
                  {
                    id: "404b39ab-e291-49be-a8ee-b571b5051c50",
                    name: "January 17",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "946bca81-04ee-4215-bb11-f78c683bb8ea",
                        name: "Courage National - NDP Working Group Meeting Minutes - January 17, 2021",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1PWgxnNzRfPuxgBFJxOC0WDAyQUNCPXQndpN466vmbAM/edit?usp=sharing",
                      }
                    ]
                  }
                ]
              },
              {
                id: "aa3fe9d8-c16b-4a7f-8884-4c33be716226",
                name: "February",
                types: [DIRECTORY],
                children: [
                  {
                    id: "c346fcd3-93b3-4f63-a23c-b3fb7893f2d9",
                    name: "February 14",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "c9a586df-a751-46e1-acd6-d26e681cd95d",
                        name: "Courage Coalition - NDP Working Group Meeting Minutes - February 14, 2021",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1Gmt-unPPN0tPLlStWOqrMfndgw4d9wg3h8Fn-t7u9N8/edit?usp=sharing",
                      },
                    ]
                  },
                  {
                    id: "ddc797fd-3168-4830-a901-926c6eb03da1",
                    name: "February 21",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "1f9f14d0-6b4f-49ef-b2bf-da907d45b1b2",
                        name: "Courage Coalition - NDP Working Group Meeting Minutes - Feb 21, 2021",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1RsoKgzMlFcKELXOHQZEy7efCVM0uRYx7cId2rDZvfy0/edit",
                      }
                    ]
                  }
                ]
              }
            ],
          }
        ]
      },
      {
        id: "7a4df5bc-e7b0-4584-9451-c05504108a29",
        name: "NDP Convention 2021",
        types: [DIRECTORY],
        children: [
          {
            id: "a063af66-6719-4cd1-bce5-150151caff76",
            name: "NDP Convention 2021 Website",
            types: [WEBSITE],
            link: "https://www.ndp.ca/convention2021",
          },
          {
            id: "88927dde-6cf3-494b-bf4a-60070307dcd9",
            name: "NDP Convention 2021 - Timeline of Important Dates and Events",
            types: [DOCUMENT_FILE],
            link: "https://docs.google.com/document/d/15JPSkm4g2owT2JDYM5u5w_v1Lv-D7YYDo6hxNvsjuOA/edit",
          },
          {
            id: "c0fb1b37-1e79-4460-8b78-53e0def2ec82",
            name: "Winter 2021 Open Letter Calling for Transparency and Democracy in the NDP",
            types: [DOCUMENT_FILE],
            link: "https://docs.google.com/document/d/10QHG7VRMQHLeIxoZbAbqH_fomcKHE6e6LTVFPWH06Uk/edit",
          },
          {
            id: "7d6dd3ad-162d-44b6-bd82-f7ae09a0cc70",
            name: "Resolutions",
            types: [DIRECTORY],
            children: [
              {
                id: "43946a73-3498-4684-93ff-a947d187a137",
                name: "Palestine Resolution #1",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/1zTN3o6vjR6QSu8xepXko5v2ag4p6kvA7LPseQvBJJBU/edit",
              },
              {
                id: "414a7809-19c9-422f-b893-50ae67dce4ec",
                name: "Telecomms Resolution",
                types: [DOCUMENT_FILE],
                link: "https://drive.google.com/file/d/1qd8DvR418NTOVU7y6jmFWObMUiuqjJap/view?usp=sharing",
              },
              {
                id: "5f0bd235-1478-40bf-b31e-06c98b39fae9",
                name: "Transit Resolutions (Free Transit and Inter-City Transit)",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/1t4j31NX9a03WfKSCMAliGGdPubD5KfzIymuRozygRrQ/edit?usp=sharing",
              },
              {
                id: "9b17a15a-f19b-48a1-b899-79b04edd8179",
                name: "Defund the RCMP",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/1kGRx54xjkSKAjabt1kaFuwRj126-Exjab3nZRPutP94/edit?usp=sharing",
              },
              {
                id: "8359a874-b99d-4ab0-9c96-f39eee9032cd",
                name: "Stop Fossil Fuel Projects",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/19jHIeUevr0o4bHw1eappoHGHX5uNqF-APF0S6Px2jZ4/edit?usp=sharing",
              },
              {
                id: "fe6be7a9-74a8-4363-a200-ee23960be94f",
                name: "Abolish Billionaires",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/1lOvs4Og5FXsftzp8a3yRNLoxg1dT9jSRpi0W9wQS9O8/edit?usp=sharing",
              },
              {
                id: "91546edb-56d4-4ce0-9b12-9515af51cf49",
                name: "Repeal the Red Tape Reduction Act",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/1ISoReEkn53_2xRcQzmfoZXVDt5fxpzYvnDNT0JaOmBg/edit",
              },
              {
                id: "c877ea35-17fa-4ec6-a28a-94ddb1e2b13b",
                name: "EDA Autonomy",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/15_7Rr6U6nbV5faJjwqTrkk2m54w7f45b4G-t5MLkm8c/edit",
              },
              {
                id: "d43a99fa-e0bd-47cc-980c-b51d01c6ab32",
                name: "EDA Engagement",
                types: [WEBSITE],
                link: "http://www.couragecoalition.ca/en-2017-amendment/",
              },
              {
                id: "9323e654-564a-45a1-8289-caa0df3e9e56",
                name: "Nationalize and Re-Tool GM Oshawa Plant",
                types: [DOCUMENT_FILE],
                link: "http://www.couragecoalition.ca/resolution-re-tool-oshawa-gm/",
              },
              {
                id: "e3f412fb-64a8-4596-947c-b2b4655fdad2",
                name: "Providing Income Support for Election Candidates",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/1tp57aJY9FSbCacZX6lCUr7kJhLC7NWbtgZFAxn2QJ_M/edit",
              },
              {
                id: "cee51421-c600-402d-84fb-dda8998ef151",
                name: "Strengthening Internal Democracy Through Enhanced Transparency",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/1Kokm-Rf54xf2wOX8cwXjo34q2M6_kgb_ipUSy1Dy9Ds/edit",
              },
              {
                id: "bd0113dc-5360-4e2e-9cdf-20df0ca0dbf2",
                name: "Land Back",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/1ClcAkHZlQdlwXxXW6xJuWMd0mPxtXn5vYCgTqfivqGo/edit",
              },
              {
                id: "65a9bdb9-f660-4c99-9803-a72a8d310601",
                name: "People First #COVIDzero",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/1QA4imyMkxWsB3UHSlg5kwSyQO7thQbZTZkJOGFas6jM/edit",
              },
            ]
          },
          {
            id: "0c97fea2-6046-4d10-8f12-d19eb2a4b78b",
            name: "Resolutions Drafted Outside of Courage",
            types: [DIRECTORY],
            children: [
              {
                id: "2d26ef26-41c0-4ebc-8741-0b5121c148c7",
                name: "NDP Socialist Caucus Resolutions",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/1NCys8h2HBP56LbwEdq5chOmLCTP37w4G/edit",
              }
            ]
          },
          {
            id: "c084eafa-51de-4fb8-bda1-e41c30896ba3",
            name: "Convention Training",
            types: [DIRECTORY],
            children: [
              {
                id: "12ebc549-21cd-4dd4-adcf-5ee08d51d7a2",
                name: "NDP Convention 101 Training - Brainstorming Doc",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/17KlnYhZ2I-p4NBonZYh-Of8SOy1vZFOH6H3hZjfmdrE/edit",
              },
            ]
          },
          {
            id: "3a5b4eec-6914-405f-a09b-ccf452bb8666",
            name: "Tracking Spreadsheet for All Resolutions",
            types: [SPREADSHEET_FILE],
            link: "https://docs.google.com/spreadsheets/d/1gcxaOjwDsbszTCkNPpzbO_k6UULlxd8nIV4v6ZjDnRA/edit?usp=sharing",
          },
          {
            id: "5aec33e5-6c09-4ed1-8241-bdc239caf7a8",
            name: "Peripheral Resources",
            types: [DIRECTORY],
            children: [
              {
                id: "6aaedc84-c9a3-4ae5-8dae-f0a7d5b35564",
                name: "Policy Books",
                types: [DIRECTORY],
                children: [
                  {
                    id: "1f1102ce-01de-461c-9a7e-1a0214529d92",
                    name: "NDP Convention 2021 - Resolution Guide",
                    types: [PDF_FILE, DOCUMENT_FILE],
                    link: "https://drive.google.com/file/d/1uhISXMwBRB-VRgRCDqEYxi0-g2ApMYOH/view?usp=sharing",
                  },
                  {
                    id: "e106a064-07ed-4bfc-883f-de6393755121",
                    name: "Federal NDP",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "f57c9771-1683-4188-a815-cae5e334c964",
                        name: "NDP - February 2018 Policy Book",
                        types: [PDF_FILE, DOCUMENT_FILE],
                        link: "https://drive.google.com/file/d/1AkAq_h6m5HGmJ4OGds9n-gcWUYVofxJv/view?usp=sharing",
                      }
                    ]
                  },
                  {
                    id: "96f21808-23b5-4ca7-984f-393d7f752027",
                    name: "Ontario NDP",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "a76a7610-ee2b-4a92-af96-5e12bf4afd60",
                        name: "ONDP Policy Book - 2019 Draft v1",
                        types: [PDF_FILE, DOCUMENT_FILE],
                        link: "https://drive.google.com/file/d/11hS1FLXhkHVplzNAOLg-2Xbr9QICOhvp/view?usp=sharing",
                      }
                    ]
                  }
                ]
              }
            ]
          },
        ]
      },
      {
        id: "0fa6706a-c3ff-4957-99c7-460e3aded50c",
        name: "General Resource Commons",
        types: [DIRECTORY],
        children: [
          {
            id: "167c69da-dc40-43b5-942b-479d4284d5aa",
            name: "Papers/Reports",
            types: [DIRECTORY],
            children: [
              {
                id: "2f89d072-74ab-4236-a976-50c14da8bdab",
                name: "Land Back: A Yellowhead Institute Red Paper",
                types: [DOCUMENT_FILE],
                link: "https://drive.google.com/file/d/1uhISXMwBRB-VRgRCDqEYxi0-g2ApMYOH/view?usp=sharing",
              },
              {
                id: "1384181a-b91b-4e39-b6fd-2d31906ff391",
                name: "[B'Tselem] - A Regime of Jewish Supremacy from the Jordan River to the Mediterranean Sea: This is Apartheid",
                types: [DIRECTORY],
                children: [
                  {
                    id: "459f557a-2e52-458a-8854-d46c6ed36d3d",
                    name: "Position Paper Overview",
                    types: [WEBSITE],
                    link: "https://www.btselem.org/apartheid",
                  },
                  {
                    id: "63a74f54-3aee-4f2b-a5f2-ba1213eb6a28",
                    name: "[B'Tselem] - A Regime of Jewish Supremacy from the Jordan River to the Mediterranean Sea: This is Apartheid",
                    types: [PDF_FILE, DOCUMENT_FILE],
                    link: "https://drive.google.com/file/d/1bWBZw6PL_YIqYP9OoiQQZ1Z4KdyTwqme/view?usp=sharing",
                  },
                  {
                    id: "a9b0e5c0-313e-49af-b7d0-473a2b0e3b7c",
                    name: "Website Version with Illustrations",
                    types: [WEBSITE],
                    link: "https://www.btselem.org/publications/fulltext/202101_this_is_apartheid"
                  },
                  {
                    id: "c43e3499-806b-4808-8bfb-b41b04bb9a66",
                    name: "Illustrated Explainer",
                    types: [WEBSITE],
                    link: "https://thisisapartheid.btselem.org/eng/#1",
                  }
                ]
              },
              {
                id: "2d8c93ce-82d8-40f8-9f46-6ef3592b39b2",
                name: "[Samara Centre for Democracy] - Party Favours: How Federal Election Candidates are Chosen",
                types: [PDF_FILE, DOCUMENT_FILE],
                link: "https://drive.google.com/file/d/1H9weF-ni_tdGXLQ40MBCmapicTv6rArn/view?usp=sharing",
              },
              {
                id: "aaff8693-187a-48cc-9c7b-8864d0ac67c8",
                name: "Reputation and Brand Management by Political Parties: Party Vetting of Election Candidates in Canada",
                types: [PDF_FILE, DOCUMENT_FILE],
                link: "https://drive.google.com/file/d/1K8ynLW7AUzpFqacRzR4kckoPYd71ZRUo/view?usp=sharing",
              },
            ]
          },
          {
            id: "6da28eb9-4ccb-4706-b339-6a9b800484ea",
            name: "Articles",
            types: [DIRECTORY],
            children: [
              {
                id: "6e99dedc-a3d2-49cf-9204-621418cc655b",
                name: "[Jacobin] - A Red Deal",
                types: [WEBSITE],
                link: "https://www.jacobinmag.com/2019/08/red-deal-green-new-deal-ecosocialism-decolonization-indigenous-resistance-environment",
              },
              {
                id: "72e219b0-7f0a-4141-a4bd-1006ea7379ac",
                name: "[Courage Coalition] - Amending Internal Democracy in the NDP: A Mobilization Proposal",
                types: [WEBSITE],
                link: "http://www.couragecoalition.ca/en-2017-amendment/",
              },
              {
                id: "0e0709c9-391e-4d71-bded-1613c428d6ff",
                name: "[Georgia Straight] - BC Green Party: We Need Diverse Green Voices in Power to Help Heal Society and Halt Our Planet's Breakdown",
                types: [WEBSITE],
                link: "https://www.straight.com/news/bc-green-party-we-need-diverse-green-voices-in-power-to-help-heal-society-and-halt-our-planets",
              },
            ]
          },
          {
            id: "d4c3bb76-437c-4d54-a82c-0e32864976e7",
            name: "Videos",
            types: [DIRECTORY],
            children: [
              {
                id: "c410461a-74dc-456f-8f30-e541ffe57134",
                name: "[York-Simcoe NDP] - NDP Convention Resolutions 101",
                types: [VIDEO_FILE],
                link: "https://www.facebook.com/watch/live/?v=887741185315657&ref=watch_permalink",
              }
            ]
          }
        ],
      },
    ]
  }
}
