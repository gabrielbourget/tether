import { DIRECTORY, DOCUMENT_FILE, PDF_FILE, WEBSITE } from "../../../constants";
import { WorkingGroup } from "../../../Types";


export const electoralCommittee: WorkingGroup = {
  id: "082aa2a5-b722-4f85-922c-449d748490cd",
  name: "Electoral Committee",
  description: "Working group for Courage that's looking into electoral engagement in a variety of contexts.",
  coChairs: [

  ],
  members: [

  ],
  socials: {
    id: "6eda811c-f01e-4954-8f86-280e4f3f708d",
    name: "Socials",
    types: [DIRECTORY],
    children: [

    ]
  },
  communications: {
    id: "f57b8ad3-4434-436c-aa11-13543b88d6bd",
    name: "Communications",
    children: [
      {
        id: "4e2c2280-47e1-45f3-bf63-4d244174c409",
        name: "Slack",
        link: "https://www.independentleft.slack.com",
        children: [
          {
            id: "a9686402-846c-4fb5-867a-84b04078157e",
            name: "Slack Channels",
            children: [
              {
                id: "4c547bc0-49f0-471f-9174-f84d73d2979b",
                name: "governance",
              }
            ]
          }
        ]
      }
    ]
  },
  resources: {
    id: "4f35f528-2ebf-4d39-ace2-ca0a0508c6dd",
    name: "Electoral Committee",
    types: [DIRECTORY],
    children: [
      {
        id: "869e94f0-0771-4020-86ff-87142f1eee90",
        name: "Meeting Minutes",
        types: [DIRECTORY],
        children: [
          {
            id: "c81fee8b-a15e-4290-b859-e63b399a4ddb",
            name: "2021",
            types: [DIRECTORY],
            children: [
              {
                id: "f6a17dc3-094f-4d0d-9fe9-9b6135d4a9d6",
                name: "January",
                types: [DIRECTORY],
                children: [
                  {
                    id: "4b1d3094-f57d-4169-8f98-f1b174daa47a",
                    name: "January 04",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "a5c53985-eb5d-4b24-bd68-f66d458e47fa",
                        name: "Courage National - Electoral Committee Meeting Minutes - January 04, 2020",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1rtq9iWIOorj3XeHmgK-Yys5GZk2xntCCBeeq53JpCXA/edit?usp=sharing",
                      },
                    ]
                  }
                ]
              }
            ],
          }
        ]
      },
      {
        id: "8fedb4f6-3996-46a5-861e-59ce1dfb1e85",
        name: "Building Out Criteria and Processes Around Engaging with Electoral Campaigns & Cycles",
        types: [DIRECTORY],
        children: [
          {
            id: "84f12256-1f44-4c36-b4de-bdd1789ebceb",
            name: "[Brainstorm Draft] - Courage's Approach to Engaging with Electoral Campaigns & Cycles",
            types: [DOCUMENT_FILE],
            link: "https://docs.google.com/document/d/1H_sHkDGd-6rntyBA4ASYoXWFKnRMP6zzc55F4TJJIyA/edit",
          }
        ]
      },
      {
        id: "44a46b4e-4bfe-4930-8b85-21056bff268d",
        name: "Proposal for Courage to Campiagn for Nahum Mann and Matthew Green",
        types: [DOCUMENT_FILE],
        link: "https://docs.google.com/document/d/1KKyPXdjzGcn_e4azjWdHpgXUp1WS4dNc7g6hdgb_mZM/edit",
      },
      {
        id: "0fa6706a-c3ff-4957-99c7-460e3aded50c",
        name: "General Resource Commons",
        types: [DIRECTORY],
        children: [
          {
            id: "ac8ee0e2-179a-48fd-8304-11900c335937",
            name: "Articles",
            types: [DIRECTORY],
            children: [
              {
                id: "93ffaf00-2d9b-44a4-b143-b3df7613ce06",
                name: "[Tempest] - Lessons From the Chicago Budget Battle",
                types: [WEBSITE],
                link: "https://www.tempestmag.org/2020/12/dont-repackage-crumbs-as-cake/",
              },
              {
                id: "94a304d1-3d94-4c97-b2b0-05282f4d6fe8",
                name: "[The Call] - Class-Struggle Politicians are Organizers First, Legislators Second",
                types: [WEBSITE],
                link: "https://socialistcall.com/2019/08/01/class-struggle-politicians-dsa-elections/",
              },
              {
                id: "a7bcab51-a837-4e65-bad8-5c96ce7da97f",
                name: "[The Call] - What are Class Struggle Elections?",
                types: [WEBSITE],
                link: "https://socialistcall.com/2019/07/16/what-are-class-struggle-elections/",
              },
            ]
          }
        ]
      },
    ]
  }
}
