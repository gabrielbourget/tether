import {
  DIRECTORY, DOCUMENT_FILE, WEBSITE, TOOL, JPG_FILE, IMAGE_FILE,
  PNG_FILE, 
} from "../../../constants";
import { WorkingGroup } from "../../../Types";


export const governanceWG: WorkingGroup = {
  id: "24b5a687-3fe8-499d-9dd4-895cb271df30",
  name: "Governance",
  description: "Working group which deliberates and organizes around overall questions of governance a the national level of Courage.",
  coChairs: [

  ],
  members: [

  ],
  socials: {
    id: "66eaeca5-01ef-4a2d-8f42-09aa41e63d43",
    name: "Socials",
    types: [DIRECTORY],
    children: [

    ]
  },
  communications: {
    id: "0c32f813-8ce7-4627-9dc9-bac2ddc3a0ab",
    name: "Communications",
    children: [
      {
        id: "d3a7b1c1-a3f7-4a6c-8998-383aec443b85",
        name: "Slack",
        link: "https://www.independentleft.slack.com",
        children: [
          {
            id: "7babfdb4-538d-41fc-925b-ce43072f66fa",
            name: "Slack Channels",
            children: [
              {
                id: "1314e673-7bc3-4a86-b61e-9ab7720f7a15",
                name: "governance",
              }
            ]
          }
        ]
      }
    ]
  },
  resources: {
    id: "11fb689b-9961-4e84-a922-4ed82d7e588c",
    name: "Resources",
    types: [DIRECTORY],
    children: [
      {
        id: "28e22f3f-65e2-472f-8905-28756e9e25ea",
        name: "Meeting Minutes",
        types: [DIRECTORY],
        children: [
          {
            id: "7b284d8b-a844-4101-9f1a-725d7bc15f68",
            name: "2020",
            types: [DIRECTORY],
            children: [
              {
                id: "115e14bf-2de7-4268-b268-49a6de55758d",
                name: "December",
                types: [DIRECTORY],
                children: [
                  {
                    id: "69e36099-88a7-4c28-a823-f92c5a35bf2c",
                    name: "December 22",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "9d2de20f-995b-43b7-84b2-2f6847c0bcbb",
                        name: "Courage - Governance WG Meeting Minutes - December 22, 2020",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1z8sluDjv2ljJwiTSSBm7nZaRJ_N7MJYUi89Vi0mZVCI/edit#heading=h.ec68vdkoc6wu",
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            id: "d37f98dc-6831-4983-8d37-9e1972a28ede",
            name: "2021",
            types: [DIRECTORY],
            children: [
              {
                id: "cdcc8347-d89d-4968-95f4-fceb83fcc08c",
                name: "January",
                types: [DIRECTORY],
                children: [
                  {
                    id: "a1d49bbf-ca1a-4787-83e4-74ba29467ac2",
                    name: "January 05",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "82e33599-eddb-45b8-b035-b748347e2a03",
                        name: "Courage National - Governance WG Meeting Minutes - January 05, 2021",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1z8sluDjv2ljJwiTSSBm7nZaRJ_N7MJYUi89Vi0mZVCI/edit#heading=h.ewvzgbqi7rve",
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        id: "f2960fed-6998-47ce-b6cc-0ef392fb5fb6",
        name: "Core Documents and Organizational Planning",
        types: [DIRECTORY],
        children: [
          {
            id: "97c6658a-fee1-42e0-bc42-b76848d9f9f8",
            name: "Commentary from Tyler Paziuk Regarding Different Approaches Being Discussed for Achieving Healthy Organizational Plurality",
            types: [DOCUMENT_FILE],
            link: "https://docs.google.com/document/d/1fjAe1-HjugtpMDFqajDfjibpSixT2GcWJ_lDy8aYrzk/edit?ts=5fe3ac6c",
          },
          {
            id: "7e5f5e1f-e158-49e3-bdd3-b4b0cb1713b0",
            name: "Research Materials",
            types: [DIRECTORY],
            children:[
              {
                id: "b373a2f7-cd69-440d-b901-6a5118d46dce",
                name: "Wikipedia Articles",
                types: [DIRECTORY],
                children: [
                  {
                    id: "7ed29b32-570c-4fa4-a027-3f55988a1c10",
                    name: "[Wikipedia] - Workplace Democracy",
                    types: [WEBSITE],
                    link: "https://en.wikipedia.org/wiki/Workplace_democracy",
                  }
                ]
              },
              {
                id: "57303390-df41-40eb-b216-30a1ff48ee14",
                name: "Articles",
                types: [DIRECTORY],
                children: [
                  {
                    id: "f963869d-2724-4abe-af53-b01c5709f2de",
                    name: "[Black Socialists of America] - Compiled Thread on Social Systems Change",
                    types: [WEBSITE],
                    link: "https://threadreaderapp.com/thread/1342918769186902018.html",
                  },
                  {
                    id: "c7fc8a68-bf7f-4c3f-83f3-042a24e3dc2d",
                    name: "[Regeneration] - Mutual Aid: A Factor of Liberalism",
                    types: [WEBSITE],
                    link: "https://regenerationmag.org/mutual-aid-a-factor-of-liberalism/",
                  },
                  {
                    id: "aaa32250-a7e4-4767-b7f2-45bbb76b4e33",
                    name: "[Jacobin] - Freedom from the Boss",
                    types: [WEBSITE],
                    link: "https://www.jacobinmag.com/2018/01/freedom-from-the-boss",
                  },
                ]
              },
              {
                id: "9c3d8437-723f-47f8-b694-ca5e8f0d6c76",
                name: "Tools",
                types: [DIRECTORY],
                children: [
                  {
                    id: "14eec934-0670-49b0-892d-a95cff153a56",
                    name: "[Community Rule] - Group Dynamics and Management Strategies",
                    types: [TOOL],
                    link: "https://communityrule.info/templates/?s=09",
                  },
                  {
                    id: "e0c3891c-ecc0-46b2-88cc-3198ffc1ad87",
                    name: "The Decider App",
                    types: [TOOL],
                    link: "https://thedecider.app/",
                  }
                ]
              },
              {
                id: "e30e74b5-733f-4fc3-97c9-51a650a3ab9c",
                name: "DSA - Organizational Structure",
                types: [JPG_FILE, IMAGE_FILE],
                link: "https://www.dsausa.org/files/2020/07/National-DSA-Organization-Chart.jpg",
              },
              {
                id: "de6450f1-ac3a-407a-b420-60191e3d0273",
                name: "Organizational Structure Sketch from Mitch",
                types: [IMAGE_FILE, PNG_FILE],
                link: "https://drive.google.com/file/d/1D5DY7PVKJisKzZey085--tbLyS_yuE55/view?usp=sharing",
              },
            ],
          }
        ]
      }
    ]
  }
}