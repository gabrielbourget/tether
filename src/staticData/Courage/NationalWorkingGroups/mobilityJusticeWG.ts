import { DIRECTORY, DOCUMENT_FILE, PDF_FILE, VIDEO_FILE, WEBSITE } from "../../../constants";
import { WorkingGroup } from "../../../Types";


export const mobilityJusticeWG: WorkingGroup = {
  id: "37875301-dcc7-4011-94d3-e015b038fcc3",
  name: "Mobility Justice",
  description: "National-level working group discussing and looking into options to achieve mobility justice throughout Canada.",
  coChairs: [

  ],
  members: [

  ],
  socials: {
    id: "913ec127-f90c-4397-9b29-7484a6dcf477",
    name: "Socials",
    types: [DIRECTORY],
    children: [

    ]
  },
  communications: {
    id: "649d3eae-a70a-44ff-a697-5edfd08a815f",
    name: "Communications",
    children: [
      {
        id: "dce89d3e-5c25-440c-a94d-c73ba5fdff64",
        name: "Slack",
        link: "https://www.independentleft.slack.com",
        children: [
          {
            id: "cdc21bb8-fe30-4c45-a453-1a90b30b1705",
            name: "Slack Channels",
            children: [
              {
                id: "d47f68e8-11d6-43e5-8b96-0947a804b2e2",
                name: "governance",
              }
            ]
          }
        ]
      }
    ]
  },
  resources: {
    id: "46318e1e-1ef1-4a3c-b824-fd2df278403f",
    name: "Mobility Justice",
    types: [DIRECTORY],
    children: [
      {
        id: "8eba6144-6b44-47d1-9c6f-f8ca2b17314d",
        name: "Meeting Minutes",
        types: [DIRECTORY],
        children: [
          {
            id: "99a75bb1-ac87-4222-aa0f-28b78ee83b86",
            name: "2021",
            types: [DIRECTORY],
            children: [],
          }
        ]
      },
      {
        id: "324fd72d-8110-427f-88d1-fcdcd071e011",
        name: "Proposals/Plans/Statements",
        types: [DIRECTORY],
        children: [
          {
            id: "e7fd7bfc-bbbf-4bdb-b9be-bca6ef85b610",
            name: "Door-to-Door, Coast-to-Coast: A Strategy for Comprehensive, Zero-Emission Collective Transportation",
            types: [DOCUMENT_FILE],
            link: "https://docs.google.com/document/d/1gj8Nhvfw5dvxRfuo-DgEcvosMxzE2dNUrai8x6FyNCI/edit",
          },
          {
            id: "2441fcb1-1721-4175-8acc-e0f0df735fd6",
            name: "How We Move Is How We Survive: A Statement from Individuals and Groups for Free and Just Transit",
            types: [DOCUMENT_FILE],
            link: "https://drive.google.com/file/d/1615V8lOm-V0KHEsxovLa7gzJTyOD7gR-/view?usp=sharing",
          },
        ]
      },
      {
        id: "0fa6706a-c3ff-4957-99c7-460e3aded50c",
        name: "General Resource Commons",
        types: [DIRECTORY],
        children: [
          {
            id: "c317b253-f62b-4c13-95f9-dbbc1192e858",
            name: "Videos",
            types: [DIRECTORY],
            children: [
              {
                id: "3c004298-311e-4bab-a688-03ab60a9d8d4",
                name: "[Courage Ottawa Event] - Defending Public Transit Through the Pandemic and Beyond",
                types: [VIDEO_FILE],
                link: "https://www.youtube.com/watch?v=16njDooYroQ&feature=youtu.be",
              },    
            ]
          },
          {
            id: "ac8ee0e2-179a-48fd-8304-11900c335937",
            name: "Articles",
            types: [DIRECTORY],
            children: [
              {
                id: "a121cdae-e795-40c3-91b1-fa0ff4677dff",
                name: "[Toronto Star] - Canada's Big City Transit Systems Say Service Will be (Mostly) Back to Normal in 2021 - Even if Ridership Isn't",
                types: [WEBSITE],
                link: "https://www.thestar.com/news/gta/2021/01/03/canadas-big-city-transit-systems-say-service-will-be-mostly-back-to-normal-in-2021-even-if-ridership-isnt.html",
              },
            ]
          }
        ]
      },
    ]
  }
}
