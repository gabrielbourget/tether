import {
  FACEBOOK, TWITTER, SLACK, WEBSITE, DOCUMENT_FILE, SPREADSHEET_FILE,
  DIRECTORY, EMAIL, COMMS_CHANNEL, COURAGE, IMAGE_FILE, GOOGLE_DRIVE,
  PDF_FILE, JPG_FILE, SVG_FILE, CODE_REPOSITORY, FORM, TOOL, PNG_FILE
} from '../../constants';

import { PoliticalOrganization } from '../../Types';

import {
  courageVictoriaData, courageVancouverData, courageCalgaryData,
  courageSaskatoonData, courageReginaData, courageWinnepegData,
  courageTorontoData, courageOttawaData, courageMontrealData,
  courageHalifaxData
} from "./Chapters";
import { governanceWG, electoralCommittee, NDP_WG, mobilityJusticeWG } from "./NationalWorkingGroups";

export const courageData: PoliticalOrganization = {
  id: 'e2f399e0-51f4-488d-9392-289ec5628b88',
  name: COURAGE,
  description: "A pan-Canadian, membership-based organization that attempts to bridge the divide between movement and electoral politics. A space for learning, teaching, and collaboration; for planning and engaging in collective political action.",
  avatarURL: 'https://pbs.twimg.com/profile_images/846060121121832963/ypt0EFSR_400x400.jpg',
  socials: {
    id: 'af974c6e-efe7-4dad-aa58-3f3a76d038df',
    name: 'Socials',
    types: [DIRECTORY],
    children: [
      {
        id: '381524c9-6ae0-4fb7-a448-5b965360108f',
        name: FACEBOOK,
        link: 'https://www.facebook.com/couragecoalition/',
      },
      {
        id: '780af0c9-d9b7-4c02-8f69-e0e410480d92',
        name: TWITTER,
        link: 'https://twitter.com/Courage_CA',
      },
      {
        id: 'bc598dce-5441-4d3e-92c7-54ab7b425057',
        name: WEBSITE,
        link: 'http://www.couragecoalition.ca',
      }
    ],
  },
  communications: {
    id: '2c39143c-b96f-42bc-a981-4de28ddcc9e0',
    name: 'Communications',
    types: [DIRECTORY],
    children: [
      {
        id: '6927e541-a491-4382-bed8-f6740d305331',
        name: SLACK,
        link: 'https://www.independentleft.slack.com',
        types: [DIRECTORY],
        children: [
          {
            id: '7eff70b4-c0e2-4044-9cd7-9edff8bb31c4',
            name: 'Slack Channels',
            types: [DIRECTORY],
            children: [
              {
                id: '7349846d-ccb4-46f1-928e-8919a3a48144',
                name : '#admin',
                types: [COMMS_CHANNEL]
              },
              {
                id: '85287fef-34e5-4938-81b8-ae1ba0ab2de9',
                name : '#alberta',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'f8a3165b-cb90-49bf-94ed-a2d19072cdb1',
                name : '#anti-imperialism',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'f9817e6f-7e92-43b2-98f6-9b1a21586ecc',
                name : '#antio-mandate',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'f9817e6f-7e92-43b2-98f6-9b1a21586ecc',
                name : '#basisofunity',
                types: [COMMS_CHANNEL]
              },
              {
                id: '30f17a97-22f5-4f81-9d62-de5c654cb857',
                name : '#branches',
                types: [COMMS_CHANNEL]
              },
              {
                id: '319ba672-5fd7-4bed-b2d9-c11f228b243b',
                name : '#campus',
                types: [COMMS_CHANNEL]
              },
              {
                id: '2d4e0dae-6aac-4827-81bf-1bfda218201c',
                name : '#citizens-assemblies',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'cf3e93ec-08b1-46e9-9298-710f2693e7c7',
                name : '#climate-energy',
                types: [COMMS_CHANNEL]
              },
              {
                id: '79573263-fd16-499a-be5c-ecb08f9fe91b',
                name : '#content',
                types: [COMMS_CHANNEL]
              },
              {
                id: '73ee9681-cd34-47fd-b0c7-4a7eedf708a5',
                name : '#couragepodcastmtl',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'e94ff664-c0b4-435e-beba-7978b1a54ef2',
                name : '#covid-19',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'a4ba2a97-9aa3-4847-a448-7bb8944bffb9',
                name : '#data-viz',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'b735e414-3643-4e25-b980-ef5ba7706891',
                name : '#dominoes',
                types: [COMMS_CHANNEL]
              },
              {
                id: '2c98fa8e-2922-4850-9a33-09038d669d12',
                name : '#electoralcommittee',
                types: [COMMS_CHANNEL]
              },
              {
                id: '04c68956-8c7b-44df-bf62-1ebb0069335e',
                name : 'enfrancaissvp',
                types: [COMMS_CHANNEL]
              },
              {
                id: '11b714a8-f74d-4e2f-8e1d-055bd6daf138',
                name : 'general',
                types: [COMMS_CHANNEL]
              },
              {
                id: '5f3395a4-2096-4d3a-aa83-69002c9d6d79',
                name : 'gm-oshawa',
                types: [COMMS_CHANNEL]
              },
              {
                id: '0de3716d-afb3-4233-92a5-d7ea42017c3d',
                name : '#governance',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'd566381c-ecfe-4703-b197-182995874408',
                name : '#greennewdeal',
                types: [COMMS_CHANNEL]
              },
              {
                id: '258e437d-6528-40ff-8655-0e07a049fd24',
                name : '#halifax',
                types: [COMMS_CHANNEL]
              },
              {
                id: '1b38fefd-300b-4617-8d1b-24ebe810728b',
                name : '#indigenous-issues',
                types: [COMMS_CHANNEL]
              },
              {
                id: '65b928da-d4bb-4699-9f48-8bb07763bec2',
                name : '#lgbtq2siaa',
                types: [COMMS_CHANNEL]
              },
              {
                id: '832748eb-9a4b-4182-b58e-59a5bcaa579c',
                name : '#manitoba',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'd6488a9b-e4ee-4090-a758-f671bcc5979d',
                name : '#memes-shitposting',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'abfd62b1-ac7b-454a-baae-cc52736d4f88',
                name : '#montreal',
                types: [COMMS_CHANNEL]
              },
              {
                id: '1ad4d788-5ae1-4892-8504-0c957ca6018a',
                name : '#mtlelectioncommittee',
                types: [COMMS_CHANNEL]
              },
              {
                id: '15648ede-e88c-4abf-81b9-29b015f9792f',
                name : '#mtllongterm',
                types: [COMMS_CHANNEL]
              },
              {
                id: '3edc3fdc-d8e1-44b8-9fc6-39698e0d6194',
                name : '#music',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'e6c6d4ce-a0c1-49d6-87f7-baa3e496fe5d',
                name : '#ndp',
                types: [COMMS_CHANNEL]
              },
              {
                id: '4845c931-a479-4a20-a9f2-43629667799f',
                name : '#ndp-ridings',
                types: [COMMS_CHANNEL]
              },
              {
                id: '25befb83-6952-4836-913f-3d579682f63b',
                name : '#newmembers',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'a135f8df-37eb-4db4-b37a-91121bdc48b2',
                name : '#newsandinfo',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'ea2bf2e2-bc42-474a-b78f-473be654084b',
                name : '#nondudes',
                types: [COMMS_CHANNEL]
              },
              {
                id: '398cf6f6-4006-44f7-b646-1dd60bd23a13',
                name : '#ondp',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'd7068aeb-078e-47aa-84eb-a3b6a22fdc37',
                name : 'ott_crisis_response',
                types: [COMMS_CHANNEL]
              },
              {
                id: '21e6708a-c55c-470c-84b5-a7efdccb4ee1',
                name : '#ott_elect_socialists',
                types: [COMMS_CHANNEL]
              },
              {
                id: '1f1c8a07-b82b-4c26-9408-743456002942',
                name : '#ott_environment_organizing',
                types: [COMMS_CHANNEL]
              },
              {
                id: '69faaaf5-6142-4fde-8d4a-870fcb0034d7',
                name : '#ott_housing',
                types: [COMMS_CHANNEL]
              },
              {
                id: '7ac49cfd-39f4-4d40-9547-af75f6177930',
                name : '#ott_intsolidarity',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'b2d96442-2c52-42c4-a6fc-20f22b0fa4a7',
                name : '#ott_member_engagement',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'd81e8b02-1cab-48fd-9eb9-33a0e01d1486',
                name : '#ott_outreach',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'fdcb7867-3c7b-4695-ba18-187953bce0df',
                name : '#ott_reading_group',
                types: [COMMS_CHANNEL]
              },
              {
                id: '3f84e4ae-bc4a-4a55-8a96-69cd7b844283',
                name : '#ott_transit',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'ae0f2b32-adef-4420-ac79-84bc177b278e',
                name : '#ottawa',
                types: [COMMS_CHANNEL]
              },
              {
                id: '7222afa5-acf3-4048-8b85-ea5b6ef28feb',
                name : '#racesolidarity',
                types: [COMMS_CHANNEL]
              },
              {
                id: '73bfae3d-def3-4582-a31f-565dfaa7c4e4',
                name : '#reading',
                types: [COMMS_CHANNEL]
              },
              {
                id: '8a8e8dcd-5f48-47e3-b96c-9d4c59f34623',
                name : '#recruitmentmtl',
                types: [COMMS_CHANNEL]
              },
              {
                id: '8c6ae232-2334-490e-acb8-f906dbd14932',
                name : '#reports',
                types: [COMMS_CHANNEL]
              },
              {
                id: '073bd681-c204-4913-848d-572d6e4ba681',
                name : '#saskabush',
                types: [COMMS_CHANNEL]
              },
              {
                id: '25ab8e25-909a-4cb1-9b33-455d1d340a72',
                name : '#socialmedia',
                types: [COMMS_CHANNEL]
              },
              {
                id: '3dceb2d7-af2b-449a-a19b-649cdb008142',
                name : '#structureinternaldemocracy',
                types: [COMMS_CHANNEL]
              },
              {
                id: '7559cb11-ea37-42d3-bbd8-6680b1a239a6',
                name : '#survivorsupport',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'c2cd3c55-6011-4aca-91e2-ecab5045ee57',
                name : '#training',
                types: [COMMS_CHANNEL]
              },
              {
                id: '70cedee1-8c6e-4639-80d8-a0f3c7be206b',
                name : '#unite-the-left',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'd449a998-daf7-4f8e-b796-623b3c5a5c38',
                name : '#web',
                types: [COMMS_CHANNEL]
              },
              {
                id: '1fb62bd8-85e0-405e-9820-45209e916a80',
                name : '#westcoast',
                types: [COMMS_CHANNEL]
              },
              {
                id: '6bd910f6-c716-4a2d-aa6c-662cd6935bbf',
                name : '#wethenorth',
                types: [COMMS_CHANNEL]
              },
              {
                id: '19ad19ab-e65a-4487-a477-ee6e6e090c87',
                name : '#yyz',
                types: [COMMS_CHANNEL]
              },
              {
                id: '47627a14-5107-4f8b-b522-044ab4c5a424',
                name : '#yyz-mutual-aid',
                types: [COMMS_CHANNEL]
              },
            ]
          }
        ]
      },
      {
        id: '091d403b-5680-4208-b014-9814f22803d3',
        name: EMAIL,
        link: 'mailto:ottawa@couragecoalition.com',
        value: 'ottawa@couragecoalition.ca',
      },    
    ],
  },
  resources: {
    id: 'bde260a2-d5a7-43d3-94dd-ebb29bd6d5a1',
    name: 'Resources',
    types: [DIRECTORY],
    children: [
      {
        id: 'b491571a-249d-40b5-ad63-ecda21092dc5',
        name: 'Basis of Unity',
        types: [DOCUMENT_FILE],
        description: "Presents the organization's core values, which are agreed upon, upheld, and supported.",
        link: 'http://www.couragecoalition.ca/en-unity/',
      },
      {
        id: '522c277f-8279-44ac-b634-6b1ffd85ba3a',
        name: 'Frequently Asked Questions',
        shorthand: 'FAQ',
        types: [DOCUMENT_FILE],
        link: 'http://www.couragecoalition.ca/en-faq/',
      },
      {
        id: '4f1c4eb5-2fa5-4c2b-bccc-cbf2628fac4e',
        name: 'About',
        types: [DOCUMENT_FILE],
        link: 'http://www.couragecoalition.ca/about/',
      },
      {
        id: "2389d6ea-bcfd-4516-889d-0b36bcb9f33b",
        name: "Meeting Minutes",
        types: [DIRECTORY],
        children: [
          {
            id: "8a288aeb-2ad8-491a-9ddd-64068e6980fa",
            name: "2021",
            types: [DIRECTORY],
            children: [
              {
                id: "226e6a05-b122-462b-94ec-08bd69b12d8a",
                name: "January",
                types: [DIRECTORY],
                children: [
                  {
                    id: "21beaff6-8cfe-45eb-a950-9679e5a22af5",
                    name: "January 21",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "f68d69e4-4f90-4ce0-af09-4757d091c09c",
                        name: "Courage National - Monthly General Assembly - January 21, 2021 - Agenda",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1Fk0j5GW9ah1_wcBKW0W-vZHUEbg7m8d778XoZuEy2rU/edit",
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        id: 'e6f89e1f-6949-405a-acd1-1275b92ce9c4',
        name: 'Design Resources',
        types: [DIRECTORY],
        link: 'https://drive.google.com/drive/folders/1X4EXArTAJuM67mcdGbNKegT7Gi8eode4?usp=sharing',
        children: [
          {
            id: '3451f1ce-756d-4b51-b541-81ee0972ca90',
            name: 'Design Resources Folder',
            types: [GOOGLE_DRIVE],
            link: 'https://drive.google.com/drive/folders/1X4EXArTAJuM67mcdGbNKegT7Gi8eode4?usp=sharing'
          },
          {
            id: '076792bf-57e6-4ffd-8506-b899ee55438b',
            name: 'Courage Color Palette',
            types: [IMAGE_FILE],
            link: 'https://drive.google.com/file/d/1qHrhxVfHlFssHFbu0uuX08gT4YeaefNt/view?usp=sharing'
          },
          {
            id: 'f7ec1d88-b667-427a-96eb-898d152f5037',
            name: 'Fonts',
            types: [DIRECTORY],
            children: [
              {
                id: 'a959548f-2b08-4856-8610-8914fb61cd47',
                name: 'HK Grotesk',
                types: [GOOGLE_DRIVE],
                link: 'https://drive.google.com/drive/folders/1yW5FCd94r-NhWEpNK4a2kODPk0ndXmcx?usp=sharing',
              },
              {
                id: '7f4d423d-c233-4da0-b255-5afa3fe2f785',
                name: 'Overpass',
                types: [GOOGLE_DRIVE],
                link: 'https://drive.google.com/drive/folders/17IQ3xwPI_Z3VXNGEZ6awOG8eUjuyIzfJ?usp=sharing',
              }
            ]
          },
          {
            id: '2f40f28e-1f9e-4674-a4d3-61b0d5529a4a',
            name: 'Images',
            types: [DIRECTORY],
            children: [
              {
                id: '90a8a46e-9ef7-4eed-a3e9-baad84c9c41d',
                name: 'Banner Logo',
                types: [IMAGE_FILE, JPG_FILE],
                link: 'https://drive.google.com/file/d/1QF28B8l4EHyfOcXU7W_RAJkwqJnIOzaP/view?usp=sharing',
              },
              {
                id: '5c462197-b711-4ccd-bc34-4f13375d54cc',
                name: 'photo-treatment_01',
                types: [IMAGE_FILE, JPG_FILE],
                link: 'https://drive.google.com/file/d/1HgHa7ct3xlNKbkuI060vdZtS2btIGLKg/view?usp=sharing'
              },
              {
                id: '0d4951db-c2af-4232-808f-b2cb2084e436',
                name: 'photo-treatment_02',
                types: [IMAGE_FILE, JPG_FILE],
                link: 'https://drive.google.com/file/d/1MAao6C-tacTN5UrXf50cRnnnIn1yARnO/view?usp=sharing',
              },
              {
                id: 'c2bc008b-36e8-46ec-a335-c76dd9a78b5c',
                name: 'Courage Logo w/ Tagline',
                types: [IMAGE_FILE, SVG_FILE],
                link: 'https://drive.google.com/file/d/1H_jG5OIoYXXylq_-WPSKz9XVdeie117r/view?usp=sharing',
              },
              {
                id: '3afde350-530d-4d2e-b4d0-efeed7810ae3',
                name: 'Courage Logo Purple/Orange',
                types: [IMAGE_FILE, SVG_FILE],
                link: 'https://drive.google.com/file/d/1nzyHTu6_qlea7oHfd6fDV3ipyA-8FNlU/view?usp=sharing'
              },
              {
                id: '9605ed0a-9c22-4f2a-8bd2-0d05d60b65cd',
                name: 'Courage Logo White/Orange',
                types: [IMAGE_FILE, SVG_FILE],
                link: 'https://drive.google.com/file/d/1J0v8ORrixOYv5gtlshSfRlVtiZ2b8qXx/view?usp=sharing'
              }
            ]
          },
          {
            id: 'ea460b91-fda6-423f-bcdd-7ab846297086',
            name: 'Courage Colors',
            types: [PDF_FILE, DOCUMENT_FILE],
            link: 'https://drive.google.com/file/d/1pCZyk3tOlld60uJ9SiVWUVM82FcjUdBN/view?usp=sharing'
          }
        ]
      },
      {
        id: '20e00f8a-7386-4e55-9f5f-f2cd2ca44aaf',
        name: 'Tether - Resources',
        types: [DIRECTORY],
        children: [
          {
            id: '02ecd2f5-a279-4fe5-8121-87f25e507cf7',
            name: 'Tether - Resources Directory',
            types: [GOOGLE_DRIVE],
            link: 'https://drive.google.com/drive/folders/1xNpbjRedoSUnxumNIaZG2oFUdrwniSrh?usp=sharing'
          },
          {
            id: 'c20e8813-1d0f-40f9-a61f-544ccf194b8c',
            name: 'Tether - Web Front End Codebase',
            types: [CODE_REPOSITORY],
            link: 'https://gitlab.com/gabrielbourget/tether'
          },
          {
            id: 'a5a719d0-5698-4fb5-b903-bb05caacb81a',
            name: 'Tether - Back End Codebase',
            types: [CODE_REPOSITORY],
            link: 'https://gitlab.com/gabrielbourget/tether_core_backend'
          },
          {
            id: '516b478a-adf8-414d-8bc4-bf12313af436',
            name: 'Resource Addition Request Formatting',
            types: [DOCUMENT_FILE],
            link: 'https://docs.google.com/document/d/1E7fhD1JIDd2SgAHZnzhkZeeY-caW59rzWyVaWaw3csA/edit',
          }
        ]
      }
    ],
  },
  workingGroups: [
    { ...governanceWG },
    { ...electoralCommittee },
    { ...NDP_WG },
    { ...mobilityJusticeWG },
  ],
  chapters: [
    { ...courageVictoriaData },
    { ...courageVancouverData },
    { ...courageCalgaryData },
    { ...courageSaskatoonData },
    { ...courageReginaData },
    { ...courageWinnepegData },
    { ...courageTorontoData },
    { ...courageOttawaData },
    { ...courageMontrealData },
    { ...courageHalifaxData },
  ], // - Chapters
};
