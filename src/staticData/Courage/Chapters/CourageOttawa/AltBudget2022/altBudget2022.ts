import { Resource } from "../../../../../Types";
import { DIRECTORY, WEBSITE } from "../../../../../constants";

export const altBudget2022: Resource = {
  id: "4fe59b1a-e29a-42e6-95a3-e51b0bd88597",
  name: "Alt Budget 2022",
  types: [DIRECTORY],
  children: [
    {
      id: "8d78b823-2b31-4554-9d6e-df66729c9641",
      name: "UK National Food Service",
      types: [WEBSITE],
      link: "https://www.nationalfoodservice.uk/",
    },
  ]
}