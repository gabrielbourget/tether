import { DIRECTORY } from '../../../../../constants';

import { WorkingGroup } from "../../../../../Types/courageTypes";

export const memberEngagementData: WorkingGroup = {
  id: 'd7b1b408-82f3-4d42-bdec-dfa7cebfbdc2',
  name: 'Member Engagement',
  coChairs: [

  ],
  members: [

  ],
  socials: {
    id: '514e2b25-4d58-4b7e-8c3d-cecaf511c1aa',
    name: 'Socials',
    types: [DIRECTORY],
    children: [

    ]
  },
  communications: {
    id: 'b9076346-cb53-48cd-b5ae-f7865aabe41a',
    name: 'Communications',
    children: [
      {
        id: '2688ee0a-00e3-42e7-8c3b-e42c0f5d1ccf',
        name: 'Slack',
        link: 'https://www.independentleft.slack.com',
        children: [
          {
            id: 'ff7d3e90-7752-4f76-9bc4-9cd0e7a9a5eb',
            name: 'Slack Channels',
            children: [
              {
                id: 'cca8e665-1a52-4b0e-89fa-da1e8773b372',
                name: '#ott_member_engagement',
              }
            ]
          }
        ],
      }
    ],
  },
  resources: {
    id: 'f6c23599-a008-4482-bb81-806539b50109',
    name: 'Resources',
    types: [DIRECTORY],
    children: [

    ]
  }  
};
