import { DOCUMENT_FILE, DIRECTORY, PDF_FILE } from '../../../../../constants';

import { WorkingGroup } from "../../../../../Types/courageTypes";

export const electingSocialistsData: WorkingGroup = {
  id: 'bbd8140c-a65d-4ddb-9200-bb06b99eefb3',
  name: 'Electing Socialists',
  description: '',
  coChairs: [
    { id: '31df33c0-c62c-4813-a1c2-e2993aae6536', name: 'James Hutt' },
    { id: 'b20de0d8-12f7-4b0b-b05e-0b3e1f625bac', name: 'Julia Szwarc' },
  ],
  members: [
    { id: '31df33c0-c62c-4813-a1c2-e2993aae6536', name: 'James Hutt' },
    { id: 'b20de0d8-12f7-4b0b-b05e-0b3e1f625bac', name: 'Julia Szwarc' },
    { id: '0e8c4dd5-a36f-4ea8-92d6-31fadaf314de', name: 'Matt Lipton' },
    { id: '154f29aa-1870-4119-a018-0de3edf2b87b', name: 'Clem' },
    { id: '26d1406a-e157-4bc1-a011-f27c4c2a2dc3', name: 'Meagan Wiper' },
    { id: '94c0fab7-be85-40ca-9391-a0afe5cd0865', name: 'Ethan Mitchell' },
  ],
  socials: {
    id: '9c333d00-7937-4fca-a2d8-69d6e54feb48',
    name: 'Socials',
  },
  communications: {
    id: '08ebbce3-3b20-42bd-86e9-ae988f698bdb',
    name: 'Communications',
    children: [
      {
        id: '76a20383-e9ea-46c5-a08f-f8a62465ba9a',
        name: 'Slack',
        link: 'https://www.independentleft.slack.com',
        children: [
          {
            id: 'ff7d3e90-7752-4f76-9bc4-9cd0e7a9a5eb',
            name: 'Slack Channels',
            children: [
              {
                id: 'cca8e665-1a52-4b0e-89fa-da1e8773b372',
                name: '#ott_elect_socialists'
              }
            ]
          }
        ],
      }
    ],
  },
  resources: {
    id: '6d92aef4-148d-4a3e-a1d4-8aeb5cc77f17',
    name: 'Resources',
    types: [DIRECTORY],
    children: [
      {
        id: '43f4b427-a19e-40d6-ae47-9cb8727462dc',
        name: 'The NDP: A Marxist Analysis (1997)',
        types: [DOCUMENT_FILE, PDF_FILE],
        link: 'https://drive.google.com/file/d/1iCeMCgpl0kNHjKf7NZwgkLPckEwqu2Zl/view',
      }
    ]
  },
};
