import { SPREADSHEET_FILE, DIRECTORY } from '../../../../../constants';

import { WorkingGroup } from "../../../../../Types/courageTypes";

export const internationalSolidarityData: WorkingGroup = {
  id: 'f3971cc5-f0e2-4b43-b80d-d08893e1dea6',
  name: 'International Solidarity',
  coChairs: [
    { id: '44ffdba4-9885-4c71-8078-174797126512', name: 'Shivangi Misra' },
  ],
  members: [
    { id: '44ffdba4-9885-4c71-8078-174797126512', name: 'Shivangi Misra' },
    { id: 'c6a1f621-882e-4304-8bb7-273b986184b9', name: 'David K' },
    { id: 'ec2cf87e-0a03-4efd-b921-9161cde9fe67', name: 'Tyler Paziuk' },
    { id: 'acb74f0e-cf61-40d9-9d7f-4ae59b5f9917', name: 'Debbie Owusu-Akyeeah' },
    { id: 'a9d4b6ef-6a7d-4b17-aa87-d4de5d8379d1', name: 'Adrian Murray' },
    { id: '26d1406a-e157-4bc1-a011-f27c4c2a2dc3', name: 'Meagan Wiper' },
  ],
  communications: {
    id: 'cc878bfd-25ea-42ad-917d-4f5aeffa12ce',
    name: 'Communications',
    children: [
      {
        id: '76a20383-e9ea-46c5-a08f-f8a62465ba9a',
        name: 'Slack',
        link: 'https://www.independentleft.slack.com',
        children: [
          {
            id: 'ff7d3e90-7752-4f76-9bc4-9cd0e7a9a5eb',
            name: 'Slack Channels',
            children: [
              {
                id: 'cca8e665-1a52-4b0e-89fa-da1e8773b372',
                name: '#ott_intsolidarity',
              }
            ]
          }
        ],
      }
    ],
  },
  socials: {
    id: 'ecb258e5-36d8-42c9-8dda-2ac12536388f',
    name: 'Socials',
  },
  resources: {
    id: 'e4aa4514-6a2f-4a2c-8e34-8dad83e4f261',
    name: 'Resources',
    types: [DIRECTORY],
    children: [
      {
        id: 'c51a515d-e8fc-4da5-a625-b88ad3f3e1af',
        name: 'International Solidarity Contacts',
        types: [SPREADSHEET_FILE],
        description: 'List containing individuals and organizations to stay in contact with in the context of international solidarity.',
        link: 'https://docs.google.com/spreadsheets/d/11N9sFQOqHuiSNieTlOmPDT9Nnd-9vjx-SX_qfcF5gOk/',
      }
    ]
  },          
};
