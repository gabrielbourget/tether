import {
  WEBSITE, DOCUMENT_FILE, DIRECTORY, GOOGLE_DRIVE, PDF_FILE, WORD_DOCUMENT_FILE,
} from '../../../../../constants';

import { WorkingGroup } from "../../../../../Types/courageTypes";

export const readingGroupData: WorkingGroup = {
  id: 'a73c7fb5-f6d0-4cc1-a5e0-2d15501b423c',
  name: 'Reading Group',
  coChairs: [
    // { id: '', name: '' },
  ],
  members: [
    // { id: '', name: '' },
  ],
  socials: {
    id: 'ea28b4b7-dc8a-49da-af61-207679582109',
    name: 'Socials',
    children: [

    ]
  },
  communications: {
    id: 'b9076346-cb53-48cd-b5ae-f7865aabe41a',
    name: 'Communications',
    children: [
      {
        id: '2688ee0a-00e3-42e7-8c3b-e42c0f5d1ccf',
        name: 'Slack',
        link: 'https://www.independentleft.slack.com',
        children: [
          {
            id: 'ff7d3e90-7752-4f76-9bc4-9cd0e7a9a5eb',
            name: 'Slack Channels',
            children: [
              {
                id: 'cca8e665-1a52-4b0e-89fa-da1e8773b372',
                name: '#ott_reading_group',
              }
            ]
          }
        ],
      }
    ],
  },
  resources: {
    id: 'a022913b-cd43-4cfa-9a13-8cf5ee070082',
    name: 'Resources',
    types: [DIRECTORY],
    children: [
      {
        id: '17659fc9-4432-4e65-8739-ceae685da4b5',
        name: 'Readings',
        types: [DIRECTORY],
        children: [
          {
            id: '4b213a7e-927c-45f4-a856-986779e8679d',
            name: 'Books',
            types: [DIRECTORY],
            children: [
              {
                id: '414856bc-7b33-4c61-8281-adf3e4dd1c0f',
                name: 'Social Reproduction Theory: Remapping Class, Recentering Oppression - Tithi Bhattacharya',
                types: [DIRECTORY],
                children: [
                  {
                    id: '1d577126-db95-4cbd-a4f9-161bd27b054a',
                    name: 'Social Reproduction Theory - Directory',
                    types: [GOOGLE_DRIVE],
                    link: 'https://drive.google.com/drive/folders/1_bRiU80c6zEthhWxOsLfajHGm5mjHzfu?usp=sharing',
                  },
                  {
                    id: '070e2496-a87e-4cdc-ba2e-a8b4d1fb45c5',
                    name: 'Social Reproduction Theory: Remapping Class, Recentering Oppression - Tithi Bhattacharya',
                    types: [PDF_FILE],
                    link: 'https://drive.google.com/file/d/1OnwRsoK8r8NQD2OT1D0q04TUL-JvYyIj/view',
                  },
                  {
                    id: 'a41a0dd2-3511-4ee2-8f4d-f36ef858eb25',
                    name: 'Chapter 2 - Notes - Gabriel Bourget',
                    types: [DOCUMENT_FILE],
                    link: 'https://drive.google.com/file/d/1SYviQmynKD7IyP_oNu_I0FNUD8p2vYeD/view?usp=sharing'
                  }
                ],
              },
              {
                id: '2874afc9-3cc5-495c-bb1b-d6bb9a3bf861',
                name: 'The Wretched of The Earth - Frantz Fanon',
                types: [DIRECTORY],
                children: [
                  {
                    id: 'f7780f2b-ff5c-4126-aa00-ec5bd450aade',
                    name: 'The Wretched of the Earth - Directory',
                    types: [GOOGLE_DRIVE],
                    link: 'https://drive.google.com/drive/folders/1wwEgWEfcBc95610Im8iHiwQzfvDCW6_M?usp=sharing'
                  },
                  {
                    id: '774b87d3-1961-41fc-8313-e2f8a2bf1027',
                    name: 'Frantz Fanon - The Wretched of the Earth',
                    types: [PDF_FILE, DOCUMENT_FILE],
                    link: 'https://drive.google.com/file/d/1qXwwPA3BoVSw-s2Du_RArY2FBZyRRyhE/view?usp=sharing',
                  },
                  {
                    id: '3fb57255-e15f-4dc8-a1d9-bb1e44f1d4e4',
                    name: 'The Wretched of The Earth - Preface - Notes',
                    types: [WORD_DOCUMENT_FILE, DOCUMENT_FILE],
                    link: 'https://drive.google.com/file/d/14INouuhVc1wLdihCuRl-bZFYaubP_8sH/view?usp=sharing'
                  },
                  {
                    id: 'af062a8a-e597-4022-86f2-72eb58c920cc',
                    name: 'The Wretched of The Earth - C1 - Notes',
                    types: [WORD_DOCUMENT_FILE, DOCUMENT_FILE],
                    link: 'https://drive.google.com/file/d/1615HeRkYV8BWO_XKmA-J_hBiaEQ1H4s7/view?usp=sharing'
                  },
                  {
                    id: 'fd185860-ea23-47db-bea5-6793617abb23',
                    name: 'The Wretched of The Earth - C2 - Notes',
                    types: [WORD_DOCUMENT_FILE, DOCUMENT_FILE],
                    link: 'https://drive.google.com/file/d/1ixXJPmtug2RjmOS3xxkbGqY6lfYSHaqE/view?usp=sharing'
                  },
                  {
                    id: '220bea7f-857c-40d4-aa11-71a6ab0305c3',
                    name: 'The Wretched of The Earth - C3 - Notes',
                    types: [WORD_DOCUMENT_FILE, DOCUMENT_FILE],
                    link: 'https://drive.google.com/file/d/1_MJq_fcONzPqgxARFIifh5LJoFH5oy_V/view?usp=sharing'
                  },
                  {
                    id: 'a75f01d6-3143-48b0-975a-878909fb1dfe',
                    name: 'The Wretched of The Earth - C4 - Notes',
                    types: [WORD_DOCUMENT_FILE, DOCUMENT_FILE],
                    link: 'https://drive.google.com/file/d/1SpgtqBEQNSXXpLSWvtI_jxY5f4ZBK_5y/view?usp=sharing'
                  },
                  {
                    id: '4caf0e61-f845-49d6-a39d-1650a0086423',
                    name: 'The Wretched of The Earth - C5 - Notes',
                    types: [WORD_DOCUMENT_FILE, DOCUMENT_FILE],
                    link: 'https://drive.google.com/file/d/1wsjrGY2cOguZO3nTOmWB4sUA21EpZiUd/view?usp=sharing'
                  },
                  {
                    id: '84c2b34a-714a-4801-bf10-3e6625f3c189',
                    name: 'The Wretched of The Earth - C6 - Notes',
                    types: [WORD_DOCUMENT_FILE, DOCUMENT_FILE],
                    link: 'https://drive.google.com/file/d/17Eaz8BP8hyTlSFextARrKnANtRrILBSf/view?usp=sharing'
                  },
                  {
                    id: '168409cd-2ff2-4179-bc5a-867d17accc4b',
                    name: 'The Wretched of The Earth - Conclusion - Notes',
                    types: [WORD_DOCUMENT_FILE, DOCUMENT_FILE],
                    link: 'https://drive.google.com/file/d/1iJvaPSn_hiNFOO12qfQKJO9J5ORnniBf/view?usp=sharing'
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        id: 'e98d6332-b2cb-4de9-a3e1-bd9cbdd2d8ce',
        name: 'Black Socialists - Resource Guide',
        types: [WEBSITE],
        description: "Collection of readings and other resources in the valence of socialism, centered around a Black perspective.",
        link: 'https://blacksocialists.us/resource-guide',
      }
    ]
  },
};
