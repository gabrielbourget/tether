import {
  DOCUMENT_FILE, DIRECTORY, IMAGE_FILE, GOOGLE_DRIVE, PDF_FILE,
  SLIDES_FILE, PNG_FILE,
} from '../../../../../constants';

import { WorkingGroup } from "../../../../../Types/courageTypes";

export const mobilityJusticeData: WorkingGroup = {
  id: 'd1e67b4a-9e0d-40dc-bf3f-1c4ddff3c19c',
  name: 'Mobility Justice',
  coChairs: [
    { id: 'ec2cf87e-0a03-4efd-b921-9161cde9fe67', name: 'Tyler Paziuk' },
    { id: 'af43048e-05a6-45c8-b729-13943bf00411', name: 'Cameron Roberts' },
  ],
  members: [
    { id: 'ec2cf87e-0a03-4efd-b921-9161cde9fe67', name: 'Tyler Paziuk' },
    { id: 'af43048e-05a6-45c8-b729-13943bf00411', name: 'Cameron Roberts' },
    { id: 'b20de0d8-12f7-4b0b-b05e-0b3e1f625bac', name: 'Julia Szwarc' },
    { id: '6c711d48-219a-4ed6-a78b-cad5e2ba54c3', name: 'Laura Shantz' },
    { id: 'c40ee152-a057-49e9-a506-faabadf21798', name: 'Sam B' },
    { id: 'ee9492c9-1dcc-4f59-bb0a-d41099e42705', name: 'Karim A' },
    { id: '2dbbc389-d5cc-4ac1-b261-e3d0748fa7fa', name: 'Melanie Mathias' },
  ],
  socials: {
    id: 'b0730a5a-e5a1-4190-b8b4-3c63880a0cfa',
    name: 'Socials',
  },
  communications: {
    id: 'b9076346-cb53-48cd-b5ae-f7865aabe41a',
    name: 'Communications',
    children: [
      {
        id: '76a20383-e9ea-46c5-a08f-f8a62465ba9a',
        name: 'Slack',
        link: 'https://www.independentleft.slack.com',
        children: [
          {
            id: 'ff7d3e90-7752-4f76-9bc4-9cd0e7a9a5eb',
            name: 'Slack Channels',
            children: [
              {
                id: 'cca8e665-1a52-4b0e-89fa-da1e8773b372',
                name: '#ott_transit',
              }
            ]
          }
        ],
      }
    ],
  },
  resources: {
    id: '4fdf080c-7012-47f7-898d-43d6e12401ab',
    name: 'Resources',
    types: [DIRECTORY],
    children: [
      {
        id: '1c1e3620-585d-4f4d-9855-8d6c7055df35',
        name: 'Mobility Justice Working Group Google Drive',
        types: [GOOGLE_DRIVE],
        link: 'https://drive.google.com/drive/folders/1DhjSQ5PbFubeaR1ah2yX_oU4p0qAuxUf',
        description: '',
      },
      {
        id: '44de24e5-9fc3-4668-b57f-d49ef254d18d',
        name: 'ParaParity Campaign',
        types: [DIRECTORY],
        children: [
          {
            id: 'edeab501-0394-40a5-a6ec-4e096a144ebf',
            name: 'ParaParity Research',
            types: [DOCUMENT_FILE],
            link: 'https://docs.google.com/document/d/1JUlrM7j5X3tOb0_PpSHfBw1m_JR1GisZnjOQai8BjOU/edit?usp=sharing',
          }
        ]
      },
      {
        id: '0bb22513-6d3f-41d7-b5bd-2f9caff44e72',
        name: 'James Wilt Webinar Event',
        types: [DIRECTORY],
        children: [
          {
            id: '51fbb3f2-cf79-4c87-97e9-b23ad112d8e4',
            name: 'Welcome Slide',
            types: [SLIDES_FILE],
            link: 'https://docs.google.com/presentation/d/1cd2Z6rHh2kQ-lhlKrvv5FDd_l32vSx6JEjG-Drhusug/edit?usp=sharing'
          },
          {
            id: 'b004232e-a063-4c1d-81f2-08df216c6819',
            name: 'Welcome Slide v2',
            types: [SLIDES_FILE],
            link: 'https://docs.google.com/presentation/d/1-e54_j5d9VEWsu6qmEt_v367zjIJIxk3ntSGA-K3dtM/edit?usp=sharing',
          },
          {
            id: '8427bb34-3bbe-41e1-a225-b1829f3860b7',
            name: 'Opening Remarks',
            types: [DOCUMENT_FILE],
            link: 'https://docs.google.com/document/d/1ozMTbGppgHz20lEj87UGxuZi3ldQsyJ-sAkdT2JtRqg/edit?usp=sharing',
          },
          {
            id: 'a0974e30-91dd-4e4d-82cb-9905a27623a8',
            name: 'General Invitation',
            types: [DOCUMENT_FILE],
            link: 'https://docs.google.com/document/d/1HkhOLMVDCDiVdaB3H67fPpv_KeRmCWu5I08jzeabO2M/edit?usp=sharing',
          },
          {
            id: '887aab0f-852e-4e3d-a1bd-326f1de7d13c',
            name: 'Invitation for Elected Representatives',
            types: [DOCUMENT_FILE],
            link: 'https://drive.google.com/file/d/124YPdXiUuADwg-g_5lSqRwIbjmPNeDkv/view?usp=sharing'
          },
          {
            id: '2616ba80-20ef-4419-9dfc-eee7704c02aa',
            name: 'Invitation for Elected Representatives v2',
            types: [DOCUMENT_FILE],
            link: 'https://docs.google.com/document/d/1PMIYpmoDBbIGyRTXg8dJZ2d4j3Mm3wRMGWyScZ7sXQ0/edit?usp=sharing',
          },
          {
            id: 'f18cc82d-e681-422e-88b2-042b3e741987',
            name: 'Twitter Promo 2160x1080',
            types: [IMAGE_FILE, PNG_FILE],
            link: 'https://drive.google.com/file/d/1wylt7uRWDAuWBfOgZ4egqiMUPCthD-XX/view?usp=sharing',
          },
          {
            id: 'e68c3b80-2626-484d-b659-fd482aa0ba9e',
            name: 'Instagram Promo Square',
            types: [IMAGE_FILE, PNG_FILE],
            link: 'https://drive.google.com/file/d/1mfPrvl_VynY_mLitanmZlPGAwx0CLXV0/view?usp=sharing',
          },
          {
            id: '40b579ca-748d-48d7-acd7-4c1859291ff6',
            name: 'Portrait Promo',
            types: [IMAGE_FILE, PNG_FILE],
            link: 'https://drive.google.com/file/d/1UaCt_EpjFYb_7ibRsChcRUawQ3UoOVXB/view?usp=sharing',
          },
        ]
      },
      {
        id: '22d4ef9a-bd4d-4885-8a39-067d85ad8fe8',
        name: 'Social Media Shareables',
        types: [DIRECTORY],
        children: [
          {
            id: '40e8bc90-bc4c-4268-93fa-1e65a0a4fee5',
            name: 'Beg Buttons',
            types: [IMAGE_FILE, PNG_FILE],
            link: 'https://drive.google.com/file/d/1lbjCfHUwbVa1IaTkEwHmZkwIF9dCPvbC/view?usp=sharing'
          },
          {
            id: '5967d754-f5fd-4f09-a1c7-a0861748166d',
            name: 'Maintain Bus Service',
            types: [IMAGE_FILE, PNG_FILE],
            link: 'https://drive.google.com/file/d/1fxIKk90hv_AWh8xi3RtcSNZeGaud6LXr/view?usp=sharing',
          },
          {
            id: '941b4d70-20d8-4d6c-be23-28a0f4d312fd',
            name: 'Free Parking and Safe Transit',
            types: [IMAGE_FILE, PNG_FILE],
            link: 'https://drive.google.com/file/d/10SECA93-kyk7Tl3w75lvcDK9pcsQQ-wu/view?usp=sharing',
          },
          {
            id: '320e7409-d296-458c-8c41-ed3575846d7a',
            name: 'More Space',
            types: [IMAGE_FILE, PNG_FILE],
            link: 'https://drive.google.com/file/d/1zJPJjs0O-rcBvmqlJYthacMkN-aRnuYO/view?usp=sharing',
          }
        ]
      },
      {
        id: 'e1d61828-530a-4a3e-9cbf-ad32de361350',
        name: 'Courage Comms Committee Motion',
        types: [DOCUMENT_FILE],
        link: 'https://docs.google.com/document/d/1X6hBIaovQNh5GXPxLAQQx446I1oCIcGdOAgel21WpL4/edit?usp=sharing'
      },
      {
        id: '0d8bbfa7-cb8b-4217-98af-cc0e47bcd79e',
        name: 'Courage Mobility Justice Working Group - Our Vision for Ottawa',
        types: [DOCUMENT_FILE],
        link: 'https://docs.google.com/document/d/1TtMLT4ztMdrfPLHj6kDH8gr1QgdDHY7SXj9VfuRGoVE/edit?usp=sharing'
      },
      {
        id: 'fa057b59-77b5-4f24-8597-4498119e0a7e',
        name: 'Meetings',
        types: [DIRECTORY],
        children: [
          {
            id: 'd4cbae29-dcc2-4e5a-b281-7326a9d47a21',
            name: '2020',
            types: [DIRECTORY],
            children: [
              {
                id: '3e830a98-dcea-4292-99a4-eae2e4e41016',
                name: 'March',
                types: [DIRECTORY],
                children: [
                  {
                    id: '263dc2ab-1531-4e81-8268-f047fe42baee',
                    name: 'March 30',
                    types: [DIRECTORY],
                    children: [
                      {
                        id: '26dacfd5-15e6-4b1a-b35b-92089d6ff010',
                        name: 'COMJWG - March 30 - Meeting Agenda',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1vFaWgEDV3XmS958oYlWTCeySM2Tsz3RnZFN_X8vnrww/edit?usp=sharing',
                      },
                      {
                        id: '160a2f9a-b747-4cee-b45c-8f0c4bd1479e',
                        name: 'COMJWG - March 30 - Meeting Minutes',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1B37OTqZ_DgaiYXesxJ5Q5BPC8SE_1zBS2HFu6q27940/edit?usp=sharing'
                      }
                    ]
                  }
                ]
              },
              {
                id: 'b5d7dbe5-0ce1-4861-b631-0803b6f4ac4e',
                name: 'April',
                types: [DIRECTORY],
                children: [
                  {
                    id: 'ad645f46-a9c5-4502-bb4d-6bac3f92128f',
                    name: 'April 29',
                    types: [DIRECTORY],
                    children: [
                      {
                        id: 'b1069685-29f9-4945-834a-095c5fff3913',
                        name: 'COMJWG - April 29 - Meeting Agenda',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1xARMeCYxcYNCcpkZghsFl1uC-ObcS7LlgAEj1cJTdfE/edit?usp=sharing'
                      }
                    ]
                  }
                ]
              },
              {
                id: '49bf38bc-16d6-4824-8696-ac8e622f90f3',
                name: 'May',
                types: [DIRECTORY],
                children: [
                  {
                    id: 'bf116df0-558d-45eb-9c4e-6540da4d5346',
                    name: 'May 19',
                    types: [DIRECTORY],
                    children: [
                      {
                        id: 'cc32515a-fa38-4019-a366-6f02d207a324',
                        name: 'COMJWG - May 19 - Meeting Agenda',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1GqS8Qisl86eHywA5oXy2RUk6Ow1FcD3QUTRoysgm7Cs/edit?usp=sharing'
                      }
                    ]
                  },
                ]
              },
            ]
          }
        ]
      },
      {
        id: '85a53fa7-cd80-4577-be92-ec7b1db16ece',
        name: 'Free Transit In Ottawa - Pamphlet (June 2018)',
        types: [DOCUMENT_FILE],
        link: 'https://drive.google.com/file/d/13Ixx_5PtQpEISs37ulz3vEtwzLYfuU2V/view?usp=sharing'
      },
      {
        id: '8fc05b7c-e1e4-4b32-b56e-2fd5e7e450b9',
        name: 'Letter to Jim Watson - May 15, 2020',
        types: [DIRECTORY],
        children: [
          {
            id: 'cda931e5-d2fc-4154-a9ef-271988acbf2f',
            name: 'Open Letter to Jim Watson - May 15, 2020',
            types: [DOCUMENT_FILE],
            link: 'https://docs.google.com/document/d/14jSdRgLfzmOWFL_9cMZw8MR5ei5gKetpt2AYl0fthSg/edit?usp=sharing'
          },
          {
            id: 'fe23fa7a-322a-4051-8f3e-910886063b27',
            name: 'Open Letter to Jim Watson - May 15, 2020',
            types: [PDF_FILE, DOCUMENT_FILE],
            link: 'https://drive.google.com/file/d/1-1pPW6Oxv7FBTNedM9SLYFy7mloAKDep/view?usp=sharing',
          }
        ]
      },
      {
        id: 'd612834e-eaa3-46db-be0d-243a041273cc',
        name: 'COMJWG - Demands of Possible Allies',
        types: [DOCUMENT_FILE],
        link: 'https://docs.google.com/document/d/1y4NbZQM_ciSeAGJ-s8bvMVWQh0oxKkEsDC45nUn68FA/edit?usp=sharing',
      },
      {
        id: '5f1f60cb-bb33-49e6-b0ff-b57a479cac90',
        name: 'Works In Progress (WIP)',
        types: [DIRECTORY],
        children: [
          {
            id: 'cc344122-c61a-4204-91c4-2ef996b6b0fc',
            name: 'Courage Federal Public Transit Working Paper',
            types: [PDF_FILE, DOCUMENT_FILE],
            link: 'https://drive.google.com/file/d/1iAOFZfU4-ch_DRDXVrTO6K25q3WGtHHq/view?usp=sharing'
          }
        ]
      }
    ]
  },
};
