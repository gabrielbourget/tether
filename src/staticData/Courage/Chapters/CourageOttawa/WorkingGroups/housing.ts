import { DIRECTORY } from '../../../../../constants';

import { WorkingGroup } from "../../../../../Types/courageTypes";

export const housingData: WorkingGroup = {
  id: '49da41fb-dbfd-4fe0-9d45-7fa08d678815',
  name: 'Housing',
  coChairs: [
    { id: 'f674ec89-d44c-4432-833e-0d58e41997ab', name: 'Sam Hersh' },
  ],
  members: [
    { id: 'f674ec89-d44c-4432-833e-0d58e41997ab', name: 'Sam Hersh' },
    { id: '6d1406a-e157-4bc1-a011-f27c4c2a2dc3', name: 'Meagan Wiper' },
  ],
  communications: {
    id: 'cca30a9d-9fd3-457a-9cdf-ac9493a4cf45',
    name: 'Communications',
    children: [
      {
        id: '76a20383-e9ea-46c5-a08f-f8a62465ba9a',
        name: 'Slack',
        link: 'https://www.independentleft.slack.com',
        children: [
          {
            id: 'ff7d3e90-7752-4f76-9bc4-9cd0e7a9a5eb',
            name: 'Slack Channels',
            children: [
              {
                id: 'cca8e665-1a52-4b0e-89fa-da1e8773b372',
                name: '#ott_housing'
              }
            ]
          }
        ],
      }
    ],
  },
  socials: {
    id: '6e5ca161-4096-47ca-98bb-588d492fe223',
    name: 'Socials',
  },
  resources: {
    id: '451bfb1d-fdbb-463f-8119-05dc927d85c8',
    name: 'Resources',
    types: [DIRECTORY],
    children: [
      {
        id: '',
        name: '',
        description: '',
        link: '',
      }
    ]
  },
};
