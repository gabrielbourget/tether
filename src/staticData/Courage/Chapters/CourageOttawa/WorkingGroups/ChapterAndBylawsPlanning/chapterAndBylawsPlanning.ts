import {
  WEBSITE, DOCUMENT_FILE, SPREADSHEET_FILE, DIRECTORY, GOOGLE_DRIVE, FORM,
  WORD_DOCUMENT_FILE, SLIDES_FILE, IMAGE_FILE, PNG_FILE, SVG_FILE, JPG_FILE,
} from '../../../../../../constants';

import { WorkingGroup } from "../../../../../../Types/courageTypes";

export const chapterAndBylawsPlanningData: WorkingGroup = {
  id: '01210c54-4398-4776-9fd1-2deb5dc1e640',
  name: 'Chapter/Bylaws Planning',
  description: '',
  coChairs: [],
  members: [],
  socials: {
    id: '',
    name: '',
  },
  communications: {
    id: '2e744f92-20a3-47a8-a77c-0afc66271a25',
    name: 'Communications',
    types: [DIRECTORY],
    children: [
      {
        id: 'e397873f-b285-4895-b591-693cf7974570',
        name: 'Slack',
        link: 'https://www.independentleft.slack.com',
        children: [
          {
            id: 'cef1bdb3-d880-4918-bfd2-7fd36f72d42b',
            name: 'Slack Channels',
            children: [
              {
                id: '2cc13375-56d1-4dc8-948b-d071c77c227a',
                name: '#ott_chapter_planning'
              }
            ]
          }
        ]
      }
    ]
  },
  resources: {
    id: 'f7446fb0-5da5-4f30-a836-03d2b41c19d9',
    name: 'Resources',
    types: [DIRECTORY],
    children: [
      {
        id: '654d593c-3edb-4b13-bf7e-43ba9bd3a687',
        name: 'Meeting Minutes',
        types: [DIRECTORY],
        children: [
          {
            id: '88ad3db9-be15-45a7-8f9f-6c7c3e01530c',
            name: 'Meeting Minutes - Directory',
            types: [GOOGLE_DRIVE],
            link: 'https://drive.google.com/drive/folders/1dJJVWANQ4K8aw2SpDaH2O1OF7UBENX7D?usp=sharing',
          },
          {
            id: 'cecca266-52d8-42b7-bb44-0cbf17963360',
            name: '2020',
            types: [DIRECTORY],
            children: [
              {
                id: '320ebe4e-0db2-47b5-b5c7-1771a3767238',
                name: '2020 - Directory',
                types: [GOOGLE_DRIVE],
                link: 'https://drive.google.com/drive/folders/1k0RfX5KeyuYPYFqGEDq9ZRrEJm5dprQT?usp=sharing',
              },
              {
                id: '6e84923d-7c86-4eb3-b7b3-14c7d583f4a3',
                name: 'August',
                types: [DIRECTORY],
                children: [
                  {
                    id: '27c8320f-d642-473e-80a2-0d8649330aa5',
                    name: 'August - Directory',
                    types: [GOOGLE_DRIVE],
                    link: 'https://drive.google.com/drive/folders/1R2VLmOkfu6nqpML3lLq7y-8LPoOW73PQ?usp=sharing',
                  },
                  {
                    id: "c3d60de3-99fd-454c-b5f2-c9402c1880fc",
                    name: "August 14",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: '09e71b04-602e-48e5-a683-290cc55dc2f5',
                        name: 'Courage Ottawa - Chapter and Bylaws Planning WG - Meeting Minutes - August 14, 2020',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1W7t1Lj8AWXIASa60usIgGO462jHxshWPJH_jD5gUFCc/edit?usp=sharing',
                      },
                    ]
                  },
                  {
                    id: 'b388ddd5-325d-4929-bb01-5ec05223bc20',
                    name: 'August 25',
                    types: [DIRECTORY],
                    children: [
                      {
                        id: '85a12f24-9265-42ee-ad13-396ce0ed3e61',
                        name: 'August 25 - Directory',
                        types: [GOOGLE_DRIVE],
                        link: 'https://drive.google.com/drive/folders/1lSYN0wl066aZPITmf9oP4jKreLoDh3Sb?usp=sharing'
                      },
                      {
                        id: '1a822297-0c71-45cc-a8de-f1ec427b9e0d',
                        name: 'Courage Ottawa - Chapter Planning WG - Meeting Minutes - August 25, 2020',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1SJvEjwjNjAiH8EXZo-TM3SWoDU3469Cn4x7jKlDo4cg/edit',
                      },
                      {
                        id: 'e19de06f-f973-439d-b398-c97a862b2e45',
                        name: 'Courage Ottawa Values Statement/Manifesto Draft',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/19vIK0AjzN2ccWMojAtkTO7YokQ0hoYU-GapY5l9t6Wg/edit'
                      },
                      {
                        id: '29a8aea4-3482-4b1c-8ad1-2b1cbade8095',
                        name: 'Integral Thinking - Knowledge Module',
                        types: [WORD_DOCUMENT_FILE, DOCUMENT_FILE],
                        link: 'https://drive.google.com/file/d/1H8_D2cNTftRy0U5kwh02PkNSB5beVLA8/view?usp=sharing'
                      },
                    ],
                  },
                ]
              },
              {
                id: 'fcf66a34-b432-4824-aa1b-07b0f6b0710e',
                name: 'September',
                types: [DIRECTORY],
                children: [
                  {
                    id: 'f74f6b25-dfa2-4b2b-a996-563bb96943fc',
                    name: 'September - Directory',
                    types: [GOOGLE_DRIVE],
                    link: 'https://drive.google.com/drive/folders/12AbJOJi6nMFHUc8s0_P4McFYHm6d9zMl?usp=sharing',
                  },
                  {
                    id: '37f3b6a5-4dfa-491d-bc3d-0cc13c5e6fb6',
                    name: 'September 13',
                    types: [DIRECTORY],
                    children: [
                      {
                        id: '5ad38ef8-c742-4dfd-924b-26c1d77ba8d5',
                        name: 'September 13 - Directory',
                        types: [GOOGLE_DRIVE],
                        link: 'https://drive.google.com/drive/folders/1FEpz-Wt9m5sd04Tw4k9rIhYQGSQilzWQ?usp=sharing'
                      },
                      {
                        id: '23b76f09-f682-4bb7-87d8-b45b6b579c83',
                        name: 'Courage Ottawa - Chapter Planning WG - Meeting Minutes - September 13, 2020',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1Z4CE2dMpicpnGhpDf4VfdZdiFtgqq6EUC8sa3YT04p8/edit'
                      }
                    ]
                  },
                  {
                    id: "094c3a05-9e5c-4e23-91aa-5e11c69aa2cf",
                    name: "September 27",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "6a9ad2d2-8231-49bd-8e93-5821ed335e63",
                        name: "Courage Ottawa - Chapter Planning WG - Meeting Minutes - September 27, 2020",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1vpLvy-m3Mmc2JSsqWO19M6g7xzeYhy7q_RacHxfyBH0/edit",
                      },
                    ]
                  }
                ]
              },
              {
                id: "cb577db7-3d1f-4953-aba0-c8492fb80cf8",
                name: "October",
                types: [DIRECTORY],
                children: [
                  {
                    id: "effba34d-af66-41aa-9c2a-f33e77a35bd5",
                    name: "October 26",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "5e88af70-660d-41f6-a41b-50160ade2667",
                        name: "Courage Ottawa - Chapter Planning WG - Meeting Minutes - October 26, 2020",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1QLEcQ9LIr_iZqigb2VNlWyAFMr1c9jdrEMwXKKqAKCI/edit"
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        id: "e38ddf3b-08cc-4170-9d0c-6f14a5765922",
        name: "Core Documents",
        types: [DIRECTORY],
        children: [
          {
            id: 'e19de06f-f973-439d-b398-c97a862b2e45',
            name: 'Statement of Values',
            types: [DOCUMENT_FILE],
            link: 'https://docs.google.com/document/d/19vIK0AjzN2ccWMojAtkTO7YokQ0hoYU-GapY5l9t6Wg/edit?usp=sharing'
          },
          {
            id: '6c9eb7ea-8450-4f4a-8853-fb31b0645c17',
            name: "Courage Ottawa - Bylaws",
            types: [DOCUMENT_FILE],
            link: 'https://docs.google.com/document/d/153AUTnH5-23RvUJSfSbwHpG3XD-C-GeA-EXuYwsE4XM/edit?usp=sharing'
          },
          {
            id: "c1eb1686-1164-4516-a15b-1b3341ba0811",
            name: "Supporting Documents",
            types: [DIRECTORY],
            children: [
              {
                id: "e09f952c-f9b6-4219-aa2b-9d8c356ff2ee",
                name: "Courage Ottawa - Pluralism Strategy",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/1ie0C1n_e5TDeX3Kyw9uXDieQpRrIdp3RUz6cc4hCScY/edit?usp=sharing",
              },
              {
                id: "c7ba9fa3-d3d7-4b39-a16c-8f8478d9095a",
                name: "Courage Ottawa - Code of Conduct",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/1Xi6SY5L8w-XCOrLrJpqY3s9UeiWRkvRZw42dX-R_tMo/edit?usp=sharing",
              },
              {
                id: "97ef76ff-f7da-4095-a138-562331efb493",
                name: "Courage Ottawa - Organizational Structure",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/1WFdlSWR9ViaDIyyHukK9NvdTrVqfNj65A5HvzIdZvSg/edit",
              },              
            ]
          },
          {
            id: "45513b21-d5bc-4bea-bf2d-ee4b15dfa1dd",
            name: "Courage Ottawa - Foundational Document Structure",
            types: [IMAGE_FILE, PNG_FILE],
            link: "https://drive.google.com/file/d/1KlAPgNRtiIY_nDMBlQdpZZtYh_nU2rNT/view?usp=sharing",
          },
        ]
      },
      {
        id: "d495b038-ddca-4391-99bf-5df49e739d4b",
        name: "Bylaws Planning",
        types: [DIRECTORY],
        children: [
          {
            id: "9525a737-6506-4937-8099-2c0d43fd5a31",
            name: "Courage Ottawa - Composite Bylaws Draft from Cumulative Planning/Brainstorming Sessions",
            types: [DOCUMENT_FILE],
            link: "https://docs.google.com/document/d/1oWfRvhqPrtmM32_J-B6uWZCV9Z8btnYcdVo6KAE7tk8/edit?usp=sharing"
          },
          {
            id: "97ef76ff-f7da-4095-a138-562331efb493",
            name: "Courage Ottawa - Proposed Organizational Structure",
            types: [DOCUMENT_FILE],
            link: "https://docs.google.com/document/d/1fEbnFQGEm0k8w06YFVOQEdWteTzdeOe2P2ObdlnC_rM/edit",
          },
          {
            id: '4a3d62fc-7869-4909-929a-07c3672537cb',
            name: 'Courage Ottawa - Proposed Organogram',
            types: [DOCUMENT_FILE],
            link: 'https://docs.google.com/drawings/d/18LJX2IDdpeaN-1W7Ml_rLm7y5gfvn3Zlx5dkrSylTok/edit'
          },        
          {
            id: "2af5c39a-7d91-49a1-a5ad-978bc9ff1090",
            name: "Resources from Courage Ottawa Membership Surveys",
            types: [DIRECTORY],
            children: [
              {
                id: 'db9a342c-338f-4090-a309-862e82ac95d3',
                name: 'Courage Ottawa - Long Term Planning Brainstorming - Tabulated Results',
                types: [SPREADSHEET_FILE],
                link: 'https://docs.google.com/spreadsheets/d/1S0PFigasJwVT7shsKi7drtT51onU12-j5K6nSG5rd1E/edit?usp=sharing',
              },
              {
                id: 'bb07d037-f573-4d53-a3ce-ee3fb0f29865',
                name: 'Courage Ottawa - Results from Initial Member Survey',
                types: [SLIDES_FILE],
                link: 'https://docs.google.com/presentation/d/13j7ETyxVMrB29Oy_hUY6MMCWGK9liuxK-BxJNuPjAOw/edit?usp=sharing',
              },
              {
                id: 'b9eb5f2a-37ee-4d76-ab07-983fbbca7531',
                name: 'Courage Ottawa - Long Term Planning Priorities Survey - Outward Facing Link',
                types: [FORM],
                link: 'https://docs.google.com/forms/d/e/1FAIpQLSdsock5bZi4G4GzOThs65FL6D893IAum2_pfjDmoWMOovZ_zA/viewform',
              },
              {
                id: '8236cb2c-2d7e-4383-aaa3-6b5d5a0935ff',
                name: 'Courage Ottawa - Long Term Planning Priorities Survey - Administrative Editable Link',
                types: [FORM],
                link: 'https://docs.google.com/forms/d/17FTbDLppp4qO7PO-Gmk8Y3jN0YEKc7RVYk4TLeYcSEg/edit'
              },
            ]
          },
          {
            id: "2931bc55-d230-4501-ba05-e7c3bd4faa6f",
            name: "External Resources",
            types: [DIRECTORY],
            children: [
              {
                id: 'cd147b27-a499-44f9-ac55-193dcb8604f5',
                name: 'Courage Coalition Constitution/Bylaws - Initial Sept 2019 Draft',
                types: [DOCUMENT_FILE],
                link: 'https://docs.google.com/document/d/1JDb99m4y7Vi786UowVhVk8aC39ifkw4EhfZdfaBTS68/edit',
              },
              {
                id: '22f461c6-557d-48b8-ac35-fae03226e5e2',
                name: 'Boston DSA - Bylaws',
                types: [WEBSITE],
                link: 'https://www.bostondsa.org/bylaws/',
              },
              {
                id: '19f1dc0a-1335-4584-8181-a58f2940b387',
                name: 'Courage Montreal - Bylaws Draft',
                types: [DOCUMENT_FILE],
                link: 'https://docs.google.com/document/d/1hrzU4hBeYiYnzF2Zg7UUXg6GNZCqGMKTMHb1kxpAR6w/edit?usp=sharing',
              },
              {
                id: '095aa469-9185-4697-934b-8dbb0573af10',
                name: 'Courage Vancouver - Bylaws',
                types: [DOCUMENT_FILE],
                link: 'https://docs.google.com/document/d/1_a7KAl1xjYguKpwxgnwOnayP43aIzltu746FgoWEQYY/edit?usp=sharing',
              },
            ]
          },
        ]
      },
      {
        id: "f04a03cc-ed6b-41a4-a904-a8774a0c2ada",
        name: "Ratification Process Planning",
        types: [DIRECTORY],
        children: [
          {
            id: "b68c9373-f9c4-42cd-9a00-bcd1adcca99f",
            name: "Courage Ottawa - Narrative History of the Chapter Planning Process",
            types: [DOCUMENT_FILE],
            link: "https://docs.google.com/document/d/16ceYuVNoKvO8jP0F2AhQVJHKiXsCgKsnUg1fwNzEl_8/edit"
          },
          {
            id: "87f86009-8dd4-467e-a417-040c8c84bceb",
            name: "Courage Ottawa - Proposed Ratification Process for the Statement of Values and Bylaws",
            types: [DOCUMENT_FILE],
            link: "https://docs.google.com/document/d/1fH7MZ4wRHt7ae-vp0uJQ9uQ_yn1at35kpqLiaclidpg/edit?usp=sharing"
          },
          {
            id: "e9f151d1-261f-497a-a824-a939b4d8223a",
            name: "Ratification Presentation 1",
            types: [SLIDES_FILE],
            link: "https://docs.google.com/presentation/d/1XReDYeKQVz1P8TaakfTSRy2FgpDTqlD40xC4-NVgOPU/edit?usp=sharing",
          },
          {
            id: "ce3bc45c-ced9-4339-8bec-1eef56e6f799",
            name: "Phase I Ballot - Editable Administrator Link",
            types: [FORM],
            link: "https://docs.google.com/forms/d/17AdRt_pFA5ZxU8hsJzOUiTUQy5N9XhWDdVZ-iQEHiOw/edit?usp=sharing",
          },
          {
            id: "0e9809e0-f8c8-443a-8c11-a5116a286b73",
            name: "Phase II Ballot - Editable Administrator Link",
            types: [FORM],
            link: "https://docs.google.com/forms/d/1tVSsq9LuOs3LXHVdSX0L5_Kbn3PrAF-6-Edz5j-FQa8/edit?usp=sharing",
          },
          {
            id: "00b62434-04ec-4498-ae1d-3694aa049689",
            name: "Visuals",
            types: [DIRECTORY],
            children: [
              {
                id: "a544fef9-52d0-45af-9bcb-f3ce753c44c6",
                name: "Ratification Flow v1",
                types: [IMAGE_FILE, JPG_FILE],
                link: "https://drive.google.com/file/d/17fZ6eTDT-qZe7EAPnjOwdpZ9WjmFFAUu/view?usp=sharing",
              },
              {
                id: "ce7030e4-3cd6-4973-823e-f2371b10d9bf",
                name: "Ratification Flow v1",
                types: [IMAGE_FILE, SVG_FILE],
                link: "https://drive.google.com/file/d/11hSHCWsk3GOiDQsFbrU5JtvhuC9B3EuA/view?usp=sharing",
              },
              {
                id: "bb9da2f6-a1f1-4e78-b1af-9495314ed1e2",
                name: "Ratification Flow v2",
                types: [IMAGE_FILE, JPG_FILE],
                link: "https://drive.google.com/file/d/1701kwN5QuLJ4sM8grisLpv2xWNLdzRFj/view?usp=sharing",
              },
              {
                id: "e78aec62-40ca-4405-914f-50dc11ebbdaa",
                name: "Ratification Flow v2",
                types: [IMAGE_FILE, SVG_FILE],
                link: "https://drive.google.com/file/d/15YeM8C7JyPuVLpca0KU3pTtqgtD9LZFv/view?usp=sharing",
              },
              {
                id: "173d118e-83f0-4547-bdee-c31327735f85",
                name: "Ratification Flow v3",
                types: [IMAGE_FILE, JPG_FILE],
                link: "https://drive.google.com/file/d/1bq8V8q6FsAiE1YvefAgpSPbgtJl2tobL/view?usp=sharing",
              },
              {
                id: "81179943-fb10-4c21-b330-779eeaad1060",
                name: "Ratification Flow v3",
                types: [IMAGE_FILE, SVG_FILE],
                link: "https://drive.google.com/file/d/1FnRVThIPGrqlpWO14vizMQzs5WuxU-s4/view?usp=sharing",
              },
            ]
          }
        ]
      },
      {
        id: "d5183292-5f41-4992-853b-6d4b45e1a321",
        name: "List of View-Only Links to Core Documents Ahead of Ratification",
        types: [DOCUMENT_FILE],
        link: "https://docs.google.com/document/d/1Ol28HR9V929PARYB2SyqpXOhV2jA2peVirp9mIZtm4E/edit?usp=sharing",
      },
      {
        id: "8327c7cd-45bb-4c34-9423-cac8c091b881",
        name: "Courage Ottawa - Social Systems Decomposition",
        types: [DOCUMENT_FILE],
        link: "https://docs.google.com/document/d/1e5YYBF_t00G2FIsrRGDaOqoHM4Py-hI-i2MoHWsI-Gs/edit?usp=sharing",
      },
      {
        id: "14efcec6-7538-4d39-907e-4a17de81e72d",
        name: "Courage Ottawa - New Campaign Proposal Template",
        types: [DOCUMENT_FILE],
        link: "https://docs.google.com/document/d/1cbPhFBdxWd6l8yyxhKDjfUhpTBeYbjgWRlsBVSN_uFg/edit?usp=sharing",
      }
    ]
  }
};
