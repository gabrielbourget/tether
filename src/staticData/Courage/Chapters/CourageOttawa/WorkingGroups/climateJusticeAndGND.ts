import { SPREADSHEET_FILE, DIRECTORY } from '../../../../../constants';

import { WorkingGroup } from "../../../../../Types/courageTypes";

export const climateJusticeAndGNDData: WorkingGroup = {
  id: 'e027dce0-722a-46f2-8516-2b3a6e5e177c',
  name: 'Climate Justice / GND',
  description: '',
  coChairs: [
    { id: '5e88faf4-263d-4d3f-b98c-0534c3b1c317', name: 'Silas Xuereb' }
  ],
  members: [
    { id: '5e88faf4-263d-4d3f-b98c-0534c3b1c317', name: 'Silas Xuereb' },
    { id: '74df8f54-9892-492a-b4c5-7bce88605e45', name: 'Fred Langlois' },
    { id: 'b1df823f-6da4-4660-9c30-a8fc7a34adfb', name: 'Gabriel Bourget' },
  ],
  socials: {
    id: '',
    name: '',
  },
  communications: {
    id: '08ebbce3-3b20-42bd-86e9-ae988f698bdb',
    name: 'Communications',
    types: [DIRECTORY],
    children: [
      {
        id: '76a20383-e9ea-46c5-a08f-f8a62465ba9a',
        name: 'Slack',
        link: 'https://www.independentleft.slack.com',
        children: [
          {
            id: 'ff7d3e90-7752-4f76-9bc4-9cd0e7a9a5eb',
            name: 'Slack Channels',
            children: [
              {
                id: 'cca8e665-1a52-4b0e-89fa-da1e8773b372',
                name: '#ott_environment_organizing'
              }
            ]
          }
        ],
      }
    ],
  },
  resources: {
    id: '4a4e8841-a544-4cc4-8d46-190249f90d01',
    name: 'Resources',
    types: [DIRECTORY],
    children: [
      {
        id: '2981f548-2e9e-4ed8-b094-7b673c690000',
        name: 'Organization Tracking - Climate Justice / GND Working Group',
        types: [SPREADSHEET_FILE],
        description: 'Spreadsheet containing structured information on groups of interest involved with climate justice, and related topics.',
        link: 'https://docs.google.com/spreadsheets/d/1FLBkzyzczzgJt2RNfKeDGGJCePNTNoentF9J7sb3I8k/edit#gid=0',
      },
      {
        id: '',
        name: '',
        description: '',
        link: '',
      },
    ],
  },
};
