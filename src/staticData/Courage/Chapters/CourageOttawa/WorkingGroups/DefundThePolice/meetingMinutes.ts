import {
  DOCUMENT_FILE, DIRECTORY, IMAGE_FILE, GOOGLE_DRIVE,
  WORD_DOCUMENT_FILE, SLIDES_FILE, POWERPOINT_FILE, ILLUSTRATOR_FILE,
  PNG_FILE
} from '../../../../../../constants';

export const meetingMinutes = {
  id: 'aee7bc64-057a-4fe6-9f93-e17becf696f0',
  name: 'Meeting Minutes',
  types: [DIRECTORY],
  children: [
    {
      id: '40728199-0e6a-497c-8412-ff43b60570ca',
      name: 'Meeting Minutes - Directory',
      types: [GOOGLE_DRIVE],
      link: 'https://drive.google.com/drive/folders/15leRC9npg9o3ZeIDGyr9zE-z5xxOmitH?usp=sharing',
    },
    {
      id: '7e8b7ed8-8ad1-494b-928e-fecd4702a1a9',
      name: '2020',
      types: [DIRECTORY],
      children: [
        {
          id: 'fb4cb5b1-93eb-4c90-be79-4bf605a12a43',
          name: '2020 - Directory',
          types: [GOOGLE_DRIVE],
          link: 'https://drive.google.com/drive/folders/1suZtwhHyWXFbgd1JerxbeOpOTI2IDa5c?usp=sharing',
        },
        {
          id: 'b842d4b0-2bc2-4b5c-ab3d-3e712c947eb8',
          name: 'September',
          types: [DIRECTORY],
          children: [
            {
              id: 'a1d80c08-f99f-4de0-bf4e-412b7c03c321',
              name: 'September - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/12qK-r11SoArzh7PiJ2zqBWo_Ni0t4yO9?usp=sharing',
            },
            {
              id: 'de2af635-a523-4e1f-b3cd-3d87c054dbb1',
              name: 'September 20',
              types: [DIRECTORY],
              children: [
                {
                  id: '11648a78-2f4b-4d13-a244-d201fb521f8b',
                  name: 'September 20 - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1Eqr0lENjye58Kw4AL0CmpYV6MQx-bbIx?usp=sharing'
                },
                {
                  id: '52cad1c8-d08e-4cbb-b56f-5c85aabda63f',
                  name: 'Courage Ottawa - Defund the Police WG - Meeting Minutes - September 20, 2020',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1CjSPsq1N1tfUfGtvat1olFvG3xZP_xu0HRt_SyZlZ6c/edit'
                },
                {
                  id: '98854fcb-15c3-4609-a2bf-a661044e671f',
                  name: 'Defund the Police - Campaign Brainstorming - Report',
                  types: [WORD_DOCUMENT_FILE, DOCUMENT_FILE],
                  link: 'https://drive.google.com/file/d/1te40QTw5eekxYi86P2qvsn_gBxI7GFqy/view?usp=sharing'
                },
                {
                  id: '1abe0aa1-f47f-4fd8-a90d-79d0fb0c499b',
                  name: 'Defund the Police - Campaign Brainstorming - Slides',
                  types: [POWERPOINT_FILE, SLIDES_FILE],
                  link: 'https://drive.google.com/file/d/1Di3OXEWxoYv8zNXr8j6J5RDG3eXE-cHx/view?usp=sharing'
                },
                {
                  id: '7bb96898-616d-489f-a42d-e96a62020e46',
                  name: 'Defund the Police - Campaign Brainstorming - Design Artboard',
                  types: [ILLUSTRATOR_FILE],
                  link: 'https://drive.google.com/file/d/1t8T5VpKbtSBudTmfumMH2I0OspZn2sKB/view?usp=sharing'
                },
                {
                  id: 'f2de44f2-245c-475e-9d3f-5902942624cf',
                  name: 'Defund the Police - Campaign Brainstorming - Visual Assets',
                  types: [DIRECTORY],
                  children: [
                    {
                      id: 'b477330c-6ad5-4d33-9b02-7ec313a12750',
                      name: 'Communications Scenarios',
                      types: [IMAGE_FILE, PNG_FILE],
                      link: 'https://drive.google.com/file/d/1-7TOoeuwk_MiKapB1Z44yVm7RMav1asR/view?usp=sharing'
                    },
                    {
                      id: '43b5b642-1f0b-46cb-a759-b2bb966f3713',
                      name: 'Ottawa 2022 Municipal Budget Transformation',
                      types: [IMAGE_FILE, PNG_FILE],
                      link: 'https://drive.google.com/file/d/1N2MFjHSRkvMIgJb95EoVFExTAyAVeVBw/view?usp=sharing'
                    },
                    {
                      id: '8564c6e1-3b57-40ab-b039-d0675b99885c',
                      name: 'Interactive Timeline of Relevant Events',
                      types: [IMAGE_FILE, PNG_FILE],
                      link: 'https://drive.google.com/file/d/1RiX24zQ-C1XVvYI_xrPxuueH8-UZaJO3/view?usp=sharing'
                    },
                  ]
                },
              ]
            }
          ]
        },
        {
          id: "c1d5d606-ddcd-4325-864e-c36160f2fda4",
          name: "October",
          types: [DIRECTORY],
          children: [
            {
              id: "8c9806b3-82b2-4bd9-bf62-3f190467dd1f",
              name: "October 11",
              types: [DIRECTORY],
              children: [
                {
                  id: "7c5f2d78-b088-4ef0-9c04-4d49626f30e3",
                  name: "Courage Ottawa - Defund the Police WG - Meeting Minutes - October 11, 2020",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1Jp31Wn1PW1WqGJDGEBJHKmnPr0A2ThPM98vT2IQJvxo/edit?usp=sharing",
                }
              ]
            },
            {
              id: "43f3d6f3-218f-4217-93b6-4cafca526b71",
              name: "October 25",
              types: [DIRECTORY],
              children: [
                {
                  id: "abc496b1-b17e-4e14-909a-f1157ca75eaa",
                  name: "Courage Ottawa - Defund the Police WG - Meeting Minutes - October 25, 2020",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1LlHKo_ZvBk147MuYGkbAO0ufIx1MQ7dZELxBwGKFmH0/edit",
                },
                {
                  id: "2bca40f4-c9cf-498b-a2c2-50f1edb22b72",
                  name: "Potential Coalition Formation",
                  types: [IMAGE_FILE, PNG_FILE],
                  link: "https://drive.google.com/file/d/1B_N4zNNGhcXY_hInzjgIrd46cKyvv_A8/view?usp=sharing"
                },
                {
                  id: "9eefac1e-d485-4748-bdba-290f26241b42",
                  name: "Potentially Tiered Initial Outreach Strategy",
                  types: [IMAGE_FILE, PNG_FILE],
                  link: "https://drive.google.com/file/d/1iT1NHqT2dVicb2VFMLqWQKcBQ_72c_g2/view?usp=sharing",
                }
              ]
            },
          ]
        }
      ]
    }
  ]
};