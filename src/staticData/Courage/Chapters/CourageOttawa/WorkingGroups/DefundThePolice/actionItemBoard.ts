import {
  DOCUMENT_FILE, DIRECTORY, GOOGLE_DRIVE, PDF_FILE, SPREADSHEET_FILE
} from '../../../../../../constants';

import { Resource } from "../../../../../../Types";

export const actionItemBoard: Resource = {
  id: "a1c46737-e8ee-492b-bbb7-04baf14d8a5c",
  name: "Action Item Board",
  types: [DIRECTORY],
  children: [
    {
      id: "003cca8f-8131-4015-9230-bb5e0fad8321",
      name: "Action Item - Template Document",
      types: [DOCUMENT_FILE],
      link: "https://docs.google.com/document/d/1k-gTDqh3sQsq2BmOTw4_Wal-Q_Z5yxEOKmkK07Zo0Bg/edit",
    },
    {
      id: "32f28b1c-2f3f-424d-a59e-fc508082040b",
      name: "Potential Action Items - Summary List",
      types: [DOCUMENT_FILE],
      link: "https://docs.google.com/document/d/19d0QPrGKb-DPdIS2dTEsjlXFMO78wWM4LVM_Qrskh0o/edit",
    },
    {
      id: "7a58c802-1f1f-472c-872f-4775a6721694",
      name: "Action Items - Skill Bank",
      types: [DOCUMENT_FILE],
      link: "https://docs.google.com/document/d/1j2bUURa5kyZAydx_fAQVFGnlVUfN-PqsTEVa8520Wfo/edit",
    },
    {
      id: "00f454c4-0a05-4993-937d-1a6534755482",
      name: "Potential Action Items",
      types: [DIRECTORY],
      children: [
        {
          id: "bb698a3e-4aa8-4fb8-8c3a-a3ac3ab5c3f6",
          name: "Documentation of the Ottawa Police Service’s Real Estate Profile",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1qgEOyabmZM3brEsLdrIFO9HoxijCoX8z4fMql-lbf9g/edit",
        },
        {
          id: "99b430c9-754d-4424-b32c-6172fc0c5c2c",
          name: "Documents to Develop Further Resources On",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/11wXoh6uSlfm_QGbzo6mbvE5oz6cyX9dA_Nw4_NavO34/edit",
        },
        {
          id: "90ac5197-f8d8-4a79-b380-4794b6f545b6",
          name: "Timeline/Flowchart of What a Successful Process Looks Like",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/18BJMDeCb_ym6mymJHelarQnlcIekrqByPALp_NESrwE/edit",
        },
        { 
          id: "3a0209fb-c91a-461a-bf7f-b1041c5490a1",
          name: "Further Analysis of the OPS 2020-2029 Capital Forecast",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1OrMJKzsWMjzCmt0rWTUg7yYMidI_altUkUVNtYNpuzI/edit",
        },
        {
          id: "c2158096-52d1-4a69-8792-0b3e888e72f3",
          name: "Research on the Relationships/Agreements that the Police Service Has With Other Institutions in the City and Beyond",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1ZzxzdL8M7dQ7fAbDBhcRzz5ajLMgOr8nv-Uegqk-K8I/edit",
        },
        {
          id: "45c6156e-69d7-41be-b4c8-ae18c9ec5bd6",
          name: "Mapping Out Key Factors That the OPS Points to In Order to Justify Current Funding Levels and Proposing Additional Funding",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/16BmAeskKiiqyec7hlNbJx9fVAtT7H7xGSKmUD3eg0TE/edit",
        },
        {
          id: "8320aba0-608f-40eb-9ab9-1104625ae3d0",
          name: "Research On the OPS Outreach and Recruitment Strategy",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1oaHU4K6xnfKHqq6h5sGh63viBoRltN3d9t3jBNINQmA/edit",
        },
        {
          id: "b0a6cdef-3336-4d23-bfb3-9f99ec99b122",
          name: "Research On the Categorizations Given to Expenditures by the Police Service",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1G_gp3u36Rgvp7q1wxdutjlQvRsZv-9qHzJgEaFJeDNE/edit",
        },
      ]
    }
  ]
}