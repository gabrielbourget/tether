import {
  DOCUMENT_FILE, DIRECTORY, GOOGLE_DRIVE, SPREADSHEET_FILE
} from '../../../../../../constants';

import { WorkingGroup } from "../../../../../../Types/courageTypes";

import { meetingMinutes } from "./meetingMinutes";
import { politicalOutreachAndStrategyData } from "./WorkStreams";
import { generalResourceCommons } from "./generalResourceCommons";
import { coalitionMemberResources } from "./coalitionMemberResources";
import { actionItemBoard } from "./actionItemBoard";
import { powerMapping } from "./powerMapping";
import { communications } from "./WorkStreams/Communications/communications";

export const defundThePoliceData: WorkingGroup = {
  id: 'd74f346a-46d7-4ce2-98ef-c076a60f0412',
  name: 'Defund the Police',
  description: 'A working group formed to develop the possibility of a coalition of groups in the city with the goal of significantly defunding/diverting the Ottawa Police Service in the context of flowing those newly released funds into alternatives that better suit community desires and needs.',
  coChairs: [],
  members: [],
  socials: {
    id: '',
    name: '',
  },
  communications: {
    id: 'c8ea6a6c-e2d5-4e70-984c-d9089479a09d',
    name: 'Communications',
    types: [DIRECTORY],
    children: [
      {
        id: '0d07dcda-75f5-4b12-a0de-c2ca2f220272',
        name: 'Slack',
        link: 'https://www.independentleft.slack.com',
        children: [
          {
            id: '016c6094-ad48-4360-8558-a77702926ae3',
            name: 'Slack Channels',
            children: [
              {
                id: '214f87ec-44a4-47a6-8390-355c691258f7',
                name: '#ott_defund_police'
              }
            ]
          }
        ]
      }
    ]
  },
  resources: {
    id: '64a8bc22-6237-43a9-9498-638f470c5398',
    name: 'Resources',
    types: [DIRECTORY],
    children: [
      {
        id: "8ca6f1e2-138c-4ef4-9847-82a676de4302",
        name: "Defund the Police 2022 Campaign - Resource Index",
        types: [DOCUMENT_FILE],
        link: "https://docs.google.com/document/d/1C02druHzkOz1sN4ALPaK7B-v_lGB-ElsLLXshXb4H54/edit?usp=sharing",
      },
      { ...meetingMinutes },
      {
        id: "a6adf1a5-944a-49e1-97a9-7c01773c6135",
        name: "City of Ottawa - Alternative Budget 2021",
        types: [DIRECTORY],
        children: [
          {
            id: "02d20be0-53ef-4200-997f-346f74d14173",
            name: "City of Ottawa - Alternative Budget 2021",
            types: [DOCUMENT_FILE],
            link: "https://docs.google.com/document/d/1ExP4tCYqTrPVrg0_ghRgI7ONzNvuTB6zMnM9w-0mkhE/edit"
          },
          {
            id: "093dbd78-0d3b-4965-958e-70ed674d3b6b",
            name: "City of Ottawa - Alternative Budget 2021 - Abridged Notes",
            types: [DOCUMENT_FILE],
            link: "https://docs.google.com/document/d/1tNHdsetsQmnauvwHd_IjX_v0q3kHstdUzJTFiahE7cw/edit?usp=sharing",
          },
          {
            id: "4244435b-56a1-4019-9982-88f79cd3ce7a",
            name: "City of Ottawa - Alt Budget 2021 - Major Challenges and Obstacles to Passing/Implementing It",
            types: [DOCUMENT_FILE],
            link: "https://docs.google.com/document/d/1EMtksJyQyOy8T6zFSbkmQejkl4EbPzQwG6FzeyxDE-E/edit?usp=sharing"
          },
          {
            id: "a2966196-3f0d-4354-90a4-c3a9726f515c",
            name: "City of Ottawa - Alternative Budget 2021 - Data Tables",
            types: [SPREADSHEET_FILE],
            link: "https://docs.google.com/spreadsheets/d/1Kf8N9kQE-6_cOxNsrIWthpcV1G7rWBiR0Gd5XFmgR6A/edit?usp=sharing",
          }
        ]
      },
      { ...generalResourceCommons },
      {
        id: '208afdd1-4939-47c8-a0ca-0082cb0529a9',
        name: 'Coalition Resources',
        types: [DIRECTORY],
        children: [
          {
            id: '839a51b5-4104-4b21-9f05-865d0a4bd275',
            name: 'General Coalition Resources - Directory',
            types: [GOOGLE_DRIVE],
            link: 'https://drive.google.com/drive/folders/19IeF7rA5pvSV824oZLEGwmvT97KOdviD?usp=sharing',
          },
          {
            id: '15803c4c-2aac-497a-9937-6c2e649b0f2d',
            name: 'Coalition Overview',
            types: [DOCUMENT_FILE],
            link: 'https://docs.google.com/document/d/1VhcQB_tgJurLIcO9HOVsYWOPdvVLJJIJ3XocfTHE3b4/edit?usp=sharing',
          },
          {
            id: "d641ec40-27dc-45c9-8cea-e4d42fb828f1",
            name: "Defund the Police 2022 Campaign - Potential Coalition Members Overview",
            types: [SPREADSHEET_FILE],
            link: "https://docs.google.com/spreadsheets/d/1x7g4gjNFXBPFSTYgoidndqv0ZaEtgNK7JwZmHCMfxl4/edit#gid=0"
          },
          {
            id: "432d5c19-b894-4cb4-be46-ca38a4435e97",
            name: "Defund the Police 2022 Campaign - Research Leads for Potential Coalition Partners",
            types: [DOCUMENT_FILE],
            link: "https://docs.google.com/document/d/1ReyR3PY7URBXkSpWq-xO61rEKY79VCQ6yz7Tkg9pxA4/edit?usp=sharing",
          },      
          {
            id: 'a0abfc19-fd95-441e-8be4-e5ab82eff9c2',
            name: "Initial Coalition Outreach",
            types: [DIRECTORY],
            children: [
              {
                id: 'b083037e-89ff-48ca-b3d0-b752837fc193',
                name: "Defund the Police 2022 Campaign - Coalition Outreach Email - Brainstorming",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/1MWR21XyIlSDAN9MdqLtWBsnd9DCP9K0hMWlv8GokJQQ/edit?usp=sharing"
              },
              {
                id: "586a1f50-c1fb-4f91-aded-c6e80754a89e",
                name: "Defund the Police 2022 Campaign - Coalition Outreach Email - Draft",
                types: [DOCUMENT_FILE],
                link: "https://docs.google.com/document/d/1U2ayGzmybB2Wmc798mIFhGlQO40QkmgH1Md45ZkpEE0/edit?usp=sharing"
              },
            ]
          },
          {
            id: "26c3548e-718e-4565-b0ac-998f8ad8d80f",
            name: "Potential Coalition Member - Background Report Template",
            types: [DOCUMENT_FILE],
            link: "https://docs.google.com/document/d/1szi5c_vRgdaRppq0p2Xz9Sp8Gz_zbVKgKl7nyE0H0YI/edit?usp=sharing"
          },
          { ...coalitionMemberResources },
        ]
      },
      {
        id: '04da2b68-f79c-45ff-8d50-ac3683d582d2',
        name: 'Work Stream Directories',
        types: [DIRECTORY],
        children: [
          {
            id: '94995ef0-a944-4109-a50b-bfc0e0fee30c',
            name: 'Work Stream Directories - Directory',
            types: [GOOGLE_DRIVE],
            link: 'https://drive.google.com/drive/folders/1LrLf3H2ly6SsUYZ6ldxZo5jRsos9oEB0?usp=sharing',
          },
          { ...politicalOutreachAndStrategyData },
          {
            id: 'ee008981-f8f1-48fa-ac8e-0927cd3af53f',
            name: 'Communications',
            types: [DIRECTORY],
            children: [...communications],
          },
          {
            id: '690078ea-d82b-46a4-9ec8-bf95e0f8e6a1',
            name: 'Design',
            types: [DIRECTORY],
            children: [

            ]
          },
          {
            id: 'fa3f2d7f-3889-42e4-8c98-5ff98b5633bd',
            name: 'Legal/Policy Research',
            types: [DIRECTORY],
            children: [

            ]
          },
          {
            id: '93739865-57d4-45a3-a577-a981975f75a6',
            name: 'Financial Analysis',
            types: [DIRECTORY],
            children: [

            ]
          },
          {
            id: '1121b332-800c-4e15-8cbb-9c6f0471bc37',
            name: 'Direct Action/Demonstrations',
            types: [DIRECTORY],
            children: [

            ]
          },
        ]
      },
      { ...actionItemBoard },
      {
        id: "75d90ae4-e712-49f5-a1a0-3e2e75a83764",
        name: "Initial Coalition Meeting Contribution Brainstorming",
        types: [DOCUMENT_FILE],
        link: "https://docs.google.com/document/d/1DKpmraeJZY0lQT1sYKsyKIrVr8Me0sKSXA6r5jxsRJ4/edit?usp=sharing",
      },
      {
        id: "f473e095-29fe-477d-b9fc-3884d0fd75ab",
        name: "Chapter Campaign Priorities Brainstorming",
        types: [DOCUMENT_FILE],
        link: "https://docs.google.com/document/d/1PDH01ObFREdGMS0Ps9lZFAGxctblOhRFScVUEd9Lo1Q/edit?usp=sharing"
      },
      {...powerMapping}
    ]
  }
};
