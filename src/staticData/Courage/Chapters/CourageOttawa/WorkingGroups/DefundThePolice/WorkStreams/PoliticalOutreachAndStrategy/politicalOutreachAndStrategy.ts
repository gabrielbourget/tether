import {
  DOCUMENT_FILE, SPREADSHEET_FILE, DIRECTORY, GOOGLE_DRIVE,
} from '../../../../../../../../constants';

export const politicalOutreachAndStrategyData = {
  id: '9b67dcb5-52ac-4c47-a13d-babf0d6b5591',
  name: 'Political Outreach and Strategy',
  types: [DIRECTORY],
  children: [
    {
      id: '3a8e61fc-58ce-4477-973a-a40d966bfbbd',
      name: 'Political Outreach and Strategy - Directory',
      types: [GOOGLE_DRIVE],
      link: 'https://drive.google.com/drive/folders/1suAuSz0A8X3W5ga6uj9qOcW2TWMZ-yQ7?usp=sharing',
    },
    {
      id: 'ae4891da-7eac-4600-a4be-52cc913f6a69',
      name: 'City of Ottawa - Municipal Election 2022 - Candidate Slate',
      types: [SPREADSHEET_FILE],
      link: 'https://docs.google.com/spreadsheets/d/1inKwlOpRBUcBb90vUQPOVVjQWMnI2edParHhqZE-UgI/edit?usp=sharing'
    },
    {
      id: '2e348bcf-0560-490c-a5cb-b2012d1a6806',
      name: "Political Candidate Background Report - Template",
      types: [DOCUMENT_FILE],
      link: 'https://docs.google.com/document/d/1E1bJMdFtfCnz8ZFVhNYlMzio8maWLLC_YHmaJX576Us/edit?usp=sharing',
    },
    {
      id: '962ada7c-a52e-4621-8480-57fe7af51714',
      name: "City of Ottawa Standing Committee Background Report - Template",
      types: [DOCUMENT_FILE],
      link: 'https://docs.google.com/document/d/1M5pVB_iVUfImz4s_rIRoWcZD7AOiSgRkRutPX6tcwJs/edit?usp=sharing',
    },
    {
      id: '335f48d7-b561-4064-b48d-7679a01cdefc',
      name: 'Candidate Resource Directories',
      types: [DIRECTORY],
      children: [
        {
          id: 'e9fb05f4-2157-486d-846a-1bbf346c3cdc',
          name: 'Candidate Resource Directories - Directory',
          types: [GOOGLE_DRIVE],
          link: 'https://drive.google.com/drive/folders/1xAqDeBNKey6m8NNSuwKN1tsghoEVDYls?usp=sharing'
        },
        {
          id: '67832057-e4b2-4f36-934b-0fbe5adafe7c',
          name: 'Mayor - Ottawa',
          types: [DIRECTORY],
          children: [
            {
              id: 'f0bed869-83b2-495a-a56c-6e0ba377a336',
              name: 'Mayor - Ottawa - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1FR5fEp7je4mjzEAmsCdEojSAWjNanu0w?usp=sharing',
            },
            {
              id: '7b808957-4f93-464e-88e4-777a71d6bb4a',
              name: 'Jim Watson - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: 'd5c60d0c-bfe3-4ada-be00-b7943940d088',
                  name: 'Jim Watson - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/16LUIJHsJp1lANcupMtfXdIoWS1LKUBZM?usp=sharing',
                },
                {
                  id: 'd469b080-4dc4-4dfb-a654-f749a64c06b8',
                  name: 'Jim Watson - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1qmQbrDFn4lAEi4Xd-ZLsYcIGIIIQDwv3r21oVUc85-U/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: '4d45cea4-7a1c-4f8a-807a-ddd156e06814',
          name: 'Counsillor - Kanata South',
          types: [DIRECTORY],
          children: [
            {
              id: '5e56a271-ef54-473b-8c5c-b7674a3f5da3',
              name: 'Counsillor - Kanata South - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1cb0bxCyn3-Vzd_AQ3ypWk_dShJ9pNcV3?usp=sharing',
            },
            {
              id: '6fab6ba0-0310-4f27-bb77-3ff26dd8be79',
              name: 'Allan Hubley - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '4d19b970-d7a0-4a80-be6a-73a9c71869b2',
                  name: 'Allan Hubley - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1fp51nBoGm4XdUEyLvsMxA55OS-8zTf1n?usp=sharing',
                },
                {
                  id: 'f2f67cf4-8024-4ea1-b2bd-0ec3902038d4',
                  name: 'Allan Hubley - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1j8Ymwz7qKfv0wvi34dXYgSwaAyvd7WgqmRozdraKHeI/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: '9e3a6ed0-abff-4c14-99dd-d9b0f018ab00',
          name: 'Counsillor - Barrhaven',
          types: [DIRECTORY],
          children: [
            {
              id: '89471e55-efb6-450f-ba5e-f7a7c837d8dc',
              name: 'Counsillor - Barrhaven - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1KK-4B4RAsehVAxjZZ_f69rAOuKETGTgE?usp=sharing',
            },
            {
              id: '81c0df95-80de-464f-8586-4803651fa9c3',
              name: 'Jan Harder - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '7be09cee-7c12-467f-8788-70efa968ca58',
                  name: 'Jan Harder - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1e0L39qlDt_LQotCKrS3Sd1cc9UBtbCCe?usp=sharing',
                },
                {
                  id: 'a26be823-e74a-4a42-ac6d-9cc61484915c',
                  name: 'Jan Harder - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1faFrX5PQaS0PQfEjh2a5ms_K9qVVdsAH8UeJf1LAsyg/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: 'f324d4e0-61ac-4233-9c6e-e2b2b8dd17bd',
          name: 'Counsillor - Cumberland',
          types: [DIRECTORY],
          children: [
            {
              id: '4b6a6ee0-6575-46e6-8d42-2184df9bb964',
              name: 'Counsillor - Cumberland - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/14X0WXN_8PV2vPANDJCUgUFGxYm_JAqNR?usp=sharing',
            },
            {
              id: "c27cae31-3d6a-4b44-a671-761263ec672f",
              name: "Catherine Kitts - Resources",
              types: [DIRECTORY],
              children: [
                {
                  id: "2999a237-39ff-4652-8b7d-b6d3f018792b",
                  name: "Catherine Kitts - Background Report",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1kNdZMhAHTCzKko7hn0hG6I76tv4yOkMBQYNB2_jnWBM/edit?usp=sharing",
                }
              ]
            }
          ],
        },
        {
          id: '3c6cf85c-7e02-4160-81bb-1f38e437e3be',
          name: 'Counsillor - Beacon Hill - Cyrville',
          types: [DIRECTORY],
          children: [
            {
              id: 'f5d72fe6-281c-4cb9-8683-290b60c14ab6',
              name: 'Counsillor - Beacon Hill - Cyrville - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1138u8V9tYb1ydYa81xO6c7mWJA84GAv9?usp=sharing',
            },
            {
              id: 'fe504f8b-6060-4e70-b0c9-e1e59951889b',
              name: 'Tim Tierney - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: 'e1dbb5da-6fb8-4139-a0b0-8179dc7ecd59',
                  name: 'Tim Tierney - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1JKHrSeNB7fV8K4hDFipkHc8TeE94X8qO?usp=sharing',
                },
                {
                  id: '60faf82b-93c0-4e1e-949e-583ae01819eb',
                  name: 'Tim Tierney - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1jJC1MXRQYxJzjXtXFp16hH992exQCFvtdcsdMGKJ74g/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: '0fd1ef67-64c9-4e17-adfa-004f682a1640',
          name: 'Counsillor - Osgoode',
          types: [DIRECTORY],
          children: [
            {
              id: '2dc10806-33ac-4d53-9014-26c991a6a3be',
              name: 'Counsillor - Osgoode - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1NV8lTjrcODLz_OT7-E9HVvhoUq-Yuiak?usp=sharing',
            },
            {
              id: '34ab469e-a938-4a77-8bfa-a33aff5f9e4e',
              name: 'George Darouze - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: 'd21e1c71-c4e3-45a0-8f43-7da95149e317',
                  name: 'George Darouze - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/165D6fLMdjGGlPe3SwTnIL5Sjgju7L7pS?usp=sharing',
                },
                {
                  id: 'b7cc1912-7920-47f1-a827-ec8637634e36',
                  name: 'George Darouze - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1dvpKeyIt0khKmbQbx7_rAIIH9RGoYXurR0EKjRDC7sY/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: 'c9238e7d-0655-4556-ac40-84e1fbea6b05',
          name: 'Counsillor - West Carleton - March',
          types: [DIRECTORY],
          children: [
            {
              id: '18dc6ca1-d0d6-45f5-b372-3cd6a354bf99',
              name: 'Counsillor - West Carleton - March - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1GaGw-lTSHgmqC1StB2a8wydMKHqNO5N_?usp=sharing',
            },
            {
              id: '2c08c44f-c741-41df-b6fc-2c54d4acbd27',
              name: 'Eli El-Chantiry - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: 'fd3aebc4-acf8-4243-b0fa-a20e1db7c136',
                  name: 'Eli El-Chantiry - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/16AFILldnPdBVpjfgys69LGAB0RBBMT1I?usp=sharing',
                },
                {
                  id: '7f4288f9-5b25-49f7-9cac-e666f6d72226',
                  name: 'Eli El-Chantiry - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1YbUB_nmkikJc8jFrsBIhNbut5fvPqXnjc3VLhkzZmrE/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: '49272a5c-c894-44f9-acdf-a5c79f127715',
          name: 'Counsillor - Rideau - Goulbourn',
          types: [DIRECTORY],
          children: [
            {
              id: '20ee9547-47c4-4932-873e-8c8e8bd445a6',
              name: 'Counsillor - Rideau - Goulbourn - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1W7iF0bKeqqaiV3aGXMKnNJzltjnsvtVx?usp=sharing',
            },
            {
              id: 'dfaf92ad-d442-4fac-850e-43fd6686da72',
              name: 'Scott Moffatt - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '0cf51ae1-9b36-40c5-9285-6cfcf2d64248',
                  name: 'Scott Moffatt - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1EK-VeSNWJsG2ztASk955ywjV1bb5bP1k?usp=sharing',
                },
                {
                  id: 'b2bc2425-c651-42fb-a27a-c7da643d07a3',
                  name: 'Scott Moffatt - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1QhbGgnkJ02ngjOQVJAWI4rtQA7cGmUZxpVZ3vN1QBHk/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: '5108954d-cea3-45e5-adbb-acd5d88c823d',
          name: 'Counsillor - College',
          types: [DIRECTORY],
          children: [
            {
              id: '86f4bf37-020f-4d3d-9d60-aba05333cf7e',
              name: 'Counsillor - College - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1l0l6dM5m1rxcUTvZ53cnrhVUYmLJRkgg?usp=sharing',
            },
            {
              id: '52f59335-9d46-463e-b7a9-1ab999734d12',
              name: 'Rick Chiarelli - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '89b81edf-e0ba-4858-af1c-10f656ca4f50',
                  name: 'Rick Chiarelli - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1AnsTnBDeeXmTQ7NmoxiVDF73XQADNy1B?usp=sharing',
                },
                {
                  id: '8858f077-b1f8-44fc-b176-1efb5021b421',
                  name: 'Rick Chiarelli - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1P_tYaoWs6LCT4dBIjjiYw5awfF8QQ74tZ-O9bcbDmTw/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: '885c09d7-49ab-488a-bd68-9e065ed5a905',
          name: 'Counsillor - Gloucester - Southgate',
          types: [DIRECTORY],
          children: [
            {
              id: 'f79fb45d-7817-4b92-b650-b5a47779aaed',
              name: 'Counsillor - Rideau - Goulbourn - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1jF_Ztfg0GIejeGm-UUJwCRq4xaU6DGJU?usp=sharing',
            },
            {
              id: 'c44ad082-0a21-4f8e-b881-5a188bec2ba4',
              name: 'Diane Deans - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '0e32fc3d-8eec-42ab-b8b7-fc8f674f52e4',
                  name: 'Diane Deans - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1sr2pKna0umz-rPFO2auHERMHlNla2qiY?usp=sharing',
                },
                {
                  id: 'b375acc5-7b8f-47a9-aa11-e17786fef713',
                  name: 'Diane Deans - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1q8L2iQysP-H1EUuGI9jdgb_vpMuDtonqcy8hWxOsOCQ/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: '9c2cbc6b-9b59-40bc-8406-7dbb678205ea',
          name: 'Counsillor - Gloucester - South Nepean',
          types: [DIRECTORY],
          children: [
            {
              id: '64a3feac-7ea1-41b0-b897-cd30e7b561e4',
              name: 'Counsillor - Gloucester - South Nepean - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/12nmLRWAbJc4Yl-ArLQ-M2bka6mI1V91R?usp=sharing',
            },
            {
              id: '3c79a9fc-81a2-491d-b2a6-d0ef38cdb6ac',
              name: 'Carol Anne Meehan - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '8607883a-c927-484d-af6d-89a7327963e7',
                  name: 'Carol Anne Meehan - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1Ux2ty6E7b7_Dz36GC4ZAqJrU534T4R6i?usp=sharing',
                },
                {
                  id: '934e9601-7e62-4e46-8909-dbc18a8c528b',
                  name: 'Carol Anne Meehan - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1lPJXz4NpretCWxCE1uwTvz7SwliZl5FNQoUo8wIIykc/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: 'a62f2088-701f-40a2-8536-da66527f652a',
          name: 'Counsillor - Alta Vista',
          types: [DIRECTORY],
          children: [
            {
              id: '8d69deb1-5062-4682-b0cd-bb28a8dd7f41',
              name: 'Counsillor - Alta Vista - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1RY3by7mznzCCTkJX4RXtWFSF93GmKbD3?usp=sharing',
            },
            {
              id: '771c884f-a98d-447c-a340-c0bac001ecbe',
              name: 'Jean Cloutier - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: 'c2b46e3e-b6c2-4233-8d6f-e4a01756ba3a',
                  name: 'Jean Cloutier - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/145ZQj2lRdUVltPQgKTa1bAIKTXhL6kRj?usp=sharing',
                },
                {
                  id: '69e4aa84-8498-42ec-8daf-bcaaf1d2516d',
                  name: 'Jean Cloutier - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1SSLZqTc51OeHQosveZ72BOENb1ChldHFt6DauMVUJA8/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: 'e3b7ec2e-3616-48e3-a0c9-3c60c1efb96d',
          name: 'Counsillor - Knoxdale - Merivale',
          types: [DIRECTORY],
          children: [
            {
              id: '9665c07c-918b-4028-aa53-d309780928eb',
              name: 'Counsillor - Knoxdale - Merivale - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/18R_Smwwi-U3MXO2JzFvfbYXhYNfnHJ-I?usp=sharing',
            },
            {
              id: 'c0219d26-cba5-4383-9dff-d2ac844e7214',
              name: 'Keith Egli - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '3a85dc07-3435-4822-be82-b9f21b514439',
                  name: 'Keith Egli - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1IQ45w5B_HIsIAl67UwXapu88Py36myy1?usp=sharing',
                },
                {
                  id: 'e456f88e-c80e-491e-8ed5-08967fe4ebc3',
                  name: 'Keith Egli - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1mmNZSefUn1_vTWL9qsy9hMWl3Xr7SkyTj1PyiKKyqtg/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: 'a628bfef-ee0e-41ec-b9d8-3b7f38e4621e',
          name: 'Counsillor - River',
          types: [DIRECTORY],
          children: [
            {
              id: '47ce0a35-9a53-4e55-b5cb-eab15590c9a8',
              name: 'Counsillor - River - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1HCCcExmkOs-lIY42fT35GUE4VVXlFGCi?usp=sharing',
            },
            {
              id: '87563663-13c2-48b3-a59a-5d5ea2a14f2a',
              name: 'Riley Brockington - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '5c521b34-3b86-48c9-bc7a-7ec79b0f4212',
                  name: 'Riley Brockington - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1iQLHQBw1-ugGBcsh6MVkSuiwUGaEgpZn?usp=sharing',
                },
                {
                  id: 'fc6cf3d4-5251-4893-a4ae-3d8f4dbbf2e0',
                  name: 'Riley Brockington - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1Gt1Oqywy5MkcpEUCwo5_r9Fy89x_Kme0eD58bJRI_ZI/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: '1e335c28-0cab-4081-b53f-2805ad9960c6',
          name: 'Counsillor - Innes',
          types: [DIRECTORY],
          children: [
            {
              id: '722c9c68-1cef-41aa-995d-2e5e30afa915',
              name: 'Counsillor - Innes - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/145gccJr-qqz-5uq8O0c6HVgW_T7vy27Q?usp=sharing',
            },
            {
              id: '2ffb2114-d048-4ed6-8c49-03ab26c3d281',
              name: 'Laura Dudas - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '6fc46a44-060f-4653-82b7-1a0339238318',
                  name: 'Laura Dudas - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1I5N3XFSmiSr0sEAz_WE9uRAcI21T9QoY?usp=sharing',
                },
                {
                  id: '3d76c149-9a74-4511-938f-7fc426b3992c',
                  name: 'Laura Dudas - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1icN688KRs7kPecQtQWOh34UdgAu5cWuJu9VHmoZbAyI/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: '3a3a5424-2bc6-40b9-8be6-fda9bcba9f06',
          name: 'Counsillor - Orléans',
          types: [DIRECTORY],
          children: [
            {
              id: '5a972a98-e6ae-4e22-a44b-6aa3c833c76e',
              name: 'Counsillor - Orléans - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1bZd63gt5Um_h4Rjd6WPSUuM0ogSWzSRT?usp=sharing',
            },
            {
              id: 'ce2728aa-ea2d-40a5-acdb-612d328bace3',
              name: 'Matthew Luloff - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '707dc0fd-2ec3-4a0e-8960-2ca07ebab3f9',
                  name: 'Matthew Luloff - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1J96i5ss2RDC6ie3_2uTnB6FQVJ74qxwN?usp=sharing',
                },
                {
                  id: '112c2b39-b90b-4f5d-9316-747c0458d951',
                  name: 'Matthew Luloff - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1zRyIVN0Xn4q2aR0PzHYmdVoKzs--bma_5MZVk59aD7w/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: '3a40ea8c-39cc-47cd-8461-9ce11e16be56',
          name: 'Counsillor - Kanata North',
          types: [DIRECTORY],
          children: [
            {
              id: 'bd3ec702-1e55-463d-b490-d78ca3558894',
              name: 'Counsillor - Kanata North - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1kwzr8rNiConma1HvKg8ro51Lo6IPGWjY?usp=sharing',
            },
            {
              id: '11c13064-1197-40a8-bf6f-7626578f5c07',
              name: 'Jenna Sudds - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '20b33d81-b851-4368-b5d7-edd2a5286ab0',
                  name: 'Jenna Sudds - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1308PwJ9TW2ZA6BgPFvPaFjz5vGCv_7LH?usp=sharing',
                },
                {
                  id: '1565e583-baf0-41c8-83b2-7f01738347ba',
                  name: 'Jenna Sudds - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1BBbUNSS4V4A4ehzt_S7GMhjbBNl0mBHxJEo3BQzhEjI/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: '039caa79-905f-47d1-ae3a-14fb82d0cdc5',
          name: 'Counsillor - Stittsville',
          types: [DIRECTORY],
          children: [
            {
              id: '463ce7d7-2b8b-40c9-aad1-fc28f20952d6',
              name: 'Counsillor - Stittsville - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1ud_L0JEBUQQ6zB29VvtcW0Y_fhJZVUpY?usp=sharing',
            },
            {
              id: 'cd258ed6-615e-4b6d-b5e2-5d69669cec3b',
              name: 'Glen Gower - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '6f29289f-2be7-4c85-8493-1098ab2a6cd1',
                  name: 'Glen Gower - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1DvwdYmQO2rZnuivppX67pcNnbci6_RIc?usp=sharing',
                },
                {
                  id: '9891dfbc-2034-4acf-a3f6-20733e80e73b',
                  name: 'Glen Gower - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1S3OEWBTs02nzwqllu0GieKhXJgapVtYFaJ4FwBuRzIo/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: 'c4bf5352-6ef3-438e-8499-a5053e29b25a',
          name: 'Counsillor - Capital',
          types: [DIRECTORY],
          children: [
            {
              id: '324b5e63-6882-4171-8689-6db5c911623c',
              name: 'Counsillor - Capital - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/19EKKEWg5-JfVIMJNISYGuimIlPZVoqlt?usp=sharing',
            },
            {
              id: '969700b8-a194-450a-8078-d776714df081',
              name: 'Shawn Menard - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '3a2fe98c-9a9c-4222-89b5-f02e6580a259',
                  name: 'Shawn Menard - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1qcFpQG_EwqzjOKTH41iR1TKXHVNx2sW7?usp=sharing',
                },
                {
                  id: '39b89ac4-7131-4db3-9979-a404264171f0',
                  name: 'Shawn Menard - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1J4erRCAhii5gvGzkLL2WSayeKB4SYJxJVgreG3rZTic/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: '17b720ed-6c54-4d7a-92b1-aff193133ff6',
          name: 'Counsillor - Rideau - Rockcliffe',
          types: [DIRECTORY],
          children: [
            {
              id: '9e5241b1-7e2e-4f61-b499-ae5b2e11ab59',
              name: 'Counsillor - Rideau - Rockcliffe - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1_FPEx84VJSLhqQ8myIROh9JTeuKW9wcg?usp=sharing',
            },
            {
              id: '8d8406bc-7cb7-402f-9d5a-e503d5edc25e',
              name: 'Rawlson King - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '8f198666-d197-4a92-9ef8-723cf6408d38',
                  name: 'Rawlson King - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1jB5KzMBE8O6fLsRl4SmiDK2xEQd0aeSx?usp=sharing',
                },
                {
                  id: '551faea9-6bab-44de-9d0b-e46c66031c22',
                  name: 'Rawlson King - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/11aI8iNFDgjZheKBz8uVyBF62qkOiFoV3U7LJcWh0sT8/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: 'f5bb3545-fc3e-459a-b4b5-3a6028bdb738',
          name: 'Counsillor - Kitchissippi',
          types: [DIRECTORY],
          children: [
            {
              id: '7a205aa0-0a90-4028-8532-b550394916b2',
              name: 'Counsillor - Kitchissippi - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1U_pndHYIsQputK4g8mDCiDQLGCvyJIDl?usp=sharing',
            },
            {
              id: 'd7bccba3-c495-486b-a8ac-2dfbccc2525a',
              name: 'Jeff Leiper - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: 'e2342a61-e39b-4fdb-bc4b-491dfa100430',
                  name: 'Jeff Leiper - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/117S5TdJ9b1UVquRpPkCKDnIZ6lkPosYJ?usp=sharing',
                },
                {
                  id: 'd95003cc-c2af-487d-964a-cea9ecd6ba8d',
                  name: 'Jeff Leiper - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/17w9hByFpaCKmnd55CfA_BocDDYtT7pwrRfkzvi81LyU/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: 'c365a5e2-f1cc-47ad-af7b-3df92a53f8e8',
          name: 'Counsillor - Somerset',
          types: [DIRECTORY],
          children: [
            {
              id: '5d32c16f-d6bc-4513-9321-9f71918e294c',
              name: 'Counsillor - Somerset - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/19QJ1jQL-5mCfUAwpk1OkE_2iE1htNVo3?usp=sharing',
            },
            {
              id: '0e7ddb98-f14e-4b0f-8ea1-ad73066d9f8c',
              name: 'Catherine McKenney - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '566dba68-1712-4709-b76a-dfd275794c2e',
                  name: 'Catherine McKenney - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1tD6Eqjkxk_98tS3tBxclNAjFTPK4xWLQ?usp=sharing',
                },
                {
                  id: '1955035c-cbde-40da-bf83-c0280e25310b',
                  name: 'Catherine McKenney - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1VYEE1D7NiRTZBI9BA8npzoBDt7rkXnPvZ7NLc8w1Qr4/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: 'e3eb3f60-03db-4e6f-8f29-675e7257409b',
          name: 'Counsillor - Bay',
          types: [DIRECTORY],
          children: [
            {
              id: 'b074623b-7528-4b6f-84ff-006b097a168c',
              name: 'Counsillor - Bay - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1Qu4DqlypZ5X2mFErQSp1vtUZS4SQNBYn?usp=sharing',
            },
            {
              id: '3a9478c9-1357-47e9-a409-9ba21b3b1906',
              name: 'Theresa Kavanagh - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '89913e99-d0c2-4752-84d9-d56b4d2cc0e1',
                  name: 'Theresa Kavanagh - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1x0_KD0gcbyg1lhqybSHNwbq5KRUHmElA?usp=sharing',
                },
                {
                  id: 'fa6d2c5d-9fdf-4239-808e-091856a57bd1',
                  name: 'Theresa Kavanagh - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/18Ro5M-hkNsxr6-1kGXqyvDRK96n_lbK4NoIYQCoeJrM/edit?usp=sharing',
                }
              ]
            },
          ],
        },
        {
          id: '60348239-61b4-4f83-a766-607998561a60',
          name: 'Counsillor - Rideau - Vanier',
          types: [DIRECTORY],
          children: [
            {
              id: '1ab0558f-2338-4a42-89a5-7dd517fc9aaa',
              name: 'Counsillor - Rideau - Vanier - Directory',
              types: [GOOGLE_DRIVE],
              link: 'https://drive.google.com/drive/folders/1IlW5pegdRuTYiyhPgqIojm7MogQVQU6c?usp=sharing',
            },
            {
              id: '760d8597-e1c0-405a-8c4a-436f978cf774',
              name: 'Mathieu Fleury - Resources',
              types: [DIRECTORY],
              children: [
                {
                  id: '99a7abde-7f35-43bb-8f02-6f9b7f4681a9',
                  name: 'Mathieu Fleury - Resources - Directory',
                  types: [GOOGLE_DRIVE],
                  link: 'https://drive.google.com/drive/folders/1xym2b441zMLtSt02WmgHxu9FhHGArHp4?usp=sharing',
                },
                {
                  id: '4ad93613-b673-4abb-8cde-abf56ce27f4c',
                  name: 'Mathieu Fleury - Background Report',
                  types: [DOCUMENT_FILE],
                  link: 'https://docs.google.com/document/d/1jVBWeKIPz1ECVEMwRKulNTINvIHJW4LhVYhTEet0X3c/edit?usp=sharing',
                }
              ]
            },
          ],
        },
      ]
    },
    {
      id: 'c931c203-00d8-4101-a395-72ecab6f19d0',
      name: "City of Ottawa Standing Committees - Resource Directories",
      types: [DIRECTORY],
      children: [
        {
          id: '53551f2f-2e3f-4848-92bd-739554cf1621',
          name: "City of Ottawa Standing Committees - Resource Directories - Directory",
          types: [GOOGLE_DRIVE],
          link: "https://drive.google.com/drive/folders/1U7Va1VVJvN2EK5bTkzo0dZM6hpfGu0Vm?usp=sharing",
        },
        {
          id: "a38d9652-5ef4-45aa-9abf-fa3905a457c4",
          name: "Agriculture and Rural Affairs Committee - Resources",
          types: [DIRECTORY],
          children: [
            {
              id: "615b33b6-a464-4bdf-ae2b-e58eb7708c18",
              name: "Agriculture and Rural Affairs Committee - Resources - Directory",
              types: [GOOGLE_DRIVE],
              link: "https://drive.google.com/drive/folders/1cIXatRNN_TdmRFlWXrmGIuMSDffFws8e?usp=sharing",
            },
            {
              id: "b8a39ebb-77f4-4cbc-8e34-e82ed298472c",
              name: "Agriculture and Rural Affairs Committee - Background Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1sX87Q1PvqI3uJvRNuOCCmhzMZwIyiMkVKMitvtJ6z_s/edit?usp=sharing"
            },
          ]
        },
        {
          id: "d97c7f42-0b57-474d-88c1-1cd28298d5a0",
          name: "Audit Committee - Resources",
          types: [DIRECTORY],
          children: [
            {
              id: "02089023-d041-44a0-abab-f42b1c1a3817",
              name: "Audit Committee - Resources - Directory",
              types: [GOOGLE_DRIVE],
              link: "https://drive.google.com/drive/folders/1qiTOwsRZovAdManCvnQQigwPUoF-0BCN?usp=sharing",
            },
            {
              id: "da2107aa-e87a-4db9-8eed-9719caff73a9",
              name: "Audit Committee - Background Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1mAboJAzMbKqjpKVgt4UhS24r8D6cBRn1jIEuhav__VQ/edit?usp=sharing"
            },
          ]
        },
        {
          id: "3b8b22f5-a681-4295-9194-7c2cc713be7e",
          name: "Built Heritage Sub-Committee - Resources",
          types: [DIRECTORY],
          children: [
            {
              id: "d5feb827-4b69-4658-b650-00d6fc6322a8",
              name: "Built Heritage Sub-Committee - Resources - Directory",
              types: [GOOGLE_DRIVE],
              link: "https://drive.google.com/drive/folders/15mKR_IWPmYlX4Q9-sY4XmVmXJNCsODMo?usp=sharing",
            },
            {
              id: "61d1415f-052e-4a09-83a4-a18959f80a68",
              name: "Built Heritage Sub-Committee - Background Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1HxFS15hUKLLSHM6bTrsQmi8egvTTfSuhqOfcI4mBNlk/edit?usp=sharing"
            },
          ]
        },
        {
          id: "3d555364-bcc0-43b7-a948-7131c96c1b85",
          name: "Community and Protective Services Committee - Resources",
          types: [DIRECTORY],
          children: [
            {
              id: "b9acb740-5a9b-4738-bbd8-1adfbc48ee9d",
              name: "Community and Protective Services Committee - Resources - Directory",
              types: [GOOGLE_DRIVE],
              link: "https://drive.google.com/drive/folders/1yheJ1D4Azo6JKxiDxumeTZ8bWFk8kX6f?usp=sharing",
            },
            {
              id: "60bb64ce-2f01-4b21-8be6-773460ab62d7",
              name: "Community and Protective Services Committee - Background Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1vUtiNraUocFR_NKkTHC83_iEXkacNd0GADmiq6qW0JM/edit?usp=sharing"
            },
          ]
        },
        {
          id: "0c2ca65a-7172-43e4-aa1d-101156493bd1",
          name: "Standing Committee on Environmental Protection, Water, and Waste Management - Resources",
          types: [DIRECTORY],
          children: [
            {
              id: "baa48f6f-51f3-4d44-9fa5-5f9f56cc36ca",
              name: "Standing Committee on Environmental Protection, Water, and Waste Management - Resources - Directory",
              types: [GOOGLE_DRIVE],
              link: "https://drive.google.com/drive/folders/10p0m_MkHEiUUraxCed6IY4PhnUh2wW1W?usp=sharing",
            },
            {
              id: "b3c4dfb6-9c3f-4428-bfcc-48458f76f14b",
              name: "Standing Committee on Environmental Protection, Water, and Waste Management - Background Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1Cv_nM0yZZJVONKif8Uqvw69I4vK8gdHatUUwWhFZU_E/edit?usp=sharing"
            },
          ]
        },
        {
          id: "7037bc21-6da7-4899-9550-1b9aaec4d52d",
          name: "Finance and Economic Development Committee - Resources",
          types: [DIRECTORY],
          children: [
            {
              id: "2412f1c3-4d26-4643-adbf-a9a7aba8bb82",
              name: "Finance and Economic Development Committee - Resources - Directory",
              types: [GOOGLE_DRIVE],
              link: "https://drive.google.com/drive/folders/1URnXqKlsNAcJ2WIvSBULpKq1ONigtE40?usp=sharing",
            },
            {
              id: "ca2c13c7-6c88-481e-b3b7-ca42dd0cd776",
              name: "Finance and Economic Development Committee - Background Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1AKrYuNSli-xKuoC84wjxVBMW_oNlQSiym9J4Uxrq9Uc/edit?usp=sharing"
            },
          ]
        },
        {
          id: "dbc2227a-3ddc-4ada-8605-455071c85e73",
          name: "Information Technology Sub-Committee - Resources",
          types: [DIRECTORY],
          children: [
            {
              id: "4f271744-7e1c-434f-8710-a8ac751025c5",
              name: "Information Technology Sub-Committee - Resources - Directory",
              types: [GOOGLE_DRIVE],
              link: "https://drive.google.com/drive/folders/15Oqvq_sOjbXHel5GCHLdjn2hXs1oa8Z3?usp=sharing",
            },
            {
              id: "938c5bef-8521-4e04-ae27-5aed8ee35fde",
              name: "Information Technology Sub-Committee - Background Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1MIJLCdX0P-IUFUIQInwKn8VtOmRY8H9xwVnPoF6AYTs/edit?usp=sharing"
            },
          ]
        },
        {
          id: "563f8054-71d6-4000-b394-21b45f9c8687",
          name: "Planning Committee - Resources",
          types: [DIRECTORY],
          children: [
            {
              id: "71de4f05-552d-4ff2-8d99-8178e459343c",
              name: "Planning Committee - Resources - Directory",
              types: [GOOGLE_DRIVE],
              link: "https://drive.google.com/drive/folders/19JBlkOpgF7fKP0CQzNh4HsfjvqhT3LpG?usp=sharing",
            },
            {
              id: "50faa390-3f94-4e90-ac63-5f28e134e9a6",
              name: "Planning Committee - Background Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1aJ3rmrk1_xBAgviL8fy6Lg2hN79jo_L2RtqqyoLw6bU/edit?usp=sharing"
            },
          ]
        },
        {
          id: "b59cd0b3-2bcb-40a8-a96e-6a4acd4fea5b",
          name: "Transit Commission - Resources",
          types: [DIRECTORY],
          children: [
            {
              id: "14e03e8b-ae96-4b4b-b6a6-ec884e21ef13",
              name: "Transit Commission - Resources - Directory",
              types: [GOOGLE_DRIVE],
              link: "https://drive.google.com/drive/folders/1TrvUBJyTgYUZrc_6eK3iLOyMPE5lvI7c?usp=sharing",
            },
            {
              id: "352dcd7a-bf4c-4ff3-843e-385f981dfa57",
              name: "Transit Commission - Background Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1PAoGwciDsTcBkGcCzW2KLSsBOjLS9cksty4I4YWCpsU/edit?usp=sharing"
            },
          ]
        },
        {
          id: "204ae88e-7728-4406-99b9-c5f78e260b8b",
          name: "Transportation Committee - Resources",
          types: [DIRECTORY],
          children: [
            {
              id: "c5fd679c-7bd9-4c83-aa1b-a26a039e15c9",
              name: "Transportation Committee - Resources - Directory",
              types: [GOOGLE_DRIVE],
              link: "https://drive.google.com/drive/folders/1t4yebWRr729bLleGRG-xOBmvijpWynXx?usp=sharing",
            },
            {
              id: "24770caf-a3a9-4fbb-a9fd-253cfa3bbc47",
              name: "Transportation Committee - Background Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1ws4-yf3xHoYhSlubkw4Jmd4kVvu01NmdEdbqMnwZZbU/edit?usp=sharing"
            },
          ]
        },
      ]
    }
  ]
}