import { DIRECTORY, DOCUMENT_FILE, WEBSITE } from "../../../../../../../../constants";
import { Resource } from "../../../../../../../../Types";

export const communications: Resource[] = [
  {
    id: "d5e5daa5-287b-4d85-8c43-373bc8ad30a5",
    name: "Comms Scenarios Planning",
    types: [DIRECTORY],
    children: [
      {
        id: "fa47dcb4-f793-46cb-a146-115c4abaa9f7",
        name: "Communications Scenarios Brainstorming",
        types: [DOCUMENT_FILE],
        link: "https://docs.google.com/document/d/1xqg-1OTEsALti4yXeQilkACTwIIlFEDUb9NXv3yTeWY/edit?usp=sharing",
      }
    ],
  },
  {
    id: "9433e5a0-21c8-4e86-8c59-3bbc1ad60cad",
    name: "Media Analysis",
    types: [DIRECTORY],
    children: [
      {
        id: "caedbeed-2167-4f26-8b3e-6ec583e7c059",
        name: "Blue Line",
        types: [DIRECTORY],
        children: [
          {
            id: "a62ec1db-83f0-45ac-a134-90872b3309e1",
            name: "Blue Line - Website",
            types: [WEBSITE],
            link: "https://www.blueline.ca/",
          },
          {
            id: "7bc5bbcd-04a5-412d-b4bd-52f953dfdc7d",
            name: "Articles",
            types: [DIRECTORY],
            children: [
              {
                id: "e6886c64-b424-4218-9360-404ebcce7090",
                name: "[Blue Line] - Ontario Police College: Then and Now",
                types: [WEBSITE],
                link: "https://www.blueline.ca/ontario-police-college-then-and-now-5292/",
              }
            ]
          }
        ]
      }
    ]
  }
]
