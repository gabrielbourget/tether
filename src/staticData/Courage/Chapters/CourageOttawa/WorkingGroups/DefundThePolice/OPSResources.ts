
import {
  DOCUMENT_FILE, DIRECTORY, GOOGLE_DRIVE, PDF_FILE, SPREADSHEET_FILE, PNG_FILE, IMAGE_FILE, SLIDES_FILE, WEBSITE, VIDEO_FILE
} from '../../../../../../constants';

import { Resource } from "../../../../../../Types";

export const OPSResources: Resource = {
  id: "88c82863-8b86-4554-97ec-3a0794d94559",
  name: "Ottawa Police Service - Resources",
  types: [DIRECTORY],
  children: [
    {
      id: 'aef3ed2b-8491-45f3-aafd-c276b0b1260e',
      name: 'Ottawa Police Service - Resources - Directory',
      types: [GOOGLE_DRIVE],
      link: 'https://drive.google.com/drive/folders/1PN6w2WE7WnP-OuxkIXcpoj3laBx8Schu?usp=sharing'
    },
    {
      id: "1dd1ac3e-fc38-4db9-a28b-7291c4f8ecb9",
      name: "Ottawa Police Service - Glossary of Terms",
      types: [DOCUMENT_FILE],
      link: "https://docs.google.com/document/d/1j-N_PEQP3ZQmfwLZTjadUGuKTM9uxIgGaUreEtah_AA/edit?usp=sharing",
    },
    {
      id: "59a67a76-209c-4d61-b250-c562ac3150bf",
      name: "Power Structure Mapping",
      types: [DIRECTORY],
      children: [
        {
          id: "fd13b26f-4dbc-4062-ae61-f7ddd2ad060f",
          name: "OPS - Organizational Chart",
          types: [IMAGE_FILE, PNG_FILE],
          link: "https://drive.google.com/file/d/1qEg4fbpujxKYgwvxNexxPuCS5k3Z-rCA/view?usp=sharing",
        }
      ]
    },
    {
      id: "661d6617-84ff-4711-a938-df3753072bdd",
      name: "Sections and Units",
      types: [DIRECTORY],
      children: [
        {
          id: "40504aa0-ff4a-405d-8f5a-3b1688734025",
          name: "911/Police Communicators",
          types: [DIRECTORY],
          children: [
            {
              id: "e48d5101-aa48-4d20-aa23-40c88afafe77",
              name: "911/Police Communicators - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/16fg9I7PFd76tgaGnJaaznIYPkgNKQ4LCpxy-VYXLww8/edit?usp=sharing",
            }
          ]
        },
        {
          id: "91b81f3f-a523-472d-895d-f092e52ef53e",
          name: "Airport Policing Section",
          types: [DIRECTORY],
          children: [
            {
              id: "c9429ee2-62fe-461c-9fef-b1eeba851c97",
              name: "Airport Policing Section - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/16fg9I7PFd76tgaGnJaaznIYPkgNKQ4LCpxy-VYXLww8/edit?usp=sharing",
            }
          ]
        },
        {
          id: "fb3d25a5-a19e-4fc4-9ff2-edbc25d90dd9",
          name: "Arson Unit",
          types: [DIRECTORY],
          children: [
            {
              id: "b77a3a67-68a1-4549-9f0b-bf3a8b1a1f64",
              name: "Arson Unit - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1YXnmIE_f8dl0TpcRSqSrZ--FU4h7QIoEY8DWGVuNhuo/edit?usp=sharing"
            }
          ]
        },
        {
          id: "0e12bf74-9225-4d24-b3b1-1a173acc07f0",
          name: "Break and Enter",
          types: [DIRECTORY],
          children: [
            {
              id: "4e74315f-e000-4e8c-af46-0a89953e7dfa",
              name: "Break and Enter - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/14O41b8lRccHtV0xxXqicNxqiz93MEGw4htUmvZWvNqw/edit?usp=sharing"
            }
          ]
        },
        {
          id: "4d060b85-bc4c-42f5-b450-370371da3c15",
          name: "Canine Unit",
          types: [DIRECTORY],
          children: [
            {
              id: "491384ea-fc10-4de3-831e-7fa21827d2c1",
              name: "Canine Unit - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1DWFV4nC_80Rlgno_u4y3glblGGWTmNosbVFV0Ic3cbM/edit?usp=sharing",
            }
          ]
        },
        {
          id: "5775a53c-f8c8-4f09-9939-6a6e5c4f45e5",
          name: "Collisions Investigation Unit",
          types: [DIRECTORY],
          children: [
            {
              id: "8bb57808-279c-4f94-b790-fc2951b6e23b",
              name: "Collisions Investigation Unit - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/19DKIA0ZoRn2ApSgy1Npm9jbqNSgCkFqg3zb3xuxuCI8/edit?usp=sharing"
            }
          ]
        },
        {
          id: "c9f04261-2639-4b29-9d91-a1dc8e4d7c98",
          name: "Community Development",
          types: [DIRECTORY],
          children: [
            {
              id: "16c3a146-375b-46ac-83ed-3b94f41d5682",
              name: "Community Development - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/14AVqxYmoc3FESbDAmv-4rlWxFDMituhC_vzx02pEROo/edit?usp=sharing"
            }
          ]
        },
        {
          id: "6aa9ffad-188e-4d08-8fa6-d727daa4c525",
          name: "Computer Forensics",
          types: [DIRECTORY],
          children: [
            {
              id: "dcf3abe5-bc05-46a4-83ed-6ce22e63cf9b",
              name: "Computer Forensics - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1SRexY2o81G6J9iBaYj9wvnqHmRPvU8QrBKZ_oNm352s/edit?usp=sharing"
            }
          ]
        },
        {
          id: "764c3f35-ac39-4476-a44d-49c4d802eaaa",
          name: "Diversity and Race Relations",
          types: [DIRECTORY],
          children: [
            {
              id: "ff2ca634-91d2-4a68-b0e7-2988f91c7cc0",
              name: "Diversity and Race Relations - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1bQSeAVFzUVt3nc3ELOa5Q0DgDKiBnANHtJBQavbCspM/edit?usp=sharing"
            }
          ]
        },
        {
          id: "e822d616-d4a1-46b7-900b-a8e145fc0132",
          name: "Drugs",
          types: [DIRECTORY],
          children: [
            {
              id: "c2ebb3ef-46ed-416d-8086-7d5fc0666c0d",
              name: "Drugs - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1kRU8jw5HBuWw5gLq2Qmr15vSa5_rCW8ar_iEl3jBxg4/edit?usp=sharing"
            }
          ]
        },
        {
          id: "821b45cf-7e36-4ab0-a3db-937526dd9527",
          name: "Elder Abuse",
          types: [DIRECTORY],
          children: [
            {
              id: "2f372701-e52f-4a48-ac13-03f29035dad7",
              name: "Elder Abuse - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1E9C3mNbBj1XXJmegCr4xOqrwBgk8ErNMPcR9UkL4_3g/edit?usp=sharing",
            }
          ]
        },
        {
          id: "4f132371-3323-4419-9014-bd80be113c38",
          name: "Forensic Identification",
          types: [DIRECTORY],
          children: [
            {
              id: "a076d295-dd91-4f74-80cc-1da7676a29e1",
              name: "Forensic Identification - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1gF7NPzXp9ACIJWOG5gvqqopL2bqcZO78A0Zr31M9AMM/edit?usp=sharing"
            }
          ]
        },
        {
          id: "7484de5c-b592-4b47-a672-aeb85481282d",
          name: "Fraud",
          types: [DIRECTORY],
          children: [
            {
              id: "d4a2f8e3-dcf9-4b95-ad84-ca58c9e064c0",
              name: "Fraud - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1U12EO4wLMX6q_xx2hcj1CTjFJk3hMdMumw6IPet-WNg/edit?usp=sharing"
            }
          ]
        },
        {
          id: "674ce606-5bbe-41ea-8a6e-5f8732e7cad2",
          name: "Guns and Gangs",
          types: [DIRECTORY],
          children: [
            {
              id: "bd0eb66f-cd0f-4954-bd2e-e2a54b09455f",
              name: "Guns and Gangs - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1xUd8oh-2cG3BAdBkkMvvTHQjeMw9T5SE3RJ0JZvgmQE/edit?usp=sharing"
            }
          ]
        },
        {
          id: "95f01062-6c12-47c5-b8f0-6dda2e5066a9",
          name: "Hate Motivated Incidents",
          types: [DIRECTORY],
          children: [
            {
              id: "81b5258a-3025-4cce-b0fa-cc5e2ba2401f",
              name: "Hate Motivated Incidents - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1ABmi6ZeO9UHOoZCcd7zxSdXLtrthljQH8hVswL6bDK4/edit?usp=sharing"
            }
          ]
        },
        {
          id: "72daa0cf-99c5-4f8b-b6d5-13145bbf29f5",
          name: "Homicide",
          types: [DIRECTORY],
          children: [
            {
              id: "234602e7-2609-4b7e-8751-025506cd1375",
              name: "Homicide - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1sIek9cQI68JNBsYJOvgLObp6Nswc880lReRGZkFOnLk/edit?usp=sharing"
            }
          ]
        },
        {
          id: "d92c631d-76b4-40f1-aa5e-ee812f2ca9da",
          name: "Human Trafficking Unit",
          types: [DIRECTORY],
          children: [
            {
              id: "16ff3f51-9d52-4ded-aca6-7f412c57dde5",
              name: "Human Trafficking Unit - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1RHyb_SfNyUOgYGXGqijdBqZngqp2hVh3tcV_QCMdDJ0/edit?usp=sharing"
            }
          ]
        },
        {
          id: "26bcc463-ce28-476c-aee9-0ae974b495c6",
          name: "Marine, Dive, and Trails Unit",
          types: [DIRECTORY],
          children: [
            {
              id: "9d5592a9-565b-4267-8698-a7a981407c90",
              name: "Marin, Dive, and Trails Unit - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1FFoRz-M1_T0Y4G18-RnNgcMnI-7tPMqf4oBKDUo4w6o/edit?usp=sharing"
            }
          ]
        },
        {
          id: "ded203d3-6b32-426b-acf0-5d2c2e424ee0",
          name: "Media Relations",
          types: [DIRECTORY],
          children: [
            {
              id: "4bcbf24e-2891-40d0-8841-ee0f2e2c3549",
              name: "Media Relations - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1BL_DaBd-IztDfXJDR5IYqbgn_rZQavW8hlHaG2QdFrY/edit?usp=sharing"
            }
          ]
        },
        {
          id: "504c41d5-431e-4692-a942-ee05e3608122",
          name: "Mental Health Unit",
          types: [DIRECTORY],
          children: [
            {
              id: "214f1466-da6e-4922-bf34-a40789176deb",
              name: "Mental Health Unit - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1bX-wWwANkb-b0l9Po4mgwQyiDaG9EqNAGOk49IooqQw/edit?usp=sharing"
            }
          ]
        },
        {
          id: "4a5776b4-1473-4f99-a002-c0d1a4778bce",
          name: "Missing Persons",
          types: [DIRECTORY],
          children: [
            {
              id: "6cf14bef-85c8-4339-94a0-ffdc8e5bac32",
              name: "Missing Persons - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1EoESXeTda_ZJr7QS2EGIkt9uyLs9iJmVGvpaS_5Mpbs/edit?usp=sharing"
            }
          ]
        },
        {
          id: "9b352faa-62a2-4149-9073-cd6a1faed5dc",
          name: "Organized Auto Theft",
          types: [DIRECTORY],
          children: [
            {
              id: "8fb0530b-9457-4d9f-a9bd-077442c40ae0",
              name: "Organized Auto Theft - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/17Q-_ZJ3LoeSCnVXYEP2i5pR_u7OvYR_M9e0aU2-CsKY/edit?usp=sharing",
            }
          ]
        },
        {
          id: "d9d34967-1491-4a73-938c-8d11fa338c79",
          name: "Partner Assault",
          types: [DIRECTORY],
          children: [
            {
              id: "7f152bac-dd33-4821-8082-07aa3afe59bb",
              name: "Partner Assault - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1r4Tv0a6xyVT6jby8AEUQvtSY3QTyvv6HZlo_7C21DM4/edit?usp=sharing"
            }
          ]
        },
        {
          id: "989c2614-8187-4e40-b7aa-48e06111b424",
          name: "Police Reporting Unit",
          types: [DIRECTORY],
          children: [
            {
              id: "bda302ca-0e8e-487d-82c3-bb3fcc202750",
              name: "Police Reporting Unit - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1lM0m9pqVh60s7eS-1nOWzZfu7njug0LKsD1NHSEd_6g/edit?usp=sharing"
            }
          ]
        },
        {
          id: "b51f38ee-885b-4ae6-b290-120b33c4cb39",
          name: "Professional Standards",
          types: [DIRECTORY],
          children: [
            {
              id: "4ba83f39-4678-46ac-a167-92c2798b5076",
              name: "Professional Standards - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/11Mbh7VXBPp7MO_W9wesb55ZTLG3Zg-5nwVEffJpJsag/edit?usp=sharing"
            }
          ]
        },
        {
          id: "9c13db2e-3f9a-4a87-995b-c18f033adbd8",
          name: "Property",
          types: [DIRECTORY],
          children: [
            {
              id: "8605a4bf-902a-4c27-bbce-a3e51b0fe90b",
              name: "Property - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1GuXLPvVyu-tsZJVatFxVeN7UMkC3ynOrhMlZS-rhmyw/edit?usp=sharing"
            }
          ]
        },
        {
          id: "ecd1c8f2-c58f-4e8f-87aa-998ce65b888f",
          name: "Robbery",
          types: [DIRECTORY],
          children: [
            {
              id: "1c138361-de73-41c9-a460-56696e78774f",
              name: "Robbery - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1YzKrEbOqMRDo7bboyLCMqXMnEzCQt0zXawLAsQFu0bE/edit?usp=sharing"
            }
          ]
        },
        {
          id: "aee1af94-0d79-4bd5-b233-fad178f2b7f3",
          name: "Sexual Assault and Child Abuse",
          types: [DIRECTORY],
          children: [
            {
              id: "7218fee2-8396-4370-afc9-69188782908a",
              name: "Sexual Assault and Child Abuse - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1QPXrYUO1b3jaBZVdIWWbnMVCk_bqdVSCuP56f5L5iAI/edit?usp=sharing"
            }
          ]
        },
        {
          id: "eb7aca65-16de-4c60-81c3-f884003c7924",
          name: "Special Events",
          types: [DIRECTORY],
          children: [
            {
              id: "3f5cfdca-6752-4a52-ad14-fa0b6d05f549",
              name: "Special Events - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1ZLmGXY59Tjh0iKmLbWMDHxuSk0dU_h7vQyTKS9dPGG4/edit?usp=sharing",
            }
          ]
        },
        {
          id: "b6a0aefd-af3d-4ff6-9a0f-6c2915cac95c",
          name: "Special Constables",
          types: [DIRECTORY],
          children: [
            {
              id: "7e942f56-ac1b-4581-8ff6-b82058deef77",
              name: "Special Constables - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1oqctV6suQ8Qg_Ry-Ouq00WjPOPQ12L3xGM1899Jdvbc/edit?usp=sharing"
            }
          ]
        },
        {
          id: "5327548a-d2bd-4dd6-996a-49748df749fa",
          name: "Tactical Unit",
          types: [DIRECTORY],
          children: [
            {
              id: "282f4252-22be-4bba-88c2-ca05433f8f56",
              name: "Tactical Unit - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1DVX9HH-Tv5OiKAINBXtTugIMfG640jOyIsdhhHVevEQ/edit?usp=sharing"
            }
          ]
        },
        {
          id: "673727d6-f731-4694-8769-b061cba3d285",
          name: "Traffic Section",
          types: [DIRECTORY],
          children: [
            {
              id: "b077f36f-85f2-40b0-b88d-311578108ce3",
              name: "Traffic Section - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1yv66FEgpN6ikcv7JlHwPI8Juv1XW4V1VZuNCnwLEpLg/edit?usp=sharing"
            }
          ]
        },
        {
          id: "c7a72a48-c77f-45e6-8b6a-514bfbd205fc",
          name: "Victim Crisis Unit",
          types: [DIRECTORY],
          children: [
            {
              id: "7bb81068-6820-49d2-ae85-b64e61ca7694",
              name: "Victim Crisis Unit - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1oGPA3DAr-fKBJkFViXSIbfEk7hp_Zm0Bp--BnkWYuOQ/edit?usp=sharing"
            }
          ]
        },
        {
          id: "4300b24a-9413-4df2-8512-2edfbfdfe9aa",
          name: "Youth",
          types: [DIRECTORY],
          children: [
            {
              id: "1386bbb3-ebf2-46d7-b159-39194975ab8f",
              name: "Youth - OPS Section/Unit Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/12MEt7VQZycnW_dikqcKsopgf1FW2fd3FV0-aZzKZjYs/edit?usp=sharing"
            }
          ]
        },
      ]
    },
    {
      id: "048b5a11-2f35-4c62-9851-93b06f75f014",
      name: "Sworn Selection Process",
      types: [DIRECTORY],
      children: [
        {
          id: "d88a3dcb-4295-4cec-a5ec-2e4d412052e6",
          name: "Ottawa Police Service - Sworn Selection Process Overview",
          types: [WEBSITE],
          link: "https://www.ottawapolice.ca/en/careers-and-opportunities/sworn-selection-process.aspx",
        },
        {
          id: "95f597fa-2d0d-4295-a09a-b4dbd2fde13a",
          name: "Ottawa Police Service - New Recruits",
          types: [WEBSITE],
          link: "https://www.ottawapolice.ca/en/careers-and-opportunities/new-recruits.aspx",
        },
        {
          id: "97789217-7778-4a9f-97c2-8c298e2b61f3",
          name: "Ottawa Police Service - Sworn New Recruit - Online Application",
          types: [WEBSITE],
          link: "https://ottawapolice.recruitmentplatform.com/FO/QE6FK026203F3VBQBLOV7V7Q8/components/details.html?jobId=81&jobTitle=Sworn%20New%20Recruit%202020",
        },
        {
          id: "905100e7-0307-4256-a430-58c39bc52e98",
          name: "OPS - Self-Assessment Questionnaire for Entry Into Policing",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1BrqcX3tnwveozDuRV2KWm5zO6kvcp5QJ/view?usp=sharing",
        },
        {
          id: "885fd341-8e54-46df-9076-6c092c02ee47",
          name: "OPS - Key Sworn Skills and Competencies",
          types: [WEBSITE],
          link: "https://www.ottawapolice.ca/en/careers-and-opportunities/sworn-skills-and-competencies.aspx",
        },
        {
          id: "8b8187d4-b863-426e-a5a2-bbd9c0c1ca0d",
          name: "The Physical Readiness Evaluation for Police (PREP) Test",
          types: [VIDEO_FILE],
          link: "https://www.youtube.com/watch?v=zWBPodHpieQ&feature=youtu.be",
        },
        {
          id: "3b20efbe-c57f-4371-af6d-f41a622168aa",
          name: "Ontario Fitness PIN Test Overview",
          types: [VIDEO_FILE],
          link: "https://www.youtube.com/watch?v=P3n-LJ7ETZI",
        }
      ]
    },
    {
      id: "1131504c-7701-4faf-868b-a87a5e8edf66",
      name: "External Comms Materials",
      types: [DIRECTORY],
      children: [
        {
          id: "58a47cb8-9410-4471-b9e5-017ac7623fd5",
          name: "[Recruitment Material] - Discover the Ottawa Police Force (2015)",
          types: [VIDEO_FILE],
          link: "",
        }
      ]
    },
    {
      id: "c84d8a17-b4f5-48d4-bb98-f3297f6e1978",
      name: "Annual Reports",
      types: [DIRECTORY],
      children: [
        {
          id: "5db5dbcd-da14-40f6-b636-ebcac04b175d",
          name: "OPS - 2018 Annual Report",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1S2kgirbr062NYCwJnCxWeSIzKpO6p617/view?usp=sharing",
        },
        {
          id: "6de811b4-a9df-4181-8b47-ba1708c79f50",
          name: "OPS - 2017 Annual Report",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1frz6sudW0fjYtccZgdkyiauXdsnfhzfg/view?usp=sharing",
        },
        {
          id: "31f59c02-433c-483b-8533-1360e88737dc",
          name: "OPS - 2016 Annual Report",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1scE0n2935vi7h1I0038H-o1ZOoENyVv-/view?usp=sharing",
        },
        {
          id: "484d0456-8b0f-47e1-8df2-7ec90009717d",
          name: "OPS - 2015 Annual Report",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1htajyJo_yvsvMyINOZJOnxcdAF_aEOOu/view?usp=sharing",
        },
        {
          id: "93c93684-3252-47bd-80b2-543a42a1772b",
          name: "OPS - 2014 Annual Report",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/19xFwl113DvhOqLVTklAalp0Cu8Fj0qUU/view?usp=sharing",
        },
        {
          id: "746840f4-3f86-430c-be7e-1e9f846f7e11",
          name: "OPS - 2013 Annual Report",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1nazI_Qcf7VI-WibLfwP58gRmCLdBdRTG/view?usp=sharing",
        },
        {
          id: "3e65c3a0-2ccb-47dc-94c2-1e1406e90315",
          name: "OPS - 2012 Annual Report",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1vq0q7mQCsvwpDuinRZIG95uqAxEShjKX/view?usp=sharing",
        },
      ]
    },
    {
      id: "396ee755-6304-495f-adc0-64f2e2f24773",
      name: "Publications",
      types: [DIRECTORY],
      children: [
        {
          id: "cca87183-93cb-471c-96bd-b9b58ee28912",
          name: "OPS - Strategic Direction 2019/2020",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1OYu-SUVMLwL15V-kXs2a89Et4IMT03jO/view?usp=sharing",
        },        
        {
          id: "63bfa9e5-f7af-4d8c-9363-0f2c6e7e764a",
          name: "OPS - Equity, Diversity & Inclusion Plan",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1zaelfHwYb8EPQwFoL6wSCiJ_rAN6MVt7/view?usp=sharing",
        },
        {
          id: "edb6fdf7-2008-42e9-9cc8-923a29ae48b5",
          name: "OPS - Workplace Sexual Violence and Harassment Project",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1VYc7IfAxT-kSZiY30w5SmSNB3Q_NOlD5/view?usp=sharing",
        },
        {
          id: "b402904a-af2a-4feb-8a36-8fc1a65399af",
          name: "OPS - Back Office Integration - Recommendations Report",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1DEycc2tVifmrrbvaNiJCNXJEYwcAebEr/view?usp=sharing",
        },
        {
          id: "bc16eea9-2adc-4de6-8c31-f32e9afa648f",
          name: "OPS - Neighborhood Policing Pilot Project",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1aehWnCPL7waIDG00B9dJ1HN7-YjnVs4T/view?usp=sharing",
        },
        {
          id: "77d5dde9-2930-4346-a9a2-ebaedda97a88",
          name: "OPS - 2014 Violence Against Women Consultation Report",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1pv2T4mh8DIrxmVXDbZktScDJmprvAliJ/view?usp=sharing",
        },
        {
          id: "10f87787-6a4a-4eb1-a998-c930e38822b8",
          name: "OPS - 2012 Report on 'Taking Action Together' on Gangs",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/13txvzv-nt6zJz1L0vxieglVOjUdmaqXn/view?usp=sharing",
        },
        {
          id: "09f75604-ffb5-4ba9-939b-2577b919db0d",
          name: "OPS - Traffic Stop Race Data Collection Project II",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1eQT0Nw2Gmu8PqL4ulprjIOqHNKVUUXEV/view?usp=sharing"
        },
        {
          id: "89f5ddd1-8e33-4b79-b1ec-414ca5443d77",
          name: "OPS - Crime Statistics and Trends - 2018-2019",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/174zxkN7gNIVsveLuQRxkj9BOufTEKA5r/view?usp=sharing",
        }
      ]
    },
    {
      id: '205d1e66-ecfb-4115-96d9-f9ae2f8297bd',
      name: 'OPS Budgets',
      types: [DIRECTORY],
      children: [
        {
          id: '3bf5d57e-2bcf-4dcc-989d-6c194df5b806',
          name: 'Ottawa Police Service Budgets - Directory',
          types: [GOOGLE_DRIVE],
          link: 'https://drive.google.com/drive/folders/1GinihTa96Yb7YFygbHVUwo0CEdnDzKi9?usp=sharing'
        },
        {
          id: "d5af6e38-5d98-4a11-9e64-5c46e1cfe06a",
          name: "2019",
          types: [DIRECTORY],
          children: [
            {
              id: 'df9f4872-2dc3-471c-b430-9fbbfff99c4b',
              name: 'OPS - 2019 Budget',
              types: [PDF_FILE, DOCUMENT_FILE],
              link: 'https://drive.google.com/file/d/1XH_7FuvlC0yJ0EcCochBauWjuKFPI_31/view?usp=sharing'
            },
          ]
        },
        {
          id: "d85eb0d3-eb2b-4923-afbc-52b7c91586e1",
          name: "2020",
          types: [DIRECTORY],
          children: [
            {
              id: "c156350c-6e40-40c5-bd50-8e76c43e3435",
              name: "OPS - 2020 Budget",
              types: [PDF_FILE, DOCUMENT_FILE],
              link: "https://drive.google.com/file/d/1G6vh0eBfeJrGZDGAz5YH9XQA0MJnPa40/view?usp=sharing",
            },
            {
              id: "c9dd1e9d-47f1-43a8-b146-81ba31c0c237",
              name: "Research Resources",
              types: [DIRECTORY],
              children: [
                {
                  id: "09397c4d-9874-4c57-b26a-343fa40d8309",
                  name: "Analysis Report on the Ottawa Police Service and Its 2020 Draft Operating Budget",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1t3vIixi5pPpMk6MXi71IjzauObaQUHejuun6-vagI2Q/edit?usp=sharing"
                },
                {
                  id: "65caae8c-1ccb-4fb2-9c57-91b49181e9df",
                  name: "Analysis of the OPS 2020 Draft Operating Budget",
                  types: [SLIDES_FILE],
                  link: "https://docs.google.com/presentation/d/1LA69Mg-VkYGiTxplJ9s6aYzqKzRbdaGiJvc5dDaxynk/edit?usp=sharing"
                },
                {
                  id: "359a7bcd-3c6e-40d1-8a82-0dd4318a47ef",
                  name: "Staff Report",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "4071734d-f0c5-4c54-86a1-02a78bfce5c0",
                      name: "Data Visualizations",
                      types: [DIRECTORY],
                      children: [
                        {
                          id: "238a4466-3ed5-42df-b5d5-64c0c4597779",
                          name: "OPS - Draft Budget 2020 - Incremental Financial Requirement",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1p_zo7yf2sliceAJn13UtzWg42mJEO1o4/view?usp=sharing",
                        },
                        {
                          id: "8a39c4ed-d3c1-4e62-9872-34bc94a984aa",
                          name: "Ottawa - Population - 2011-2018",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1J1is7QkrPOztIPrQb898Xn0r_Ch8TXcb/view?usp=sharing",
                        },
                        {
                          id: "5f89cece-ffcd-4846-89d9-8c7a3e13f0e4",
                          name: "Ottawa - Population Per Police Member - 2011-2018",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1KP2p6_I3TCFHfaEa5Ibva959oVedjhju/view?usp=sharing",
                        },
                        {
                          id: "f8ae73e5-a3b8-4919-b04c-9f9cc6985c3a",
                          name: "Popularion Per Police Member - Major Canadian Municipalities - 2018",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1waLSj-LVmweKg1nm9oTakfGtWvWAWkLh/view?usp=sharing",
                        },
                      ]
                    },
                    {
                      id: "d1ad4c0a-8cbf-4286-982e-df5755302a2e",
                      name: "Ottawa Police Service - Draft Budget 2020 - Staff Report - Notes",
                      types: [DOCUMENT_FILE],
                      link: "https://docs.google.com/document/d/1gULn6gscBmB-F82GNI0aWKnXMFttW4cyCyti0JfB1eo/edit?usp=sharing",
                    },
                    {
                      id: "7d776676-1189-4452-bdaa-7f7fda2321fa",
                      name: "OPS 2020 Draft Budget - Staff Report - Tables",
                      types: [SPREADSHEET_FILE],
                      link: "https://docs.google.com/spreadsheets/d/1S80PXq_GxJgOTVkHMxAlkX3de93d8AMNCDR8QB9W_hU/edit?usp=sharing"
                    }
                  ]
                },
                {
                  id: "5307a96b-2cf0-45af-b732-063d5070e2d8",
                  name: "Appendix A",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "60c2ef76-4f30-4c7b-97f4-ca51e8cdd5f3",
                      name: "Ottawa Police Service - Draft Budget 2020 - Appendix A - Notes",
                      types: [DOCUMENT_FILE],
                      link: "https://docs.google.com/document/d/10v5YlVff4wkUlutgq0VJk2WpjlBBzRX91IUKhAmpsJU/edit?usp=sharing",
                    },
                    {
                      id: "a2e4af5f-e7b0-4406-bb2e-cd7b90edfcbb",
                      name: "Ottawa Police Service - Draft Budget 2020 - Appendix A Tables",
                      types: [SPREADSHEET_FILE],
                      link: "https://docs.google.com/spreadsheets/d/1LDi6P4_KqJLO22tZZc9TiAnZyYIju3jv_9S64q7wJog/edit?usp=sharing",
                    },
                    {
                      id: "983bcd36-a486-46d9-a796-b30beef2fc91",
                      name: "A1 - OPS 2020-2023 Operating Forecast",
                      types: [DIRECTORY],
                      children: [
                        {
                          id: "1751a1b4-34de-434d-b9a2-c8816e27ea20",
                          name: "OPS Projected Full Time Equivalents (FTEs) Staffing - 2020-2023",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1FigCzTNKBBVv7XmKgzHwN3Uhj3Bitvu1/view?usp=sharing"
                        },
                        {
                          id: "81fe1311-583e-45f7-8b2e-d3066494d05c",
                          name: "OPS Projected Police Tax Rate Increase - 2020-2023",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/14by1_VZLsEz5oxX1iVvHXGltDKQsM_rE/view?usp=sharing"
                        }
                      ]
                    },
                    {
                      id: "838bb2ce-dcc5-4bef-ae66-6b05a176990f",
                      name: "A2 - OPS 2020 Staff Complement Summary by Section",
                      types: [DIRECTORY],
                      children: [
                        {
                          id: "46899df8-490b-485d-9274-934342cba840",
                          name: "OPS 2020 Staff Complement by Section",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1dt_PvSLk-rntv961vvoqGA810hs0_6cF/view?usp=sharing"
                        },
                        {
                          id: "a9ab2702-1b43-4550-bfeb-225abdba22ed",
                          name: "OPS 2020 Staff Complement by Position Types",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/11d-iYuZ6eWlCUnW_I_HbbN9alIuC46uu/view?usp=sharing"
                        },
                        {
                          id: "e6f89a28-315a-4e6d-81bf-726904bbc295",
                          name: "OPS 2020 Staff Complement - Corporate Support/Corporate Accounts",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1KxUzOFD7UoxJMYffasHpP5SfA4JpQHW5/view?usp=sharing"
                        },
                        {
                          id: "e8a6f3e5-2d6a-45a0-8e0c-0c2b8ddce2d6",
                          name: "OPS 2020 Staff Complement - Executive Directorate",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/12uePEHYgDkxZfKjsepTzZ8ZJlpGFkyBz/view?usp=sharing"
                        },
                        {
                          id: "e018f7be-8484-4db9-93f5-bb2649eb5ced",
                          name: "OPS 2020 Staff Complement - Frontline Directorate",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/17JTi6YHopwXJ-_cESCaJxmhj8CoxwMmw/view?usp=sharing"
                        },
                        {
                          id: "eb1429a1-933e-4b43-8ba0-ed468c8053c8",
                          name: "OPS 2020 Staff Complement - Operations - Community Relations & Frontline Specialized Support",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1NAxq-wLX_V3JfifH3y8Adgc0RxCaOrUF/view?usp=sharing"
                        },
                        {
                          id: "860de365-d99f-433d-984e-4545fe3b0a42",
                          name: "OPS 2020 Staff Complement - Operations Support - Criminal Investigative Directorate",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1nwrwLqK1fZIEghjIJpXIzAzAM-bRXd68/view?usp=sharing"
                        },
                        {
                          id: "a11faf96-4da1-44ca-9498-4a2179d7170f",
                          name: "OPS 2020 Staff Complement - Operations Support - Support Services Directorate",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1pOcOsYLY80LNqlr6Fh3Grr32vNNtR_dg/view?usp=sharing"
                        },
                        {
                          id: "62b096b7-5e81-48da-9d76-eb0d65b91e16",
                          name: "OPS 2020 Staff Complement - People & Culture Directorate",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1WHJp2M5zb6YpI91L0N5UuKSjNmCN1NL8/view?usp=sharing"
                        },
                        {
                          id: "8dc28610-2cff-414c-88ab-c9794d531109",
                          name: "OPS 2020 Staff Complement - Planning, Performance & Analytics",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1LDkJqomKfcKomS9BjVh70EczdPMFVgki/view?usp=sharing"
                        },
                        {
                          id: "53919a38-1852-4749-b41e-d2127c2534d4",
                          name: "OPS 2020 Staff Complement - Police Services Board",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1bD_ceaY6bAEbhRUuLMJgZa2CQEpvqtSb/view?usp=sharing"
                        },
                        {
                          id: "5fbacc28-dd6e-4104-be94-c4be2cc6d38f",
                          name: "OPS 2020 Staff Complement - Training & Development Directorate",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1n80jfBiDE0vzB7FlvhT5_nRUqqXpPYd9/view?usp=sharing"
                        },
                      ]
                    },
                    {
                      id: "07924383-c689-48a8-9127-b79e64e5c762",
                      name: "A3.0 - OPS 2020 Draft Budget - 2019 and 2020 Staff Complement by Source",
                      types: [DIRECTORY],
                      children: [
                        {
                          id: "26f7df31-8a74-411c-a6af-c45f61c70b7e",
                          name: "2019",
                          types: [DIRECTORY],
                          children: [
                            {
                              id: "cfd33325-d896-458b-bb05-c838a9a7aed1",
                              name: "OPS 2019 Staff Complement by Source - Overall Staff",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1C1iNNeUobVsqPPmvONsoulI73hcOWhV0/view?usp=sharing",
                            },
                            {
                              id: "f9c79307-a3f3-4ce8-8a28-a4c8c817bf0e",
                              name: "OPS 2019 Staff Complement by Source - Civilian Staff",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/16Gu5sc2Us56DcHtcvXbbM6ff7nUNv5Z-/view?usp=sharing",
                            },
                            {
                              id: "8a6e4375-9aa0-4961-a008-1c6bd7412c61",
                              name: "OPS 2019 Staff Complement by Source - Sworn Staff",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1QItEm1h5U6Tbw1kCWDs_cBqQGQgqzZRU/view?usp=sharing",
                            },
                          ]
                        },
                        {
                          id: "fce8a6bd-11de-4d27-a0a3-a23b5662442f",
                          name: "2020",
                          types: [DIRECTORY],
                          children: [
                            {
                              id: "72f7c36e-f61a-4a02-af2d-58a697b7c22e",
                              name: "OPS 2020 Staff Complement by Source - Overall Staff",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1XAC6Pa8aCHQTfosz7QBo2qQwazGkb8hT/view?usp=sharing",
                            },
                            {
                              id: "945abe72-9831-4df6-b198-5c89e0238eea",
                              name: "OPS 2020 Staff Complement by Source - Civilian Staff",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/13hhuZAaIX6V3uoHw5FYa_jYWQ3lnxrRi/view?usp=sharing",
                            },
                            {
                              id: "0f15978c-0510-4863-9c87-6128686b633e",
                              name: "OPS 2020 Staff Complement by Source - Sworn Staff",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1QAb-EpZA2aqWRNbOnCAc0U4IBduvKZMM/view?usp=sharing",
                            },
                          ]
                        },
                      ]
                    },
                    {
                      id: "daf38c32-da93-49ce-912b-2b5199b6330f",
                      name: "A9 - OPS History of Net Expenditures 2000-2020",
                      types: [DIRECTORY],
                      children: [
                        {
                          id: "6e587b9f-1c0a-44a3-ad8f-edd36457024e",
                          name: "OPS History of Net Expenditures 2000-2020",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1vYnSgXfMHk6qPh6gvm90LIBQ-5DaK0R6/view?usp=sharing",
                        },
                      ]
                    },
                  ]
                },
                {
                  id: "60ef131f-8202-4713-b920-809b75bc3cb9",
                  name: "Draft Operating Estimates",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "ad98b2c9-b29e-4959-b2e0-82350af12ca7",
                      name: "Draft Operating Estimates",
                      types: [SPREADSHEET_FILE],
                      link: "https://docs.google.com/spreadsheets/d/1vdV4RkhLxmlgsI2dh9rkcHsL8tTAFpHlI72EKa_GXfg/edit?usp=sharing"
                    },
                    {
                      id: "c8969a42-b72a-4234-8675-d032c6505927",
                      name: "Data Visualizations",
                      types: [DIRECTORY],
                      children: [
                        {
                          id: "f706cda0-ab5c-4b97-8f88-fb8aafe79545",
                          name: "Overall Police Service",
                          types: [DIRECTORY],
                          children: [
                            {
                              id: "463131fa-7011-4067-8ad6-3917471cdcfe",
                              name: "OPS Gross 2020 Estimate Expenditures by Program",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1uptPXYE1lDqP105Jls8qWiDlRwHUeVLn/view?usp=sharing",
                            },
                            {
                              id: "b3db43a8-15eb-4570-ae54-3dc1846d415d",
                              name: "OPS Gross 2020 Estimate Expenditures by Type",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1oIrECDeIiIsFk2EbrfKFPzrFNMIwpG30/view?usp=sharing",
                            },
                          ]
                        },
                        {
                          id: "e3fc4108-e9ab-47e3-a10f-ff7062a5c09d",
                          name: "Community Relations and Frontline Specialized Support",
                          types: [DIRECTORY],
                          children: [
                            {
                              id: "f79879bd-053b-48c4-804f-2eb6fe05409f",
                              name: "OPS - Community Relations and Frontline Specialized Support - Gross 2020 Estimate Expenditures by Program",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1JpnmdZa7BdTJkURI78oXjTB5W4Ky68B2/view?usp=sharing",
                            },
                            {
                              id: "5339ef45-e3eb-4417-b930-5ab43d76cb2e",
                              name: "OPS - Community Relations and Frontline Specialized Support - Gross 2020 Estimate Expenditures by Type",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1rSu7nE8Jo4ib4ErqBIZvE3r4T8FIGdqI/view?usp=sharing",
                            },
                          ]
                        },
                        {
                          id: "52adb983-48e2-4d15-8424-5e7712ccf91d",
                          name: "Corporate Accounts",
                          types: [DIRECTORY],
                          children: [
                            {
                              id: "57d303ef-fe22-47a1-ba37-3ac33ae13ff6",
                              name: "OPS - Corporate Accounts - Gross 2020 Estimate Expenditures by Program",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1kxzI0fqQxPLSWocpVjHufOt-q80TWe47/view?usp=sharing",
                            },
                            {
                              id: "23bcdc5e-20e3-4669-a393-60b7748d5716",
                              name: "OPS - Corporate Accounts - Gross 2020 Estimate Expenditures by Type",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/12wkcJo3hoQEc_HOJgJ1kJNTjcrft-dbA/view?usp=sharing",
                            },
                          ]
                        },
                        {
                          id: "be35fe67-c1b6-41a7-a38e-9559c519f16f",
                          name: "Corporate Support",
                          types: [DIRECTORY],
                          children: [
                            {
                              id: "932da2c5-7afe-4992-bb68-761182c1dcc1",
                              name: "OPS - Corporate Support - Gross 2020 Estimate Expenditures by Program",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1aPV13OnBzztsYFmVGsEGr4z78WS-RMS7/view?usp=sharing",
                            },
                            {
                              id: "5d46dbf7-3177-4fcb-8c5c-583805631c7a",
                              name: "OPS - Corporate Support - Gross 2020 Estimate Expenditures by Type",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1w0XSqR0mRW0uxKI0vtfDIpS2NIHEOO2U/view?usp=sharing",
                            },
                          ]
                        },
                        {
                          id: "a6357965-8075-4197-9519-eae6fce9acb0",
                          name: "Criminal Investigative Directorate",
                          types: [DIRECTORY],
                          children: [
                            {
                              id: "4088c477-aafb-4c58-98cf-21b3daa2a818",
                              name: "OPS - Criminal Investigative Directorate - Gross 2020 Estimate Expenditures by Program",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1-JUHTpMS7BkwZL0-xl1yMbM_-k7JQ_Po/view?usp=sharing",
                            },
                            {
                              id: "93737dd1-8e9d-4d36-9914-051c8c7c0734",
                              name: "OPS - Criminal Investigative Directorate - Gross 2020 Estimate Expenditures by Type",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/15ETHTwK5KfJcs7Mnm9Wt45bE6k7Z4_lS/view?usp=sharing",
                            },
                          ]
                        },
                        {
                          id: "06826d1e-6ced-4a71-8f8b-5acf7ae1baf3",
                          name: "Executive Services",
                          types: [DIRECTORY],
                          children: [
                            {
                              id: "88faa731-6fb5-487a-85be-13be048c66dd",
                              name: "OPS - Executive Services - Gross 2020 Estimate Expenditures by Program",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1CrUPDvMEV6RfIMGUHVyVSQMyHXZxuGU5/view?usp=sharing",
                            },
                            {
                              id: "d9a319ad-2150-4c7a-8cdb-aa58e777a5de",
                              name: "OPS - Executive Services - Gross 2020 Estimate Expenditures by Type",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/15hcFsJF3gvOSEiZahwpyZbpc9pmwTNqh/view?usp=sharing",
                            },
                          ]
                        },
                        {
                          id: "daad612f-c990-4c2e-82d9-56d13cae8210",
                          name: "Financial Accounts",
                          types: [DIRECTORY],
                          children: [
                            {
                              id: "c297c050-d89e-4f7c-86e1-b611a3ea2f96",
                              name: "OPS - Financial Accounts - Gross 2020 Estimate Expenditures by Program",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1iYD1DCvkP4CRTWKts6mCLHfieHuk1AMF/view?usp=sharing",
                            },
                            {
                              id: "11bae3d3-f0c6-4f5a-abe9-b749fd20d822",
                              name: "OPS - Financial Accounts - Gross 2020 Estimate Expenditures by Type",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1mY1vuCtTShGLz5qQJ_TjBW5IgA1rNXqw/view?usp=sharing",
                            },
                          ]
                        },
                        {
                          id: "8a6e3365-2e7f-461d-afad-c7374e9e0f8a",
                          name: "Frontline Operations Directorate",
                          types: [DIRECTORY],
                          children: [
                            {
                              id: "eb52f153-e246-4bea-be22-b950b934e822",
                              name: "OPS - Frontline Operations Directorate - Gross 2020 Estimate Expenditures by Program",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1IWf02Mjasv-TjPLv4AmQJeMoqB4T2TOO/view?usp=sharing",
                            },
                            {
                              id: "1449d760-3559-4015-95d3-4981db26005c",
                              name: "OPS - Frontline Operations Directorate - Gross 2020 Estimate Expenditures by Type",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1GXoDXstrtSJ5IjUJkNBgR0l33QHykvrS/view?usp=sharing",
                            },
                          ]
                        },
                        {
                          id: "f082a501-d438-43ae-859c-8b04fbda56d2",
                          name: "People and Culture Directorate",
                          types: [DIRECTORY],
                          children: [
                            {
                              id: "ca5d44b1-f7ae-49f6-b74f-a4026ad0d2ea",
                              name: "OPS - People and Culture Directorate - Gross 2020 Estimate Expenditure by Type",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1fo4w7G9B4uuRp8bVY5ktXLL9tJYG0B5U/view?usp=sharing",
                            },
                            {
                              id: "b32ffafe-70d0-4010-b578-ae1041ac2680",
                              name: "OPS - People and Culture Directorate - Gross 2020 Estimate Expenditures by Program",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1DUPzY8HuTwqqBHJ1CGVUhMuLSJrb7dSB/view?usp=sharing",
                            },
                          ]
                        },
                        {
                          id: "6edcbb4b-f611-4119-bff8-0e714275e6af",
                          name: "Planning, Performance, and Analytics",
                          types: [DIRECTORY],
                          children: [
                            {
                              id: "65b04b77-00e0-4184-94d1-f7b2f5a09216",
                              name: "OPS - Planning, Performance & Analytics - Gross 2020 Estimate Expenditures by Program",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/10VqzyXpGDKAppG9BU6V4z1lPxMd5dWwS/view?usp=sharing",
                            },
                            {
                              id: "0e3b4b99-286f-48e7-9cfe-fa95c79d304f",
                              name: "OPS - Planning, Performance & Analytics - Gross 2020 Estimate Expenditures by Type",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1bkRpX_Yzb8gG4_OUtV-fNaaW1EPPiYKb/view?usp=sharing",
                            },
                          ]
                        },
                        {
                          id: "dfdbf50d-4205-499c-b09e-1a86580a5627",
                          name: "Police Services Board",
                          types: [DIRECTORY],
                          children: [
                            {
                              id: "ba48dc3b-9b13-43d4-92e6-7db1cfbed57b",
                              name: "OPS - Police Services Board - Gross 2020 Estimate Expenditures by Program",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1I_RVCZ1KkR0jxDf5nqTHptLuLZ8ZzVlL/view?usp=sharing",
                            },
                            {
                              id: "4d7f0ce4-8724-4ccc-9898-01e53710725d",
                              name: "OPS - Police Services Board - Gross 2020 Estimate Expenditures by Type",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1sOnoBUplh89H61VogpHOZmre59pnJVcm/view?usp=sharing",
                            },
                          ]
                        },
                        {
                          id: "2cadadea-568a-4b72-92cc-1dcf62f49eea",
                          name: "Support Services",
                          types: [DIRECTORY],
                          children: [
                            {
                              id: "2da728f5-67f8-4ea7-b08e-feb1a6bffec1",
                              name: "OPS - Support Services - Gross 2020 Estimate Expenditures by Program",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1Wr40by1zkgXqp85E7NyMH7L0pTCfWSXH/view?usp=sharing",
                            },
                            {
                              id: "d82de7d8-4c86-4c72-abd6-b47fa34fbdb1",
                              name: "OPS - Support Services - Gross 2020 Estimate Expenditures by Type",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1Wdi_JkPzzlhWcyWzSBYaosmW0wK6yyXR/view?usp=sharing",
                            },
                          ]
                        },
                        {
                          id: "01f1dee0-1ffa-4862-8f58-e0770f77f149",
                          name: "Training and Development Directorate",
                          types: [DIRECTORY],
                          children: [
                            {
                              id: "2f63ae47-df8f-40b3-af67-bed436416ee6",
                              name: "OPS -  Training and Development Directorate - Gross 2020 Estimate Expenditures by Program",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1suj-1T4SOLFnlmXXyGjOAC9eMkY5pACf/view?usp=sharing",
                            },
                            {
                              id: "84e7d264-b3e8-4d0c-ab98-f559176c2c2d",
                              name: "OPS - Training and Development Directorate - Gross 2020 Estimate Expenditures by Type",
                              types: [IMAGE_FILE, PNG_FILE],
                              link: "https://drive.google.com/file/d/1ONaq0I_sus_V6YJ0PV-qW9WTw8kKDiRO/view?usp=sharing",
                            },
                          ]
                        },
                      ]
                    }
                  ]
                },
                {
                  id: "e5069d2a-d8e0-43a7-82f2-d02a080f2c19",
                  name: "Summary by Expenditure Type",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "39ba6270-f48e-4081-9a23-225b6a8b5e25",
                      name: "OPS Draft Budge 2020 - Summary by Expenditure Type",
                      types: [SPREADSHEET_FILE],
                      link: "https://docs.google.com/spreadsheets/d/1x43-Gue-C7UanhXKitAYDo5-TVUVHR1O6YtWbKF843E/edit?usp=sharing"
                    },
                    {
                      id: "047f7f22-c2fd-48c3-8f65-8826eae2d100",
                      name: "Data Visualizations",
                      types: [DIRECTORY],
                      children: [
                        {
                          id: "b4f115bb-a085-450e-ba53-ae2e868a898f",
                          name: "Ottawa Police Service - Gross 2020 Estimated Expenditures by Type",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1X9z4qaUlEIhnjUCwHVeFMNhfXihshtoj/view?usp=sharing",
                        },
                        {
                          id: "13984275-5939-498c-bbf2-3c1216cf7cd8",
                          name: "OPS - Community Relations & Frontline Specialized Support - Gross 2020 Estimated Expenditures by Type",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1mCcjbGut77V60e1lqOv0EEHmly3pcOPJ/view?usp=sharing",
                        },
                        {
                          id: "c9f17967-836a-4dd7-92e8-e5a0504c1474",
                          name: "OPS - Corporate Accounts - Gross 2020 Estimated Expenditures by Type",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1Uunf0KEN2OIF0r1Rv2SmtEBOJJebeUho/view?usp=sharing",
                        },
                        {
                          id: "fedb7a44-80ea-454f-aad2-e306721084da",
                          name: "OPS - Corporate Support Directorate - Gross 2020 Estimate Expenditures by Type",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1YkzSeaL7HA0U1bozRndLPEgIIegIVGKz/view?usp=sharing",
                        },
                        {
                          id: "d3215910-b1c0-47b5-912f-777604c8bfbf",
                          name: "OPS - Criminal Investigative Directorate - Gross 2020 Estimated Expenditures by Type",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1WwnrL_VSRPQelF2xcTEOTx8jgS1ivEHg/view?usp=sharing",
                        },
                        {
                          id: "19889d06-182d-4133-8b68-a39f35e8e6a2",
                          name: "OPS - Executive Services Directorate - Gross 2020 Estimated Expenditures by Type",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1uYGtooud0wgqbaYJ5DgLMPBqvQQo-s1Z/view?usp=sharing",
                        },
                        {
                          id: "5d8ce631-ae2b-4feb-ae78-0f611de3764f",
                          name: "OPS - Financial Accounts - Gross 2020 Estimated Expenditures by Type",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1BXfW_rU3ocX-3bUfFN8SjqpZ4TuVPoKx/view?usp=sharing",
                        },
                        {
                          id: "9432413d-0faa-4ce6-a6c3-1dfeaba8b7bf",
                          name: "OPS - People and Culture Directorate - Gross 2020 Estimated Expenditures by Type",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1qbituynyLStG_FM7v4Zxw34ZZwXbgB28/view?usp=sharing",
                        },
                        {
                          id: "cf95eb41-5f48-4ad6-878d-2e3c6ba87495",
                          name: "OPS - Planning, Performance & Analytics - Gross 2020 Estimated Expenditures by Type",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1XYzrsKSB3hJsYaNpYltc3-vaKE1jb1-u/view?usp=sharing",
                        },
                        {
                          id: "d9ad0999-3035-48b5-a121-6a4db2d7f74e",
                          name: "OPS - Police Services Board - Gross 2020 Estimate Expenditures by Type",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1XsohX_Rkn0b2NpwxrIasq1oFsgzawNOo/view?usp=sharing",
                        },
                        {
                          id: "89792615-b78b-4a54-bc1f-06dad444ad5a",
                          name: "OPS - Support Services Directorate - Gross 2020 Estimated Expenditures by Type",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1q98BES9PqQcSWh58mjZedJ28wLXaGpEc/view?usp=sharing",
                        },
                        {
                          id: "f95d2e5b-8432-4131-b829-1506d0b6e2e3",
                          name: "OPS - Training and Development Directorate - Gross 2020 Estimated Expenditures by Type",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/13I56gd-_5VRl1L4c1GNfp_wwfdMLhSGk/view?usp=sharing",
                        },
                      ],
                    }
                  ]
                },
                {
                  id: "bebbb8e2-6409-4756-a71b-60c8ccc4fa35",
                  name: "Appendix B",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "e70c02fb-7fea-4216-86c5-3dd56ca8c4b7",
                      name: "B1 - Capital Budget Works in Progress",
                      types: [DIRECTORY],
                      children: [
                        {
                          id: "d72baa1f-95c1-4b00-b804-612b3978fa37",
                          name: "OPS Draft Budget 2020 - Appendix B1 - Capital Works in Progress",
                          types: [SPREADSHEET_FILE],
                          link: "https://docs.google.com/spreadsheets/d/1VJVmZyMw-MOrbXBPhoA42gHltGWzw8aMyDSh1dJ1qEc/edit?usp=sharing"
                        },
                        {
                          id: "d72baa1f-95c1-4b00-b804-612b3978fa37",
                          name: "OPS - Capital Works in Progress by Category - 2019",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1sw9Lgggigx9oZOAeHK4cUkej8l4q0tVJ/view?usp=sharing"
                        },
                      ],
                    },
                    {
                      id: "13526c0b-5480-4db7-b382-4c40961a9307",
                      name: "B2 - 2020-2029 Capital Forecast",
                      types: [DIRECTORY],
                      children: [
                        {
                          id: "d7d52479-f5f1-444b-b515-aa9e4b963e18",
                          name: "B2 - 2020-2029 Capital Forecast",
                          types: [SPREADSHEET_FILE],
                          link: "https://docs.google.com/spreadsheets/d/1VzkQzR0HTrZVEepuiBA-Q57SLiCb76JyQ8vM91z2mnQ/edit?usp=sharing"
                        },
                        {
                          id: "6e9636d5-ea05-49c0-9ea5-1b3ca6c43afe",
                          name: "OPS - 2020-2029 Capital Requirement Forecast",
                          types: [IMAGE_FILE, PNG_FILE],
                          link: "https://drive.google.com/file/d/1IxJ2HuXZtkCxxulmT_jxyo6Ks9btt0aO/view?usp=sharing"
                        },
                      ],
                    },
                    {
                      id: "6e4ac926-03ff-4705-be87-a492652a3635",
                      name: "B3 - Capital Budget Project Details",
                      types: [DIRECTORY],
                      children: [
                        {
                          id: "cfe92fd7-1dfd-4628-a4b8-497a0b0670b1",
                          name: "B3 - Capital Budget Project Details",
                          types: [SPREADSHEET_FILE],
                          link: "https://docs.google.com/spreadsheets/d/1dkWmuXYPlJsJUwl_srGNw6Sdssqd1cPavsDVWSBogbI/edit?usp=sharing"
                        },
                      ],
                    },
                    {
                      id: "ae2f3866-d76f-444f-991e-cb8123192958",
                      name: "B4 - 2020-2029 Continuity Schedules",
                      types: [DIRECTORY],
                      children: [
                        {
                          id: "67279c57-5dc6-41d7-a3c8-cdd110c96ed7",
                          name: "B4 - 2020-2029 Continuity Schedule",
                          types: [SPREADSHEET_FILE],
                          link: "https://docs.google.com/spreadsheets/d/1xRQ4XqnhX3QVLntDmCu0hrEWopGDScmgqlpmD6unCog/edit?usp=sharing",
                        },
                      ],
                    },
                  ]
                },
              ]
            }
          ]
        },
        {
          id: "204b789a-6fcb-4128-a99e-0a8f4397d397",
          name: "2021",
          types: [DIRECTORY],
          children: [
            {
              id: "4b2761fa-f657-47e7-9142-043581da1c1a",
              name: "OPS - 2021 Draft Budget",
              types: [PDF_FILE, DOCUMENT_FILE],
              link: "https://drive.google.com/file/d/1_D455kBL3bMvorNqYTCCw2JIgZN0r0yH/view?usp=sharing",
            },
            {
              id: "f8dd7b81-0730-44d5-ad97-b5fac120c9ac",
              name: "OPS - 2021 Draft Budget - Slides",
              types: [SLIDES_FILE, PDF_FILE],
              link: "https://drive.google.com/file/d/15M8ieSMgoanNxgPVgzKwg57_IlBQ6L99/view?usp=sharing",
            }
          ]
        }
      ]
    }      
  ]
}
