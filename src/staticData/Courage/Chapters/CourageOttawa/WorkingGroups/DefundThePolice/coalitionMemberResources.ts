import {
  DOCUMENT_FILE, DIRECTORY, GOOGLE_DRIVE, SPREADSHEET_FILE, FORM
} from '../../../../../../constants';

import { Resource } from "../../../../../../Types";

export const coalitionMemberResources: Resource = {
  id: '0b303030-d69a-4dde-8031-d3262f8c91e2',
  name: 'Potential Coalition Members - Resources',
  types: [DIRECTORY],
  children: [
    {
      id: 'cdb84d69-6e59-4b4f-9a57-d2c76d70ea3f',
      name: 'Coalition Members - Resources - Directory',
      types: [GOOGLE_DRIVE],
      link: 'https://drive.google.com/drive/folders/1FiLVf76sJp6JNVyWgZ-C2mtukOb95uvT?usp=sharing'
    },
    {
      id: '7edd8310-d5bc-4c83-afd5-b2fc709bf655',
      name: 'Courage Ottawa - Resources',
      types: [DIRECTORY],
      children: [
        {
          id: '76cf984a-9cb0-48e1-9e98-027e18b1db9b',
          name: 'Courage Ottawa - Resources - Directory',
          types: [GOOGLE_DRIVE],
          link: "https://drive.google.com/drive/folders/1SsKWRNRSI7TnXF2lwtOfMwOR1-We2a0m?usp=sharing",
        },
        {
          id: '31a0483e-83ba-48fd-ad1d-d45f3e4267e4',
          name: 'Courage Ottawa - Background Report',
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1OqiuZFwuLmP3W7LeD7ZdamoGWbPKh-NVekglLKpuHTs/edit?usp=sharing",
        }
      ]
    },
    {
      id: '93d01fbe-605b-473b-bffc-268315181432',
      name: 'CPEP - Resources',
      types: [DIRECTORY],
      children: [
        {
          id: '16b2e5d2-d074-45eb-8f8d-0564510f822f',
          name: 'CPEP - Resources - Directory',
          types: [GOOGLE_DRIVE],
          link: 'https://drive.google.com/drive/folders/1QGqbiCKqLTCjpfJBtvLubWz-n1ScbRsK?usp=sharing',
        },
        {
          id: '39b4fadf-dcdd-46b9-b127-b429ff2ca747',
          name: 'CPEP - Background Report',
          types: [DOCUMENT_FILE],
          link: 'https://docs.google.com/document/d/1Y3vDgYPb8qABMR8EecI0sJW5KKIaoc5p4hsJm9Kz-9w/edit?usp=sharing'
        }
      ]
    },
    {
      id: '866956bf-d1e3-4f31-abd8-9b930b8b8889',
      name: 'Horizon Ottawa - Resources',
      types: [DIRECTORY],
      children: [
        {
          id: 'f46ca488-a4ab-4043-9b33-08cc34e6884f',
          name: 'Horizon Ottawa - Resources - Directory',
          types: [GOOGLE_DRIVE],
          link: 'https://drive.google.com/drive/folders/1TG2Wrh_3FQRRv0yrNUG1blTN4XuC3Ub9?usp=sharing',
        },
        {
          id: '1acb4084-9cfc-40b0-83ee-b5d2c541ee0f',
          name: 'Horizon Ottawa - Background Report',
          types: [DOCUMENT_FILE],
          link: 'https://docs.google.com/document/d/1NVH_2SiaLcLQ2DXUvLgx2JNGzAK1bCzo3_ZIcRIoQ0k/edit?usp=sharing'
        },
        {
          id: "1448224e-e2aa-45ef-b0db-85486afc4e17",
          name: "Horizon Ottawa - Defunding the Police Proposal 2020",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1TAVuxsdd8YQloNxKKq955Z3bs7wGevxsZnQxyo8cepI/edit"
        },
      ]
    },
    {
      id: '5389e809-7f50-4301-8159-35f1dda1367e',
      name: 'Asilu Collective - Resources',
      types: [DIRECTORY],
      children: [
        {
          id: '6b7a6805-3f71-4acf-bc78-9ad8d326eb93',
          name: 'Asilu Collective - Resources - Directory',
          types: [GOOGLE_DRIVE],
          link: 'https://drive.google.com/drive/folders/1MJ-8Ap-JxhFxGAZqypqctg788dQD_kz8?usp=sharing',
        },
        {
          id: 'ce0a6be3-0b6d-48f4-b2bc-626ac7e19754',
          name: 'Asilu Collective - Background Report',
          types: [DOCUMENT_FILE],
          link: 'https://docs.google.com/document/d/15zC3z3TakwmV064Sg3_yp69nLE8AUl7fVCYE01Vbkbc/edit?usp=sharing'
        },
        {
          id: "262d70d6-5cc1-46ea-bf3d-70e74416ed9d",
          name: "List of Recommended Documentaries",
          types: [SPREADSHEET_FILE],
          link: "https://docs.google.com/spreadsheets/d/1GlNRWHmnzQ41xCG-gzsVxFtSCyPdxhgaBSM8M0FaozU/htmlview?usp=gmail&gxids=7757",
        },
        {
          id: "7f8fc264-30d2-41c8-bce9-9ad4e9d2716d",
          name: "List of Recommended Readings",
          types: [SPREADSHEET_FILE],
          link: "https://docs.google.com/spreadsheets/u/0/d/10ch2npFNVZ1-lFzrNSDFcRN52Sfiu_noifV7F0bNxrI/htmlview?usp=gmail&gxids=7757",
        },
        {
          id: "77398498-42dd-48d7-8941-9d206a83c499",
          name: "Petition Seeking the Removal of SROs From All Ottawa Schools",
          types: [FORM],
          link: "https://docs.google.com/forms/d/e/1FAIpQLSeiIIcid8Sa2Gy5N7Sopi5PHd4Ip9rvOW0lcEzFXacVFMQJtw/viewform"
        },
      ]
    },
    {
      id: '79401faf-818c-4168-9ea7-cf127f334a5e',
      name: 'Justice for Abdirahman - Resources',
      types: [DIRECTORY],
      children: [
        {
          id: '7c522cde-9cfd-4933-8627-a7dbad4dc59e',
          name: 'Justice for Abdirahman - Resources - Directory',
          types: [GOOGLE_DRIVE],
          link: 'https://drive.google.com/drive/folders/1jZkauzO2RjtatYFaCS7E7gQOzJaQIajv?usp=sharing',
        },
        {
          id: 'db6279a0-8abb-40e2-89c1-dfc2c6f5164b',
          name: 'Justice for Abdirahman - Background Report',
          types: [DOCUMENT_FILE],
          link: 'https://docs.google.com/document/d/1lwa8TfV1LViA1SPPAYhUKnnjoUuR6ysn9ZDTm6jM-1I/edit?usp=sharing'
        }
      ]
    },
    {
      id: '70243773-f759-4c2c-88ec-e9ffacbf5e81',
      name: 'SES - Resources',
      types: [DIRECTORY],
      children: [
        {
          id: 'd3c4f1f5-41a7-46e2-928d-2dcfd75735bd',
          name: 'SES - Resources - Directory',
          types: [GOOGLE_DRIVE],
          link: 'https://drive.google.com/drive/folders/1C3LlOv1R2DGOdFYKAyZ__btJUqBqMR-y?usp=sharing',
        },
        {
          id: '6c1b22cc-71c9-4b35-beea-675e273dcfd5',
          name: 'SES - Background Report',
          types: [DOCUMENT_FILE],
          link: 'https://docs.google.com/document/d/14ibEAD809XdQWyxRrQQ6HxS8in8HbMLT4ZUTZcwbbEo/edit?usp=sharing'
        }
      ]
    },
    {
      id: '0689145d-9b11-4ecc-b5e7-b6260de029c6',
      name: 'Black Lives Matter Toronto - Resources',
      types: [DIRECTORY],
      children: [
        {
          id: 'b3725ef2-8983-427f-a33e-3b5b435e09fc',
          name: 'Black Lives Matter Toronto - Resources - Directory',
          types: [GOOGLE_DRIVE],
          link: 'https://drive.google.com/drive/folders/1oCYrGqaV82IxqBWegb7GebZSfKyPVb2P?usp=sharing',
        },
        {
          id: '24c10184-2944-42d2-ac2e-7c1d77746c0a',
          name: 'Black Lives Matter Toronto - Background Report',
          types: [DOCUMENT_FILE],
          link: 'https://docs.google.com/document/d/18Fc3r0rdxNq14cOZv15xnod3L7kpNVRh5sPMYkqUZTY/edit?usp=sharing'
        }
      ]
    },
    {
      id: "7f95d84b-ba65-42bd-8665-4b746636d85c",
      name: "Defund.ca - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "0e6e137d-5fb9-4e7d-b626-2484d0a28cbc",
          name: "Defund.ca - Resources - Directory",
          types: [GOOGLE_DRIVE],
          link: "https://drive.google.com/drive/folders/14OFvbsD65J1lkZ1geQU8rSsp4UtOhxao?usp=sharing",
        },
        {
          id: "d9f93895-4895-4440-8c3c-a9b2e51b0f60",
          name: "Defund.ca - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1yZ7qtajdjX2SwhiVI0piNbjza8_zL_sHunHi1bEks9A/edit?usp=sharing"
        }
      ]
    },
    {
      id: '6b6fe3b8-4021-4b55-bf44-cc888de86f7d',
      name: 'City for All Women Initiative - Resources',
      types: [DIRECTORY],
      children: [
        {
          id: '6e8585ef-299c-4c17-8df4-125384be0202',
          name: 'City for All Women Initiative - Resources - Directory',
          types: [GOOGLE_DRIVE],
          link: 'https://drive.google.com/drive/folders/1sqAzpb_T8hYi3MyikxWhQgnUMux_GKlh?usp=sharing',
        },
        {
          id: '5cfe10d8-96d9-4698-a205-9f698a13140d',
          name: 'City for All Women Initiative - Background Report',
          types: [DOCUMENT_FILE],
          link: 'https://docs.google.com/document/d/1NijxQNlyscs51q8b-_xyw8rzsvGGuStSthAYJVNsgDA/edit?usp=sharing'
        }
      ]
    },
    {
      id: 'ce25dcb6-f4fa-40e7-82e5-9a42497929c6',
      name: 'Herongate Tenant Coalition - Resources',
      types: [DIRECTORY],
      children: [
        {
          id: 'feb476f7-c351-4cb8-ba4e-1d5443129eb1',
          name: 'Herongate Tenant Coalition - Resources - Directory',
          types: [GOOGLE_DRIVE],
          link: 'https://drive.google.com/drive/folders/1OQy4Xk-LLKGbo1aJ0XPcDncMLGisESor?usp=sharing',
        },
        {
          id: 'c0dede02-0911-47d7-87c4-286045e78e1e',
          name: 'Herongate Tenant Coalition - Background Report',
          types: [DOCUMENT_FILE],
          link: 'https://docs.google.com/document/d/1Nef7IK1YUcjvIcg6vtCanyZoBmvG-U43U9Ef1d8MNRI/edit?usp=sharing'
        }
      ]
    },
    {
      id: '7741903e-bdd2-4a4c-8427-c64a7123991a',
      name: 'Doctors for Defunding Police - Resources',
      types: [DIRECTORY],
      children: [
        {
          id: 'ead45587-2fd0-48fa-901d-30d119ac1a52',
          name: 'Doctors for Defunding Police - Resources - Directory',
          types: [GOOGLE_DRIVE],
          link: 'https://drive.google.com/drive/folders/1ADgQsvbQUD5j16iumvSFdWbph4DAsooR?usp=sharing',
        },
        {
          id: '0ab2b811-1934-4821-ba6c-022eeebd7166',
          name: 'Doctors for Defunding Police - Background Report',
          types: [DOCUMENT_FILE],
          link: ''
        }
      ]
    },
    {
      id: '3da650f3-c558-4604-8413-c14ff6f1b76a',
      name: 'Overdose Prevention Ottawa - Resources',
      types: [DIRECTORY],
      children: [
        {
          id: '27f4895e-9d2e-4a95-812f-8336c587b8af',
          name: 'Overdose Prevention Ottawa - Directory',
          types: [GOOGLE_DRIVE],
          link: 'https://drive.google.com/drive/folders/1IbnqzHQK64EXXMenKvLu2Ng2fYt9LOZM?usp=sharing',
        },
        {
          id: '1fdd4106-4e8d-46e3-bc6d-74e227b303cf',
          name: 'Overdose Prevention Ottawa - Background Report',
          types: [DOCUMENT_FILE],
          link: 'https://docs.google.com/document/d/1N8TFnOyAsJFXNk_vJITH89JjeQ94nwXKqBDF9T8MJqg/edit'
        }
      ]
    },
    {
      id: "8b04486a-e8a9-4076-ae2e-d0bfc9eb82d7",
      name: "613/819 Black Hub Noir - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "cf353f1b-2965-41ff-8a45-a8976908a620",
          name: "613/819 Black Hub Noir - Directory",
          types: [GOOGLE_DRIVE],
          link: "https://drive.google.com/drive/folders/1wi6SFv7PFmHflFoyUwVi62Zo-351-eh8?usp=sharing",
        },
        {
          id: "1ec1d146-a3a1-4bc6-9ac7-f28ed30644d3",
          name: "613/819 Black Hub Noir - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1zKMtpsHtbwym_EAiX-SMz5jLxZwWeK7BZZDd1Dtz_4k/edit?usp=sharing"
        }
      ]
    },
    {
      id: "821f8dce-0cdd-4ecb-9f76-672dbaacc4de",
      name: "Women's March Ottawa - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "7b68d0dc-71a9-47c1-9c5b-c87269ec808e",
          name: "Women's March Ottawa - Directory",
          types: [GOOGLE_DRIVE],
          link: "https://drive.google.com/drive/folders/1OON1uIePsBt4aCFeGOff2PYPnpnbHHE5?usp=sharing",
        },
        {
          id: "fda6f7fb-8af8-4bf7-8faa-e9c1586eb78a",
          name: "Women's March Ottawa - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1Oenilcg5ckfvuF_Iwnu8nm9GSLdfjW-iZYrExd0wmks/edit?usp=sharing"
        }
      ]
    },
    {
      id: "f1337c4f-5274-4414-a9a3-d68782417b6f",
      name: "No Peace Until Justice - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "5c773cfb-5d14-4486-b445-19c6364ac215",
          name: "No Peace Until Justice - Directory",
          types: [GOOGLE_DRIVE],
          link: "https://drive.google.com/drive/folders/1_bMEjv3ZlQkK8X_6dfJkAT9WdU8dfs-X?usp=sharing",
        },
        {
          id: "cdbb181c-d96a-4779-86cf-8be7713074ea",
          name: "No Peace Until Justice - Background Report",
          types: [DOCUMENT_FILE],
          link: ""
        }
      ]
    },
    {
      id: "c916ba1b-5e3c-4e75-a226-c8f915b7ce21",
      name: "African Canadian Association of Ottawa - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "b53bb1ca-0462-4fec-8135-6ad42ef8e6fc",
          name: "African Canadian Association of Ottawa - Directory",
          types: [GOOGLE_DRIVE],
          link: "https://drive.google.com/drive/folders/14vzqOlHqUMv1hOUQvaoWr4naWbt3qhMl?usp=sharing",
        },
        {
          id: "165da693-0ae2-41d5-80bb-e2e59d95303c",
          name: "African Canadian Association of Ottawa - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/18V0jinCAPGJTdWP3ZKz4BYkMLRkg09Uf4Qw7V0R0o5U/edit?usp=sharing"
        }
      ]
    },
    {
      id: "1088ca22-4f9b-49bc-9334-086d4200b8fa",
      name: "Equal Chance - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "4c9a4982-b66e-40c2-863b-9d715954b36e",
          name: "Equal Chance - Directory",
          types: [GOOGLE_DRIVE],
          link: "https://drive.google.com/drive/folders/1MsnQa9v8wIZeYvfj2Dy7WV99mh005wYE?usp=sharing",
        },
        {
          id: "8282fa0d-f04d-4a3a-8a9e-08369a7861df",
          name: "Equal Chance - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1ZtuEfitcvKn_4PSWBenHXLIIAnDj72bl9uA1sFQZPKk/edit?usp=sharing"
        }
      ]
    },
    {
      id: "8943c76b-b76c-43af-bacf-ac69f1e9e535",
      name: "Coalition Against More Surveillance - Ottawa - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "ab4823f6-6f64-4b96-b500-7d592426ab42",
          name: "Coalition Against More Surveillance - Ottawa - Directory",
          types: [GOOGLE_DRIVE],
          link: "https://drive.google.com/drive/folders/1LCDTdCiRcafZDhb9fsduQKRX8SPnzmrU?usp=sharing",
        },
        {
          id: "d3bdd12e-f945-46c8-a8a3-4cfce5205b37",
          name: "Coalition Against More Surveillance - Ottawa - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://drive.google.com/drive/folders/1LCDTdCiRcafZDhb9fsduQKRX8SPnzmrU?usp=sharing"
        }
      ]
    },
    {
      id: "975dd222-37d4-465a-bc18-aa2500bfe136",
      name: "ACORN Ottawa - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "5576223d-2106-4f71-aafe-eb95c2b6a043",
          name: "ACORN Ottawa - Directory",
          types: [GOOGLE_DRIVE],
          link: "https://drive.google.com/drive/folders/173ruUnEMKvRon2nP9DkTghQmCnNa8ABm?usp=sharing",
        },
        {
          id: "d6941d96-b036-4d57-a598-d52c4728c331",
          name: "ACORN Ottawa - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1Bz2y-9Ku0gAFa8Nyy3tLm3_C0SDTOeuyjpJMeQzMnn8/edit?usp=sharing"
        }
      ]
    },
    {
      id: "2dbc995f-9dd2-4fcf-ac26-ac9336e7825e",
      name: "Ecology Ottawa - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "cd693293-745d-426f-9483-03ab65035b9a",
          name: "Ecology Ottawa - Directory",
          types: [GOOGLE_DRIVE],
          link: "https://drive.google.com/drive/folders/1FkhRuy0nmpNBF6nr0NOySjeoZrO8WL1s?usp=sharing",
        },
        {
          id: "7d6e5533-eb30-45a4-a6f3-3d050e644589",
          name: "Ecology Ottawa - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1l_Uq7VmJ0sReVVgF16Ot78iI50eKV6DSe1GNRtaE5uQ/edit?usp=sharing"
        }
      ]
    },
    {
      id: "7f5f511a-2954-4776-b4f3-418ef318d1de",
      name: "Punch Up Collective - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "9c90b350-0529-45a0-bdee-853e027099a3",
          name: "Punch Up Collective - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1bsH0OsmhGe_dNpnPxa8UckYsRnwyqbxXTIxrz8BwoMI/edit?usp=sharing"
        }
      ]
    },
    {
      id: "7785e06f-2ed5-49b8-94f5-540e1e2d481b",
      name: "Centretown Community Association",
      types: [DIRECTORY],
      children: [
        {
          id: "f259b85a-7368-4600-9dac-7f1fc248c33f",
          name: "Centretown Community Association",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1jKKYIdBYfg7sZKMUqtR5DV9mP9_Bu34XAr3t-CbNz4w/edit"
        }
      ]
    },
    {
      id: "bebba337-a190-4592-aae3-db83164a3ab5",
      name: "Centre for Urban Research and Education - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "bffad5b9-733d-4395-ac1e-de8239682f02",
          name: "Centre for Urban Research and Education - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1H_tdiPzJDDJgCb1wv_TS7529E5vJYFKINhCzDBztqZo/edit?usp=sharing"
        }
      ]
    },
    {
      id: "01781e77-ebbb-4612-915c-4a5c97bf8818",
      name: "SAEFTY - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "3e56da60-e28b-4fe3-a8f6-d815d9d4eed4",
          name: "SAEFTY - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1MvGup4Mym938zbAERYdnOCCgqvQWiKwrrHa2_Gvg5AE/edit?usp=sharing"
        }
      ]
    },
    {
      id: "68fba8c6-fd89-4e5c-9be5-076c578ee64f",
      name: "Kind Space - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "806f456a-f344-481d-bc19-b75277b47291",
          name: "Kind Space - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/16pYZSCSc_jqOzjFYbzKg58rdADWgDrq8tGdcEoV8oG8/edit?usp=sharing"
        }
      ]
    },
    {
      id: "64c5c9fc-0180-48e1-a227-af323324e081",
      name: "Canadian Centre for Gender and Sexual Diversity - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "7a036c27-7b76-46ab-9378-d22bacb4db57",
          name: "Canadian Centre for Gender and Sexual Diversity - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/180Nz8P_d4GoVhgsjKOiGLhH_AjmkRnGE0B3cIb4gGjg/edit?usp=sharing"
        }
      ]
    },
    {
      id: "942db2d4-8e93-4be7-86f3-e8ee997b39d1",
      name: "Free Transit Ottawa - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "1ea54e51-46e9-4489-b7e9-0b9f1ef43d2a",
          name: "Free Transit Ottawa - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/18nWndsDy8jA3ol-iC25n0lo_6U7gYe0QvdlnMUl69y8/edit?usp=sharing"
        }
      ]
    },
    {
      id: "83213477-3183-4683-b15e-dc4dc2239b23",
      name: "OCTEVAW - Ottawa Coalition to End Violence Against Women - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "447b8d80-5cf5-4bce-b021-ca4fdb18d13a",
          name: "OCTEVAW - Ottawa Coalition to End Violence Against Women - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1JeTGfqm-5Bg7zrVGqMyUNm_tWKQTO20V2kWzO58hAMk/edit?usp=sharing"
        }
      ]
    },
    {
      id: "08ad6772-908e-478e-bc21-3c6d9cff8bc1",
      name: "Alliance to End Homelessness Ottawa - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "d01035ed-cce5-471d-aeed-70db2a751fbd",
          name: "Alliance to End Homelessness Ottawa - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1dJsMp9KS0-nkusBR9PKLekIcZnQFwrdTSkZFionUQ_s/edit?usp=sharing"
        }
      ]
    },
    {
      id: "5ac578d3-a40f-4906-9ae5-2948dfc532f9",
      name: "Indigenous Solidarity Ottawa - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "153285da-1bf6-4e30-b61c-ca74efb7a39c",
          name: "Indigenous Solidarity Ottawa - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1RoUH3LazPEUDOiZwk7MslqavnQKZKCrS0as2pWCbT_A/edit?usp=sharing"
        }
      ]
    },
    {
      id: "13d2bfd2-b835-42a1-aa1e-36ade99c9d21",
      name: "Ottawa Black Diaspora Coalition - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "79c583c3-a108-4d47-9342-56fc67545191",
          name: "Ottawa Black Diaspora Coalition - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1COWC1DdOxDgADrFUz7RSlHmO78ctzh5Vi0VZRfyt400/edit?usp=sharing"
        }
      ]
    },
    {
      id: "e5c72ae6-e086-470a-8b01-fab6b3b2c9df",
      name: "Childcare Now Ottawa - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "1227c100-acf4-4416-87b4-eb48b8fff510",
          name: "Childcare Now Ottawa - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1l5rVn-fXlztqiEQuyX0PxPID9pQelZ-IgGfsu2Y-qss/edit?usp=sharing"
        }
      ]
    },
    {
      id: "9bbeb71c-f39d-4462-8487-0fb300c37da3",
      name: "uRacism - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "5a23eb1b-fdfd-4947-8a5d-3dd247f33674",
          name: "uRacism - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1qtc2FQg1AIbS6WYjrSR6gaMNFElnrB5P2M359-RB6Y8/edit?usp=sharing"
        }
      ]
    },
    {
      id: "11426c8c-b626-400b-9d38-fce1de8e1e4c",
      name: "Fridays for Future Ottawa - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "9dec5936-29e5-40d6-b02f-808f88a69ffc",
          name: "Fridays for Future Ottawa - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1zM1NGn65TH5vgUwZx-wvj-h30DGkiH04-rgxc8Uoyio/edit?usp=sharing"
        }
      ]
    },
    {
      id: "ab711335-bb35-4a0f-b04b-d6cd893b5e10",
      name: "Ottawa Community Foundation - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "b00ccd08-aff4-466e-a389-e7bdc9d3c23a",
          name: "Ottawa Community Foundation - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/12qVtTCXpeP3Rxs4QBA9WTITGUZcjNr-C3BGIBA2FLG0/edit?usp=sharing"
        }
      ]
    },
    {
      id: "a208dc46-02e2-4392-9cfb-a1cda7fcd086",
      name: "The Street Resilience Project - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "e0238451-e9f3-43d6-a796-dc8ee20e5e25",
          name: "The Street Resilience Project - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1AhN01IXMh8xAUa6bzjKDuSbVoTQ2pam8zgrOrzJ6YAc/edit?usp=sharing"
        }
      ]
    },
    {
      id: "093d42ff-dafc-4859-b974-851fa661626a",
      name: "Somerset West Community Health Centre - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "3cbc1ff3-6686-4f27-92c0-43071387f43f",
          name: "Somerset West Community Health Centre - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1ddtw4gykDwtXDNpq7MHV6QkWGiZYGYpU_AqJciuNzZ0/edit?usp=sharing"
        }
      ]
    },
    {
      id: "c1a132a1-65cc-44cc-8f44-4c94e0fd30c8",
      name: "The African and Caribbean Council on HIV/AIDS in Ontario - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "c5e8fb87-87ed-4ecc-8994-ca25889f0bb6",
          name: "The African and Caribbean Council on HIV/AIDS in Ontario - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1oS-VrjvkjxNt7T7RZ3O8eL8sQwAE6esqguLqmP3nbu8/edit?usp=sharing"
        }
      ]
    },
    {
      id: "7b39ae05-5ae6-4e7d-b8c5-d24e9fd5410f",
      name: "Mood Disorders Ottawa - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "1cc310b6-cd44-457b-859f-0ea64addef33",
          name: "Mood Disorders Ottawa - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1GOZQWbkvEtRkIuIExSRgU8vD526qRdM5G0VXMbIofRY/edit?usp=sharing"
        }
      ]
    },
    {
      id: "0ba8a394-d5fa-4815-8806-08ef419d0f20",
      name: "The Afro-Canadian Chamber of Commerce - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "31033ac2-f2f3-4838-b19b-38b9943764d4",
          name: "The Afro-Canadian Chamber of Commerce - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1WQYwugMKDCnt08GUpUMKub1AIOjVb2WYNItyumpdMO0/edit?usp=sharing"
        }
      ]
    },
    {
      id: "ee0c1741-8520-4cb6-af89-409c60becf64",
      name: "Arts Connect Ottawa - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "f50b743b-7536-481f-b6f9-465237665670",
          name: "Arts Connect Ottawa - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1Qd04qqxBSp2PaFhY4BGbj512YopErwOAkGcU4_whG_k/edit?usp=sharing"
        }
      ]
    }
    // {
    //   id: "",
    //   name: " - Resources",
    //   types: [DIRECTORY],
    //   children: [
    //     {
    //       id: "",
    //       name: " - Background Report",
    //       types: [DOCUMENT_FILE],
    //       link: ""
    //     }
    //   ]
    // },
  ]
};