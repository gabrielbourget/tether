import {
  DOCUMENT_FILE, DIRECTORY, PDF_FILE, WEBSITE, VIDEO_FILE, EPUB_FILE, AUDIO_FILE, IMAGE_FILE,
} from '../../../../../../constants';
import { Resource } from "../../../../../../Types/"

import { OPSResources } from "./OPSResources";

export const generalResourceCommons: Resource = {
  id: "9470f447-389b-4539-b209-779c86e0636f",
  name: "General Resource Commons",
  types: [DIRECTORY],
  children: [
    {
      id: "9ddec7ea-c1ce-46ae-b64c-06639938060d",
      name: "Ontario Colleges",
      types: [DIRECTORY],
      children: [
        {
          id: "9906513b-d449-4e60-920d-e6253ed055ef",
          name: "Ontario Colleges Offering Police Programs",
          types: [WEBSITE],
          link: "https://www.ontariocolleges.ca/en/programs/fire-justice-and-security/police",
        },
      ]
    },
    {
      id: "6381cccf-455e-46ea-882d-3d22a2305743",
      name: "Ontario Statutes",
      types: [DIRECTORY],
      children: [
        {
          id: "2f61e294-fa01-472b-a188-52b68e0e3b3a",
          name: "Comprehensive Ontario Police Services Act, 2019",
          types: [DIRECTORY],
          children: [
            {
              id: "776f2e3d-5d6d-4661-a20f-cd971a65dd65",
              name: "Comprehensive Ontario Police Services Act, 2019",
              types: [PDF_FILE, DOCUMENT_FILE],
              link: "https://drive.google.com/file/d/16H-MYWPCzG8Fw0XZL14J-Po6PmEGMvnn/view?usp=sharing",
            }
          ]
        }
      ]
    },
    {
      id: "f329f21d-6d25-4543-862e-d6222ef6461b",
      name: "Somerset West Community Health Centre",
      types: [DIRECTORY],
      children: [
        {
          id: "d3558c28-cef0-4f75-9a38-89b67f1d9889",
          name: "Who to Call - Police Alternatives for First Response/Community Service Needs",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1ZAg2H4OF06Bt3sM6D-XKedQAroZeIbXd/view?usp=sharing"
        }
      ]
    },
    {
      id: "c71b30df-6313-452d-a8fc-cf1ca856b4b5",
      name: "SURJ Toronto",
      types: [DIRECTORY],
      children: [
        {
          id: "58965836-5698-4bcd-895e-f535f3b7fd0f",
          name: "[Jan 07, 2021] - Defunding the Toronto Police: City Budget Edition - Notes",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1gkdsfyxeUAlY2CnakC863KveRoeTB4UiHIRSvrta8ns/edit?usp=sharing",
        }
      ]
    },
    {
      id: "8f2ac2ee-5857-474e-8ece-26983fc8ca2d",
      name: "Jane Finch Action Against Poverty (JFAAP)",
      types: [DIRECTORY],
      children: [
        {
          id: "790f7d10-3739-4aee-8c13-b23a60fc0a2b",
          name: "Permanently Temporary: Labour, Precarity and Resistance in Jane Finch",
          types: [DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1wQ35schvZ-QmOwmqrgqWcIZPIGqRvmsQ/view",
        },
        {
          id: "27560383-d555-4b5f-be40-bf3607857655",
          name: "Home Feeling: Struggle for a Community (1984)",
          types: [VIDEO_FILE],
          link: "https://www.nfb.ca/film/home_feeling_struggle_for_a_community",
        },
        {
          id: "67e86be1-1c9f-4d05-b0ea-98837b094a7a",
          name: "Jane Finch Again!",
          types: [VIDEO_FILE],
          link: "https://www.cbc.ca/cbcdocspov/features/despite-what-you-have-heard-torontos-jane-and-finch-community-is-joyful-res",
        }
      ]
    },
    {
      id: "555f93ad-0022-4bf2-a817-788332f9b19a",
      name: "Alex Vitale",
      types: [DIRECTORY],
      children: [
        {
          id: "524a0496-daa5-4729-ac6e-cae826d70071",
          name: "The End of Policing",
          types: [DIRECTORY],
          children: [
            {
              id: "9c31c5fd-f276-44ca-a912-e554accabb6b",
              name: "The End of Policing - Alex Vitale",
              types: [DOCUMENT_FILE, EPUB_FILE],
              link: "https://drive.google.com/file/d/1wt8-8cfnnnSjH7Ci4aMQhBaUQA3B0eWX/view?usp=sharing"
            },
            {
              id: "1b973ca3-73c1-4b03-9407-1a8e1514fa99",
              name: "The End of Policing - Alex Vitale",
              types: [DOCUMENT_FILE, PDF_FILE],
              link: "https://drive.google.com/file/d/1RcVWLzXrPLDcQH5D6m9mYkqOwGGlRIaP/view?usp=sharing",
            },
            {
              id: "078db76e-878a-4718-83b3-6e7f85b2b325",
              name: "Notes",
              types: [DIRECTORY],
              children: [
                {
                  id: "1c5d1866-fca7-457f-83c1-55c0141e4792",
                  name: "C1 - The Limits of Police Reform - Notes",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1urHKHCZqGqzE80yszKoNtJDKy0r8dTpphrIib8A4nSk/edit?usp=sharing",
                },
                {
                  id: "7b1b390b-ba9b-468b-8164-0624108cf32e",
                  name: "C2 - The Police Are Not Here to Protect You - Notes",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1jXatgIhP3ebJUZO4v2mOCBUDnd1EOkw8ZMuw7yUJlnQ/edit?usp=sharing",
                },
                {
                  id: "6115dc1d-e0b0-43e3-ba0a-092723cfb983",
                  name: "C3 - The School-To-Prison Pipeline - Notes",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1l3R2-ZqzIGsba2cy7nX1IVNY0L9JXdsqwvIrV5XX1r4/edit?usp=sharing",
                },
                {
                  id: "02a0e58e-db2b-42ff-b089-31e08bba8e3e",
                  name: "C3 - The School-To-Prison Pipeline - Abridged Notes",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1l3R2-ZqzIGsba2cy7nX1IVNY0L9JXdsqwvIrV5XX1r4/edit?usp=sharing",
                },
                {
                  id: "d0aa983a-851c-4032-b42e-56a0194997be",
                  name: "C4 - 'We Called For Help, and They Killed My Son' - Notes",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1511FTuhUDa-IWEZeDadMPxtXxj-6RWutDg20gBX9Jmo/edit?usp=sharing",
                },
                {
                  id: "b6a2db79-91c1-4de5-b93a-efab9de5c71e",
                  name: "C5 - Criminalizing Homelessness - Notes",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1DwGKJWlJJt2IbeCtjZhwivvEHzOXnP2op0JIm-tThIg/edit?usp=sharing",
                },
                {
                  id: "4d88217d-f4cb-47f1-a828-5943f3a18a20",
                  name: "C6 - The Failures of Policing Sex Work - Notes",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1KOwQJucefuacxDwF2XjT2RGLTIe4XhD8ktGegZ9rf-I/edit?usp=sharing",
                },
                {
                  id: "3d2571b8-0be4-4769-8f96-615d51c6e481",
                  name: "C7 - The War on Drugs - Notes",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/17dgGc3IKlkLOzp84H_cFmei2YRwq13uAO1fs1fVD1UU/edit?usp=sharing",
                },
                {
                  id: "c9a141e5-a8dc-49d5-9b70-113a58d6fe97",
                  name: "C8 - Gang Suppression - Notes",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1iQshfhNmdC0JGJZuKjBMIhcCYn_07tOro4s8dkDnaXk/edit?usp=sharing",
                },
                {
                  id: "f34a2500-43c4-42df-aeef-1c16577ba994",
                  name: "C9 - Border Policing - Notes",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1KHjQL9kD_0vUNeQBlgSg9HRee5CXi_QZqHj2u8CKDSo/edit?usp=sharing",
                },
                {
                  id: "bee3dc66-4889-4039-add3-f2abd6b83a95",
                  name: "C10 - Political Policing - Notes",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1P1z71WElp7tz3oUDiyHDr1E4sJWFq5Gw-6qiLwF-WRk/edit?usp=sharing",
                },
                {
                  id: "1eea4892-f596-4284-a7e9-5c32a0f809d4",
                  name: "Conclusion - Notes",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1B6B7qJ8VCNAMJd6YzqfQYaUQkKmJ9h3GAkEqzDDNlEc/edit?usp=sharing",
                },
                {
                  id: "d2e0f8c2-63af-418e-bacc-3c862a5186e5",
                  name: "Knowledge Resources Overview",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1leiW-Mb97evyH-4r686B1IXhf5_GW9Z6SscCgOa8MEQ/edit?usp=sharing",
                },
              ]
            }
          ]
        }
      ]
    },
    {
      id: "dbcb5689-9620-4d7a-b948-677a98be0347",
      name: "Papers/Reports",
      types: [DIRECTORY],
      children: [
        {
          id: "e493ffef-3548-43b4-8997-d23023f2d6bf",
          name: "[Interrupting Criminalization] - The Demand is Still #DefundThePolice: Lessons from 2020",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1DoEt20oeXu-VzhRVF_clsz5YhZu67BUp/view?usp=sharing"
        },
        {
          id: "b6b9fa27-57b7-4970-967b-b1e72fd09ef0",
          name: "[Mariame Kaba and collaborators] - What's Next? Safer and More Just Communities Without Policing",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1X0dE1B3W0N7E7ymw4-79xBRVanh-uRnk/view?usp=sharing"
        }
      ]
    },    
    {
      id: "6f99e89f-ee2c-45c2-be4f-3237a336979d",
      name: "Articles",
      types: [DIRECTORY],
      children: [
        {
          id: "c9c130a4-88e6-4429-a68f-8be3dc491506",
          name: "[Bloomberg] - Where Calling the Police Isn't the Only Option",
          types: [WEBSITE],
          link: "https://www.bloomberg.com/news/articles/2020-09-03/alternative-policing-models-emerge-in-u-s-cities"
        },
        {
          id: "f70c1854-6eba-455b-9ff4-4490892bf93e",
          name: "[Crosscut] - Ahead of Seattle Police Layoffs, Officers Are Leaving on Their Own",
          types: [WEBSITE],
          link: "https://crosscut.com/news/2020/10/ahead-seattle-police-layoffs-officers-are-leaving-their-own",
        },
        {
          id: "2cd2cffe-9fc7-48da-8d9b-8b350466bc42",
          name: "[Jacobin] - The Best Way to 'Reform' the Police Is to Defund the Police",
          types: [WEBSITE],
          link: "https://jacobinmag.com/2020/06/defund-police-reform-alex-vitale?fbclid=IwAR3MHAFd7a9INqrMSwKDbY_bMncfn5dZUXyUAhkfHAAVwtfmtmdjqju4R2I",
        },
        {
          id: "03b72b0d-04ca-4051-b433-c983e1956597",
          name: "[Broadbent Institute] - Policy Options for Defunding the Police & Creating Alternative Services of Safety and Support",
          types: [WEBSITE],
          link: "https://www.broadbentinstitute.ca/policy_options_for_defunding_the_police_creating_alternative_services_of_safety_and_support",
        },
        {
          id: "938f88a7-f3cd-4243-a04f-baf43497f563",
          name: "PACA - Pan-African Community Action",
          types: [DIRECTORY],
          children: [
            {
              id: "9a40fddb-3312-45d5-912f-f7bb024f8b50",
              name: "Community Control Over Police - A Proposition",
              types: [WEBSITE],
              link: "https://pacapower.org/index.php?page=community-control-over-police-2"
            },
          ]
        },
        {
          id: "14a7a381-e376-46bd-a8b9-92246293da30",
          name: "CBC",
          types: [DIRECTORY],
          children: [
            {
              id: "9e9b13e7-e2b8-4769-9f93-483985938e80",
              name: "[CBC] - Toronto Pilot Project Could Remove Police from Mental Health Calls - But Not in Emergencies",
              types: [WEBSITE],
              link: "https://www.cbc.ca/news/canada/toronto/police-mental-health-crisis-toronto-pilot-1.5882296#:~:text=After%20months%20of%20mounting%20calls,calls%20for%20people%20in%20crisis",
            }
          ]
        },
        {
          id: "d6c420b1-31e5-49ab-89de-1f75a6beaa02",
          name: "[Vice] - How to Build a GLobal Abolition Movement",
          types: [WEBSITE],
          link: "https://www.vice.com/en/article/qjpjv7/how-to-build-a-global-movement-to-abolish-prison-police-v27n4",
        }        
      ]
    },
    {
      id: "ce7dbcab-21d8-4f00-bc0e-77c710d15f99",
      name: "Videos",
      types: [DIRECTORY],
      children: [
        {
          id: "229e957f-e80e-417b-be63-5000397dbf50",
          name: "[Jacobin] - Alex Vitale: We Need to Defund the Police Now",
          types: [VIDEO_FILE],
          link: "https://www.youtube.com/watch?v=79fgvA21QCQ",
        },
        {
          id: "4643f079-784d-4c8b-87f0-769669ce0443",
          name: "The Michael Brooks Show",
          types: [DIRECTORY],
          children: [
            {
              id: "ce289676-7053-48d2-9d72-ae4f16281574",
              name: "Defund the Police in Texas - How to Build a Movement That Can Win",
              types: [DIRECTORY],
              children: [
                {
                  id: "9268fb12-c8ae-437f-a7cf-1466b4720856",
                  name: "Defund the Police in Texas - How to Build a Movement That Can Win",
                  types: [VIDEO_FILE],
                  link: "https://www.youtube.com/watch?v=jcjEam0YHWg"
                },
                {
                  id: "bdfb43e0-28e5-4f09-9ada-53066eb0f51a",
                  name: "Defund the Police in Texas - How to Build a Movement That Can Win - Notes",
                  types: [DOCUMENT_FILE],
                  link: "https://docs.google.com/document/d/1d1tkQ0WE6K24dKv_JAuP6Xdu_hHQJJrxICX1k1juQDY/edit?usp=sharing"
                }
              ]
            }
          ]
        },
        {
          id: "dd3a5b63-f04d-46af-a7fb-4df1758b2b42",
          name: "[Chicago DSA] - Organizer Training Session on Power Mapping",
          types: [VIDEO_FILE],
          link: "https://www.youtube.com/watch?v=FWTwYAbVFRc",
        },
      ]
    },
    {
      id: "dad22d1f-4b66-478d-b4f1-91e0da463741",
      name: "Audio",
      types: [DIRECTORY],
      children: [
        {
          id: "8750ae59-fcd3-4cc5-ad43-273991bfb4cb",
          name: "[CBC - All in a Day] - Helpful or Harmful: A Psychotherapist on Student Resource Officers",
          types: [AUDIO_FILE],
          link: "https://www.cbc.ca/listen/live-radio/1-92-all-in-a-day/clip/15815167-helpful-harmful-a-psychotherapist-student-resource-officers"
        },
        {
          id: "6aac3444-18ca-47db-bf42-5f401ce02b87",
          name: "Practical Prison Abolition Politics in Health Care Contexts",
          types: [AUDIO_FILE],
          link: "https://talkingradical.ca/2021/01/19/radio-practical-prison-abolition-politics-in-health-care-contexts/",
        },
      ]
    },
    {
      id: "a8b0d248-5af7-4903-abb1-88b708d46344",
      name: "Restorative Justice",
      types: [DIRECTORY],
      children: [
        {
          id: "993df94b-f777-4152-9e0f-cfa3118c047c",
          name: "Restorative Practices - Fostering Health Relationships and Promoting Positive Discipline in Schools",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://drive.google.com/file/d/1gmjj5PSz4jZY390TKbsWOEzaEDY4SoNo/view?usp=sharing",
        }
      ]
    },
    {
      id: "18d8dd20-2a9e-4dad-a2d7-4a666a321e6e",
      name: "Police Associations",
      types: [DIRECTORY],
      children: [
        {
          id: "9751583f-e4b1-49b3-a7ee-010e27ee6514",
          name: "Ontario Association of Chiefs of Police",
          types: [DIRECTORY],
          children: [
            {
              id: "a8a560fb-e28b-4db0-91e0-9db63ae88f5e",
              name: "Ontario Association of Chiefs of Police - Website",
              types: [WEBSITE],
              link: "https://www.oacp.ca/en/index.aspx",
            },
            {
              id: "c915dcaa-6add-4012-9fc6-8eb76118b4b2",
              name: "Ontario Association of Chiefs of Police - Background Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1MO4Xw6catawvoWF4pE_PxPjQjJW4IPiql0qsXrCxa3I/edit?usp=sharing",
            },
            {
              id: "60c83973-0fb9-4612-b9cf-9091d1d6e679",
              name: "History of the Ontario Association of Chiefs of Police",
              types: [PDF_FILE, DOCUMENT_FILE],
              link: "https://drive.google.com/file/d/1Fo1rDQLomwEoLPTeia4tWp5DToPHSWZm/view?usp=sharing",
            },
            {
              id: "0ad5f2f4-8e17-4b2e-9fb5-00155b2c550f",
              name: "OACP Five Year Plan - 2017",
              types: [PDF_FILE, DOCUMENT_FILE],
              link: "https://drive.google.com/file/d/1IM8WfhW8Nxd2CNKI5adyYZn3AOW65oCf/view?usp=sharing",
            },
            {
              id: "aa6ef0cf-f802-4c65-9445-b2cd459ad2c7",
              name: "Constable Selection System",
              types: [DIRECTORY],
              children: [
                {
                  id: "cd400a28-7b17-4161-b523-adc74d501d52",
                  name: "Becoming a Police Officer in Ontario",
                  types: [WEBSITE],
                  link: "https://oacpcertificate.ca/becoming-a-police-officer/",
                },
                {
                  id: "3b280ec8-2331-4cca-a0e3-823b339a10ed",
                  name: "Constable Selection System - Infographic",
                  types: [PDF_FILE, DOCUMENT_FILE],
                  link: "https://drive.google.com/file/d/1GxCsoIkWD4dGxHcbo2-HG2I2cLHEckLi/view?usp=sharing",
                },
                {
                  id: "6e97ded7-6bc0-4083-9f99-0ef7d8b20831",
                  name: "Constable Selection System - Brochure",
                  types: [PDF_FILE, DOCUMENT_FILE],
                  link: "https://drive.google.com/file/d/1kWiQOFzZsWzaO94rMikepGHTs-xx6p13/view?usp=sharing",
                },
              ]
            },
            {
              id: "2c27cef8-5ab6-4cc5-a22e-8c62837f8cb6",
              name: "Ontario Association of Chiefs of Police Certificate",
              types: [DIRECTORY],
              children: [
                {
                  id: "15294722-3b53-4bfb-bf60-33967e9a8b07",
                  name: "Ontario Association of Chiefs of Police Certificate - Website",
                  types: [WEBSITE],
                  link: "https://oacpcertificate.ca/",
                },
                {
                  id: "70c788e8-3bb0-4c48-a48f-1a294c7ebecb",
                  name: "Ontario Association of Chiefs of Police Certificate - Information Page",
                  types: [WEBSITE],
                  link: "https://www.oacp.ca/en/careers-in-policing/oacp-certificate.aspx",
                },
                {
                  id: "bea058d2-216d-4260-81e3-460dfd49b73d",
                  name: "Ontario Association of Chiefs of Police Certificate Testing Process - Infographic",
                  types: [PDF_FILE, DOCUMENT_FILE],
                  link: "https://oacpcertificate.ca/wp-content/uploads/2019/12/OACP-CSS-Chart-2.pdf",
                }
              ]
            },
          ]
        },
        {
          id: "f3b449d9-0688-4e3c-8532-06a662616d3d",
          name: "Police Association of Ontario",
          types: [DIRECTORY],
          children: [
            {
              id: "0c8c79a3-d051-403b-920d-a2bf8c75d627",
              name: "Police Association of Ontario - Website",
              types: [WEBSITE],
              link: "https://pao.ca/",
            },
            {
              id: "bdacb7de-ad62-4d07-9bea-ca017d0c8bb8",
              name: "Police Association of Ontario - Background Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1QGBLHHU8GEJwfF1lbRpguyZuYnZGe0-jukiA2PYDlAM/edit?usp=sharing",
            },
          ],
        },
        {
          id: "431a0c7e-cbea-4fb3-badb-3943b7edf6f1",
          name: "Ontario Provincial Police Association",
          types: [DIRECTORY],
          children: [
            {
              id: "34fb4346-d4d3-4c49-b573-b17785689613",
              name: "Ontario Provincial Police Association - Website",
              types: [WEBSITE],
              link: "https://www.oppa.ca/",
            },
          ]
        },
        {
          id: "9092732e-01b7-478b-b5a7-7581ff85a6a7",
          name: "Ottawa Police Association",
          types: [DIRECTORY],
          children: [
            {
              id: "549ae986-ebf1-4d7f-b2ac-61f985650850",
              name: "Ottawa Police Association - Background Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/12dXie7GjBV45qluDYvMY9pOwVuWW4w4pCYxSYDYMXPc/edit?usp=sharing",
            },
            {
              id: "df1e272a-c110-4577-a0e9-81a336159d05",
              name: "Ottawa Police Association - Website",
              types: [WEBSITE],
              link: "https://www.ottawapa.ca/"
            }
          ]
        },
        {
          id: "efb90bf0-634f-4ab2-bde5-805bd467b7e9",
          name: "Ontario Special Constables Association",
          types: [DIRECTORY],
          children: [
            {
              id: "58b47600-8653-4575-93a9-f35e1e032917",
              name: "Ontario Special Constables Association - Background Report",
              types: [DOCUMENT_FILE],
              link: "https://docs.google.com/document/d/1I_l_LA2LkI5Hrzi9yxgm_3UH7JHRapwVrVouZoEwpj0/edit?usp=sharing",
            },
            {
              id: "538fe292-07a4-4131-a788-e40105963cfd",
              name: "Ontario Special Constables Association - Website",
              types: [WEBSITE],
              link: "https://specialconstables.ca/",
            }
          ]
        }
      ]
    },
    {
      id: "f6dafcf5-7978-49cd-a06e-24b420bf1e2e",
      name: "Special Investigations Unit",
      types: [DIRECTORY],
      children: [
        {
          id: "e825bcc1-a145-4ec1-9e21-695a5d1d99e4",
          name: "Special Investigations Unit - Website",
          types: [WEBSITE],
          link: "http://www.siu.on.ca/en/index.php",
        },
        {
          id: "50876c99-591c-4aca-a1c4-32af64ab1974",
          name: "Special Investigations Unit - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1TvBWobWDE55loiseGwXhgB-Cb_pZ-5qoehdZq30nCKc/edit?usp=sharing",
        },
      ]
    },
    {
      id: "cdac1b47-706c-400c-8f78-73522c15e9ca",
      name: "Office of the Independent Police Review Director",
      types: [DIRECTORY],
      children: [
        {
          id: "37241ea9-602d-4fe5-8490-59707aaf0444",
          name: "Office of the Independent Police Review Director - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1htyYZMcMRPxG4wNupT3eeHmLvHJUTwxTU6eRugBAXrM/edit?usp=sharing",
        },
        {
          id: "c0b5017c-8725-44d8-a9b4-98c9047e6871",
          name: "Systemic Reviews",
          types: [DIRECTORY],
          children: [
            {
              id: "98763ac1-a691-443f-a27c-8c8bc6272071",
              name: "OIPRD - Police Interactions with People in Crisis and Use of Force - Interim Systemic Review Report",
              types: [DOCUMENT_FILE],
              link: "https://drive.google.com/file/d/1tO_KTwzJXcaiTShSVa2iT2na142kdxkH/view?usp=sharing",
            }
          ]
        },
      ]
    },
    {
      id: "d682305a-6710-438c-be27-7d11a3472652",
      name: "Ontario Police College",
      types: [DIRECTORY],
      children: [
        {
          id: "8766945b-9202-4d35-af69-798090f5f32d",
          name: "Ontario Police College - Background Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1voe04qUTMEbGfezbs2--qeA9hbTA836qIMOJAR_pxOM/edit?usp=sharing",
        },
        {
          id: "7c7d8b22-65fb-4f84-a70b-9c7cb62cd600",
          name: "Ontario Police College - Website",
          types: [WEBSITE],
          link: "https://www.mcscs.jus.gov.on.ca/english/police_serv/OPC/OPC_about.html",
        },
        {
          id: "7db6d81a-ca38-4b49-872f-473a00953764",
          name: "Ontario Police College - Entry on Ministry of the Solicitor General's Website",
          types: [WEBSITE],
          link: "https://www.mcscs.jus.gov.on.ca/english/police_serv/OPC/OPC_about.html",
        },
        {
          id: "e7b6f264-61df-4ec1-92e2-735bfa4037d8",
          name: "Ontario Police College - Wikipedia Entry",
          types: [WEBSITE],
          link: "https://en.wikipedia.org/wiki/Ontario_Police_College",
        },
        {
          id: "51a68698-821a-4fe5-9b4c-2de961cd01fb",
          name: "Course Calendars",
          types: [DIRECTORY],
          children: [
            {
              id: "b002a63d-5f35-4454-b820-1a6ec642d024",
              name: "Ontario Police College - Course Calendar - 2006",
              types: [PDF_FILE, DOCUMENT_FILE],
              link: "https://drive.google.com/file/d/19l76wrKqiHelYIkAWDkqTIg6omCbIvfs/view?usp=sharing",
            },
          ]
        },
        {
          id: "6ce239f3-118e-402b-a50a-7b140e7f1ba9",
          name: "Media Coverage",
          types: [DIRECTORY],
          children: [
            {
              id: "63cd8f23-ce53-4eca-92c0-1ae564ccbba2",
              name: "[The London Free Press] - Pandemic Postpones Constable Training, Forces Aylmer Police College to Rethink Instruction Model",
              types: [WEBSITE],
              link: "https://lfpress.com/news/local-news/ontario-police-college-exploring-new-ways-of-training-amid-covid-19-pandemic/",
            },
            {
              id: "e6886c64-b424-4218-9360-404ebcce7090",
              name: "[Blue Line] - Ontario Police College: Then and Now",
              types: [WEBSITE],
              link: "https://www.blueline.ca/ontario-police-college-then-and-now-5292/",
            }            
          ]
        }
      ]
    },
    {
      id: "1fffaeca-0318-4055-8d30-d5ae45df7a41",
      name: "Ottawa Police Services Board - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "91d66c05-a961-407c-aabb-e5228df5d5d4",
          name: "Ottawa Police Services Board - Top Level Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1hfGgjVuZS5lVbhVtHCaC66yL_gtXnrg6F8sqnZBDmxY/edit?usp=sharing",
        },
        {
          id: "ebaa3a6a-246d-4bef-b634-611acce7242b",
          name: "Police Services Board Vacancy Information Sheet",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "https://ottawapoliceboard.ca/opsb-cspo/sites/default/files/docs/psb_vacancy_information_sheet_s_31_09_24_14.pdf"
        },
        {
          id: "4fb9fb29-0b65-4a59-9438-03fa6f4ff161",
          name: "Ottawa Police Services Board - Website",
          types: [WEBSITE],
          link: "https://ottawapoliceboard.ca/opsb-cspo",
        },
        {
          id: "15bd1809-720c-4828-93df-3680b2b7a680",
          name: "[Ministry of the Solicitor General] - Police Service Boards",
          types: [WEBSITE],
          link: "https://www.mcscs.jus.gov.on.ca/english/police_serv/PolicingServicesBoards/PSB.html",
        }
      ]
    },
    { ...OPSResources },
    {
      id: "5a2c9425-c81f-47aa-9fdd-c1e1b7f201aa",
      name: "Special Constables - Resources",
      types: [DIRECTORY],
      children: [
        {
          id: "2f3ec234-2eb0-47cb-9cb9-2976431f80e4",
          name: "Ottawa Special Constables - Top Level Report",
          types: [DOCUMENT_FILE],
          link: "https://docs.google.com/document/d/1FHki1M_M490ME0ArDeDtiic5JPiEaBtiZG92qeFO0-Y/edit?usp=sharing",
        },
        {
          id: "7d76e00f-6155-45f9-bfc9-568df571de77",
          name: "Becoming a Special Constabe in Ontario",
          types: [WEBSITE],
          link: "https://oacpcertificate.ca/becoming-a-special-constable/",
        },
        {
          id: "89507881-5430-4071-aab8-ac7909846447",
          name: "[July 31, 2018] Memorandum of Understanding Between The OPSB and City of Ottawa Transportation Services Regarding the Appointment of OC Transpo Special Constable Unit of Officers as Special Constables",
          types: [PDF_FILE, DOCUMENT_FILE],
          link: "http://ottwatch.ca/meetings/file/529186",
        }
      ]
    },
    {
      id: "9242a129-df39-4950-80c1-ec0ad2663cdc",
      name: "SROs - School Resource Officers",
      types: [DIRECTORY],
      children: [
        {
          id: "c56247f0-03d8-4622-a18c-74abc1f13fcd",
          name: "[CBC] - Canada's Largest School Board [Toronto] Votes to End Armed Police Presence in Schools",
          types: [WEBSITE],
          link: "https://www.cbc.ca/news/canada/toronto/school-resource-officers-toronto-board-police-1.4415064",
        },
        {
          id: "9442419d-34f0-407e-8720-cd4898f77d05",
          name: "[Ottawa Citizen] - 'Prisoner in your own school': Movement Afoot to Remove Community Police Officers in Several Ottawa School Boards",
          types: [WEBSITE],
          link: "https://ottawacitizen.com/news/local-news/police-in-schools-2"
        },
        {
          id: "8750ae59-fcd3-4cc5-ad43-273991bfb4cb",
          name: "[CBC - All in a Day] - Helpful or Harmful: A Psychotherapist on Student Resource Officers",
          types: [AUDIO_FILE],
          link: "https://www.cbc.ca/listen/live-radio/1-92-all-in-a-day/clip/15815167-helpful-harmful-a-psychotherapist-student-resource-officers"
        },
        {
          id: "4fbc7232-f978-4ba1-9106-d637c7518591",
          name: "[City News] - Blck Girl, 6, to Receive $35K in Damages After Being Handcuffed by Peel Police at School",
          types: [WEBSITE],
          link: "https://toronto.citynews.ca/2021/01/07/damages-handcuffed-peel-police-school/",
        },
      ]
    },
    {
      id: "36cd882a-5fcc-43c9-b541-d8624f89fc50",
      name: "Bylaw and Regulatory Services Branch",
      types: [DIRECTORY],
      children: [

      ]
    }
  ]
};
