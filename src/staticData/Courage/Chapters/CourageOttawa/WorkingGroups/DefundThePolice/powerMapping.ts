import { DIRECTORY, DOCUMENT_FILE, ILLUSTRATOR_FILE, IMAGE_FILE, JPG_FILE, SVG_FILE } from "../../../../../../constants";
import { Resource } from "../../../../../../Types";

export const powerMapping: Resource = {
  id: "f3f217a7-f8f0-40d9-9c6a-56bda403733f",
  name: "Municipal and Provincial Power Mapping",
  types: [DIRECTORY],
  children: [
    {
      id: "032bdbab-5d86-4f59-87b0-b3ffc9cc1156",
      name: "Defund the Ottawa Police Service - Municipal and Provincial Power Mapping",
      types: [DOCUMENT_FILE],
      link: "https://docs.google.com/document/d/1QX_hY3kQMV7cvfZsJit9OM91zzEcgnGIEmBsfC7Wg1Q/edit?usp=sharing",
    },
    {
      id: "71f159c2-5375-4746-8d1c-dd6aa6682506",
      name: "Visualizations Design Board",
      types: [ILLUSTRATOR_FILE],
      link: "https://drive.google.com/file/d/1YOwAmGlMu1HkX2PB7g7JjnYXwEQllZKB/view?usp=sharing",
    },
    {
      id: "7bf7e0b7-0a9e-4425-90e2-8d1b5cd1511e",
      name: "Design Board for Overall Database Visualization",
      types: [ILLUSTRATOR_FILE],
      link: "https://drive.google.com/file/d/1cX5l6sxEVLuF2gblTXjJXQlOM-wNlg1h/view?usp=sharing",
    },
    {
      id: "85f1af8f-1c24-4f82-bea8-58797ecef664",
      name: "Visualizations",
      types: [DIRECTORY],
      children: [
        {
          id: "ab87a581-ec38-4694-8a2f-493324e3b256",
          name: "Police Services",
          types: [DIRECTORY],
          children: [
            {
              id: "25c6544d-00ad-42a0-81b4-14dbe5a2a513",
              name: "OPS - Ottawa Police Service",
              types: [DIRECTORY],
              children: [
                {
                  id: "5690601d-767e-416c-8a51-69f9db8c4579",
                  name: "OPS - Basic",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1XWLVI3vSgVegTFvjzkxW8TUxlkoLozak/view?usp=sharing",
                },
                {
                  id: "06f22fb6-81c2-46eb-a9fb-8be053a73e3c",
                  name: "OPS - Basic",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1hNGkKHlfnoA2Q9khbB1oBdSRSI3JpO8Q/view?usp=sharing",
                },
                {
                  id: "4d23558b-accd-464e-8c38-5cb3bc90d33a",
                  name: "OPS - Wider",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1LyXoPWxSVfSx6qyB7K-OnNDyUvDXnPo5/view?usp=sharing",
                },
                {
                  id: "674a8295-16bf-4f53-9037-a4c1691f8c75",
                  name: "OPS - Wider",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1RlJ8FKb7wFYOezVvwwxDdiAUgI1xiXhc/view?usp=sharing",
                },                  
              ]
            }
          ]
        },
        {
          id: "019141d4-c3d6-4f6b-98bf-11886e1e566c",
          name: "Police Associations",
          types: [DIRECTORY],
          children: [
            {
              id: "1631dc30-92c2-436d-9a32-54ac281e9e11",
              name: "OACP - Ontario Association of Chiefs of Police",
              types: [DIRECTORY],
              children: [
                {
                  id: "c91c0c02-07c6-4a05-83ce-d62491855506",
                  name: "OACP",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1HaQ-AhihQH0MDCoA-L-yCnl-LmvruLwj/view?usp=sharing",
                },
                {
                  id: "a14c2814-8777-4875-b858-4096c37792e1",
                  name: "OACP",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1OwzZKf2fuxF8UirDbKflefEh1cz40oRQ/view?usp=sharing",
                },
              ]
            },
            {
              id: "7292db98-0315-4bcd-b811-6f5c35600d0a",
              name: "OPA - Ottawa Police Association",
              types: [DIRECTORY],
              children: [
                {
                  id: "6e22e8f4-5693-4ad5-81c6-15b0d02863ad",
                  name: "OPA",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1i9gkZU1BgN7pwsY2NLQ70lGCSWB7V1Kw/view?usp=sharing",
                },
                {
                  id: "4ab75645-898e-445c-9bd9-0439cff97bde",
                  name: "OPA",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1COvohLcxA6BME3UEz7aI7dNXqDr3hxsG/view?usp=sharing",
                },
              ]
            },
            {
              id: "e9c60c09-71ba-4bc3-ba8a-c27c185c24d1",
              name: "OSCA - Ontario Special Constables Association",
              types: [DIRECTORY],
              children: [
                {
                  id: "358d99fe-17da-4090-803c-d03f1ca04142",
                  name: "OSCA",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1GIjoCPZ1QGGJVFDwGpOCXLPWfrIVbW_G/view?usp=sharing",
                },
                {
                  id: "50d4c4ce-8666-4103-8bec-f1d24caa6b2f",
                  name: "OSCA",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/10Ms-dcBT1glX4KKATVtuoAuoeG-JW_rb/view?usp=sharing",
                },
              ]
            },
            {
              id: "3f499b45-a22f-41b2-8005-78138eed0e66",
              name: "PAO - Police Association of Ontario",
              types: [DIRECTORY],
              children: [
                {
                  id: "f25bbfae-1a87-4727-b580-9cbc9a3ed8df",
                  name: "PAO",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1F46rIjKVJ-Oul8oqFBzSvSy1_vXHnk7U/view?usp=sharing",
                },
                {
                  id: "c5e0fbe5-0136-4f9d-8ec8-14b7c84912f0",
                  name: "PAO",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1pIGcUD5k12MHk2Kv3aDEDMH8Nl9JOI-b/view?usp=sharing",
                },
              ]
            },
          ]
        },
        {
          id: "8d34151f-24cc-4c5e-8f71-1bb86f577858",
          name: "Police Oversight Institutions",
          types: [DIRECTORY],
          children: [
            {
              id: "bcb9f347-2163-4b66-8f06-a178e8d9e975",
              name: "SIU - Special Investigations Unit",
              types: [DIRECTORY],
              children: [
                {
                  id: "bd8421c8-edd0-4062-aa6e-538fba1dc45c",
                  name: "SIU",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/189pSMy2thBI3SYIsZ9q3pilp4qsUSROs/view?usp=sharing",
                },
                {
                  id: "cfd2d045-3fcc-4d70-9483-baf7dfaf47b6",
                  name: "SIU",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1H6O--W7atn1yQANsHpciNyGPNUhefYIL/view?usp=sharing",
                },
              ]
            },
            {
              id: "7b723c31-e59f-4654-bd1f-cafa9c7fd59d",
              name: "OIPRD - Office of the Independent Police Review Director",
              types: [DIRECTORY],
              children: [
                {
                  id: "75afadfe-12c1-45c0-ab93-ae64e9098ae7",
                  name: "OIPRD",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1ja_kXnFdLocPD6W23xXhLjyzhOrkIGWL/view?usp=sharing",
                },
                {
                  id: "8e6df153-740c-4efc-9b02-e62391d0c2b7",
                  name: "OIPRD",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1OlAdTd97jbjBgTmT1gfQXRDf52EmP3uN/view?usp=sharing",
                },
              ]
            }
          ]
        },
        {
          id: "d993d9fc-ff90-49ed-8cf3-9f41fe5cbac3",
          name: "Educational Institutions",
          types: [DIRECTORY],
          children: [
            {
              id: "d7c84c20-5197-481b-a42e-4e524ed68699",
              name: "Ontario Police College",
              types: [DIRECTORY],
              children: [
                {
                  id: "7f59a20d-b44c-408f-a77d-59f782f85c4b",
                  name: "Ontario Police College",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1WJnGqryRG1z_QUtQu-rjSxOEpmCd0mtN/view?usp=sharing",
                },
                {
                  id: "14499850-f0c8-40b8-9cec-59e5f68d5342",
                  name: "Ontario Police College",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1v_nY_RZmvB_Jw9xgV-ALvTNA-QhRM3iL/view?usp=sharing",
                },                
              ]
            }
          ]
        },
        {
          id: "5f64509a-aa55-4f45-8ec4-fadf6a0c7d84",
          name: "Municipal Government of Ottawa",
          types: [DIRECTORY],
          children: [
            {
              id: "7b563468-52f7-45f3-9e4b-d890a767ddbc",
              name: "Municipal Government of Ottawa",
              types: [JPG_FILE, IMAGE_FILE],
              link: "https://drive.google.com/file/d/1n40LSod76ou6yet1J8XuB27rfTQ1CCh6/view?usp=sharing",
            },
            {
              id: "be87e321-85aa-4bc3-a45a-d769a2826721",
              name: "Municipal Government of Ottawa",
              types: [IMAGE_FILE, SVG_FILE],
              link: "https://drive.google.com/file/d/1rP2sklBHcWIH8BOtR__Q3e65_4UnRwtA/view?usp=sharing",
            },
            {
              id: "b9abe7d5-b9d7-49ed-a988-ac2428d392a9",
              name: "City Wards",
              types: [DIRECTORY],
              children: [
                {
                  id: "6545ace4-d578-462e-8c14-d6fc9287a60a",
                  name: "City Wards",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1kAXcPgSXRMnOjCeT4C6LnLs_p_Mo7o3p/view?usp=sharing",
                },
                {
                  id: "d3d0cda4-7e66-4678-9c10-40c0b58f71f1",
                  name: "City Wards",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1D7ohV6hcoiR23xhNU16JjufQFkEHafbS/view?usp=sharing",
                },
              ]
            },
            {
              id: "7c9e18e4-34c5-4f13-b83e-6a00054a3831",
              name: "City Council",
              types: [DIRECTORY],
              children: [
                {
                  id: "d23a381a-1ac1-4bba-b139-dd6f9ac5b0fa",
                  name: "City Council",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/102wRaprVeYDfGPzgQT-O9rEtz3dTSGSH/view?usp=sharing",
                },
                {
                  id: "44806491-effc-4b9f-95a3-881870ddc03d",
                  name: "City Council",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1eyC2d4QyWqehqz5eJPSbXIWeRyret_6Z/view?usp=sharing",
                },
              ]
            },
            {
              id: "632e13c2-c1a4-4934-8fcf-d4023f734591",
              name: "Standing Committees",
              types: [DIRECTORY],
              children: [
                {
                  id: "8697ace4-7c68-45d0-aded-f7f4379bb2a4",
                  name: "Standing Committees",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/19uUXd2FtPpkjiPfX2tqh684fycslpdMn/view?usp=sharing",
                },
                {
                  id: "0276b69c-e223-4832-afd2-9c6046cce0d0",
                  name: "Standing Committees",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1jPNTo4RAqmMM1Xz5N7OInVsh62-avRj9/view?usp=sharing",
                },                
                {
                  id: "c8e03c48-b514-4a85-ba72-8b74b428828f",
                  name: "Agriculture and Rural Affairs Committee",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "8260bf86-6653-49c7-95f6-fcb4d5497685",
                      name: "Agriculture and Rural Affairs Committee - Basic",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1vnToAaALYSp0Nck4Vfj4odD-WUdecKHH/view?usp=sharing",
                    },
                    {
                      id: "111bee2d-da1a-42d1-bc66-ac004c17582d",
                      name: "Agriculture and Rural Affairs Committee - Basic",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1fcZEmgP03Bp0PbiTuK24cbjEFhw6egJ3/view?usp=sharing",
                    },
                    {
                      id: "c7f2a321-53ac-4953-a244-6918532f46c2",
                      name: "Agriculture and Rural Affairs Committee - Wider",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1EX2l2yDjkzGL7mv-n2hqgI8hgaxcTzXI/view?usp=sharing",
                    },
                    {
                      id: "ff71df8a-8b69-452e-8f4f-14e6cbf88603",
                      name: "Agriculture and Rural Affairs Committee - Wider",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1JCkopxhDQo8-5k4X6XIJPiJ7bQPNQ0Jg/view?usp=sharing",
                    },                    
                  ]
                },
                {
                  id: "e4b57114-bbb3-4390-b67a-6cd9b2fea7d8",
                  name: "Audit Committee",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "fa680d92-a3f9-4178-bed2-696620642d2e",
                      name: "Audit Committee - Basic",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1Y6k4cVqNh0nypSPeRZTWuIkFFwnZpOuV/view?usp=sharing",
                    },
                    {
                      id: "588a8709-39f9-4201-8a1c-09998c9c9fb3",
                      name: "Audit Committee - Basic",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1V7r41BebfFeYVBU1Kh_0_l-tkIUIng92/view?usp=sharing",
                    },
                    {
                      id: "0e7c95c4-5819-486e-aedb-6e7f1a2087e2",
                      name: "Audit Committee - Wider",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1GVDNTF6PdDG-Rm8lZai2cVz9mLjpu_1L/view?usp=sharing",
                    },
                    {
                      id: "6e0820b3-ae01-4625-9147-df24bb9f4150",
                      name: "Audit Committee - Wider",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1tpkrJ_OgRatht8izexkAMD0Qv8kXGkDc/view?usp=sharing",
                    },
                  ]
                },
                {
                  id: "c758092f-40c6-4d92-b08a-f692da3c6ca9",
                  name: "Built Heritage Sub-Committee",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "1c018e1a-e626-4f31-b0c2-64dc5976de5d",
                      name: "Built Heritage Sub-Committee - Basic",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1Iu0-jFxX9jcUAe94KSp-t-ktkVh5DzPb/view?usp=sharing",
                    },
                    {
                      id: "e6d2e3a6-b02b-4009-b2b9-201a4ad2a2cd",
                      name: "Built Heritage Sub-Committee - Basic",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1x2iR43hwFDqYnMW4pCW0TDSlgIw9rEIY/view?usp=sharing",
                    },
                    {
                      id: "0461b0d4-6410-4130-b26a-2f2b7e60bf84",
                      name: "Built Heritage Sub-Committee - Wider",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1j41ghC6FsaddPmJ--o_uXfJjkU6oAJUp/view?usp=sharing",
                    },
                    {
                      id: "fa3af764-b564-47a9-9602-3466a18645f9",
                      name: "Built Heritage Sub-Committee - Wider",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/12MUbkJroEMQv6wlJhHgrVgsqEiklo5at/view?usp=sharing",
                    },
                  ]
                },
                {
                  id: "a4f6e270-6f6e-46e7-a413-a6f9331c3a42",
                  name: "Community and Protective Services Committee",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "7329b906-77c7-40e2-84c0-dfb51e888484",
                      name: "Community and Protective Services Committee - Basic",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1qiUAsCEW8L9iB55FwJh3ZB4LEsoqqhWC/view?usp=sharing",
                    },
                    {
                      id: "934d1dc2-c66c-4a11-92ee-9516e84d70d0",
                      name: "Community and Protective Services Committee - Basic",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/17E4h3D_lwQS66TJHMHoPl_ISO4NVU_s8/view?usp=sharing",
                    },
                    {
                      id: "d93ba073-10b2-4ff2-a82f-484a3d490bec",
                      name: "Community and Protective Services Committee - Wider",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1e3LMKX2tvLX59r-UROMrhNRa2REP2Tdl/view?usp=sharing",
                    },
                    {
                      id: "fcf9d1fb-2464-49d3-b0d9-45fa1144f0ea",
                      name: "Community and Protective Services Committee - Wider",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1fRSdlTwsMSWOs-M_rSJOwdjHAhoaPtMA/view?usp=sharing",
                    },
                  ],
                },
                {
                  id: "0ed73f36-e37d-457f-891e-6aba99003bf8",
                  name: "Standing Committee on Environmental Protection, Water, and Waste Management",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "39349adc-7556-4a2d-a4c6-7ccd22275ab2",
                      name: "Standing Committee on Environmental Protection, Water, and Waste Management - Basic",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1fauVbCvz2zfP0iV9sh9kTiz1omIYe6Nk/view?usp=sharing",
                    },
                    {
                      id: "682d77e3-850a-4218-a142-6b988193ab99",
                      name: "Standing Committee on Environmental Protection, Water, and Waste Management - Basic",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/16I099WvBD-mfLg39pm_moH5_cOO4L2AD/view?usp=sharing",
                    },
                    {
                      id: "60bc7f8a-c1be-4b9d-b951-28646fce3e21",
                      name: "Standing Committee on Environmental Protection, Water, and Waste Management - Wider",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1blSjuamusOegXzYA_0MRkDl0TqGIdWlZ/view?usp=sharing",
                    },
                    {
                      id: "c17b0bcc-4bd2-43d1-9ff1-5683228a6a88",
                      name: "Standing Committee on Environmental Protection, Water, and Waste Management - Wider",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1425OUBAS9g9zGnDhv2OMMg_9JEU0q4Fx/view?usp=sharing",
                    },
                  ]
                },
                {
                  id: "0e78d6ed-58e2-43c8-b040-e95bfa118d72",
                  name: "Finance and Economic Development Committee",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "e181155a-dfec-4ff7-9138-072c1e54e5cf",
                      name: "Finance and Economic Development Committee - Basic",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1NBXj2JM2hzB2a2f_Oh1u3HE1ecxYVoPa/view?usp=sharing",
                    },
                    {
                      id: "cad8e5ef-f6a5-46db-99a8-4efe571d84e6",
                      name: "Finance and Economic Development Committee - Basic",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1riV2HlUmZIy0Xlf6BR89fBN8KJJbXAvl/view?usp=sharing",
                    },
                    {
                      id: "5480a011-36b4-492d-afba-54d60470eb47",
                      name: "Finance and Economic Development Committee - Wider",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1OKVhqgGUSLB0FAie334_jHj6WzsLtf5l/view?usp=sharing",
                    },
                    {
                      id: "9cf7a822-3fdc-4a70-9242-681f8237d60f",
                      name: "Finance and Economic Development Committee - Wider",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1DjeJcw70eGoVodjdjbnY0bCLxEA8udhs/view?usp=sharing",
                    },
                  ]
                },
                {
                  id: "28664944-b7fa-40fb-aaae-f9c52532f2e3",
                  name: "Information Technology Sub-Committee",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "d0ee2506-f7cc-46c0-80fe-be44798aacdd",
                      name: "Information Technology Sub-Committee - Basic",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1aW3u28dm9nHb4C6qCyjLWXL48hc9xBof/view?usp=sharing",
                    },
                    {
                      id: "34c869d3-7573-47d1-b343-3b9f9649b954",
                      name: "Information Technology Sub-Committee - Basic",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1pMxBmUg2LztaRPtq9OcEgQCtfxKV3JUe/view?usp=sharing",
                    },
                    {
                      id: "e3dcf7e1-2bb3-4841-929e-866f2e92cec1",
                      name: "Information Technology Sub-Committee - Wider",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1uD27w-JBmnoKT0IR7Kif9ggjWR4U64z0/view?usp=sharing",
                    },
                    {
                      id: "22ac1f45-5b00-4056-bd0e-e66e78e6f0c6",
                      name: "Information Technology Sub-Committee - Wider",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1a22_Me3zZuG7u3Tb9ptuhAVm8IZH1j08/view?usp=sharing",
                    },
                  ]
                },
                {
                  id: "ed4451df-788c-491e-9070-8dfe6e328bca",
                  name: "Planning Committee",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "795e3a38-5656-4a26-8c3f-8b8c32c191cb",
                      name: "Planning Committee - Basic",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1U_nQjLbiX87yBxcsLF8yHSaEU8nycqJe/view?usp=sharing",
                    },
                    {
                      id: "2ac12d27-17cc-435d-a968-5b7adb4c3132",
                      name: "Planning Committee - Basic",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1R2C-0DrQfxA6vDqXx2RTNmqwkJdiTyb1/view?usp=sharing",
                    },
                    {
                      id: "2c854bf1-c366-49d0-b6e0-3e2a41ec1302",
                      name: "Planning Committee - Wider",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1ETbvuQguOPoELU5imOJEdyQc_gxGsyGx/view?usp=sharing",
                    },
                    {
                      id: "4ab2e6f0-2587-4637-90b9-c83714f04d06",
                      name: "Planning Committee - Wider",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1okBCixjNyB3JlkU4Zntjd1aRli-AxZam/view?usp=sharing",
                    },
                  ]
                },
                {
                  id: "e3a80b0b-febf-4dd8-8aaf-bedc45965710",
                  name: "Transit Commission",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "cfe10b2a-5700-4cb5-bab4-e7fbe2de9551",
                      name: "Transit Commission - Basic",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1SRM9u0_XyFFd--aldMG7jsSMeEe0r2sr/view?usp=sharing",
                    },
                    {
                      id: "1c621dbb-2468-47d3-8cff-9d23e856630f",
                      name: "Transit Commission - Basic",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1SBgPnexE9I8XOSOWEnMnHiSR4NxF43rP/view?usp=sharing",
                    },
                    {
                      id: "8709ff19-70bb-42a9-9892-d10640c401f6",
                      name: "Transit Commission - Wider",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1g4F2xpR_xaYD1fmh9tUZfLardgepktmK/view?usp=sharing",
                    },
                    {
                      id: "9415335a-5982-49a5-bf6c-ae91d158aeb3",
                      name: "Transit Commission - Wider",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/11au45Hlb_IVEWnwCXzhY3lUrZIDcLAuq/view?usp=sharing",
                    },
                  ]
                },
                {
                  id: "4eced42d-03d9-439a-b782-9acdafe3c8fa",
                  name: "Transportation Committee",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "dddbe008-18cb-4bfa-b31f-474c7484dacc",
                      name: "Transportation Committee - Basic",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1eYJI8u7gLtMd6qBHsErbE4z-9DIYYLDQ/view?usp=sharing",
                    },
                    {
                      id: "9a348343-df4a-4203-b4f7-9650d9d9b06f",
                      name: "Transportation Committee - Basic",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1gTOZlr7b5i2QSrO3eYTjidgc20-x-_Am/view?usp=sharing",
                    },
                    {
                      id: "63cceb7e-3df8-415c-b2b4-bf4bd9655b39",
                      name: "Transportation Committee - Basic",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1svRugHQr0nPxPfkANOvdSVQkdmy6_APC/view?usp=sharing",
                    },
                    {
                      id: "948be649-277b-4b1d-8ce2-987d25baab4d",
                      name: "Transportation Committee - Wider",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/19PcDYI_5GhIoj-Af-aZ4km2zA_ef8RpT/view?usp=sharing",
                    },
                  ]
                },
              ]
            },
            {
              id: "e8855c1a-fcb8-4921-b837-81aa12b7058e",
              name: "Ottawa Police Services Board",
              types: [DIRECTORY],
              children: [
                {
                  id: "a9d4b061-b8b7-4dd3-b67e-01c3a4ed530b",
                  name: "OPSB - Basic",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/18hKvJNvRGfquBu_YDlkBwHm_mTc1CXyc/view?usp=sharing",
                },
                {
                  id: "0aeb3177-0c8f-4c20-88f9-56fb8cd823b7",
                  name: "OPSB - Basic",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1zQkwSR3XKmUD4tPaf5Ky1LZaSBfFXA4t/view?usp=sharing",
                },
                {
                  id: "240ffd3b-0928-44d7-89f6-dd2d92b392d8",
                  name: "OPSB - Wider",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1VhOP1OBNTDoEtaGXP3q8BXzSuJIDd4i3/view?usp=sharing",
                },
                {
                  id: "190fed65-f759-48db-988c-f4e15504b2e7",
                  name: "OPSB - Wider",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1P9fZ_-WUpxdpSj5SsKzxz_jybvZx2JX6/view?usp=sharing",
                },
              ]
            },
            {
              id: "f375dcea-a4ef-460e-8cfb-eea1abb29daf",
              name: "OC Transpo",
              types: [DIRECTORY],
              children: [
                {
                  id: "e0fdd88c-de04-45c3-8706-9715fa8d44b5",
                  name: "OC Transpo - Basic",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1TyYHzg_5Z4JQXl103FBAqC4g9ufSStPF/view?usp=sharing",
                },
                {
                  id: "99c7f742-dd6b-4ae6-8bb9-0f6f8b8b1365",
                  name: "OC Transpo - Basic",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1yZ7GoMjfHIUAbHVr-avvUNK0ZGsf0Xjz/view?usp=sharing",
                },
                {
                  id: "ad9f7587-cb0e-4097-9753-187f0af87dae",
                  name: "OC Transpo - Wider",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/13CuxLrfi9ReMwZFwX3-E778jOm7ejG5d/view?usp=sharing",
                },
                {
                  id: "6e096a87-2299-4605-a42f-b23703b56271",
                  name: "OC Transpo - Wider",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/19CdhWbz3835fRzmSrBMVdKuDgNdixqid/view?usp=sharing",
                },
              ]
            },
            {
              id: "b5f94d9b-50ce-42d1-93a4-90f5032de60c",
              name: "Ottawa Public Health",
              types: [DIRECTORY],
              children: [
                {
                  id: "195f2e5d-73c4-4aef-a13e-f00d4f94616a",
                  name: "OPH - Basic",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/14JT9RsTzZGEnBcdmbx7S7V0F90e8AnwB/view?usp=sharing",
                },
                {
                  id: "9499845f-bff1-493e-867f-818cb5af354a",
                  name: "OPH - Basic",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/13UlayS9wuqojnzY1fF0cGyHRYERla9rl/view?usp=sharing",
                },
                {
                  id: "9b7e651a-fba0-48b8-a000-d9d3cd3e250a",
                  name: "OPH - Wider",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1ydJ596ubJzmMP1juZiWWBlL6pAGoRtSn/view?usp=sharing",
                },
                {
                  id: "6efda2c6-010f-4b51-9aed-63a2a6f9145e",
                  name: "OPH - Wider",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1MHZ77e1csVXh0chwABVeYdTHRKZue3jc/view?usp=sharing",
                },
              ]
            }
          ]
        },
        {
          id: "8dce47ee-81f1-40c6-bbf3-7d332a350db2",
          name: "Government of Ontario",
          types: [DIRECTORY],
          children: [
            {
              id: "6b701caa-c869-4efa-9926-4be8f59d7a00",
              name: "Electoral Districts",
              types: [JPG_FILE, IMAGE_FILE],
              link: "https://drive.google.com/file/d/1f-RqOGVUvlhavAlanMvv9pPPhNUiVDEa/view?usp=sharing",
            },
            {
              id: "7da33f00-c1f8-43ba-b073-afbcb8997fb2",
              name: "Electoral Districts",
              types: [IMAGE_FILE, SVG_FILE],
              link: "https://drive.google.com/file/d/1Q5yvi7D-rfxxaKT6B45nov4oBTLmjew1/view?usp=sharing",
            },            
            {
              id: "ab1c44b5-0c8d-4670-ad51-bfa0850fdcf7",
              name: "Government of Ontario",
              types: [JPG_FILE, IMAGE_FILE],
              link: "https://drive.google.com/file/d/1mp4w-tmDHoSIK6h48G8k7jL5KvHBOriF/view?usp=sharing",
            },
            {
              id: "d6989d9b-3b44-4307-8e29-99b4b4463672",
              name: "Government of Ontario",
              types: [IMAGE_FILE, SVG_FILE],
              link: "https://drive.google.com/file/d/13RnUXjUU7KLkQYxGQFvN5kQllbLJpHsM/view?usp=sharing",
            },            
            {
              id: "915d320b-a007-4d5b-bf0e-7a27aa9094f0",
              name: "Legislative Assembly of Ontario",
              types: [DIRECTORY],
              children: [
                {
                  id: "e2537fa4-0ee9-4e69-8641-e20c3f7f26bc",
                  name: "Legislative Assembly of Ontario - Overall",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1L14XqPThOQOL6DMCnWZN1QoA6FJtHWBJ/view?usp=sharing",
                },
                {
                  id: "c3e09f19-ef4a-4762-95e0-b5fa937de6d0",
                  name: "Legislative Assembly of Ontario - Overall",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1M8N7j47kUFYRN-2-TRzeb5fShwTvUXGF/view?usp=sharing",
                },
                {
                  id: "53b24d35-dba6-4889-b8bd-c8e4db73332b",
                  name: "Independent MPP Pattern",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1SUDpOOdDzLfBXcZvEx24CoWkagYQEXuS/view?usp=sharing",
                },
                {
                  id: "c93aad16-8ff2-4c48-ae01-9e73ca841f0e",
                  name: "Independent MPP Pattern",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1G5gzJsoW91PANiYdiIHxY1Y8tH4tpjOv/view?usp=sharing",
                },
                {
                  id: "9702a4cf-5254-4404-a71d-b44ca0e3b0b3",
                  name: "MPP With Party Affiliation Pattern",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1xZVyXMOZqpUHSyoqcRx0qaEh3ePoA6HO/view?usp=sharing",
                },
                {
                  id: "cfff0454-3982-461f-9042-e52285cb57e7",
                  name: "MPP With Party Affiliation Pattern",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1EDKQFyhmgetYURlA7cy-Iwn0koc8gVxt/view?usp=sharing",
                },
                {
                  id: "afe5a1c7-94aa-4f84-a472-b9166a4ac5f9",
                  name: "All MPP Roles and Electoral Districts",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1hOVCev15z5N0vcfU_hHkXjycNnQrG_C0/view?usp=sharing",
                },
                {
                  id: "28d4ffca-c063-4957-bb1b-2fbb1f342c81",
                  name: "All MPP Roles and Electoral Districts",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1UjXizvIooqRcXG9TQIPNm2wJFXsWUsDy/view?usp=sharing",
                },
                {
                  id: "2c2e3977-19fd-4044-b69f-1074b99004c7",
                  name: "All MPP Roles and Electoral Districts and Parties",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1kdXqHSFBpN6RNPcA6dKrBm0bi3PjAgl0/view?usp=sharing",
                },
                {
                  id: "f83b9b1b-d15f-426f-ac79-a172a6e1c693",
                  name: "All MPP Roles and Electoral Districts and Parties",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1_upwTzoqmUOhyGcAdYM-GhtZfYLPWQ08/view?usp=sharing",
                },
                {
                  id: "93ab6994-649a-49e6-a446-5e5ae54fc8df",
                  name: "Progressive Conservative Party MPPs",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "cf9bfd7a-3993-460e-93bc-cccfb8201775",
                      name: "Progressive Conservative Party MPPs",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1-ocm1EuJw4MePsqiCM5ZrvLvhgkwXzJM/view?usp=sharing",
                    },
                    {
                      id: "a97d6f98-1835-4435-8640-aadef3162b23",
                      name: "Progressive Conservative Party MPPs",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1YPTMelEGlrE32fy-iRb-iFjcqbkBauXd/view?usp=sharing",
                    },
                  ]
                },
                {
                  id: "799ea27c-3ca9-4c1f-a31b-ad951fc0c715",
                  name: "Ontario Liberal Party MPPs",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "04f7571f-38fc-4719-ad42-eb8730b50b88",
                      name: "Ontario Liberal Party MPPs",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/185uwdbfYExjqg2WraWdAHHabeob-qVp2/view?usp=sharing",
                    },
                    {
                      id: "b46cb880-fc92-4414-a2dd-cf4fb2c2eef3",
                      name: "Ontario Liberal Party MPPs",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1jbztfwnTtEbwBFFS6Dxf8Wx2Q8Z8-x4L/view?usp=sharing",
                    },
                  ]
                },
                {
                  id: "76dcfbfa-fc25-4fbc-b68d-6ba3dd6730c6",
                  name: "New Democratic Party of Ontario MPPs",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "ed9f1e2c-ab3e-4b26-8469-1ecc4c586f17",
                      name: "New Democratic Party of Ontario MPPs",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1rNTAULNIf1-VInHmQm6ZRdceY0GJcJlN/view?usp=sharing",
                    },
                    {
                      id: "c7d222a0-cf6d-431e-854e-7a722043b5b3",
                      name: "New Democratic Party of Ontario MPPs",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1ypXvzuKOPMeWvdlvdkoXSp8JQvepPKRm/view?usp=sharing",
                    },
                  ]
                },
                {
                  id: "e953689f-84f6-4321-8bba-36f52f9f61b9",
                  name: "Green Party of Ontario MPP",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "895e058d-03a5-4c17-b6b5-08927852a8b7",
                      name: "Green Party of Ontario MPP",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1_RMCyjMKdqEi-NxWwmk5CiDwwcAx65SD/view?usp=sharing",
                    },
                    {
                      id: "c5b54243-b74b-4db2-abbf-95f844471bf6",
                      name: "Green Party of Ontario MPP",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1qTTovqQ0xeRwRSuCtf1Wh8vGNkh2j8PT/view?usp=sharing",
                    },
                  ]
                },
                {
                  id: "b82a39d4-ab32-4a0c-82dc-1b16fe6838d9",
                  name: "Executive Council of Ontario",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "09ad3adc-b52b-4449-8659-01709c216ad1",
                      name: "Executive Council of Ontario - Basic",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1MK98IVoECA4oobmBks2DHM1Z_OYXSd95/view?usp=sharing",
                    },
                    {
                      id: "15b3c2e3-12cf-47e9-b36c-00be44c65214",
                      name: "Executive Council of Ontario - Basic",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1_foJuVFlA0opZoiDNgvuVgRduJsBDbhI/view?usp=sharing",
                    },
                    {
                      id: "80b802d2-36b9-49ac-8897-12ccb0bb844b",
                      name: "Executive Council of Ontario - Wider",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1-Bo6uhIXqJG2Kmn_qL1MB8r53heIE7ZP/view?usp=sharing",
                    },
                    {
                      id: "53de1d64-b950-46b2-856e-24e20b0a4dbb",
                      name: "Executive Council of Ontario - Wider",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1Lm7S8egkAGTMPs_e_aTRUWZIUY6rbsZj/view?usp=sharing",
                    },
                  ]
                },
                {
                  id: "7737abd4-2866-4c40-97da-7b937b9d69ec",
                  name: "Opposition Critics",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "3c840d24-43b3-439b-90a9-e4de39cad335",
                      name: "Opposition Critics - Basic",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1O02ZSbt-UQuEnXZ_jdghhZacFEhu_7GF/view?usp=sharing",
                    },
                    {
                      id: "3e7af8e8-ec04-4a8f-a192-1cf138f3b2aa",
                      name: "Opposition Critics - Basic",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1msEmNFbAW2LQJ_-30o8VlUxBd6g4Nqnn/view?usp=sharing",
                    },
                    {
                      id: "4532be4b-33bb-4c4c-b870-c757035bb205",
                      name: "Opposition Critics - Wider",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1q94hD93lYdqsnzigWUtpCeGdME7ng3En/view?usp=sharing",
                    },
                    {
                      id: "4fc145f3-7c9c-491f-9bd2-fbcf41d8c100",
                      name: "Opposition Critics - Wider",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1_LiPk1q9-DD2Z0boyEiipXBsZh2HICGD/view?usp=sharing",
                    },
                  ]
                },
                {
                  id: "b1aabd0d-98eb-4965-967a-221f8e2372b7",
                  name: "Parliamentary Assistants",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "a40eb2c1-9d2c-4883-93c2-a8f90e76f7de",
                      name: "Parliamentary Assistants - Basic",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1hIm4PnIEsLefylxX7SQGdg43s0avkoCs/view?usp=sharing",
                    },
                    {
                      id: "e1d65037-6837-4c43-bd25-6c250c65574c",
                      name: "Parliamentary Assistants - Basic",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1jg8GeV1xqsWyjeERcBT4aSitW31BcYAC/view?usp=sharing",
                    },
                    {
                      id: "2a5cd92a-2f6e-4d52-af14-19a25bd20456",
                      name: "Parliamentary Assistants - Wider",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1GY_5AdfmJDi_uWcJtqJWYVvb-yPoRZNJ/view?usp=sharing",
                    },
                    {
                      id: "d1911f30-ba1a-404f-aa4e-4d5347276c12",
                      name: "Parliamentary Assistants - Wider",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1PnsKM_B8RcOXXSWlzR9GZKfffWw5_Lev/view?usp=sharing",
                    },
                  ]
                },
                {
                  id: "8a76cfe9-cb70-4242-a6a4-2a85db015907",
                  name: "Office of the Assembly",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "4096e334-7d42-4a94-b766-09a504a58128",
                      name: "Office of the Assembly",
                      types: [JPG_FILE, IMAGE_FILE],
                      link: "https://drive.google.com/file/d/1DqadpYwlMb-z2z336phMx-rJZqlzDmQ9/view?usp=sharing",
                    },
                    {
                      id: "11202bdb-b073-43c2-b369-7c4ab376f080",
                      name: "Office of the Assembly",
                      types: [IMAGE_FILE, SVG_FILE],
                      link: "https://drive.google.com/file/d/1FqTpb2_Y0q4i7XjyUBoe7LzkbCtkYB4Y/view?usp=sharing",
                    },
                  ]
                }
              ]
            },
          ]
        },
        {
          id: "e323f3d7-0b07-4785-abab-930196f678cb",
          name: "Provincial Political Parties",
          types: [DIRECTORY],
          children: [
            {
              id: "73046851-af96-4c2d-a1c5-fbdf74260249",
              name: "Progressive Conservative Party of Ontario",
              types: [DIRECTORY],
              children: [
                {
                  id: "48f48f7e-ceaf-47f3-bb2d-d511c7d6ebaf",
                  name: "Progressive Conservative Party of Ontario",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1FqTpb2_Y0q4i7XjyUBoe7LzkbCtkYB4Y/view?usp=sharing",
                },
                {
                  id: "13aa6d18-5ed5-4715-8e67-e40265aabfbb",
                  name: "Progressive Conservative Party of Ontario",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1yBGFH0SXQXCgcP-WPiI0VlQNjbObKt_t/view?usp=sharing",
                },
                {
                  id: "c1017ecf-81ac-4157-946e-5db75d950997",
                  name: "PC - MPPs",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1hG2M6_sP6ZzFJ8Ko1XUpdgvcDVLMRzsF/view?usp=sharing",
                },
                {
                  id: "cddb845a-ea12-425b-ab37-857c511f6d53",
                  name: "PC - MPPs",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1A3LqemH5wqSx_Bnt3WueK5ULXei9TAkJ/view?usp=sharing",
                },
                {
                  id: "87375f83-77db-47ec-827f-8c161906de59",
                  name: "PC - Party Executive",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1VcPYmcfrLzcSdcSBpKGLV2xwp0biJxXk/view?usp=sharing",
                },
                {
                  id: "f682cb17-6496-4fbd-8136-dafdc60e783e",
                  name: "PC - Party Executive",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/17WMCwGLfLXHwJfpZFX7migG6UchhnbY6/view?usp=sharing",
                },
                {
                  id: "bcd1d46f-6e96-4704-87cf-40d01f9e81e1",
                  name: "PC - Overall Structure",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1viMpR0ZWo8KhzaRGpCeVNKJn7JVG-lHl/view?usp=sharing",
                },
                {
                  id: "ee58bace-509d-4627-a9ba-af0c500e3ad5",
                  name: "PC - Overall Structure",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1PGCLaLGFCpPSUYjzkyX5XFm8rHEgxt62/view?usp=sharing",
                },
              ]
            },
            {
              id: "711d8bd1-8c4e-4170-9ad7-339f2c3e572e",
              name: "Liberal Party of Ontario",
              types: [DIRECTORY],
              children: [
                {
                  id: "5f82306f-3206-429e-b573-1fef8388353e",
                  name: "OLP",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1kZmFe3Apup4QNKoFayEORByLKuYtLRSy/view?usp=sharing",
                },
                {
                  id: "8e75e148-cb39-40f8-b9c9-3af2ff0a99bc",
                  name: "OLP",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1dPQkET55YZvhfTzu70wUrE9m5fCKjiWe/view?usp=sharing",
                },
                {
                  id: "6172da0f-066e-4e4e-9948-e8c9d3d3ba2c",
                  name: "OLP - MPPs",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1OJFNnq60MWMcONoZ_7giv-QOgtd7ziJA/view?usp=sharing",
                },
                {
                  id: "c4da00bf-3746-4740-98f9-94966a13dbd9",
                  name: "OLP - MPPs",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1Xul6v6zDI_IZ18UqolWFkroVbJPst-_X/view?usp=sharing",
                },
                {
                  id: "b6d945c5-7950-44c4-8c30-d31ba7062baa",
                  name: "OLP - Executive Council",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1Jao2AjBN69bXS5N7NeoJSEpFjwr8O4pb/view?usp=sharing",
                },
                {
                  id: "0530ab3d-f392-45b0-a28c-9f98e13d22a1",
                  name: "OLP - Executive Council",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1Gu8YLral7G2onHQcfFxwqhwf49jGAf2P/view?usp=sharing",
                },
                {
                  id: "4b7d038f-d010-4e74-9952-1eef2bcb6131",
                  name: "OLP - Overall Structure",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1CtHGADxn8OP3B4xuMK2XRMsKGN1B6BNH/view?usp=sharing",
                },
                {
                  id: "40217f04-1c3a-4193-8c98-3aef6995e79e",
                  name: "OLP - Overall Structure",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1gmmozU7VhDrjekYWWT3BYL8zRcIm_JB4/view?usp=sharing",
                },
              ]
            },
            {
              id: "847be7c2-60e7-42a0-bcf2-8faa3d1071ec",
              name: "New Democratic Party of Ontario",
              types: [DIRECTORY],
              children: [
                {
                  id: "2f28bc31-a000-484f-8774-e38e884c9035",
                  name: "ONDP",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1186waHx2JcuWWmXjqFg1jIzn_8A_xDBi/view?usp=sharing",
                },
                {
                  id: "0e89e5eb-670d-49eb-955a-f2bbd551ddc6",
                  name: "ONDP",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1ivrjJNLUbU_FYVbw4xSu5WrDtZwzwXWx/view?usp=sharing",
                },
                {
                  id: "e8f2fe20-0e29-44eb-b43f-5e682fca69e3",
                  name: "ONDP - MPPs",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1msmjnXcC0vtoLn2b4Q0kdc1W7KXXbcu8/view?usp=sharing",
                },
                {
                  id: "12a4d454-6fbd-4f8f-b5fe-b805b5a2bee4",
                  name: "ONDP - MPPs",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1E7F9XnZ0GruYugDUo9zG-8gt28pb-2_X/view?usp=sharing",
                },
                {
                  id: "6336c8b4-30be-4bc4-aa31-054a1845f11b",
                  name: "ONDP - Party Executive",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1Bt-1jboW1B8a-WqCWwtUr4rGU0vhKbw5/view?usp=sharing",
                },
                {
                  id: "3840a549-9d63-4780-88d9-add5b72eaa87",
                  name: "ONDP - Party Executive",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/10ZAmWvadX26xQXjnmE7F3J7q-2jB4WCp/view?usp=sharing",
                },
                {
                  id: "ff306b97-ae2f-4c87-aade-d49fcbcfafd0",
                  name: "ONDP - Overall Structure",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1hPKQfzAqsPapIV5jZwZMTevbKeguZi6d/view?usp=sharing",
                },
                {
                  id: "8d9339ee-9b70-45d5-a243-3a50a0ea6be1",
                  name: "ONDP - Overall Structure",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1P9m8hM5mTg6fOCUywp4xQzdQp7tgT9Ep/view?usp=sharing",
                },
              ]
            },
            {
              id: "0d70ef3d-bcb8-4d89-a0ef-d6836c29cc00",
              name: "Green Party of Ontario",
              types: [DIRECTORY],
              children: [
                {
                  id: "961dd8f1-9efa-4dea-8e15-aa7f4a8d0900",
                  name: "GPO",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/18s-SRc_aYCyCfqAEnreUaU7ebSOQ05s8/view?usp=sharing",
                },
                {
                  id: "db39b1b6-e19f-41a6-a838-dc1374be7904",
                  name: "GPO",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/19V-mMi87Fyq6b3PlIK0tgiXCBX_s2t6x/view?usp=sharing",
                },
                {
                  id: "524242c4-570f-4a85-8d94-21a0fee8668e",
                  name: "GPO - MPP",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1glYBThrpi8dkC-IDDtK87pQBBduWi-rm/view?usp=sharing",
                },
                {
                  id: "855f49dd-40a6-408e-b4e5-3c02db7b3919",
                  name: "GPO - MPP",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1hg8qwcBd5mt7T30g_Ol8kGPIlRlB6NbQ/view?usp=sharing",
                },
                {
                  id: "04b4505f-2873-4f0b-b790-b6932f4d3ff2",
                  name: "GPO - Provincial Executive",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1-ddegROs30DeAgtbP-Hy6_KwFe0So3U2/view?usp=sharing",
                },
                {
                  id: "e8de9957-8933-4f10-a5a0-5f02dbd9607a",
                  name: "GPO - Provincial Executive",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/14xoQfYp5CNfEgvbkD0KNGc7RYv8xTKtB/view?usp=sharing",
                },
                {
                  id: "233adad2-5730-4457-9681-c11d0b0242d3",
                  name: "GPO - Shadow Cabinet",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1xni26g52pS52gbaxQ7vKKdpFl2yh-jQM/view?usp=sharing",
                },
                {
                  id: "28c850b2-fac4-4cd7-b633-ae9437bec940",
                  name: "GPO - Shadow Cabinet",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/12DIYjgaDnEt8nLDERsOBtkk5mJY1RFRA/view?usp=sharing",
                },
                {
                  id: "1a768f5d-85ec-494c-a4ca-885d515e6d14",
                  name: "GPO - Overall Structure",
                  types: [JPG_FILE, IMAGE_FILE],
                  link: "https://drive.google.com/file/d/1VC5ZDusKCFGF0Q15Lqk-rraQ1GS8z5Ks/view?usp=sharing",
                },
                {
                  id: "8d74eeaa-f93d-4b88-91c6-6b6832ef3cd0",
                  name: "GPO - Overall Structure",
                  types: [IMAGE_FILE, SVG_FILE],
                  link: "https://drive.google.com/file/d/1R2l-BhVdbWh3-tproorIb-tvX0OcvYfg/view?usp=sharing",
                },
              ]
            }
          ]
        }
      ]
    },
  ]
}