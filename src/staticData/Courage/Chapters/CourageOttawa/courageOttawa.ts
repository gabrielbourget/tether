import {
  TWITTER, SLACK, WEBSITE, ONTARIO, OTTAWA, DOCUMENT_FILE,
  SLIDES_FILE, SPREADSHEET_FILE, DIRECTORY, EMAIL, COMMS_CHANNEL,
  COURAGE_OTTAWA, IMAGE_FILE, GOOGLE_DRIVE, PDF_FILE, PNG_FILE,
  ILLUSTRATOR_FILE, POWERPOINT_FILE, WORD_DOCUMENT_FILE, FORM, TOOL,
} from '../../../../constants';

import { CourageChapter } from "../../../../Types/courageTypes";

import {
  chapterAndBylawsPlanningData, defundThePoliceData, climateJusticeAndGNDData,
  electingSocialistsData, housingData, internationalSolidarityData,
  mobilityJusticeData, readingGroupData, memberEngagementData,
} from "./WorkingGroups";
import { altBudget2022 } from "./AltBudget2022";

export const courageOttawaData: CourageChapter = {
  id: '39f49ec3-0bc7-499d-841f-905975813568',
  name: COURAGE_OTTAWA,
  location: OTTAWA,
  province: ONTARIO,
  // - TODO: -> Dynamically generate this value from the member list once it's filled in.
  memberCount: 30,
  members: [
    // { id: '', name: '' }
  ],
  avatarURL: 'https://pbs.twimg.com/profile_images/862002736325316608/Xq1jTN6D_400x400.jpg',
  socials: {
    id: '3e202336-ab2f-42ac-ad87-28da1b93ac2b',
    name: 'Socials',
    types: [DIRECTORY],
    children: [
      {
        id: '8fa51a7e-20f7-4ab1-90ec-41486040e70b',
        name: TWITTER,
        link: 'https://twitter.com/CourageOttawa',
      }
    ]
  },
  communications: {
    id: '010711cf-c6c9-4a2c-a7a4-21be312b19fe',
    name: 'Communications',
    types: [DIRECTORY],
    children: [
      {
        id: 'da30f52d-b28b-4190-81d3-4d480f8283a3',
        name: SLACK,
        link: 'https://www.independentleft.slack.com',
        children: [
          {
            id: 'b6210ccb-5793-4dbf-818e-7a7913c40c53',
            name: 'Slack Channels',
            types: [DIRECTORY],
            children: [
              {
                id: 'd7068aeb-078e-47aa-84eb-a3b6a22fdc37',
                name : '#ott_crisis_response',
                types: [COMMS_CHANNEL]
              },
              {
                id: '21e6708a-c55c-470c-84b5-a7efdccb4ee1',
                name : '#ott_elect_socialists',
                types: [COMMS_CHANNEL]
              },
              {
                id: '1f1c8a07-b82b-4c26-9408-743456002942',
                name : '#ott_environment_organizing',
                types: [COMMS_CHANNEL]
              },
              {
                id: '69faaaf5-6142-4fde-8d4a-870fcb0034d7',
                name : '#ott_housing',
                types: [COMMS_CHANNEL]
              },
              {
                id: '7ac49cfd-39f4-4d40-9547-af75f6177930',
                name : '#ott_intsolidarity',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'b2d96442-2c52-42c4-a6fc-20f22b0fa4a7',
                name : '#ott_member_engagement',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'd81e8b02-1cab-48fd-9eb9-33a0e01d1486',
                name : '#ott_outreach',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'fdcb7867-3c7b-4695-ba18-187953bce0df',
                name : '#ott_reading_group',
                types: [COMMS_CHANNEL]
              },
              {
                id: '3f84e4ae-bc4a-4a55-8a96-69cd7b844283',
                name : '#ott_transit',
                types: [COMMS_CHANNEL]
              },
              {
                id: 'ae0f2b32-adef-4420-ac79-84bc177b278e',
                name : '#ottawa',
                types: [COMMS_CHANNEL]
              },
            ]
          }
        ]
      },
      {
        id: 'dfce6b40-e041-45d8-a842-84f7387f5b9f',
        name: EMAIL,
        value: 'ottawa@couragecoalition.ca',
        link: 'mailto:ottawa@couragecoalition.ca',
      },        
    ],
  },
  resources: {
    id: '6ddaec4d-cdaf-48ae-90a5-a9ee0b8146aa',
    name: 'Resources',
    types: [DIRECTORY],
    children: [
      {
        id: 'a2103f63-2086-4903-bf3c-8ca7e73a543c',
        name: 'Working Groups Overview',
        types: [DOCUMENT_FILE],
        link: 'https://docs.google.com/document/d/1NX_fZu2SFy1gQF5eFMFHgebB4wthRNB6tA1vWTPKnU8/edit',
      },
      {
        id: '20deb67e-0ac6-40a1-9e5b-37b3dd38e022',
        name: 'Meeting Minutes',
        types: [DIRECTORY],
        children: [
          {
            id: 'd8521e93-764e-4596-9477-dd8ce8ed690a',
            name: '2020',
            types: [DIRECTORY],
            children: [
              {
                id: '4ef2c5fb-5303-453d-904a-00e428477d98',
                name: '2020 - Directory',
                types: [GOOGLE_DRIVE],
                link: 'https://drive.google.com/drive/folders/1eyMXWmI4oWsptWGVDsQGr1SY0hK0v33x?usp=sharing'
              },
              {
                id: 'f02a6b0a-d81e-4e37-b1f7-5dedd0184ec8',
                name: 'April',
                types: [DIRECTORY],
                children: [
                  {
                    id: '10eba56b-d42f-49b9-a24a-5abd0abacd24',
                    name: 'April - Directory',
                    types: [GOOGLE_DRIVE],
                    link: 'https://drive.google.com/drive/folders/1dVbkxdNvq4npYkTjRr5OjQ2mhUe78C0C?usp=sharing',
                  },
                  {
                    id: 'aea060c0-28e5-4974-a346-b4a9afd43689',
                    types: [DIRECTORY],
                    name: 'April 15',
                    children: [
                      {
                        id: '1682a687-49db-4588-9391-4b167538334c',
                        types: [GOOGLE_DRIVE],
                        name: 'April 15 - Directory',
                        link: 'https://drive.google.com/drive/folders/100IM9AhhSKT6xnqsAdONf3EWMKTF66_L?usp=sharing'
                      },
                      {
                        id: '05ec2edb-656e-4c8d-8929-1eb26a099ea3',
                        name: 'Courage Ottawa - Meeting Minutes - April 15, 2020',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1tDR3LGwNHdIbFNl2mKVpR6KnRBoIp_bcfPnoFdMDo10/edit?usp=sharing',
                      }
                    ]
                  }
                ]
              },
              {
                id: '53e362b8-b108-452d-bcfd-6bdddba3a2bc',
                name: 'May',
                types: [DIRECTORY],
                children: [
                  {
                    id: '244508c7-22fe-4bb5-8682-1d0d2cbe99d6',
                    name: 'May - Directory',
                    types: [GOOGLE_DRIVE],
                    link: 'https://drive.google.com/drive/folders/1PGZKLg2lT-cdq7c2yP_kyGTy86wcGHDf?usp=sharing',
                  },
                  {
                    id: '209f1bf8-2c1e-45b2-9a0e-d7ac7400661a',
                    name: 'May 13',
                    types: [DIRECTORY],
                    children: [
                      {
                        id: '04f7cc64-189b-444f-827c-4ecf5db47d7f',
                        name: 'May 13 - Directory',
                        types: [GOOGLE_DRIVE],
                        link: 'https://drive.google.com/drive/folders/1A_bhdnBnYNvaMghuRuX6eFZ9d78y3BqC?usp=sharing',
                      },
                      {
                        id: '2723b2c9-44af-4634-a323-928215343412',
                        name: 'Courage Ottawa - Meeting Minutes - May 13, 2020',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1AkaryeSumY5NS5-_KUHcS_skSmSCgVQkvckuYHHtIx0/edit?usp=sharing'
                      }
                    ]
                  }
                ]
              },
              {
                id: 'cbbdc47d-bd2b-4dac-9ccf-b16f8d4d94b8',
                name: 'June',
                types: [DIRECTORY],
                children: [
                  {
                    id: 'cfda9cf1-bccd-4716-9f41-90d48867856b',
                    name: 'June - Directory',
                    types: [GOOGLE_DRIVE],
                    link: 'https://drive.google.com/drive/folders/13A3pfQnLFsrove8ElpHqKwvtCdugM0kl?usp=sharing',
                  },
                  {
                    id: 'd91e6bb2-3d01-4bc9-a03c-34d21d2d9612',
                    name: 'June 30',
                    types: [DIRECTORY],
                    children: [
                      {
                        id: '37629c3a-97be-49ae-ab6e-4d0e18105f0e',
                        name: 'June 30 - Directory',
                        types: [GOOGLE_DRIVE],
                        link: 'https://drive.google.com/drive/folders/1LSB88A_Y6Qjl26bHRixIAkmUTHj7Fwl0?usp=sharing'
                      },
                      {
                        id: 'f7deb082-76c6-4427-8bb8-43b794df630c',
                        name: 'Courage Ottawa - Meeting Minutes - June 30, 2020',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1Tc_KcufSVzMFFr4uA1oL_MdnpIqdnZLOlez0sGsVUHM/edit?usp=sharing',
                      }
                    ]
                  }
                ]
              },
              {
                id: '367cb3c8-27f6-4627-9bb1-6b82d7ee6261',
                name: 'July',
                types: [DIRECTORY],
                children: [
                  {
                    id: 'f72c674c-c7ae-4f52-9fab-df37848ef091',
                    name: 'July - Directory',
                    types: [GOOGLE_DRIVE],
                    link: 'https://drive.google.com/drive/folders/1RwK0NGIjWkS0OFPC-p4eLwvseQLWnx4i?usp=sharing',
                  },
                  {
                    id: 'c39cb0c0-d115-4bc2-82a8-62089717b948',
                    name: 'July 29',
                    types: [DIRECTORY],
                    children: [
                      {
                        id: '8099c0f1-5882-435f-a34e-bbbdb7fb7cde',
                        name: 'July 29 - Directory',
                        types: [GOOGLE_DRIVE],
                        link: 'https://drive.google.com/drive/folders/1FQEv6UM1ObnavOTocAAJMaGLTprT90Al?usp=sharing'
                      },
                      {
                        id: '1981f451-b3bc-42bc-831b-515bc3bc8f71',
                        name: 'Courage Ottawa - Meeting Minutes - July 29, 2020',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1oYgkOL-H1ZyIof7Hb8CKQq_aMzfHau-H11bIaf-Ep0A/edit?usp=sharing',
                      },
                      {
                        id: 'db9a342c-338f-4090-a309-862e82ac95d3',
                        name: 'Courage Ottawa - Long Term Planning Brainstorming',
                        types: [SPREADSHEET_FILE],
                        link: 'https://docs.google.com/spreadsheets/d/1S0PFigasJwVT7shsKi7drtT51onU12-j5K6nSG5rd1E/edit?usp=sharing',
                      },
                      {
                        id: 'b9eb5f2a-37ee-4d76-ab07-983fbbca7531',
                        name: 'Courage Ottawa - Long Term Planning Priorities Survey',
                        types: [FORM],
                        link: 'https://docs.google.com/forms/d/e/1FAIpQLSdsock5bZi4G4GzOThs65FL6D893IAum2_pfjDmoWMOovZ_zA/viewform',
                      }
                    ]
                  }
                ]
              },
              {
                id: 'f60daf23-3587-4d5c-a4f4-0aac351c441b',
                name: 'August',
                types: [DIRECTORY],
                children: [
                  {
                    id: '5db39e49-4be5-403d-95d5-621ccd971f4a',
                    name: 'August - Directory',
                    types: [GOOGLE_DRIVE],
                    link: 'https://drive.google.com/drive/folders/1tkWX9lNF-X_krG_x2iYhPMMAhEW3pr5q?usp=sharing'
                  },
                  {
                    id: '01fe85d4-ce13-428c-a5ff-817d28513e1f',
                    name: 'August 14',
                    types: [DIRECTORY],
                    children: [
                      {
                        id: 'db061fd2-030e-4242-9521-2a2bfe9426b1',
                        name: 'August 14 - Directory',
                        types: [GOOGLE_DRIVE],
                        link: 'https://drive.google.com/drive/folders/1riuByDukGslHOdZGOWpjfM2TFuuUw7l4?usp=sharing'
                      },
                      {
                        id: '5cdba0c9-8267-407f-88aa-50cb16c0a9a3',
                        name: 'Courage Ottawa Bylaws - Draft',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1HEHwZatXFOJVRMbsXMXiZzoDt2ZNSEpbnmx1hTGE7gE/edit?usp=sharing',
                      },
                      {
                        id: '09e71b04-602e-48e5-a683-290cc55dc2f5',
                        name: 'Courage Ottawa Bylaws Brainstorming Meeting - Minutes',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1W7t1Lj8AWXIASa60usIgGO462jHxshWPJH_jD5gUFCc/edit?usp=sharing',
                      },
                      {
                        id: '88988874-77dd-41dc-b17f-1517ed7e6de8',
                        name: 'Courage Ottawa - August 19th Meeting - Meeting Planning',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1vVIxuCxiJdXHj-i4wfXdmmiXxE745RdW7hdDBBESd98/edit'
                      },
                      {
                        id: 'db9a342c-338f-4090-a309-862e82ac95d3',
                        name: 'Courage Ottawa - Long Term Planning Brainstorming - Tabulated Results',
                        types: [SPREADSHEET_FILE],
                        link: 'https://docs.google.com/spreadsheets/d/1S0PFigasJwVT7shsKi7drtT51onU12-j5K6nSG5rd1E/edit?usp=sharing',
                      },
                      {
                        id: '22f461c6-557d-48b8-ac35-fae03226e5e2',
                        name: 'Boston DSA - Bylaws',
                        types: [WEBSITE],
                        link: 'https://www.bostondsa.org/bylaws/',
                      },
                      {
                        id: '19f1dc0a-1335-4584-8181-a58f2940b387',
                        name: 'Courage Montreal - Bylaws Draft',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1hrzU4hBeYiYnzF2Zg7UUXg6GNZCqGMKTMHb1kxpAR6w/edit?usp=sharing',
                      },
                      {
                        id: '095aa469-9185-4697-934b-8dbb0573af10',
                        name: 'Courage Vancouver - Bylaws',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1_a7KAl1xjYguKpwxgnwOnayP43aIzltu746FgoWEQYY/edit?usp=sharing',
                      },
                      {
                        id: 'bb07d037-f573-4d53-a3ce-ee3fb0f29865',
                        name: 'Courage Ottawa - Results from Initial Member Survey',
                        types: [SLIDES_FILE],
                        link: 'https://docs.google.com/presentation/d/13j7ETyxVMrB29Oy_hUY6MMCWGK9liuxK-BxJNuPjAOw/edit?usp=sharing',
                      },
                      {
                        id: 'b9eb5f2a-37ee-4d76-ab07-983fbbca7531',
                        name: 'Courage Ottawa - Long Term Planning Priorities Survey - Outward Facing Link',
                        types: [FORM],
                        link: 'https://docs.google.com/forms/d/e/1FAIpQLSdsock5bZi4G4GzOThs65FL6D893IAum2_pfjDmoWMOovZ_zA/viewform',
                      },
                      {
                        id: '8236cb2c-2d7e-4383-aaa3-6b5d5a0935ff',
                        name: 'Courage Ottawa - Long Term Planning Priorities Survey - Administrative Editable Link',
                        types: [FORM],
                        link: 'https://docs.google.com/forms/d/17FTbDLppp4qO7PO-Gmk8Y3jN0YEKc7RVYk4TLeYcSEg/edit'
                      },
                      {
                        id: 'e19de06f-f973-439d-b398-c97a862b2e45',
                        name: 'Courage Ottawa Values Statement/Manifesto Draft',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/19vIK0AjzN2ccWMojAtkTO7YokQ0hoYU-GapY5l9t6Wg/edit'
                      }
                    ]
                  },
                  {
                    id: '3453e323-f98e-44a6-ab48-f81f3e268d06',
                    name: 'August 19',
                    types: [DIRECTORY],
                    children: [
                      {
                        id: '0fa1e44e-b322-4468-8f32-d8ef1bbfcf51',
                        name: 'Slides',
                        types: [SLIDES_FILE],
                        link: 'https://docs.google.com/presentation/d/1Yn2twuCrvZjPixjBAHaPiK5AL3dy1hnoFGerKf6mWiY/edit?usp=sharing',
                      },
                      {
                        id: '90580c63-f5b8-4e37-bac0-8ff4b59d17fc',
                        name: 'Agenda/Meeting Minutes',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1vVIxuCxiJdXHj-i4wfXdmmiXxE745RdW7hdDBBESd98/edit?usp=sharing'
                      }
                    ]
                  }
                ]
              },
              {
                id: '71075547-aaaf-4439-b4da-49d5eb1c6ff9',
                name: 'September',
                types: [DIRECTORY],
                children: [
                  {
                    id: 'd42b1911-bbbc-4b36-b645-4577a3b87b5a',
                    name: 'September - Directory',
                    types: [GOOGLE_DRIVE],
                    link: 'https://drive.google.com/drive/folders/1rZ0sMwWxSJqYmCGckzB8wATDzzVgPl5f?usp=sharing',
                  },
                  {
                    id: '17d816bf-31e5-4aa1-a9e5-43fe6892b48e',
                    name: 'September 02',
                    types: [DIRECTORY],
                    children: [
                      {
                        id: 'cb789811-cad8-4709-bcdd-0fb319a8e270',
                        name: 'September 02 - Directory',
                        types: [GOOGLE_DRIVE],
                        link: 'https://drive.google.com/drive/folders/146F_O0tZwXTpCNze_BJRkjE4EHw5Qu5i?usp=sharing'
                      },
                      {
                        id: '2c86e915-4495-40bd-bb69-8dfbbcc25d1c',
                        name: 'Courage Ottawa - Meeting Minutes - September 02, 2020',
                        types: [DOCUMENT_FILE],
                        link: 'https://docs.google.com/document/d/1sT03p1cyHKT5oisQ9jO-o8TXxrvKWu2lOJwSH95hVmA/edit',
                      },
                      {
                        id: '4bd8085b-2804-4c99-8d30-c890f4929952',
                        name: 'Courage Ottawa - Potential Chapter Structure/Roles',
                        types: [SLIDES_FILE],
                        link: 'https://docs.google.com/presentation/d/11RxZWBNS5Wyof5SR-nJWA6ywCr_iCyxlOy2OW2wgHv8/edit?usp=sharing'
                      }
                    ]
                  }
                ]
              },
              {
                id: "bee64fb5-8b19-4b0f-acb4-dc357bee2427",
                name: "October",
                types: [DIRECTORY],
                children: [
                  {
                    id: "989e0ccf-f4f4-4ed3-a0c2-9833b080f754",
                    name: "October - Directory",
                    types: [GOOGLE_DRIVE],
                    link: "https://drive.google.com/drive/folders/1u6nfrL0tBWCs-fornaLIRRI71FUDQwI2?usp=sharing"
                  },
                  {
                    id: "b2e3e762-814b-4a5b-9ce2-c28d458c3a41",
                    name: "October 14",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "49878634-e5a4-4a81-9ce4-8eb2d7faade0",
                        name: "Courage Ottawa - Meeting Minutes - October 14, 2020",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1jJkHFPI1GalpHQ9_jQdoPwG8BSn3aHu6cJk4SEMWVS4/edit?usp=sharing"
                      }
                    ]
                  },
                  {
                    id: "1fdb3f34-3fbc-4fb4-83c8-062f8b0657b3",
                    name: "October 28",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "4b4f9f09-9e5a-4ed1-9099-1afbdd4a5c72",
                        name: "Courage Ottawa - Meeting Minutes - October 28, 2020",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/18KtpvUJJd7oRHTY7XJ-X0M0UkCit3PZd0kZBpWzhaXY/edit?usp=sharing"
                      },
                      {
                        id: "3079cd4d-c71f-40e7-a307-67ddd225be6f",
                        name: "Courage Ottawa - Meeting Agenda - October 28, 2020",
                        types: [PDF_FILE, DOCUMENT_FILE],
                        link: "https://drive.google.com/file/d/1W-twbi9-bRqMg4SQSPyLIHCopd-ntH6y/view?ts=5f922580"
                      }
                    ]
                  }
                ]
              },
              {
                id: "dbbf6024-54a4-4560-bde3-849b0329aac8",
                name: "November",
                types: [DIRECTORY],
                children: [
                  {
                    id: "30fd1acd-9ce7-4e6f-816b-29e9a523c2cb",
                    name: "November 23",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "a0dae819-e612-42d8-b281-fc25c4752b4a",
                        name: "Courage Ottawa - Meeting Minutes - November 23, 2020",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1VbryuvgBILp2yTZvSsu3V_WPBngwcl5-Cn1Y_j4NieY/edit?usp=sharing"
                      }
                    ]
                  }
                ]
              },
              {
                id: "d32d40cb-d8e6-4b46-946d-9d0fdef05c7a",
                name: "December",
                types: [DIRECTORY],
                children: [
                  {
                    id: "e4c1e828-a27e-432f-9ac2-67e643af14ef",
                    name: "December 09",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "20ca5b10-1048-498e-84b0-dec482f8532c",
                        name: "Courage Ottawa - Meeting Minutes - December 09, 2020",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1l8hTmtS4CLT_BPV2kiuWG175VzaiLePt3y-xU3IZaww/edit?usp=sharing"
                      }
                    ]
                  },
                  {
                    id: "0ad0cc14-2d08-47fa-b09b-ba68a01c5dfd",
                    name: "December 16",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "3dade90d-1284-4a0b-a60e-ba530c2ffe76",
                        name: "Courage Ottawa - Meeting Minutes - December 16, 2020",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1zgAdTYYTH6e-SSTr9vlR65EEcOLvjnX-Gznppg1_po0/edit?usp=sharing"
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            id: "9d755f99-adf1-41f0-bb40-7e3e0daf7e87",
            name: "2021",
            types: [DIRECTORY],
            children: [
              {
                id: "b553419f-10ee-48e6-85ee-f96d3b4ffa2e",
                name: "January",
                types: [DIRECTORY],
                children: [
                  {
                    id: "16f7dab3-ce72-4610-9f2f-030dc0d134fa",
                    name: "January 13",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "79352142-2d23-4a7b-b7bb-cdfa1d32ef59",
                        name: "Courage Ottawa - Meeting Minutes - January 13, 2021",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1MQGl-TeS4aE6pRixwen5CO9EuepI_IpFG_muLMvfC4k/edit?usp=sharing",
                      }
                    ]
                  }
                ]
              },
              {
                id: "ae7f6ab3-8dba-4fde-84b6-edbae4a70b69",
                name: "February",
                types: [DIRECTORY],
                children: [
                  {
                    id: "320b42a6-032a-43f0-b89e-10ed7dcec001",
                    name: "February 10",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "89bc9d17-c800-4534-b10f-efb2c86bc630",
                        name: "Courage Ottawa - Meeting Minutes - February 10, 2021",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1sSB6EpHYrD-goxYFHNXJSWwyaQNgOoNFj---BJLaeSs/edit?usp=sharing",
                      }
                    ]
                  }
                ]
              },
              {
                id: "d1ca4732-cf2a-444a-b365-7ab2d7e6982c",
                name: "March",
                types: [DIRECTORY],
                children: [
                  {
                    id: "3df695a8-0155-4a42-a5f1-c7c8e62aaf5a",
                    name: "March 10",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "d99d57e1-fbfb-4dbc-907c-55d345c49af4",
                        name: "Courage Ottawa - Meeting Minutes - March 10, 2021",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1Tq4JTFNbVX0mxuMq7jPIe-5ceIuWVIK1oWvAG3D-7T4/edit?usp=sharing",
                      }
                    ]
                  }
                ]
              },
              {
                id: "eea64bc3-eda5-40ca-96ba-3be80143670d",
                name: "April",
                types: [DIRECTORY],
                children: [
                  {
                    id: "87c5e78d-ba25-4f74-9b4b-819d58f8f663",
                    name: "April 14",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "926d1046-963b-47ed-bece-18126289e987",
                        name: "Courage Ottawa - Meeting Minutes - April 14, 2021",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1nolrOcPhov5JbeKPMm1meRIam8ROTik-4DVbCH8C8nY/edit?usp=sharing",
                      }
                    ]
                  },
                  {
                    id: "77fd5384-aba8-420f-829d-aad9fa4f5cd5",
                    name: "April 28",
                    types: [DIRECTORY],
                    children: [
                      {
                        id: "9883d01c-1ebd-47bf-adf0-6fd5e1594af2",
                        name: "Courate Ottawa - Meeting Minutes - April 28, 2021",
                        types: [DOCUMENT_FILE],
                        link: "https://docs.google.com/document/d/1AaMi20DaYXgg5uQBkZxc2hG4GW6OcDFuMJrV3xC8x38/edit?usp=sharing",
                      }
                    ]
                  }
                ]
              },
            ]
          },
          {
            id: "e1a72ee6-1abd-4663-bfb4-7f659f36e9c7",
            name: "May",
            types: [DIRECTORY],
            children: [
              {
                id: "005065fd-e6c7-4344-be78-80852b21d99a",
                name: "May 12",
                types: [DIRECTORY],
                children: [
                  {
                    id: "752ba56b-bd93-41ee-b3be-949a0ca77529",
                    name: "Courage Ottawa - Meeting Minutes - May 12, 2021",
                    types: [DOCUMENT_FILE],
                    link: "https://docs.google.com/document/d/15fwOSxozw0x-bPR8XB3xdQNmAR2QOEY9UYC9CtN2Qjg/edit?usp=sharing",
                  }
                ]
              }
            ]
          },
          {
            id: "4dc398bb-718b-4d44-b4c4-5c1fbd4ada9b",
            name: "June",
            types: [DIRECTORY],
            children: [
              {
                id: "837ff0fa-b296-4c7a-9bff-e01704a969df",
                name: "June 16",
                types: [DIRECTORY],
                children: [
                  {
                    id: "8b5a9f7b-00ed-4e81-8538-c837b868a1bf",
                    name: "Courage Ottawa - Meeting Minutes - June 16, 2021",
                    types: [DOCUMENT_FILE],
                    link: "https://docs.google.com/document/d/1kSVUpShEeu793Y444XUp8a-IBhc4yEQS7BPf9y6G0SU/edit?usp=sharing",
                  }
                ]
              }
            ]
          },
          {
            id: "daf051b6-04ca-472c-9be8-6703283c3dd8",
            name: "July",
            types: [DIRECTORY],
            children: [
              {
                id: "4a89d295-352f-481a-9437-51c1039c42a1",
                name: "July 14",
                types: [DIRECTORY],
                children: [
                  {
                    id: "41f69db8-a5e7-4605-b13d-fbce3bad492b",
                    name: "Courage Ottawa - Meeting Minutes - July 14, 2021",
                    types: [DOCUMENT_FILE],
                    link: "https://docs.google.com/document/d/1jLAKrbuiLGFvicXlosdP-7Qh9w8YtgpBJSnLEUgwrWA/edit?usp=sharing",
                  }
                ]
              }
            ]
          },
          {
            id: 'd0426ffd-795e-495d-8396-e25e471753e2',
            name: 'Courage Ottawa Chapter Meeting - April 15, 2020 - Minutes',
            types: [DOCUMENT_FILE],
            link: 'https://drive.google.com/open?id=1tDR3LGwNHdIbFNl2mKVpR6KnRBoIp_bcfPnoFdMDo10'
          },
          {
            id: 'e32f26cd-65ed-40a3-9f72-2e8f2d9f53be ',
            name: 'Courage Ottawa Chapter Meeting - May 13, 2020 - Minutes',
            types: [DOCUMENT_FILE],
            link: 'https://drive.google.com/open?id=1AkaryeSumY5NS5-_KUHcS_skSmSCgVQkvckuYHHtIx0'
          },
        ]
      },
      {
        id: '957223a2-76c6-4f78-90cb-d81c36235539',
        name: 'Network Economics Resources',
        types: [DIRECTORY],
        children: [
          {
            id: '84cd4ea7-0a8d-4353-9a0a-9693da883232',
            name: 'Network Economics Resources Directory',
            types: [GOOGLE_DRIVE],
            link: 'https://drive.google.com/drive/folders/1JRTOQEA_kY8cM4S3CrjNZ_ZFX6SP1B26?usp=sharing',
          },
          {
            id: '241ce924-0504-4ef1-a58a-de2f031079d5',
            name: 'Figures Directory',
            types: [GOOGLE_DRIVE],
            link: 'https://drive.google.com/drive/folders/1tJWcR9Sd7fCXf1pa0joZo4M80z5UV5fm?usp=sharing',
          },
          {
            id: '50adfedf-6d4e-44c3-b814-916269139c42',
            name: 'Amsterdam Doughnut Report',
            types: [PDF_FILE, DOCUMENT_FILE],
            link: 'https://drive.google.com/file/d/1QiKMZWM9vo3WuyXuVFPHfa7zF4wU5ouK/view?usp=sharing'
          },
          {
            id: 'fa2dbf81-b4fb-4b27-b477-3e7c6fdc6911',
            name: 'Welsh Doughnut Report',
            types: [PDF_FILE, DOCUMENT_FILE],
            link: 'https://drive.google.com/file/d/1U113c7Fx6yQIfsaZ_5BMJo6rzD_v4KTG/view?usp=sharing'
          },
          {
            id: '1ac62304-597d-4fda-a339-5dea2d661535',
            name: 'Ottawa Doughnut Proposal',
            types: [POWERPOINT_FILE, SLIDES_FILE],
            link: 'https://drive.google.com/file/d/1-TkpSBEQyfrMbNqjgBOYFYg_7Gwz2SZO/view?usp=sharing',
          },
          {
            id: 'ce7e3c09-27f4-4a5d-acfd-4731f082bfbb',
            name: 'Design Resources',
            types: [ILLUSTRATOR_FILE],
            link: 'https://drive.google.com/file/d/1tsf2cUSC57glKTQjy1eraYczkgbZ_btU/view?usp=sharing'
          }
        ]
      },
      {
        id: "e13b936e-9dd4-4ca1-be28-82a383532e37",
        name: "Tools",
        types: [DIRECTORY],
        children: [
          {
            id: "1dc8ca91-cace-4f7f-9512-1446dc4063cc",
            name: "Menti",
            types: [TOOL],
            link: "https://menti.com",
          }
        ]
      },
      { ...altBudget2022 },
    ],
  },   
  workingGroups: [
    { ...chapterAndBylawsPlanningData },
    { ...defundThePoliceData }, 
    { ...climateJusticeAndGNDData },
    { ...electingSocialistsData },
    { ...housingData },
    { ...internationalSolidarityData },
    { ...mobilityJusticeData },
    { ...readingGroupData },
    { ...memberEngagementData },
  ],
}

