import {
  TWITTER, SASKATCHEWAN, REGINA, DIRECTORY, COURAGE_REGINA,
} from '../../../constants';

import { CourageChapter } from "../../../Types/courageTypes";

export const courageReginaData: CourageChapter = {
  id: '981d5f06-39ba-4aa6-805a-434a24ee380c',
  name: COURAGE_REGINA,
  location: REGINA,
  province: SASKATCHEWAN,
  avatarURL: 'https://pbs.twimg.com/profile_images/861664087389818882/uwHLqC54_400x400.jpg',
  socials: {
    id: 'cb616b08-91b5-4cbf-895f-a6816bf79bda',
    name: 'Socials',
    types: [DIRECTORY],
    children: [
      {
        id: 'da59618f-1e36-43b4-84e2-cb848d902774',
        name: TWITTER,
        link: 'https://twitter.com/Courage_REG',
      }
    ]
  },
  communications: {
    id: 'd064811f-dead-4881-b3ee-aa38e601b499',
    name: 'Communications',
    types: [DIRECTORY],
    children: [

    ]
  },
  resources: {
    id: 'd685228a-91c9-40de-9251-59cf36781b2b',
    name: 'Resources',
    types: [DIRECTORY],
    children: [

    ]
  },
  workingGroups: [
    // {
    //   id: '',
    //   name: '',
    //   coChairs: [
    //     { id: '', name: '' }
    //   ],
    //   members: [
    //     { id: '', name: '' },
    //   ],
    //   resources: [
    //     {
    //       id: '',
    //       name: '',
    //       types: [],
    //       link: '',
    //     }
    //   ],
    // }
  ],
};
