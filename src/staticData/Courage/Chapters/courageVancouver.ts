import {
  TWITTER, BRITISH_COLUMBIA, VANCOUVER, DIRECTORY, COURAGE_VANCOUVER,
} from '../../../constants';

import { CourageChapter } from "../../../Types/courageTypes";

export const courageVancouverData: CourageChapter = {
  id: '4b8c85db-3baa-447e-b60e-ee480310626f',
  name: COURAGE_VANCOUVER,
  location: VANCOUVER,
  province: BRITISH_COLUMBIA,
  avatarURL: 'https://pbs.twimg.com/profile_images/861657807333937152/Ui5advoo_400x400.jpg',
  socials: {
    id: 'c87f2c86-c6d1-4760-8678-7fb143117605',
    name: 'Socials',
    types: [DIRECTORY],
    children: [
      {
        id: 'a7b4acc3-f214-47b1-95f4-5eb239e08244',
        name: TWITTER,
        link: 'https://twitter.com/Courage_VAN',
      }
    ]
  },
  communications: {
    id: '1871665a-d9a8-4571-bd1e-a037326210a3',
    name: 'Communications',
    types: [DIRECTORY],
    children: [

    ]
  },
  resources: {
    id: '9af93593-253e-4317-b7fc-67f9ca5d2090',
    name: 'Resources',
    types: [DIRECTORY],
    children: [

    ]
  },
  workingGroups: [
    // {
    //   id: '',
    //   name: '',
    //   coChairs: [
    //     { id: '', name: '' }
    //   ],
    //   members: [
    //     { id: '', name: '' },
    //   ],
    //   resources: [
    //     {
    //       id: '',
    //       name: '',
    //       types: [],
    //       link: '',
    //     }
    //   ],
    // }
  ],
}