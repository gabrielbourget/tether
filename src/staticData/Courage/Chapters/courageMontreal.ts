import {
  TWITTER, QUEBEC, MONTREAL, DIRECTORY, COURAGE_MONTREAL,
} from '../../../constants';

import { CourageChapter } from "../../../Types/courageTypes";

export const courageMontrealData: CourageChapter = {
  id: 'e7129e80-3e71-4a25-b900-5a2997acd1d5',
  name: COURAGE_MONTREAL,
  location: MONTREAL,
  province: QUEBEC,
  avatarURL: 'https://pbs.twimg.com/profile_images/862010072674598912/wMJnToWE_400x400.jpg',
  socials: {
    id: '3270e7e7-117b-4ca7-9b9b-4574236dc0a9',
    name: 'Socials',
    types: [DIRECTORY],
    children: [
      {
        id: '1c23c5aa-cdaf-4d16-8ee6-185dfbbb5f3b',
        name: TWITTER,
        link: 'https://twitter.com/Courage_MTL',
      }
    ],
  },
  communications: {
    id: 'ad3b0f80-3f3c-4116-a269-13cdd3418625',
    name: 'Communications',
    types: [DIRECTORY],
    children: [

    ]
  },
  resources: {
    id: '16dce0ea-8c31-4831-a77a-56e35c4fcd06',
    name: 'Resources',
    types: [DIRECTORY],
    children: [

    ]
  },
  workingGroups: [
    // {
    //   id: '',
    //   name: '',
    //   coChairs: [
    //     { id: '', name: '' }
    //   ],
    //   members: [
    //     { id: '', name: '' },
    //   ],
    //   resources: [
    //     {
    //       id: '',
    //       name: '',
    //       types: [],
    //       link: '',
    //     }
    //   ],
    // }
  ],
};
