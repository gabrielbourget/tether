import {
  TWITTER, ONTARIO, TORONTO, DIRECTORY, COURAGE_TORONTO,
} from '../../../constants';

import { CourageChapter } from "../../../Types/courageTypes";

export const courageTorontoData: CourageChapter = {
  id: '60a86a4f-392b-4efe-b48d-98584e193847',
  name: COURAGE_TORONTO,
  location: TORONTO,
  province: ONTARIO,
  avatarURL: 'https://pbs.twimg.com/profile_images/860580763456028672/wFhYDaqD_400x400.jpg',
  socials: {
    id: '66551760-9cc2-478b-b87d-4d74b446d185',
    name: 'Socials',
    types: [DIRECTORY],
    children: [
      {
        id: '3c0f9991-5d68-4821-a505-fe90cff80fbb',
        name: TWITTER,
        link: 'https://twitter.com/Courage_TOR',
      }
    ]
  },
  communications: {
    id: 'c6c17ea1-0935-4ad3-b2ab-c3c018ff6906',
    name: 'Communications',
    types: [DIRECTORY],
    children: [

    ]
  },
  resources: {
    id: '3623135f-9972-4e19-a76a-933ac0527bf8',
    name: 'Resources',
    types: [DIRECTORY],
    children: [

    ]
  },
  workingGroups: [
    // {
    //   id: '',
    //   name: '',
    //   coChairs: [
    //     { id: '', name: '' }
    //   ],
    //   members: [
    //     { id: '', name: '' },
    //   ],
    //   resources: [
    //     {
    //       id: '',
    //       name: '',
    //       types: [],
    //       link: '',
    //     }
    //   ],
    // }
  ],
};
