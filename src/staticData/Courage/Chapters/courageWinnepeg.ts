import {
  TWITTER, MANITOBA, WINNEPEG, DIRECTORY, COURAGE_WINNEPEG,
} from '../../../constants';

import { CourageChapter } from "../../../Types/courageTypes";

export const courageWinnepegData: CourageChapter = {
  id: 'ea65277e-154d-45d9-9ec6-d3c04f274be2',
  name: COURAGE_WINNEPEG,
  location: WINNEPEG,
  province: MANITOBA,
  avatarURL: 'https://pbs.twimg.com/profile_images/860589695931097088/MA7ZhXzn_400x400.jpg',
  socials: {
    id: '8a59ec4c-f266-4afe-8d4a-fc31c39407a3',
    name: 'Socials',
    types: [DIRECTORY],
    children: [
      {
        id: 'b111e290-1445-4df2-9401-44b67a9e7575',
        name: TWITTER,
        link: 'https://twitter.com/Courage_WPG',
      }
    ]
  },
  communications: {
    id: '5e0a73be-eb94-4868-b15c-3afbd33d9f89',
    name: 'Communications',
    types: [DIRECTORY],
    children: [

    ]
  },
  resources: {
    id: '471cf03e-ceb6-48f0-9051-22011c379bf5',
    name: 'Resources',
    types: [DIRECTORY],
    children: [

    ]
  },
  workingGroups: [
    // {
    //   id: '',
    //   name: '',
    //   coChairs: [
    //     { id: '', name: '' }
    //   ],
    //   members: [
    //     { id: '', name: '' },
    //   ],
    //   resources: [
    //     {
    //       id: '',
    //       name: '',
    //       types: [],
    //       link: '',
    //     }
    //   ],
    // }
  ],
};
