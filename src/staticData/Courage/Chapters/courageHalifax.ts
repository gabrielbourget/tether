import {
  TWITTER, NOVA_SCOTIA, HALIFAX, DIRECTORY, COURAGE_HALIFAX,
} from '../../../constants';

import { CourageChapter } from "../../../Types/courageTypes";

export const courageHalifaxData: CourageChapter = {
  id: '5d6d6ff8-b8ea-4d2a-9bfe-a9e381b99035',
  name: COURAGE_HALIFAX,
  location: HALIFAX,
  province: NOVA_SCOTIA,
  avatarURL: 'https://pbs.twimg.com/profile_images/862015443908804608/6WdKo2of_400x400.jpg',
  socials: {
    id: '244de32c-3e47-4110-8619-09dce15f5d2d',
    name: 'Socials',
    types: [DIRECTORY],
    children: [
      {
        id: '40984bb2-1d3c-4166-b0d2-4f2738d623a0',
        name: TWITTER,
        link: 'https://twitter.com/Courage_MTL',
      }
    ]
  },
  communications: {
    id: '30de5f55-efb0-4349-ad7d-7d5ad8e7e739',
    name: 'Communications',
    types: [DIRECTORY],
    children: [

    ]
  },
  resources: {
    id: 'c7e9a383-6a43-4350-b5aa-77e00e837eaa',
    name: 'Resources',
    types: [DIRECTORY],
    children: [

    ]
  },
  workingGroups: [
    // {
    //   id: '',
    //   name: '',
    //   coChairs: [
    //     { id: '', name: '' }
    //   ],
    //   members: [
    //     { id: '', name: '' },
    //   ],
    //   resources: {
    //     name: 'Resources',
    //     types: [DIRECTORY],
    //     children: [
    //       {
    //         id: '',
    //         name: '',
    //         types: [],
    //         link: '',
    //       }
    //     ],
    //   }
    // }
  ],
}
