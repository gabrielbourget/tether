import {
  TWITTER, ALBERTA, CALGARY, DIRECTORY, COURAGE_CALGARY,
} from '../../../constants';

import { CourageChapter } from "../../../Types/courageTypes";

export const courageCalgaryData: CourageChapter = {
  id: '0c04dbe2-1e89-444f-8cc0-ffde534d6116',
  name: COURAGE_CALGARY,
  location: CALGARY,
  province: ALBERTA,
  avatarURL: 'https://pbs.twimg.com/profile_images/862002736325316608/Xq1jTN6D_400x400.jpg',
  socials: {
    id: 'bb8181e0-d4f1-48b3-8d6d-f308159ba97b',
    name: 'Socials',
    types: [DIRECTORY],
    children: [
      {
        id: '3e7cc784-be09-47b8-be7c-c2007892e35f',
        name: TWITTER,
        link: 'https://twitter.com/Courage_CGY',
      }
    ],
  },
  communications: {
    id: '6974b758-1d20-41e2-bab8-21965447812b',
    name: 'Communications',
    types: [DIRECTORY],
    children: [

    ]
  },
  resources: {
    id: 'c46f9f2a-9ee7-4ae9-b122-48d25bc0a9fe',
    name: 'Resources',
    types: [DIRECTORY],
    children: [

    ]
  },
  workingGroups: [
    // {
    //   id: '',
    //   name: '',
    //   coChairs: [
    //     { id: '', name: '' }
    //   ],
    //   members: [
    //     { id: '', name: '' },
    //   ],
    //   resources: [
    //     {
    //       id: '',
    //       name: '',
    //       types: [],
    //       link: '',
    //     }
    //   ],
    // }
  ],      
}

