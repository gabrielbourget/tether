import {
  TWITTER, SASKATCHEWAN, SASKATOON, DIRECTORY, COURAGE_SASKATOON,
} from '../../../constants';

import { CourageChapter } from "../../../Types/courageTypes";

export const courageSaskatoonData: CourageChapter = {
  id: '7d95ae38-eb97-40d7-bb5a-c2a9e3348fb9',
  name: COURAGE_SASKATOON,
  location: SASKATOON,
  province: SASKATCHEWAN,
  avatarURL: 'https://pbs.twimg.com/profile_images/861666943404724224/wHtPHmRK_400x400.jpg',
  socials: {
    id: '79585e0b-cbe8-4f90-a0f6-9f3add74bd10',
    name: 'Socials',
    types: [DIRECTORY],
    children: [
      {
        id: '7bf9796b-e260-4788-87d3-aece4e5921d4',
        name: TWITTER,
        link: 'https://twitter.com/Courage_SSK',
      }
    ]
  },
  communications: {
    id: 'ec2a16aa-bebd-47d1-9009-2e5712ff2848',
    name: 'Communications',
    types: [DIRECTORY],
    children: [

    ]
  },
  resources: {
    id: 'cc1e1c49-0dfd-45e0-833b-0bbe5bebd326',
    name: 'Resources',
    types: [DIRECTORY],
    children: [

    ]
  },
  workingGroups: [
    // {
    //   id: '',
    //   name: '',
    //   coChairs: [
    //     { id: '', name: '' }
    //   ],
    //   members: [
    //     { id: '', name: '' },
    //   ],
    //   resources: [
    //     {
    //       id: '',
    //       name: '',
    //       types: [],
    //       link: '',
    //     }
    //   ],
    // }
  ],
};
