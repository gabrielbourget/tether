import {
  TWITTER, BRITISH_COLUMBIA, VICTORIA, DIRECTORY, COURAGE_VICTORIA,
} from '../../../constants';

import { CourageChapter } from "../../../Types/courageTypes";

export const courageVictoriaData: CourageChapter = {
  id: 'c9d7ba7a-81c5-4bdf-be31-c1cd39e07e20',
  name: COURAGE_VICTORIA,
  location: VICTORIA,
  province: BRITISH_COLUMBIA,
  avatarURL: 'https://pbs.twimg.com/profile_images/861661665829339136/vcDldF65_400x400.jpg',
  socials: {
    id: '0081ffb2-085a-43d6-b51f-ec0983fb1a4b',
    name: 'Socials',
    types: [DIRECTORY],
    children: [
      {
        id: '61d5a05b-f155-4328-a6c1-9d6f36d608ce',
        name: TWITTER,
        link: 'https://twitter.com/Courage_VIC',
      }
    ]
  },
  communications: {
    id: '116e7bf6-adff-416a-8bf5-665e654e2539',
    name: 'Communications',
    types: [DIRECTORY],
    children: [

    ]
  },
  resources: {
    id: '15b61413-6cf0-4e4f-8b67-100c49faa497',
    name: 'Resources',
    types: [DIRECTORY],
    children: [

    ]
  },
  workingGroups: [
    // {
    //   id: '',
    //   name: '',
    //   coChairs: [
    //     { id: '', name: '' }
    //   ],
    //   members: [
    //     { id: '', name: '' },
    //   ],
    //   resources: [
    //     {
    //       id: '',
    //       name: '',
    //       types: [],
    //       link: ''
    //     }
    //   ],
    // }
  ],
};
