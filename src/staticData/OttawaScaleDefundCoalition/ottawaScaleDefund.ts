import { Resource } from "../../Types";
import { DIRECTORY, DOCUMENT_FILE, PDF_FILE, WEBSITE } from "../../constants";

export const ottawaScaleDefund: Resource = {
  id: "b1848726-86d9-46ef-aaa6-61f7772ca53b",
  name: "Ottawa Scale Defund",
  types: [DIRECTORY],
  children: [
    {
      id: "765ec29e-c315-4f68-a754-bc50e557d7fa",
      name: "Meeting Minutes",
      types: [DIRECTORY],
      children: [
        {
          id: "8bb35234-1733-4c79-9791-6b78b0c8b86a",
          name: "2021",
          types: [DIRECTORY],
          children: [
            {
              id: "efaf0d4f-ef52-416d-b236-bef970cc8185",
              name: "January",
              types: [DIRECTORY],
              children: [
                {
                  id: "1caaaf82-18fe-4f95-bd7a-6820215c23e3",
                  name: "January 22",
                  types: [DIRECTORY],
                  children: [
                    {
                      id: "2ffb379b-2852-43a5-8f0c-77caf2234af5",
                      name: "Defund OPS - Meeting Minutes - January 22, 2021",
                      types: [DOCUMENT_FILE],
                      link: "https://docs.google.com/document/d/1gJGEMBrrD6hvqk1SugIuTn-KtvRFJM7XX4A2fs9v2GM/edit?usp=sharing",
                    },
                  ]
                }
              ]
            }
          ]
        }
      ]
    },
    {
      id: "654affd0-8f70-4053-8b8e-fd4d4e925cf6",
      name: "General Resource Commons",
      types: [DIRECTORY],
      children: [
        {
          id: "dbcb5689-9620-4d7a-b948-677a98be0347",
          name: "Papers/Reports",
          types: [DIRECTORY],
          children: [
            {
              id: "e493ffef-3548-43b4-8997-d23023f2d6bf",
              name: "[Interrupting Criminalization] - The Demand is Still #DefundThePolice: Lessons from 2020",
              types: [PDF_FILE, DOCUMENT_FILE],
              link: "https://drive.google.com/file/d/1DoEt20oeXu-VzhRVF_clsz5YhZu67BUp/view?usp=sharing"
            },
            {
              id: "b6b9fa27-57b7-4970-967b-b1e72fd09ef0",
              name: "[Mariame Kaba and collaborators] - What's Next? Safer and More Just Communities Without Policing",
              types: [PDF_FILE, DOCUMENT_FILE],
              link: "https://drive.google.com/file/d/1X0dE1B3W0N7E7ymw4-79xBRVanh-uRnk/view?usp=sharing"
            }
          ]
        },
        {
          id: "9a7f153c-aede-4c24-b35e-720c004aab38",
          name: "Articles",
          types: [DIRECTORY],
          children: [
            {
              id: "7a7316e7-e819-4f29-95c1-ec85c229457c",
              name: "[CBC] - Toronto Pilot Project Could Remove Police from Mental Health Calls - But Not in Emergencies",
              types: [WEBSITE],
              link: "https://www.cbc.ca/news/canada/toronto/police-mental-health-crisis-toronto-pilot-1.5882296#:~:text=After%20months%20of%20mounting%20calls,calls%20for%20people%20in%20crisis",
            }
          ]
        },
      ]
    },
  ],
}